package SVGIO;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JPanel;

import SVGIO.intransix.osm.termite.svg.SvgDocument;
import SVGIO.intransix.osm.termite.svg.SvgGeometry;
import SVGIO.intransix.osm.termite.svg.TransformManager;


@SuppressWarnings("serial")
public class Canvas extends JPanel {

	ArrayList<SvgGeometry> objects = new ArrayList<SvgGeometry>();
	private Vector<Vector<SvgGeometry>> layers = new Vector<Vector<SvgGeometry>>();
	Vector<AffineTransform> transforms = new Vector<AffineTransform>();
	TransformManager manager = new TransformManager();
	Rectangle2D docSize;
	
	public Canvas(SvgDocument svgDoc) {
		docSize = svgDoc.getDocSize();
		objects = svgDoc.getObjectList();
		layers = svgDoc.getLayers();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		drawBackground(g2);
		
		
		//for(int i=0; i<objects.size(); i++) {
		for(int i=0; i<layers.size(); i++) {
			/*
			Random rand = new Random();
			Color c = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
			*/
			for(int j=0; j<layers.get(i).size(); j++) {
				SvgGeometry object = layers.get(i).get(j);
				//System.out.println(i + ", " + j + ": " + object.shape.getBounds2D());
				if(object.shape!=null) {
					g2.setStroke(new BasicStroke((float) object.strokeWidth));
					if(object.fill != null) {
						g2.setColor(new Color(object.fill));
						g2.fill(object.shape);
					}
					if(object.stroke != null) {
						g2.setColor(new Color(object.stroke));
						g2.draw(object.shape);
					}
				}
			}
		}
		
		/*
		for(int i=0; i<objects.size(); i++) {
			SvgGeometry object = objects.get(i);
			//System.out.println(i + ", " + j + ": " + object.shape.getBounds2D());
			if(object.shape!=null) {
				g2.setStroke(new BasicStroke((float) object.strokeWidth));
				if(object.fill != null) {
					g2.setColor(new Color(object.fill));
					g2.fill(object.shape);
				}
				if(object.stroke != null) {
					g2.setColor(new Color(object.stroke));
					g2.draw(object.shape);
				}
			}
			
		}*/
	}
	
	private void drawBackground(Graphics2D g) {
		Color c = g.getColor();
		g.setColor(Color.white);
		g.fillRect(0, 0, (int) docSize.getWidth(), (int) docSize.getHeight());
		g.setColor(c);
	}
	
	/*
	static void addAllDescendentPaths(Node e) {
		TransformManager manager = new TransformManager();
		NodeList children = e.getChildNodes();
		if(children.getLength() == 0) {
			try {
				SVGGraphicsElement node = (SVGGraphicsElement) e;
				Shape shape = SvgGeometryLoader.loadGeometry(node, manager);
				AffineTransform at = manager.getActiveTransform();
				if(shape!=null) {
					shapes.add(shape);
					ats.add(at);
				}
			} catch(Exception ee) {
				
			}
		}
		else {
			for(int i=0; i<children.getLength(); i++) {
				addAllDescendentPaths(children.item(i));
			}
		}
	}
	*/
	
}
