package SVGIO;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.svggen.SVGGraphics2DIOException;
import org.apache.batik.dom.GenericDOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import java.util.Random;

import SVGIO.intransix.osm.termite.svg.SvgConverter;
import SVGIO.intransix.osm.termite.svg.SvgDocument;

public class TestSVGGen {

	public static void paint(Graphics2D g2d, int x, int y) {
		Random rand = new Random();
		g2d.setColor(new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255)));
		//g2d.translate(1,1);
		g2d.fillOval(x, y, 10, 10);
		
		//g2d.translate(1,1);
		//g2d.fillOval(10, 10, 100, 100);
		//g2d.scale(0.5, 2);
		//g2d.setStroke(new BasicStroke(2));
		//g2d.drawRect(10, 10, 100, 100);
		/*
		g2d.setColor(Color.red);
		g2d.setTransform(AffineTransform.getTranslateInstance(0, 0));
		g2d.fillOval(100, 10, 100, 100);
		g2d.setColor(Color.green);
		g2d.drawOval(100, 10, 100, 100);
		BufferedImage im = new BufferedImage(50,50,BufferedImage.TYPE_INT_RGB);
		Graphics2D imG = (Graphics2D) im.getGraphics();
		imG.setColor(Color.green);
		imG.fillRect(0, 0, 20, 20);
		g2d.drawImage(im, 0, 0, null);
		*/
	}

	public static void paintLayer(SVGGraphics2D svgG2, String layerName, int i, int x, int y) {
		SVGGraphics2D g = (SVGGraphics2D)svgG2.create();
		Element topLevelGroup = g.getTopLevelGroup();
		for(int j=0; j<i; j++) paint(g, x + j*1, y);
		Element cmpGroup = g.getTopLevelGroup();
		cmpGroup.setAttributeNS(null, "id", layerName);
		cmpGroup.setNodeValue(layerName);
		topLevelGroup.appendChild(cmpGroup);
		svgG2.setTopLevelGroup(topLevelGroup);
	}
	
	public static void main(String[] args) throws IOException {

		SVGGraphics2D svgG2 = createSVGGraphics2D(new Dimension(50,50));

		// Ask the test to render into the SVG Graphics2D implementation.
		//TestSVGGen test = new TestSVGGen();
		
		for(int i=0; i<5; i++) {
			paintLayer(svgG2, "Layerrr_" + i, i+1, i*5, i*5);
		}
		
		//test.paint(svgG2);
		//String t = "...\n  <\tg>jhdddksdjf<g>adfl\n\t kjaf</g>sdf</g>sldfkddf...";
		//System.out.println(replaceGroupTags(t));

		String filename = "C:\\Users\\Piffany\\Desktop\\test.svg";
		writeToSVGFile(filename, svgG2);
		
		SvgConverter converter = new SvgConverter();
		String s = new File(filename).toURI().toString();
		SvgDocument svgDoc = converter.loadSvg(s);
		
		showSVGInWindow(svgDoc);
		
	}
	
	private static void showSVGInWindow(SvgDocument svgDoc) {
		Window window = new Window(svgDoc);
		window.setVisible(true);
	}
	
	public static void writeToSVGFile(String filename, SVGGraphics2D svgG2) {
		// Finally, stream out SVG to the standard output using
		// UTF-8 encoding.
		boolean useCSS = true; // we want to use CSS style attributes
		try {
			svgG2.stream(filename, useCSS);
			removeTopLevelGroup(filename);
		} catch (SVGGraphics2DIOException e) {
			e.printStackTrace();
		}
	}
	
	private static String read(String filename) throws IOException {
	    StringBuilder text = new StringBuilder();
	    String NL = System.getProperty("line.separator");
	    Scanner scanner = new Scanner(new FileInputStream(filename));
	    try {
	      while (scanner.hasNextLine()){
	        text.append(scanner.nextLine() + NL);
	      }
	    }
	    finally{
	      scanner.close();
	    }
	    return text.toString();
	  }
	
	private static void removeTopLevelGroup(String filename) {
		try {
			String s = read(filename);
			s = replaceGroupTags(s);
			PrintWriter out = new PrintWriter(filename);
			out.write(s);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String unEscapeString(String s){
	    StringBuilder sb = new StringBuilder();
	    for (int i=0; i<s.length(); i++)
	        switch (s.charAt(i)){
	            case '\t': sb.append("\\t"); break;
	            case '\b': sb.append("\\b"); break;
	            case '\n': sb.append("\\n"); break;
	            case '\r': sb.append("\\r"); break;
	            case '\f': sb.append("\\f"); break;
	            case '\'': sb.append("\\'"); break;
	            case '\"': sb.append("\\\""); break;
	            case '\\': sb.append("\\\\"); break;
	            // ... rest of escape characters
	            default: sb.append(s.charAt(i));
	        }
	    return sb.toString();
	}
	
	private static String replaceGroupTags(String s) {
		//s = s.replaceAll("(\\s)*>", ">\n");
		//s = s.replaceAll("(\\s)*/>", "/>\n");
		//s = unEscapeString(s);
		s = s.replaceFirst("<(\\s)*g(\\s)*>", "");
		s = reverse(s);
		s = s.replaceFirst(">(\\s)*g(\\s)*/(\\s)*<", "");
		s = reverse(s);
		//s = s.replaceAll( "^(.*)</g>(.*)$", "$1$2" );
		//s = s.replaceAll("www", "");
		 return s;
	}
	
	public static String reverse(String s) {
	    if (s.length() <= 1) { 
	        return s;
	    }
	    return reverse(s.substring(1, s.length())) + s.charAt(0);
	}
	
	public static SVGGraphics2D createSVGGraphics2D(Dimension dim) {
		// Get a DOMImplementation.
		DOMImplementation domImpl =
				GenericDOMImplementation.getDOMImplementation();

		// Create an instance of org.w3c.dom.Document.
		String svgNS = "http://www.w3.org/2000/svg";
		Document document = domImpl.createDocument(svgNS, "svg", null);

		// Create an instance of the SVG Generator.
		SVGGraphics2D svgG2 = new SVGGraphics2D(document);
		svgG2.setSVGCanvasSize(dim);
		return svgG2;
	}
}