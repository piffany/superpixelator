package SVGIO;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

import SVGIO.intransix.osm.termite.svg.SvgDocument;



@SuppressWarnings("serial")
public class Window
extends JFrame {

	private Canvas canvas;

	public Window(SvgDocument svgDoc) {
		super("Pixelator 2.0");
		setSize(600, 600);

		canvas = new Canvas(svgDoc);
		add(canvas);
		
		// exit program
		addWindowListener(new WindowAdapter() { 
			public void windowClosing(WindowEvent e) { 
				exit(); 
			} 
		});
	}

	public void exit() { 
		setVisible(false);
		dispose(); 
		System.exit(0); 
	}
} 



