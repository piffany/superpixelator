package SVGIO.intransix.osm.termite.svg;

public class MyBase64b {

    private final static char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    private static int[]  toInt   = new int[128];

    static {
        for(int i=0; i< ALPHABET.length; i++){
            toInt[ALPHABET[i]]= i;
        }
    }

    /**
     * Translates the specified byte array into Base64 string.
     *
     * @param buf the byte array (not null)
     * @return the translated Base64 string (not null)
     */
    public static String encode(byte[] buf){
        int size = buf.length;
        char[] ar = new char[((size + 2) / 3) * 4];
        int a = 0;
        int i=0;
        while(i < size){
            byte b0 = buf[i++];
            byte b1 = (i < size) ? buf[i++] : 0;
            byte b2 = (i < size) ? buf[i++] : 0;

            int mask = 0x3F;
            ar[a++] = ALPHABET[(b0 >> 2) & mask];
            ar[a++] = ALPHABET[((b0 << 4) | ((b1 & 0xFF) >> 4)) & mask];
            ar[a++] = ALPHABET[((b1 << 2) | ((b2 & 0xFF) >> 6)) & mask];
            ar[a++] = ALPHABET[b2 & mask];
        }
        switch(size % 3){
            case 1: ar[--a]  = '=';
            case 2: ar[--a]  = '=';
        }
        return new String(ar);
    }

    /**
     * Translates the specified Base64 string into a byte array.
     *
     * @param s the Base64 string (not null)
     * @return the byte array (not null)
     */
    /** 
     * Decodes a byte array from Base64 format. 
     *  
     * @param s 
     *            a Base64 String to be decoded. 
     * @return An array containing the decoded data bytes. 
     * @throws IllegalArgumentException 
     *             if the input is not valid Base64 encoded data. 
     */  
    public static byte[] decode(String s)  
    {  
        return decode(s.toCharArray());  
    }  
  
    /** 
     * Decodes a byte array from Base64 format. No blanks or line breaks are 
     * allowed within the Base64 encoded data. 
     *  
     * @param in 
     *            a character array containing the Base64 encoded data. 
     * @return An array containing the decoded data bytes. 
     * @throws IllegalArgumentException 
     *             if the input is not valid Base64 encoded data. 
     */  
    public static byte[] decode(char[] in)  
    {  
        int iLen = in.length;  
        if (iLen % 4 != 0)  {
        	System.out.println(iLen + " Length of Base64 encoded input string is not a multiple of 4.");
            throw new IllegalArgumentException(  
            "Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (iLen > 0 && in[iLen - 1] == '=')  
            iLen--;  
        int oLen = (iLen * 3) / 4;  
        byte[] out = new byte[oLen];  
        int ip = 0;  
        int op = 0;  
        while (ip < iLen)  
        {  
            int i0 = in[ip++];  
            int i1 = in[ip++];  
            int i2 = ip < iLen ? in[ip++] : 'A';  
            int i3 = ip < iLen ? in[ip++] : 'A';  
            if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127)  
                throw new IllegalArgumentException(  
                "Illegal character in Base64 encoded data.");  
            int b0 = map2[i0];  
            int b1 = map2[i1];  
            int b2 = map2[i2];  
            int b3 = map2[i3];  
            if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0)  
                throw new IllegalArgumentException(  
                "Illegal character in Base64 encoded data.");  
            int o0 = (b0 << 2) | (b1 >>> 4);  
            int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);  
            int o2 = ((b2 & 3) << 6) | b3;  
            out[op++] = (byte) o0;  
            if (op < oLen)  
                out[op++] = (byte) o1;  
            if (op < oLen)  
                out[op++] = (byte) o2;  
        }  
        return out;  
    }
    
    private static char[] map1 = new char[64];  
    static  
    {  
        int i = 0;  
        for (char c = 'A'; c <= 'Z'; c++)  
            map1[i++] = c;  
        for (char c = 'a'; c <= 'z'; c++)  
            map1[i++] = c;  
        for (char c = '0'; c <= '9'; c++)  
            map1[i++] = c;  
        map1[i++] = '+';  
        map1[i++] = '/';  
    }  
  
    // Mapping table from Base64 characters to 6-bit nibbles.  
    private static byte[] map2 = new byte[128];  
    static  
    {  
        for (int i = 0; i < map2.length; i++)  
            map2[i] = -1;  
        for (int i = 0; i < 64; i++)  
            map2[map1[i]] = (byte) i;  
    }  

}