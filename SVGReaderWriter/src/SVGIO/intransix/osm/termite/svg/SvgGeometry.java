package SVGIO.intransix.osm.termite.svg;

import java.util.HashMap;

import org.apache.batik.dom.svg.*;
import org.apache.batik.util.CSSConstants;
import org.apache.batik.util.SVGConstants;

import java.awt.Color;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;


public class SvgGeometry {

	//====================================
	// Fields
	//====================================
	public String id;
	public Integer fill;
	public Integer stroke;
	public double strokeWidth;
	public Shape shape;
	public BufferedImage image = null;

	//====================================
	// Public Functions
	//====================================

	private String getAncestralAttribute(SVGGraphicsElement svgObjectElement, String SVGAttribute, String CSSAttribute) {
		String attrib = getAttribute(svgObjectElement, SVGAttribute, CSSAttribute);
		if(attrib == "none") attrib = null;
		else if(attrib == null || attrib.length() == 0) {
			try {
				SVGGraphicsElement parent = (SVGGraphicsElement) svgObjectElement.getParentNode();
				String parentAttrib = getAncestralAttribute(parent, SVGAttribute, CSSAttribute);
				attrib = parentAttrib;
			}
			catch(Exception e) {
				
				try {
					SVGOMSVGElement parent = (SVGOMSVGElement) svgObjectElement.getParentNode();
					String parentAttrib = getAttribute(parent, SVGAttribute, CSSAttribute);
					//System.out.println(parentAttrib);
					attrib = parentAttrib;
				}
				catch(Exception e2) {
					attrib = null;
				}
				
				//attrib = null;
			}
		}
		return attrib;
	}

	private String getAttribute(SVGGraphicsElement svgObjectElement, String SVGAttribute, String CSSAttribute) {

		HashMap<String,String> styleTable = null;
		String style = svgObjectElement.getAttribute(SVGConstants.SVG_STYLE_ATTRIBUTE);
		if((style != null)&&(style.length() > 0)) {
			styleTable = getStyleTable(style);
		}
		
		String attribString = svgObjectElement.getAttribute(SVGAttribute);
		if(((attribString == null)||(attribString.length() == 0))&&(styleTable != null)) {
			attribString = styleTable.get(CSSAttribute);
		}
		if((attribString == null)||(attribString.length() == 0)) attribString = null;
		return attribString;
	}
	
	private String getAttribute(SVGOMSVGElement svgObjectElement, String SVGAttribute, String CSSAttribute) {

		HashMap<String,String> styleTable = null;
		String style = svgObjectElement.getAttribute(SVGConstants.SVG_STYLE_ATTRIBUTE);
		if((style != null)&&(style.length() > 0)) {
			styleTable = getStyleTable(style);
		}
		
		String attribString = svgObjectElement.getAttribute(SVGAttribute);
		if(((attribString == null)||(attribString.length() == 0))&&(styleTable != null)) {
			attribString = styleTable.get(CSSAttribute);
		}
		if((attribString == null)||(attribString.length() == 0)) attribString = null;
		return attribString;
	}

	public SvgGeometry(SVGGraphicsElement svgObjectElement, Shape shape) {

		this.shape = shape;
		image = null;

		//get id
		id = svgObjectElement.getAttribute(SVGConstants.SVG_ID_ATTRIBUTE);

		String fillString = getAncestralAttribute(svgObjectElement, SVGConstants.SVG_FILL_ATTRIBUTE, CSSConstants.CSS_FILL_PROPERTY);
		fill = (fillString == null) ? null : colorToInt(fillString);

		String strokeString = getAncestralAttribute(svgObjectElement, SVGConstants.SVG_STROKE_ATTRIBUTE, CSSConstants.CSS_STROKE_PROPERTY);
		stroke = (strokeString == null) ? null : colorToInt(strokeString);

		String widthString = getAncestralAttribute(svgObjectElement, SVGConstants.SVG_STROKE_WIDTH_ATTRIBUTE, CSSConstants.CSS_STROKE_WIDTH_PROPERTY);
		if(widthString != null) {
			widthString = widthString.replace("px", "");
			strokeWidth = Double.parseDouble(widthString);
		}
		else strokeWidth = 0;//(stroke==null) ? 0 : 1;
		//System.out.println(((fill==null)?null:new Color(fill)) + ", " + ((stroke==null)?null:new Color(stroke)) + ", " + strokeWidth);
		
	}

	public static BufferedImage deepCopy(BufferedImage bi) {
		 ColorModel cm = bi.getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = bi.copyData(null);
		 return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	
	public SvgGeometry(SVGGraphicsElement svgObjectElement, BufferedImage im) {

		this.shape = null;
		image = deepCopy(im);
		
		//get id
		id = svgObjectElement.getAttribute(SVGConstants.SVG_ID_ATTRIBUTE);

	}
	
	public boolean hasImage() {
		return image != null;
	}

	//====================================
	// Private Functions
	//====================================

	private Integer colorToInt(String colorString) {
		//System.out.println(colorString + " --> " + Color.getColor(colorString));
		if(colorString.equals("none")) return null;
		else if(ColorConverter.isPredefinedColorName(colorString)) {
			return ColorConverter.getPredefinedColor(colorString).getRGB();
		}
		/*
		else if(colorString.equals("blue")) colorString = "#0000ff"; 
		else if(colorString.equals("red")) colorString = "#ff0000";
		else if(colorString.equals("lime")) colorString = "#00ff00";
		else if(colorString.equals("black")) colorString = "#000000";
		*/
		if(colorString.startsWith("#")) {
			colorString = colorString.replace("#","0x");
			return Integer.decode(colorString);
		}
		else if(colorString.startsWith("rgb")) {
			colorString = colorString.replace("rgb","");
			colorString = colorString.replace("(","");
			colorString = colorString.replace(")","");
			String[] parts = colorString.split(",");
			int[] rgb = new int[parts.length];
			for(int i=0; i<parts.length; i++) rgb[i] = Integer.parseInt(parts[i]);
			return new Color(rgb[0],rgb[1],rgb[2]).getRGB();
		}
		return null;
	}

	private static HashMap<String,String> getStyleTable(String style) {
		HashMap<String,String> styleTable = new HashMap<String,String>();
		String[] pairs = style.split(";");
		String[] keyValue;
		for(String pair:pairs) {
			keyValue = pair.split(":");
			if(keyValue.length != 2) {
				throw new RuntimeException("Invalid style string format");
			}
			styleTable.put(keyValue[0].trim(), keyValue[1].trim());
		}
		return styleTable;
	}
}
