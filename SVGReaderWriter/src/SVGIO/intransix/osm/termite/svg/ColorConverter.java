// XAAL toolkit
// Copyright (C) 2009 Ville Karavirta <vkaravir(a)cs.hut.fi>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
package SVGIO.intransix.osm.termite.svg;

import java.awt.Color;
import java.util.HashMap;

@SuppressWarnings("unchecked")
public class ColorConverter {
    @SuppressWarnings("rawtypes")
	private static HashMap nameColor = new HashMap();
    @SuppressWarnings("rawtypes")
	private static HashMap colorName = new HashMap();
    static {
        String[] colNames = { "maroon", "red", "orange", "yellow", "olive",
                "purple", "fuchsia", "white", "lime", "green", "navy", "blue", "aqua",
                "teal", "black", "silver", "gray" };
        Color[] colors = new Color[] {
                new Color(128,0,0),
                new Color(255,0,0),
                new Color(255,165,0),
                new Color(255,255,0),
                new Color(128,128,0),
                new Color(128,0,128),
                new Color(255,0,255),
                new Color(255,255,255),
                new Color(0,255,0),
                new Color(0,128,0),
                new Color(0,0,128),
                new Color(0,0,255),
                new Color(0,255,255),
                new Color(0,128,128),
                new Color(0,0,0),
                new Color(192,192,192),
                new Color(128,128,128) };
        for (int i = 0; i < colors.length; i++) {
            nameColor.put(colNames[i], colors[i]);
            colorName.put(colors[i], colNames[i]);
        }
    }
    
    public static boolean isPredefinedColor(Color c) {
        return colorName.containsKey(c);
    }
    
    public static Color getPredefinedColor(String name) {
        return (Color) nameColor.get(name.toLowerCase());        
    }
    
    public static boolean isPredefinedColorName(String name) {
        if (name == null)
            return false;
        else
            return nameColor.containsKey(name.toLowerCase());
    }
    
    public static String getPredefinedColorName(Color c) {
        return (String) colorName.get(c);
    }

    public static String toHex(Color color) {
        if (color == null)
            return "000000";
        String red = Integer.toHexString(color.getRed());
        if (red.length() == 1)
            red = red + red;
        String green = Integer.toHexString(color.getGreen());
        if (green.length() == 1)
            green = green + green;
        String blue = Integer.toHexString(color.getBlue());
        if (blue.length() == 1)
            blue = blue + blue;
        return red + green + blue;
    }
}
