package SVGIO.intransix.osm.termite.svg;


import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Vector;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.apache.batik.dom.svg.*;
import org.w3c.dom.*;

import java.awt.*;

/**
 *
 * @author sutter
 */
public class SvgDocument extends TransformManager {
	
	//======================================
	// Private Fields
	//======================================
	private Rectangle2D docSize;
	private ArrayList<SvgGeometry> geomList = new ArrayList<SvgGeometry>();
	private Vector<Vector<SvgGeometry>> layers = new Vector<Vector<SvgGeometry>>();
	private Vector<String> layerNames = new Vector<String>();

	private final static String DISPLAY_TAG = "display";
	private final static String DISPLAY_NONE = "none";
	
	//======================================
	// Public Methods
	//======================================
	
	public Rectangle2D getDocSize() { return docSize;}
	
	public ArrayList<SvgGeometry> getObjectList() { return geomList;}

	public Vector<Vector<SvgGeometry>> getLayers() { return layers; }
	
	public Vector<String> getLayerNames() { return layerNames; }
	
	/** This method loads the SVG document. */
	public void load(Document svgDocument) {
		
		Element element = svgDocument.getDocumentElement();
		if(!(element instanceof SVGOMSVGElement)) {
			throw new RuntimeException("Invalid format for SVG document.");
		}
		SVGOMSVGElement svgElement = (SVGOMSVGElement)element;
		//SVGGraphics2D svgG2 = new SVGGraphics2D(svgDocument);
		
		//load the document size and view
		float docWidth = svgElement.getWidth().getBaseVal().getValueInSpecifiedUnits();
		float docHeight = svgElement.getHeight().getBaseVal().getValueInSpecifiedUnits();

		docSize = new Rectangle2D.Double(0,0,docWidth,docHeight);
		
		//process the elements in the document
		TransformManager transformManager = new TransformManager();
		NodeList children = element.getChildNodes();
		//Vector<Vector<SvgGeometry>> layers = new Vector<Vector<SvgGeometry>>();
		processChildren(children,transformManager);
		processLayers(children, transformManager);
	}
	
	//======================================
	// Private Methods
	//======================================
	
	
	private void processLayers(NodeList nodes, TransformManager transformManager) {
		int count = nodes.getLength();
		Node node;
		//Vector<Boolean> isLayer = new Vector<Boolean>();
		//boolean isTop = true;
		//Vector<Vector<SvgGeometry>> layers = new Vector<Vector<SvgGeometry>>();
		for(int i = 0; i < count; i++) {
			node = nodes.item(i);
			if(node instanceof Element) {
				if(((Element) node).hasAttribute("id")) {
					//if(isTop) {
						//isTop = false;
						//isLayer.add(false);
					//}
					//else {
						//System.out.println(i + " : " + ((Element) node).getAttribute("id"));
						//isLayer.add(true);
						
					//}
				}
				if(node instanceof SVGGraphicsElement) {
					//go to next if this is not displayed
					String displayValue = ((SVGGraphicsElement)node).getAttribute(DISPLAY_TAG);
					if((displayValue != null)&&(displayValue.equalsIgnoreCase(DISPLAY_NONE))) continue;
												
					if(node instanceof SVGOMGElement) {
						//flatten g, taking any transform from the group tag
						//handle the transforms
						int mark = transformManager.getMark();
						transformManager.loadTransforms((SVGOMGElement)node);	
						//process the nodees from this group element
						NodeList grandChildren = node.getChildNodes();
						Vector<SvgGeometry> geoms = processChildren(grandChildren,transformManager);
						layers.add(geoms);
						layerNames.add(((Element) node).getAttribute("id"));
						/*
						for(int j=0; j<geoms.size(); j++) {
							System.out.println(i + ", " + j + ": " + geoms.get(j).shape.getBounds2D());
						}
						*/
						transformManager.restoreToMark(mark);
					}
					/*
					else if(node instanceof SVGGraphicsElement) {
						SVGGraphicsElement element = (SVGGraphicsElement) node;
						Shape shape = SvgGeometryLoader.loadGeometry(element,transformManager);
						SvgGeometry geom = new SvgGeometry(element,shape);
						geomList.add(geom);
					}
					*/
				}
			}
		}
		//for(int i=0; i<layers.size(); i++) {
		//	System.out.println("layer " + i + " : " + layers.get(i).size());
		//}
	}
	
	/** This method creates the child node objects. */
	private Vector<SvgGeometry> processChildren(NodeList nodes, TransformManager transformManager) {
		//System.out.println("processChildren");
		//process the children
		int count = nodes.getLength();
		Node node;
		Vector<SvgGeometry> geoms = new Vector<SvgGeometry>();
		for(int i = 0; i < count; i++) {
			node = nodes.item(i);
			//check if we want to use this geometry
			if(node instanceof SVGGraphicsElement) {
								//go to next if this is not displayed
				String displayValue = ((SVGGraphicsElement)node).getAttribute(DISPLAY_TAG);
				if((displayValue != null)&&(displayValue.equalsIgnoreCase(DISPLAY_NONE))) continue;
				
				if(node instanceof SVGOMGElement) {
					//flatten g, taking any transform from the group tag
					//handle the transforms
					int mark = transformManager.getMark();
					transformManager.loadTransforms((SVGOMGElement)node);	
					//process the nodes from this group element
					NodeList grandChildren = node.getChildNodes();
					geoms.addAll(processChildren(grandChildren,transformManager));
					transformManager.restoreToMark(mark);
				}
				else if(node instanceof SVGGraphicsElement) {
					SVGGraphicsElement element = (SVGGraphicsElement) node;
					int[][] rgb = new int[(int)docSize.getWidth()][(int)docSize.getHeight()];
					boolean[] imageRead = {false};
					Shape shape = SvgGeometryLoader.loadGeometry(element,transformManager, rgb, imageRead);
					SvgGeometry geom;
					if(shape==null) {
						if(imageRead[0]) {
							try {
								int w = (int)docSize.getWidth();
								int h = (int)docSize.getHeight();
								BufferedImage im = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
								for(int ii=0; ii<w; ii++) {
									for(int jj=0; jj<h; jj++) {
										im.setRGB(ii, jj, rgb[ii][jj]);
									}
								}
								geom = new SvgGeometry(element, im);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								System.out.println("failed to load raster data");
								geom = new SvgGeometry(element, shape);
								//e.printStackTrace();
							}
						}
						else {
							geom = new SvgGeometry(element, shape);
						}
					}
					else {
						
						geom = new SvgGeometry(element, shape);
						
					}
					geoms.add(geom);
					geomList.add(geom);					
				}
			}
			
		}
		return geoms;
	}
}
