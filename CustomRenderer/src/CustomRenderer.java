import javax.swing.JFrame;

import renderer.display.CustomRasterizedDoubleBufferedCanvas;




@SuppressWarnings("serial")
public class CustomRenderer
extends JFrame {
	
	private CustomRasterizedDoubleBufferedCanvas canvas;

	public CustomRenderer() {
		super("Custom Renderer");
		setSize(1200, 500);

		// canvas
		canvas = new MyCanvas();
		add(canvas);
		
		// exit program
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		CustomRenderer app = new CustomRenderer();
		app.setVisible(true);
	}
	
}
