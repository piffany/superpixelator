package renderer.display;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JPanel;


// double buffered canvas

@SuppressWarnings("serial")
public class CustomRasterizedDoubleBufferedCanvas extends JPanel {

	// for double buffering
	private int bufferWidth;
	private int bufferHeight;
	private Image bufferImage;
	private Graphics bufferGraphics;

	public CustomRasterizedDoubleBufferedCanvas() {
		super();
	}

	@Override
	public void paintComponent(Graphics g) {
		if(bufferWidth!=getWidth() || bufferHeight!=getHeight() ||
		   bufferImage==null || bufferGraphics==null) resetBuffer();
		if(bufferGraphics!=null){
			// clear back buffer
			((Graphics2D)bufferGraphics).setBackground(Color.white);
			bufferGraphics.clearRect(0,0,bufferWidth,bufferHeight);
			// draw on back buffer
			paintBuffer(bufferGraphics);
			// draw to front buffer
			g.drawImage(bufferImage,0,0,this);
		}
	}

	private void resetBuffer(){
		bufferWidth = getWidth();
		bufferHeight = getHeight();
		// clean up previous image
		if(bufferGraphics!=null){
			bufferGraphics.dispose();
			bufferGraphics = null;
		}
		if(bufferImage!=null){
			bufferImage.flush();
			bufferImage = null;
		}
		System.gc();
		// create new image
		bufferImage = createImage(bufferWidth, bufferHeight);
		bufferGraphics = bufferImage.getGraphics();
		//paintBuffer(bufferGraphics);
		bufferGraphics.clearRect(0,0,bufferWidth,bufferHeight);
	}

	public void myPaint(MyGraphics2D g) {
	}

	private void paintBuffer(Graphics g) {
		MyGraphics2D mg2 = new MyGraphics2D((Graphics2D) bufferGraphics, bufferWidth, bufferHeight);
		myPaint(mg2);
	}
}
