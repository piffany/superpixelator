package renderer.display;

import renderer.geometry.*;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Map;
import java.util.Vector;

import renderer.geometry.Pixel;
import renderer.rasterizer.DefaultRasterizer;
import renderer.rasterizer.Rasterizer;
import renderer.rasterizer.pixelator.Pixelator;
import renderer.rasterizer.superpixelator.Superpixelator;



public class MyGraphics2D {
	
	private Graphics2D g2;
	private int width, height;
	private double zoom = 1;
	private int rasterizerType = Rasterizer.SUPERPIXELATOR;
	private Rasterizer rasterizer = new Superpixelator();
	
	public MyGraphics2D(Graphics2D g, int width, int height, int rasterizerType) {
		this(g, width, height);
		this.rasterizerType = rasterizerType;
		setRasterizerType(rasterizerType);
	}
	
	public MyGraphics2D(MyGraphics2D g) {
		this(g.getGraphics2D(), g.getWidth(), g.getHeight());
		//setState(g.getState());
		this.zoom = g.getZoom();
	}
	
	public MyGraphics2D(Graphics2D g, int width, int height) {
		g2 = (Graphics2D) g.create();
		this.width = width;
		this.height = height;
	}
	
	public MyGraphics2D(Image image) {
		g2 = (Graphics2D) image.getGraphics();
		width = image.getWidth(null);
		height = image.getHeight(null);
	}
	
	public GraphicsState getState() {
		GraphicsState gs = new GraphicsState(getZoom(), getTransform(), getStrokeWidth(),
				   getBackground(), getColor(), getPaint(), getRasterizerType(), getAA());
		return gs;
	}
	
	public boolean getAA() {
		return getRenderingHint(RenderingHints.KEY_ANTIALIASING) == RenderingHints.VALUE_ANTIALIAS_ON;
	}
	
	public void setAA(boolean AA) {
		if(AA) {
			setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		}
		else {
			setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		}
	}
	
	public void setState(GraphicsState gs) {
		setZoom(gs.getZoom());
		setTransform(gs.getTransform());
		setStrokeWidth(gs.getStrokeWidth());
		setBackground(gs.getBackground());
		setColor(gs.getColor());
		setPaint(gs.getPaint());
		setRasterizerType(gs.getRasterizerType());
		setAA(gs.getAA());
	}
	
	public double getStrokeWidth() {
		BasicStroke stroke = (BasicStroke) getStroke();
		double strokeWidth = stroke.getLineWidth();
		return strokeWidth;
	}
	
	public void setStrokeWidth(double sw) {
		BasicStroke stroke = (BasicStroke) getStroke();
		Stroke newStroke = new BasicStroke((float) sw, stroke.getEndCap(), stroke.getLineJoin());
		setStroke(newStroke);
	}
	
	public int getRasterizerType() {
		return rasterizerType;
	}
	
	public void setRasterizerType(int rasterizerType) {
		this.rasterizerType = rasterizerType;
		switch(rasterizerType) {
		case Rasterizer.DEFAULT:
			rasterizer = new DefaultRasterizer();
			break;
		case Rasterizer.PIXELATOR:
			rasterizer = new Pixelator();
			break;
		case Rasterizer.SUPERPIXELATOR:
			rasterizer = new Superpixelator();
			break;
		}		
	}
	
	public Graphics2D getGraphics2D() {
		return g2;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setZoom(double zoom) {
		this.zoom = zoom;
	}
	
	public double getZoom() {
		return zoom;
	}
	
	public void drawZoomedImage(RenderedImage img, double zoom) {
		AffineTransform at = g2.getTransform();
		g2.setTransform(getIdentityTransform());
		g2.drawRenderedImage(img, getZoomTransform());
		g2.setTransform(at);
	}
	
	public Image createImageFromGraphics() {
		Image image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		return image;
	}
	
	public Graphics2D getGraphicsWithProperties(Image image) {
		Graphics2D g2 = (Graphics2D) image.getGraphics();
		g2.setBackground(getBackground());
		g2.setPaint(getPaint());
		g2.setColor(getColor());
		g2.setTransform(getTransform());
		g2.setStroke(getStroke());
		g2.setRenderingHints(getRenderingHints());
		return g2;
	}
	
	public void draw(Shape s) {
		RenderedImage unscaled = rasterizer.draw(this, s);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void fill(Shape s) {
		RenderedImage unscaled = rasterizer.fill(this, s);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawPoints(Vector<MyPoint> pixels) {
		RenderedImage unscaled = rasterizer.drawPoints(this, pixels);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawPixels(Vector<Pixel> pixels) {
		RenderedImage unscaled = rasterizer.drawPixels(this, pixels);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawPath(Path path) {
		RenderedImage unscaled = rasterizer.drawPath(this, path);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void fillPath(Path path) {
		RenderedImage unscaled = rasterizer.fillPath(this, path);
		drawZoomedImage(unscaled, zoom);
	}

	public boolean drawImage(Image img, AffineTransform xform, ImageObserver obs) {
		RenderedImage unscaled = rasterizer.drawImage(this, img, xform, obs);
		drawZoomedImage(unscaled, zoom);
		return true;
	}
	
	public void drawImage(BufferedImage img, BufferedImageOp op, int x, int y) {
		RenderedImage unscaled = rasterizer.drawImage(this, img, op, x, y);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawRenderedImage(RenderedImage img, AffineTransform xform) {
		RenderedImage unscaled = rasterizer.drawRenderedImage(this, img, xform);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawRenderableImage(RenderableImage img, AffineTransform xform) {
		RenderedImage unscaled = rasterizer.drawRenderableImage(this, img, xform);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawString(String str, int x, int y) {
		RenderedImage unscaled = rasterizer.drawString(this, str, x, y);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawString(String str, float x, float y) {
		RenderedImage unscaled = rasterizer.drawString(this, str, x, y);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawString(AttributedCharacterIterator iterator, int x, int y) {
		RenderedImage unscaled = rasterizer.drawString(this, iterator, x, y);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawString(AttributedCharacterIterator iterator, float x, float y) {
		RenderedImage unscaled = rasterizer.drawString(this, iterator, x, y);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawGlyphVector(GlyphVector g, float x, float y) {
		RenderedImage unscaled = rasterizer.drawGlyphVector(this, g, x, y);
		drawZoomedImage(unscaled, zoom);
	}
	
	public boolean hit(Rectangle rect, Shape s, boolean onStroke) {
		return g2.hit(rect, s, onStroke);
	}
	
	public GraphicsConfiguration getDeviceConfiguration() {
		return g2.getDeviceConfiguration();
	}
	
	public void setComposite(Composite comp) {
		g2.setComposite(comp);
	}
	
	public void setPaint(Paint paint) {
		g2.setPaint(paint);
	}
	
	public void setStroke(Stroke s) {
		g2.setStroke(s);
	}
	
	public void setRenderingHint(Key hintKey, Object hintValue) {
		g2.setRenderingHint(hintKey, hintValue);
	}
	
	public Object getRenderingHint(Key hintKey) {
		return g2.getRenderingHint(hintKey);
	}
	
	public void setRenderingHints(Map<?, ?> hints) {
		g2.setRenderingHints(hints);
	}
	
	public void addRenderingHints(Map<?, ?> hints) {
		g2.addRenderingHints(hints);
	}
	
	public RenderingHints getRenderingHints() {
		return g2.getRenderingHints();
	}
	
	public void translate(int x, int y) {
		g2.translate(x, y);
	}
	
	public void translate(double tx, double ty) {
		g2.translate(tx, ty);
	}
	
	public void rotate(double theta) {
		g2.rotate(theta);
	}
	
	public void rotate(double theta, double x, double y) {
		g2.rotate(theta, x, y);
	}
	
	public void scale(double sx, double sy) {
		g2.scale(sx, sy);
	}

	public void shear(double shx, double shy) {
		g2.shear(shx, shy);
	}
	
	public void transform(AffineTransform Tx) {
		g2.transform(Tx);
	}
	
	public void setTransform(AffineTransform Tx) {
		g2.setTransform(Tx);
	}
	
	public AffineTransform getTransform() {
		AffineTransform at = g2.getTransform();
		return at;
	}
	
	public AffineTransform getZoomTransform() {
		return AffineTransform.getScaleInstance(zoom, zoom);
	}
	
	public AffineTransform getIdentityTransform() {
		return AffineTransform.getTranslateInstance(0, 0);
	}
	
	public void setIdentityTransform() {
		setTransform(AffineTransform.getTranslateInstance(0, 0));
	}

	public Paint getPaint() {
		return g2.getPaint();
	}
	
	public Composite getComposite() {
		return g2.getComposite();
	}
	
	public void setBackground(Color color) {
		g2.setBackground(color);
	}
	
	public Color getBackground() {
		return g2.getBackground();
	}
	
	public Stroke getStroke() {
		return g2.getStroke();
	}
	
	public void clip(Shape s) {
		g2.clip(s);
	}
	
	public FontRenderContext getFontRenderContext() {
		return g2.getFontRenderContext();
	}
	
	public Graphics create() {
		return g2.create();
	}
	
	public Color getColor() {
		return g2.getColor();
	}
	
	public void setColor(Color c) {
		g2.setColor(c);
	}
	
	public void setPaintMode() {
		g2.setPaintMode();
	}
	
	public void setXORMode(Color c1) {
		g2.setXORMode(c1);
	}
	
	public Font getFont() {
		return g2.getFont();
	}
	
	public void setFont(Font font) {
		g2.setFont(font);
	}
	
	public FontMetrics getFontMetrics(Font f) {
		return g2.getFontMetrics(f);
	}
	
	public Rectangle getClipBounds() {
		return g2.getClipBounds();
	}

	public void clipRect(int x, int y, int width, int height) {
		g2.clipRect(x, y, width, height);
	}
	
	public void setClip(int x, int y, int width, int height) {
		g2.setClip(x, y, width, height);
	}
	
	public Shape getClip() {
		return g2.getClip();
	}
	
	public void setClip(Shape clip) {
		g2.setClip(clip);
	}
	
	public void copyArea(int x, int y, int width, int height, int dx, int dy) {
		g2.copyArea(x, y, width, height, dx, dy);
	}
	
	public void drawLine(int x1, int y1, int x2, int y2) {
		RenderedImage unscaled = rasterizer.drawLine(this, x1, y1, x2, y2);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawRect(int x, int y, int width, int height) {
		RenderedImage unscaled = rasterizer.drawRect(this, x, y, width, height);
		drawZoomedImage(unscaled, zoom);
	}

	public void fillRect(int x, int y, int width, int height) {
		RenderedImage unscaled = rasterizer.fillRect(this, x, y, width, height);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void clearRect(int x, int y, int width, int height) {
		RenderedImage unscaled = rasterizer.clearRect(this, x, y, width, height);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawRoundRect(int x, int y, int width, int height,
			int arcWidth, int arcHeight) {
		RenderedImage unscaled = rasterizer.drawRoundRect(this, x, y, width, height, arcWidth, arcHeight);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void fillRoundRect(int x, int y, int width, int height,
			int arcWidth, int arcHeight) {
		RenderedImage unscaled = rasterizer.fillRoundRect(this, x, y, width, height, arcWidth, arcHeight);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawOval(int x, int y, int width, int height) {
		RenderedImage unscaled = rasterizer.drawOval(this, x, y, width, height);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void fillOval(int x, int y, int width, int height) {
		RenderedImage unscaled = rasterizer.fillOval(this, x, y, width, height);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawArc(int x, int y, int width, int height, int startAngle,
			int arcAngle) {
		RenderedImage unscaled = rasterizer.drawArc(this, x, y, width, height, startAngle, arcAngle);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void fillArc(int x, int y, int width, int height, int startAngle,
			int arcAngle) {
		RenderedImage unscaled = rasterizer.fillArc(this, x, y, width, height, startAngle, arcAngle);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawPolyline(int[] xPoints, int[] yPoints, int nPoints) {
		RenderedImage unscaled = rasterizer.drawPolyline(this, xPoints, yPoints, nPoints);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void drawPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		RenderedImage unscaled = rasterizer.drawPolygon(this, xPoints, yPoints, nPoints);
		drawZoomedImage(unscaled, zoom);
	}
	
	public void fillPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		RenderedImage unscaled = rasterizer.fillPolygon(this, xPoints, yPoints, nPoints);
		drawZoomedImage(unscaled, zoom);
	}
	
	public boolean drawImage(Image img, int x, int y, ImageObserver observer) {
		RenderedImage unscaled = rasterizer.drawImage(this, img, x, y, observer);
		drawZoomedImage(unscaled, zoom);
		return true;
	}
	
	public boolean drawImage(Image img, int x, int y, int width, int height,
			ImageObserver observer) {
		RenderedImage unscaled = rasterizer.drawImage(this, img, x, y, width, height, observer);
		drawZoomedImage(unscaled, zoom);
		return true;
	}
	
	public boolean drawImage(Image img, int x, int y, Color bgcolor,
			ImageObserver observer) {
		RenderedImage unscaled = rasterizer.drawImage(this, img, x, y, bgcolor, observer);
		drawZoomedImage(unscaled, zoom);
		return true;
	}
	
	public boolean drawImage(Image img, int x, int y, int width, int height,
			Color bgcolor, ImageObserver observer) {
		RenderedImage unscaled = rasterizer.drawImage(this, img, x, y, width, height, bgcolor, observer);
		drawZoomedImage(unscaled, zoom);
		return true;
	}
	
	public boolean drawImage(Image img, int dx1, int dy1, int dx2, int dy2,
			int sx1, int sy1, int sx2, int sy2, ImageObserver observer) {
		RenderedImage unscaled = rasterizer.drawImage(this, img,
				dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, observer);
		drawZoomedImage(unscaled, zoom);
		return true;
	}
	
	public boolean drawImage(Image img, int dx1, int dy1, int dx2, int dy2,
			int sx1, int sy1, int sx2, int sy2, Color bgcolor, ImageObserver observer) {
		RenderedImage unscaled = rasterizer.drawImage(this, img,
				dx1, dy1, dx2, dy2,	sx1, sy1, sx2, sy2, bgcolor, observer);
		drawZoomedImage(unscaled, zoom);
		return true;
	}
	
	public void dispose() {
		g2.dispose();
	}
}
