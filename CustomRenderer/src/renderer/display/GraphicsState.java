package renderer.display;

import java.awt.Color;
import java.awt.Paint;
import java.awt.geom.AffineTransform;

public class GraphicsState {
	
	private double zoom;
	private AffineTransform at;
	private double strokeWidth;
	private Color background, color;
	private Paint paint;
	private int rasterizerType;
	private boolean AA;
	
	public GraphicsState(double zoom, AffineTransform at, double strokeWidth,
						Color background, Color color, Paint paint, int rasterizerType,
						boolean AA) {
		setZoom(zoom);
		setTransform(at);
		setStrokeWidth(strokeWidth);
		setBackground(background);
		setColor(color);
		setPaint(paint);
		setRasterizerType(rasterizerType);
		setAA(AA);
	}

	public double getZoom() {
		return zoom;
	}

	public void setZoom(double zoom) {
		this.zoom = zoom;
	}

	public AffineTransform getTransform() {
		return at;
	}

	public void setTransform(AffineTransform at) {
		this.at = at;
	}

	public double getStrokeWidth() {
		return strokeWidth;
	}

	public void setStrokeWidth(double strokeWidth) {
		this.strokeWidth = strokeWidth;
	}

	public Color getBackground() {
		return background;
	}

	public void setBackground(Color background) {
		this.background = background;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Paint getPaint() {
		return paint;
	}

	public void setPaint(Paint paint2) {
		this.paint = paint2;
	}

	public int getRasterizerType() {
		return rasterizerType;
	}

	public void setRasterizerType(int rasterizerType) {
		this.rasterizerType = rasterizerType;
	}

	public boolean getAA() {
		return AA;
	}

	public void setAA(boolean AA) {
		this.AA = AA;
	}
	
}
