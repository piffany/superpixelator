package renderer.util;


import java.awt.geom.AffineTransform;

import renderer.geometry.MyPoint;



public class Transform {

	public static AffineTransform getIdentity() {
		return AffineTransform.getRotateInstance(0);
	}
	
	public static MyPoint transformPoint(MyPoint p, AffineTransform at) {
		MyPoint newPt = new MyPoint();
		at.transform(p, newPt);
		return newPt;
	}
	
	public static MyPoint transformPoint(double x, double y, AffineTransform at) {
		return transformPoint(new MyPoint(x,y), at);
	}
	
	public static MyPoint transformVector(MyPoint p, AffineTransform at) {
		MyPoint newPt = new MyPoint();
		at.deltaTransform(p, newPt);
		return newPt;
	}
	
	public static MyPoint transformVector(double x, double y, AffineTransform at) {
		return transformVector(new MyPoint(x,y), at);
	}

	public static int transformLength(int d, AffineTransform at) {
		return (int) transformLength((double)d, at);
		/*
		MyPoint oldVec = new MyPoint(d,0);
		MyPoint newVec = new MyPoint();
		at.deltaTransform(oldVec, newVec);
		int dist = (int) newVec.distance(new MyPoint(0,0));
		return (d>0) ? dist : -dist;
		*/
	}

	// assuming the same scaling in both x and y
	public static double transformLength(double d, AffineTransform at) {
		double scale = at.getScaleX();
		return d * scale;
		/*
		MyPoint oldVec = new MyPoint(d,0);
		MyPoint newVec = new MyPoint();
		at.deltaTransform(oldVec, newVec);
		double dist = newVec.distance(new MyPoint(0,0));
		return (d>0) ? dist : -dist;
		*/
	}
	
	public static double transformArea(double a, AffineTransform at) {
		double scale = (at.getScaleX())*(at.getScaleY());
		return a*scale;
	}
	


}
