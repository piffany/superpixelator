package renderer.util;

import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Vector;

import renderer.geometry.MyPoint;



public class MyMath {

	public static final AffineTransform identityTransform = 
			AffineTransform.getTranslateInstance(0, 0); 

	/*
	public static double round(double d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(d);
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}
	*/

	public static boolean xor(boolean a, boolean b) {
		return (a && !b) || (!a && b);
	}
	
	public static int sgn(double val) {
		return (val < 0) ? -1 : (val == 0) ? 0 : 1;
	}

	// inclusive
	public static boolean inRange(double val, double a, double b) {
		return (b-val)*(val-a) >= 0;
	}

	public static int gcd(int a, int b) {
		return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).intValue();
	}

	// point to line segment
	public static double getDistanceToLineSegment(MyPoint p, MyPoint v, MyPoint w) {
		MyPoint q = getClosestPointOnLineSegment(p, v, w);
		return p.distance(q);
	}
	
	public static MyPoint getClosestPointOnLineSegment(MyPoint p, MyPoint v, MyPoint w) {
		// Return minimum distance between line segment vw and point p
		double L2 = w.minus(v).length(); L2 = L2*L2; // i.e. |w-v|^2 -  avoid a sqrt
		if (L2 == 0.0) return v.clone();   // v == w case
		// Consider the line extending the segment, parameterized as v + t (w - v).
		// We find projection of point p onto the line. 
		// It falls where t = [(p-v) . (w-v)] / |w-v|^2
		double t = (p.minus(v)).dot(w.minus(v)) / L2;
		if (t < 0.0) return v.clone();       // Beyond the 'v' end of the segment
		else if (t > 1.0) return w.clone();  // Beyond the 'w' end of the segment
		MyPoint projection = v.plus((w.minus(v)).times(t));  // Projection falls on the segment (v + t*(w-v))
		return projection;
	}
	
	public static int floor(double val) {
		return (int) Math.floor(val);
	}
	
	public static double roundToDec(double d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(d);
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}
	
	public static int floor(double val, double from) {
		double diff = val-from;
		return (int) ((diff > 0) ? Math.floor(val) : Math.ceil(val));
	}
	
	public static int ceil(double val) {
		return (int) Math.ceil(val);
	}
	
	public static int ceil(double val, double from) {
		double diff = val-from;
		return (int) ((diff > 0) ? Math.ceil(val) : Math.floor(val));
	}

	public static int round(double val) {
		return (int) Math.round(val);
	}

	public static int round(double val, double from) { 
		double diff = val-from;
		return (diff > 0) ? roundUp(val) : roundDown(val);
	}
	
	public static int roundUp(double val) {
		if(val%1 < 0.5) return (int) Math.floor(val);
		else return (int) Math.ceil(val);
	}

	public static int roundDown(double val) {
		if(val%1 <= 0.5) return (int) Math.floor(val);
		else return (int) Math.ceil(val);
	}

	// return positive remainder
	public static double rem(double a, double b) {
		double q = Math.floor(a/b);
		double r = a-q*b;
		if(r<0) r+=a;
		return a;
	}

	// roots of ax^2 + bx + c = 0
	public static Vector<Double> getQuadraticRoots(double a, double b, double c) {
		Vector<Double> roots = new Vector<Double>();
		// if a==0, x = -c/b
		if(a==0) {
			roots.add(-c/b);
		}
		// if a!=0
		else {
			double discrim = b*b-4*a*c;
			// one real root
			if(discrim == 0) {
				roots.add(-b/(2*a));
			}
			// two real roots
			else if(discrim > 0) {
				roots.add((-b-Math.sqrt(discrim))/(2*a));
				roots.add((-b+Math.sqrt(discrim))/(2*a));
			}
		}
		return roots;

	}

	// invert affineTransform
	public static AffineTransform getInverse(AffineTransform at) {
		AffineTransform invAt = new AffineTransform(at);
		try {
			invAt.invert();
		} catch (NoninvertibleTransformException e) {
			e.printStackTrace();
		}
		return invAt;
	}

	public static double rescaleToInterval(double val, double a, double b) {
		assert(a != b);
		return (val-a)/(b-a);
	}
	
	public static int getClosestMatch(double val, double[] vals) {
		assert(vals.length > 0);
		double minDiff = Math.abs(val - vals[0]);
		int minInd = 0;
		for(int i=1; i<vals.length; i++) {
			double diff = Math.abs(val - vals[i]);
			if(diff < minDiff) {
				minDiff = diff;
				minInd = i;
			}
		}
		return minInd;
	}
	
	public static int getClosestMatch(int val, int[] vals) {
		assert(vals.length > 0);
		double minDiff = Math.abs(val - vals[0]);
		int minInd = 0;
		for(int i=1; i<vals.length; i++) {
			double diff = Math.abs(val - vals[i]);
			if(diff < minDiff) {
				minDiff = diff;
				minInd = i;
			}
		}
		return minInd;
	}
}
