package renderer.util;

public class Matrix2x2 {
	
	// first row: {a b}, second row: {c d}
	private double a, b, c, d;
	
	public Matrix2x2() {
		a = 0; b = 0; c = 0; d = 0;
	}
	
	public Matrix2x2(double a, double b, double c, double d) {
		this.a = a; this.b = b; this.c = c; this.d = d;
	}
	
	public Matrix2x2(Matrix2x2 M) {
		for(int i=0; i<1; i++) {
			for(int j=0; j<1; j++) {
				set(i, j, M.get(i,j));
			}
		}
	}
	
	public Matrix2x2 clone() {
		return new Matrix2x2(this);
	}
	
	public double get(int i, int j) {
		assert((i==0 || i==1) & (j==0 || j==1));
		if(i==0) {
			if(j==0) return a;
			else return b;
		}
		else {
			if(j==0) return c;
			else return d;
		}
	}
	
	public void set(int i, int j, double val) {
		assert((i==0 || i==1) & (j==0 || j==1));
		if(i==0) {
			if(j==0) a = val;
			else b = val;
		}
		else {
			if(j==0) c = val;
			else d = val;
		}
	}
	
	public Matrix2x2 times(Matrix2x2 M) {
		Matrix2x2 P = new Matrix2x2();
		for(int i=0; i<=1; i++) {
			for(int j=0; j<=1; j++) {
				// val = ith row times jth col
				double val = 0;
				for(int k=0; k<=1; k++) {
					val += get(i,k)*M.get(k,j);
				}
				P.set(i, j, val);
			}
		}
		return P;
	}
	
	public double[] timesColumnVector(double[] col) {
		assert(col.length==2);
		return timesColumnVector(col[0], col[1]);
	}
	
	public double[] timesColumnVector(double v1, double v2) {
		Matrix2x2 P = this.times(new Matrix2x2(v1, 0, v2, 0));
		double[] colVec = {P.get(0,0), P.get(1,0)};
		return colVec;
	}
	
	public double[] rowVectorTimes(double[] row) {
		assert(row.length==2);
		return rowVectorTimes(row[0], row[1]);
	}

	public double[] rowVectorTimes(double v1, double v2) {
		Matrix2x2 P = (new Matrix2x2(v1, v2, 0, 0)).times(this);
		double[] rowVec = {P.get(0,0), P.get(0,1)};
		return rowVec;
	}
	
	public String toString() {
		String s = "{{" + a + ", " + b + "}, {" + c + ", " + d + "}}";
		return s;
	}
	
	public boolean isZero() {
		boolean isZero = Math.abs(a) < 0.001 && Math.abs(b) < 0.001 && Math.abs(c) < 0.001 && Math.abs(d) < 0.001;
		return isZero;
	}
}
