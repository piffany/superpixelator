package renderer.test;

public class Debug {

	private static boolean DEBUG = false;
		
	public static void setDebug(boolean b) {
		DEBUG = b;
	}
	
	public static boolean getDebug() {
		return DEBUG;
	}
}
