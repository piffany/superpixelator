package renderer.rasterizer.pixelator;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Vector;

import renderer.display.GraphicsState;
import renderer.display.MyGraphics2D;
import renderer.geometry.Pixel;
import renderer.rasterizer.DefaultRasterizer;
import renderer.rasterizer.Rasterizer;
import renderer.rasterizer.Util;
import renderer.util.Transform;
import colours.Colour;


public class Pixelator extends Rasterizer {

	public RenderedImage draw(MyGraphics2D g, Shape s) {
		Vector<Path> paths = Path.getPaths(s);
		return PixelatorHelper.drawPaths(g, paths);
	}

	public RenderedImage fill(MyGraphics2D g, Shape s) {
		Vector<Path> paths = Path.getPaths(s);
		return PixelatorHelper.fillPaths(g, paths, true);
	}

	public RenderedImage drawPath(MyGraphics2D g, Path path) {
		return drawPath(g, path, true, true);
	}

	public RenderedImage drawPath(MyGraphics2D g, Path path, boolean shifted, boolean sorted) {
		GraphicsState gs = g.getState();
		Image image = g.createImageFromGraphics();
		Vector<Pixel> pixels = PixelatorHelper.pixelateThickPath(g, path.getTransformedCopy(AffineTransform.getTranslateInstance(0,0)), shifted, sorted);
		g.drawPixels(pixels);
		g.setState(gs);
		return (RenderedImage)image;
	}

	public RenderedImage fillPath(MyGraphics2D g, Path path) {
		return fillPath(g, path, true);
	}
	
	public RenderedImage fillPath(MyGraphics2D g, Path path, boolean removeOutline) {
		return fillPath(g, path, removeOutline, true, true);
	}

	public RenderedImage fillPath(MyGraphics2D g, Path path, boolean removeOutline, boolean shifted, boolean sorted) {
		GraphicsState gs = g.getState();
		g.setStrokeWidth(1);
		Vector<Pixel> pixels = PixelatorHelper.pixelateThickPath(g, path.getTransformedCopy(AffineTransform.getTranslateInstance(0, 0)), shifted, sorted);
		g.setState(gs);
		Path fillArea = new Path();
		fillArea.setWindingRule(path.getWindingRule());
		for(int i=0; i<pixels.size(); i++) {
			MyPoint p = pixels.get(i).add(new MyPoint(-0,-0));
			fillArea.addPoint(p);
		}
		MyGraphics2D g2 = new MyGraphics2D(g);
		g2.setTransform(Transform.getIdentity());
		RenderedImage image = (new DefaultRasterizer()).fillPath(g2, fillArea);
		if(removeOutline) {
			Vector<Pixel> outline = PixelatorHelper.pixelateThickPath(g, path.getTransformedCopy(AffineTransform.getTranslateInstance(0, 0)), shifted, sorted);
			PixelatorHelper.clearPixels((BufferedImage) image, outline);
		}
		g.setState(gs);
		return image;
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, AffineTransform xform, ImageObserver obs) {
		DefaultRasterizer rasterizer = new DefaultRasterizer();
		return rasterizer.drawImage(g, img, xform, obs);
	}

	public RenderedImage drawImage(MyGraphics2D g, BufferedImage img, BufferedImageOp op, int x, int y) {
		DefaultRasterizer rasterizer = new DefaultRasterizer();
		return rasterizer.drawImage(g, img, op, x, y);
	}

	public RenderedImage drawRenderedImage(MyGraphics2D g, RenderedImage img, AffineTransform xform) {
		DefaultRasterizer rasterizer = new DefaultRasterizer();
		return rasterizer.drawRenderedImage(g, img, xform);
	}

	public RenderedImage drawRenderableImage(MyGraphics2D g, RenderableImage img, AffineTransform xform) {
		DefaultRasterizer rasterizer = new DefaultRasterizer();
		return rasterizer.drawRenderableImage(g, img, xform);
	}

	// TODO: change to pixelator
	public RenderedImage drawString(MyGraphics2D g, String str, int x, int y) {
		float xf = (int)x, yf = (int)y;
		return drawString(g, str, xf, yf);
	}

	// TODO: change to pixelator
	public RenderedImage drawString(MyGraphics2D g, String str, float x, float y) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawString(str, x, y);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
	}

	// TODO: change to pixelator
	public RenderedImage drawString(MyGraphics2D g, AttributedCharacterIterator iterator, int x, int y) {
		float xf = (int)x, yf = (int)y;
		return drawString(g, iterator, xf, yf);
		// TODO: test
	}

	// TODO: change to pixelator
	public RenderedImage drawString(MyGraphics2D g, AttributedCharacterIterator iterator, float x, float y) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawString(iterator, x, y);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		// TODO: test
	}

	public RenderedImage drawGlyphVectorOutline(MyGraphics2D g, GlyphVector gv, float x, float y) {
		Vector<Path> paths = PixelatorHelper.getPathsFromGlyphVector(gv, x, y);
		for(int i=0; i<paths.size(); i++) {
		}
		Rectangle2D bounds = gv.getVisualBounds();
		g.translate(-bounds.getWidth(),-bounds.getHeight());
		return PixelatorHelper.drawPaths(g, paths);
	}

	public RenderedImage drawGlyphVector(MyGraphics2D g, GlyphVector gv, float x, float y) {
		Vector<Path> paths = PixelatorHelper.getPathsFromGlyphVector(gv, x, y);
		for(int i=0; i<paths.size(); i++) {
		}
		Rectangle2D bounds = gv.getVisualBounds();
		g.translate(-bounds.getWidth(),-bounds.getHeight());
		return PixelatorHelper.fillPaths(g, paths, false);
	}

	public RenderedImage drawLine(MyGraphics2D g, int x1, int y1, int x2, int y2) {
		Path path = new Path.Line(x1, y1, x2, y2);
		return drawPath(g, path);
	}

	public RenderedImage drawRect(MyGraphics2D g, int x, int y, int width, int height) {
		GraphicsState gs = g.getState();
		boolean outputParams[] = new boolean[2];
		Path path = PixelatorHelper.getRectanglePath(g, x, y, width, height, outputParams);
		g.setIdentityTransform();
		RenderedImage image = drawPath(g, path, outputParams[0], outputParams[1]);
		g.setState(gs);
		return image;
	}

	public RenderedImage fillRect(MyGraphics2D g, int x, int y, int width, int height) {
		GraphicsState gs = g.getState();
		boolean outputParams[] = new boolean[2];
		Path path = PixelatorHelper.getRectanglePath(g, x, y, width, height, outputParams);
		g.setIdentityTransform();
		RenderedImage image = fillPath(g, path, true, outputParams[0], outputParams[1]);
		g.setState(gs);
		return image;
	}

	// TODO: change to pixelator
	public RenderedImage clearRect(MyGraphics2D g, int x, int y, int width, int height) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.clearRect(x, y, width, height);
		return (RenderedImage) image;
	}

	public RenderedImage drawRoundRect(MyGraphics2D g, int x, int y,
			int width, int height, int arcWidth, int arcHeight) {
		GraphicsState gs = g.getState();
		boolean outputParams[] = new boolean[2];
		Path path = PixelatorHelper.getRoundRectanglePath(g, x, y, width, height,
											arcWidth, arcHeight, outputParams);
		g.setIdentityTransform();
		RenderedImage image = drawPath(g, path, outputParams[0], outputParams[1]);
		g.setState(gs);
		return image;
	}

	public RenderedImage fillRoundRect(MyGraphics2D g, int x, int y,
			int width, int height, int arcWidth, int arcHeight) {
		GraphicsState gs = g.getState();
		boolean outputParams[] = new boolean[2];
		Path path = PixelatorHelper.getRoundRectanglePath(g, x, y, width, height, arcWidth, arcHeight, outputParams);
		g.setIdentityTransform();
		RenderedImage image = fillPath(g, path, true, outputParams[0], outputParams[1]);
		g.setState(gs);
		return image;
	}

	public RenderedImage drawOval(MyGraphics2D g, int x, int y, int width, int height) {
		GraphicsState gs = g.getState();
		boolean outputParams[] = new boolean[2];
		Path path = PixelatorHelper.getOvalPath(g, x, y, width, height, outputParams);
		g.setIdentityTransform();
		RenderedImage image = drawPath(g, path, outputParams[0], outputParams[1]);
		g.setState(gs);
		return image;
	}

	public RenderedImage fillOval(MyGraphics2D g, int x, int y, int width, int height) {
		GraphicsState gs = g.getState();
		boolean outputParams[] = new boolean[2];
		Path path = PixelatorHelper.getOvalPath(g, x, y, width, height, outputParams);
		g.setIdentityTransform();
		RenderedImage image = fillPath(g, path, true, outputParams[0], outputParams[1]);
		g.setState(gs);
		return image;
	}

	public RenderedImage drawArc(MyGraphics2D g, int x, int y, int width, int height,
			int startAngle, int arcAngle) {
		Path path = new Path.Arc(x, y, width, height, startAngle, arcAngle);
		//path.lineTo(x + 0.5*width, y + 0.5*height);
		//path.closeSharply();
		return drawPath(g, path);
	}

	public RenderedImage fillArc(MyGraphics2D g, int x, int y, int width, int height,
			int startAngle, int arcAngle) {
		Path path1 = new Path.Arc(x, y, width, height, startAngle, arcAngle);
		Path path2 = path1.clone();
		path2.lineTo(x + 0.5*width, y + 0.5*height);
		path2.closeSharply();
		RenderedImage image = fillPath(g, path2, false);
		Vector<Pixel> outline = PixelatorHelper.pixelateThickPath(g, path2, true, true);
		PixelatorHelper.clearPixels((BufferedImage) image, outline);
		return image;
	}

	public RenderedImage drawPolyline(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints) {
		Path path = new Path.Polyline(xPoints, yPoints, nPoints);
		return drawPath(g, path);
	}

	public RenderedImage drawPolygon(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints) {
		Path path = new Path.Polygon(xPoints, yPoints, nPoints);
		return drawPath(g, path);
	}

	public RenderedImage fillPolygon(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints) {
		Path path = new Path.Polygon(xPoints, yPoints, nPoints);
		return fillPath(g, path, true);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, ImageObserver observer) {
		return drawImage(g, img, x, y, Colour.TRANSPARENT, observer);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, int width, int height, ImageObserver observer) {
		return drawImage(g, img, x, y, width, height, Colour.TRANSPARENT, observer);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, Color bgcolor, ImageObserver observer) {
		return drawImage(g, img, x, y, img.getWidth(observer), img.getHeight(observer), bgcolor, observer);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, 
			int width, int height, Color bgcolor, ImageObserver observer) {
		return drawImage(g, img, x, y, x+width, y+height, 0, 0, width, height, bgcolor, observer);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int dx1, int dy1, int dx2, int dy2,
			int sx1, int sy1, int sx2, int sy2, ImageObserver observer) {
		return drawImage(g, img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, Colour.TRANSPARENT, observer);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int dx1, int dy1, int dx2, int dy2,
			int sx1, int sy1, int sx2, int sy2, Color bgcolor, ImageObserver observer) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.drawImage(img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, bgcolor, observer);
		//setAlpha((BufferedImage)image, bgcolor.getAlpha());
		// TODO semi-transparent bgcolor doesn't work when width and height are specified
		return (RenderedImage) image;
	}

		
}
