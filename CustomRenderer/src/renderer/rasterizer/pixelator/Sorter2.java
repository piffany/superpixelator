package renderer.rasterizer.pixelator;

import renderer.geometry.MyCurve;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;
import renderer.util.MyMath;

import java.util.Collections;
import java.util.Vector;


public class Sorter2 {
	
	private final static double orderW = 1.0, distW = 3.0, spanOneW = 0.0, slopeW = 3.0;
	

	public static Vector<MyPoint> sortPathPoints(Path path, Vector<MyPoint> pixelPath, boolean[] sorted) {
		int sortDir = Sorter.getSortDirection(path);
		//System.out.println("sortDir =" + sortDir);
		Vector<MyPoint> sortedPixelPath = sortSlopeSpans(sortDir, pixelPath, sorted, path);
		return sortedPixelPath;
	}

	private static Vector<MyPoint> copyPolygonalPath(Vector<MyPoint> points) {
		Vector<MyPoint> copy = new Vector<MyPoint>();
		for(int i=0; i<points.size(); i++) {
			copy.add(points.get(i).clone());
		}
		return copy;
	}
	
	private static boolean isValidSpan(MyPoint v) {
		int absX = (int) Math.abs(Math.round(v.x));
		int absY = (int) Math.abs(Math.round(v.y));
		return (absX >=1 && absY >= 1) && (absX == 1 || absY == 1);
	}
	
	private static Vector<MyPoint> getPixelCentresInSpan(MyPoint corner1, MyPoint corner2) {
		int x1 = (int) Math.round(corner1.x);
		int y1 = (int) Math.round(corner1.y);
		int x2 = (int) Math.round(corner2.x);
		int y2 = (int) Math.round(corner2.y);
		int dirX = (x1 < x2) ? 1 : -1;
		int dirY = (y1 < y2) ? 1 : -1;
		int x = x1, y = y1;
		Vector<MyPoint> points = new Vector<MyPoint>();
		//int i = 0;
		while(MyMath.inRange(x, x1, x2) && MyMath.inRange(y, y1, y2)) {
			points.add(new MyPoint(x+0.5*(double)dirX, y+0.5*(double)dirY));
			//i++;
			boolean incX = MyMath.inRange(x+dirX+0.5*(double)dirX, x1, x2);
			boolean incY = MyMath.inRange(y+dirY+0.5*(double)dirY, y1, y2);
			if(incX) x += dirX;
			if(incY)y += dirY;
			if(!incX && !incY) break;
		}
		//System.out.println(corner1 + ", " + corner2 + " : ");
		//for(int i=0; i<points.size(); i++) {
		//	System.out.println(i + ", " + points.get(i));
		//}
		return points;
	}
	
	public static double getSpanError(MyPoint corner1, MyPoint corner2, Vector<MyPoint> samples) {
		Vector<MyPoint> pixelCentres = getPixelCentresInSpan(corner1, corner2);
		double error = Approximator.getMinDist(pixelCentres.get(0), samples);
		for(int i=0; i<pixelCentres.size(); i++) {
			error = Math.max(error, Approximator.getMinDist(pixelCentres.get(i), samples));
		}
		return error;
	}
	
	private static MyPoint getSlopeFromSamples(int i, Vector<MyPoint> samples) {
		assert(samples.size() >= 3);
		if(i==0) return samples.get(1).minus(samples.get(0));
		else if(i==samples.size()-1) return samples.get(samples.size()-1).minus(samples.get(samples.size()-2));
		else return samples.get(i+1).minus(samples.get(i-1));
	}
	
	public static double getSpanSlopeError(MyPoint corner1, MyPoint corner2, Vector<MyPoint> samples) {
		MyPoint mid = (corner1.add(corner2)).times(0.5);
		int minDistInd = Approximator.getMinDistInd(mid, samples);
		MyPoint slope = getSlopeFromSamples(minDistInd, samples);
		MyPoint spanSlope = corner2.minus(corner1);
		double angle = slope.getAngleTo(spanSlope);
		return Math.abs(angle);
	}
	
	/*
	public static double getJointSpanError(MyPoint corner1, MyPoint corner2, MyPoint corner3, Vector<MyPoint> samples) {
		MyPoint v1 = corner2.minus(corner1); v1.x = Math.round(v1.x); v1.y = Math.round(v1.y);
		MyPoint v2 = corner3.minus(corner2); v2.x = Math.round(v2.x); v2.y = Math.round(v2.y);
		// special case
		boolean slopeOne1 = Math.abs(v1.x) == 1 && Math.abs(v1.y)==1;
		boolean slopeOne2 = Math.abs(v2.x) == 1 && Math.abs(v2.y)==1;
		boolean slopeTwo1 = Math.abs(v1.x) == 2 || Math.abs(v1.y)==2;
		boolean slopeTwo2 = Math.abs(v2.x) == 2 || Math.abs(v2.y)==2;
		if((slopeOne1 && slopeTwo2) || (slopeOne2 && slopeTwo1)) {
			//return getSpanError(corner1, corner3, samples);
			return 0;
		}
		else return getSpanError(corner1, corner2, samples) + getSpanError(corner2, corner3, samples);
	}
	*/
	
	public static int getSpanLength(MyPoint corner1, MyPoint corner2) {
		return getPixelCentresInSpan(corner1, corner2).size();
	}
	
	private static int getNumJaggies(Vector<MyPoint> points, int sortDir) {
		Vector<MyPoint> vectors = new Vector<MyPoint>();
		for(int i=1; i<points.size(); i++) {
			vectors.add(points.get(i).minus(points.get(i-1)));
		}
		int numJaggies = 0;
		for(int i=1; i<vectors.size(); i++) {
			MyPoint v1 = vectors.get(i-1).clone(); //v1.y = -v1.y;
			MyPoint v2 = vectors.get(i).clone(); //v2.y = -v2.y;
			double angleTo = v1.getAngleTo(v2);
			//double angle1 = v1.getAngle();
			//double angle2 = v2.getAngle();
			//System.out.println(vectors.get(i-1) + ", " + vectors.get(i) + ", " + angle1 + ", " + angle2);
			if(angleTo*(double)sortDir < 0) {				
				numJaggies++;
			}
		}
		return numJaggies;
	}
	
	/*
	private static double getKinkCost(Vector<MyPoint> points) {
		Vector<MyPoint> vectors = new Vector<MyPoint>();
		for(int i=1; i<points.size(); i++) {
			vectors.add(points.get(i).minus(points.get(i-1)));
		}
		double totalLength = 0;
		double slopeOneLength = 0;
		
		for(int i=0; i<vectors.size(); i++) {
			MyPoint v = vectors.get(i);
			boolean slopeOne = Math.abs(Math.round(v.x)) == 1 && Math.abs(Math.round(v.y)) == 1;
			if(slopeOne) slopeOneLength += v.length();
			totalLength += v.length();
		}
		
		//double cost = (double) nKinks / (double) nTotal;
		double cost = slopeOneLength / totalLength;
		//System.out.println(cost);
		return cost;
	}
	*/
	
	private static double getMaxSpanOneLength(Vector<MyPoint> points) {
		Vector<MyPoint> vectors = new Vector<MyPoint>();
		for(int i=1; i<points.size(); i++) {
			vectors.add(points.get(i).minus(points.get(i-1)));
		}
		int maxLength = 0;
		int currLength = 0;
		for(int i=0; i<vectors.size(); i++) {
			MyPoint v1 = (i == 0) ? null : vectors.get(i-1);
			MyPoint v2 = vectors.get(i);
			boolean slopeOne = Math.abs(Math.abs(v2.x) - 1) < 0.01 && Math.abs(Math.abs(v2.y) - 1) < 0.01;
			boolean sameAsPrev = (v1==null) ? true : Math.abs(Math.abs(v1.x) - Math.abs(v2.x)) < 0.01 && Math.abs(Math.abs(v1.y) - Math.abs(v2.y)) < 0.01;
			if(slopeOne) {
				if(sameAsPrev) currLength++;
				else currLength = 1;
			}
			else currLength = 0;
			if(currLength > maxLength) maxLength ++;
		}
		if(maxLength <= 3) maxLength = 0;
		return Math.min(maxLength,5);
	}
	
	// fraction of segments that are out of place, in terms of slope order
	private static double getOrderingCostWRTSegment(Vector<MyPoint> points, int k, int sortDir) {
		Vector<MyPoint> vectors = new Vector<MyPoint>();
		for(int i=1; i<points.size(); i++) {
			vectors.add(points.get(i).minus(points.get(i-1)));
		}
		int nTotal = 0;
		int nOutOfOrder = 0;
		MyPoint seg_k = vectors.get(k).clone();
		//System.out.println(k + ", " + seg_k + ", " + sortDir + " : ");
		for(int i=0; i<vectors.size(); i++) {
			if(i==k) continue;
			// vector i = point i-1 to point i
			MyPoint seg_i = vectors.get(i).clone();
			double angleTo = (i < k) ? seg_i.getAngleTo(seg_k) : seg_k.getAngleTo(seg_i);
			//System.out.println(i + ", " + k + ", " + seg_i + ", " + seg_k + ", " + angleTo);
			//System.out.println(i + " // " + seg_i + ", " + angleTo);
			if(sortDir*angleTo < 0) nOutOfOrder++;
			nTotal++;
		}
		double cost = (double) nOutOfOrder / (double) nTotal;
		//System.out.println(nOutOfOrder);
		return cost;
	}
	
	// return true if shifted
	private static boolean tryShiftingPoint(Vector<MyPoint> points, int i, int dx, int dy,
			Vector<MyPoint> samples, int sortDir) {
		MyPoint p1 = points.get(i-1).clone();
		MyPoint p2 = points.get(i).clone();
		MyPoint newP2 = p2.add(new MyPoint(dx,dy));
		MyPoint p3 = points.get(i+1).clone();
		MyPoint v12 = newP2.minus(p1);
		MyPoint v23 = p3.minus(newP2);
		boolean shifted = false;
		
		if(isValidSpan(v12) && isValidSpan(v23)) {
			double distBefore = Math.max(getSpanError(p1, p2, samples), getSpanError(p2, p3, samples));
			double distAfter = Math.max(getSpanError(p1, newP2, samples), getSpanError(newP2, p3, samples));
			double slopeBefore = Math.max(getSpanSlopeError(p1, p2, samples), getSpanSlopeError(p2, p3, samples));
			double slopeAfter = Math.max(getSpanSlopeError(p1, newP2, samples), getSpanSlopeError(newP2, p3, samples));
			//System.out.println(slopeBefore + ", " + slopeAfter);
			//double distBefore = getJointSpanError(p1, p2, p3, samples);
			//double distAfter = getJointSpanError(p1, newP2, p3, samples);
			//int n = points.size();
			//double nBefore = getSpanLength(p1, p2) + getSpanLength(p2, p3);
			//double nAfter = getSpanLength(p1, newP2) + getSpanLength(newP2, p3);
			//distBefore /= nBefore;
			//distAfter /= nAfter;

			//double n = points.size();
			//double jaggiesBefore = getNumJaggies(points, sortDir);
			double orderingBefore = getOrderingCostWRTSegment(points, i-1, sortDir) +
									getOrderingCostWRTSegment(points, i, sortDir);
			double spanOneBefore = getMaxSpanOneLength(points);
			//double slopeBefore = getSlopeCost(points);
			//double kinkBefore = getKinkCost(points);
			
			Vector<MyPoint> copy = copyPolygonalPath(points);
			copy.set(i, newP2);
			//double jaggiesAfter = getNumJaggies(points, sortDir);
			double orderingAfter = 	getOrderingCostWRTSegment(copy, i-1, sortDir) +
									getOrderingCostWRTSegment(copy, i, sortDir);
			double spanOneAfter = getMaxSpanOneLength(copy);
			//double kinkAfter = getKinkCost(copy);
			//jaggiesBefore /= nBefore;
			//jaggiesAfter /= nAfter;
						
			//System.out.println("spanOne : " + spanOneBefore + ", " + spanOneAfter);
			//System.out.println(orderingBefore + ", " + distBefore + ", " + orderingAfter + ", " + distAfter);
			//System.out.println(orderingBefore + ", " + distBefore + ", " + sortDir);
			double costBefore = orderW*orderingBefore + distW*distBefore + spanOneW*spanOneBefore + slopeW*slopeBefore;
			double costAfter = orderW*orderingAfter + distW*distAfter + spanOneW*spanOneAfter + slopeW*slopeAfter;
			//System.out.println(jaggiesBefore + ", " + distBefore);
			//System.out.println(jaggiesAfter + "// " + distAfter);
			/*
			double costBefore = distBefore;
			double costAfter = distAfter;
			*/
			//System.out.println(costBefore + ", " + costAfter);
			if(costBefore -0.01 < costAfter) {}
			else {
				//System.out.println(costBefore + ", " + costAfter);
				shifted = true;
				points.set(i, newP2);
				//System.out.println("SHIFT");
			}
		}
		return shifted;
	}
	
	private static boolean trySplittingPoint(Vector<MyPoint> points, int i, int dx, int dy,
			Vector<MyPoint> samples, int sortDir) {
		MyPoint p1 = points.get(i-1).clone();
		MyPoint p2 = points.get(i).clone();
		MyPoint p2a = p2.add(new MyPoint(dx,0));
		MyPoint p2b = p2.add(new MyPoint(0,dy));
		MyPoint p3 = points.get(i+1).clone();
		MyPoint v12a = p2a.minus(p1);
		MyPoint v2a2b = p2b.minus(p2a);
		MyPoint v2b3 = p3.minus(p2b);
		boolean shifted = false;
		//System.out.println(v12a + ", " + v2a2b + "," + v2b3);
		if(isValidSpan(v12a) && isValidSpan(v2a2b) && isValidSpan(v2b3) &&
			v12a.x*v2a2b.x > 0 && v12a.y*v2a2b.y > 0 && v2b3.x*v2a2b.x > 0 && v2b3.y*v2a2b.y > 0) {
			double distBefore = Math.max(getSpanError(p1, p2, samples), getSpanError(p2, p3, samples));
			double distAfter = Math.max(
									getSpanError(p1, p2a, samples),
									Math.max(
											getSpanError(p2a, p2b, samples),
											getSpanError(p2b, p3, samples)));
			double slopeBefore = Math.max(getSpanSlopeError(p1, p2, samples), getSpanSlopeError(p2, p3, samples));
			double slopeAfter = Math.max(
									getSpanSlopeError(p1, p2a, samples),
									Math.max(
											getSpanSlopeError(p2a, p2b, samples),
											getSpanSlopeError(p2b, p3, samples)));
			//System.out.println(getSpanError(p1, p2, samples) + ", " + getSpanError(p2, p3, samples));
			//System.out.println(getSpanError(p1, p2a, samples) + ", " + getSpanError(p2a, p2b, samples) + ", " + getSpanError(p2b, p3, samples));
			//System.out.println(p1 + ", " + p2a + ", " + getSpanError(p1, p2a, samples));
			//System.out.println(p2.minus(p1) + ", " + p3.minus(p2));
			//System.out.println(v12a + ", " + v2a2b + ", " + v2b3);
			double orderingBefore = getOrderingCostWRTSegment(points, i-1, sortDir) +
									getOrderingCostWRTSegment(points, i, sortDir);
			double spanOneBefore = getMaxSpanOneLength(points);
			Vector<MyPoint> copy = copyPolygonalPath(points);
			copy.set(i, p2b);
			copy.add(i, p2a);
			double orderingAfter =	getOrderingCostWRTSegment(copy, i-1, sortDir) +
									getOrderingCostWRTSegment(copy, i, sortDir) +
									getOrderingCostWRTSegment(copy, i+1, sortDir);
			double spanOneAfter = getMaxSpanOneLength(copy);
			//orderingAfter *= 2.0/3.0;
			//System.out.println(points.get(i-1) + ", " + points.get(i) + ", " + points.get(i+1) + ", " + copy.get(i-1) + ", " + copy.get(i) + ", " + copy.get(i+1) + ", " + copy.get(i+2));
			/*
			System.out.println(
					getOrderingCostWRTSegment(points, i-1, sortDir) + ", " +
					getOrderingCostWRTSegment(points, i, sortDir) + ", " + 
					getOrderingCostWRTSegment(copy, i-1, sortDir) + ", " +
					getOrderingCostWRTSegment(copy, i, sortDir) + ", " + 
					getOrderingCostWRTSegment(copy, i+1, sortDir));
			*/
			//System.out.println("dist: " + distBefore + ", " + distAfter);
			//System.out.println("order: " + orderingBefore + ", " + orderingAfter);
			//jaggiesBefore /= nBefore;
			//jaggiesAfter /= nAfter;
			//System.out.println(jaggiesBefore + ", " + jaggiesAfter + ", " + sortDir);
			double costBefore = orderW*orderingBefore + distW*distBefore + spanOneW*spanOneBefore + slopeW*slopeBefore;
			double costAfter = orderW*orderingAfter + distW*distAfter + spanOneW*spanOneAfter + slopeW*slopeAfter;
			//System.out.println("cost: "+ costBefore + ", " + costAfter);
	
			if(costBefore-0.01 < costAfter) {}
			else {
				//System.out.println("dist: " + distBefore + ", " + distAfter);
				//System.out.println("order: " + orderingBefore + ", " + orderingAfter);
				//System.out.println(p2.minus(p1) + ", " + p3.minus(p2));
				//System.out.println(v12a + ", " + v2a2b + ", " + v2b3);
				points.set(i, p2b.clone());
				points.add(i, p2a.clone());
				shifted = true;
				//System.out.println("SPLIT");
				//System.out.println(points.get(i) + ", " + v2a2b + "," + v2b3);
			}
		}
		return shifted;
	}
	
	// merge i and i+1
	private static boolean tryMergingPoints(Vector<MyPoint> points, int i, int dx, int dy,
			Vector<MyPoint> samples, int sortDir) {
		if(i-1>=0 && i+2>=points.size()) return false;
		MyPoint p1 = points.get(i-1).clone();
		MyPoint p2 = points.get(i).clone();
		MyPoint p3 = points.get(i+1).clone();
		MyPoint p4 = points.get(i+2);
		if(Math.abs(p2.x-p3.x) > 0.9 || Math.abs(p2.y-p3.y) > 0.9) return false;
		MyPoint p23 = p2.add(new MyPoint(dx, dy));
		MyPoint v1 = p23.minus(p1);
		MyPoint v2 = p4.minus(p23);
		boolean valid1 = isValidSpan(v1);
		boolean valid2 = isValidSpan(v2);
		boolean shifted = false;
		//System.out.println(v12a + ", " + v2a2b + "," + v2b3);
		if(valid1 && valid2 && v1.x*v2.x > 0 && v1.y*v2.y > 0) {
			double distBefore = Math.max(
					getSpanError(p1, p2, samples),
					Math.max(
							getSpanError(p2, p3, samples),
							getSpanError(p3, p4, samples)));
			double distAfter = Math.max(getSpanError(p1, p23, samples), getSpanError(p23, p4, samples));
			double slopeBefore = Math.max(
					getSpanSlopeError(p1, p2, samples),
					Math.max(
							getSpanSlopeError(p2, p3, samples),
							getSpanSlopeError(p3, p4, samples)));
			double slopeAfter = Math.max(getSpanSlopeError(p1, p23, samples), getSpanSlopeError(p23, p4, samples));
			//System.out.println(p2.minus(p1) + ", " + p3.minus(p2));
			//System.out.println(v12a + ", " + v2a2b + ", " + v2b3);
			double orderingBefore =	getOrderingCostWRTSegment(points, i-1, sortDir) +
									getOrderingCostWRTSegment(points, i, sortDir) +
									getOrderingCostWRTSegment(points, i+1, sortDir);
			double spanOneBefore = getMaxSpanOneLength(points);
			Vector<MyPoint> copy = copyPolygonalPath(points);
			copy.remove(i+1);
			copy.set(i, p23);
			double orderingAfter = 	getOrderingCostWRTSegment(copy, i-1, sortDir) +
									getOrderingCostWRTSegment(points, i, sortDir);
			double spanOneAfter = getMaxSpanOneLength(copy);
			//System.out.println(points.get(i-1) + ", " + points.get(i) + ", " + copy.get(i-1) + ", " + copy.get(i) + ", " + copy.get(i+1));
			/*
			System.out.println(
					getOrderingCostWRTSegment(points, i-1, sortDir) + ", " +
					getOrderingCostWRTSegment(points, i, sortDir) + ", " + 
					getOrderingCostWRTSegment(copy, i-1, sortDir) + ", " +
					getOrderingCostWRTSegment(copy, i, sortDir) + ", " + 
					getOrderingCostWRTSegment(copy, i+1, sortDir));
			*/
			//System.out.println("dist: " + distBefore + ", " + distAfter);
			//System.out.println("order: " + orderingBefore + ", " + orderingAfter);
			//jaggiesBefore /= nBefore;
			//jaggiesAfter /= nAfter;
			//System.out.println(jaggiesBefore + ", " + jaggiesAfter + ", " + sortDir);
			double costBefore = orderW*orderingBefore + distW*distBefore + spanOneW*spanOneBefore + slopeW*slopeBefore;
			double costAfter = orderW*orderingAfter + distW*distAfter + spanOneW*spanOneAfter + slopeW*slopeAfter;
			if(costBefore-0.01 < costAfter) {}
			else {
				points.remove(i+1);
				points.set(i, p23.clone());
				shifted = true;
				//System.out.println("MERGE");
				//System.out.println(points.get(i) + ", " + v2a2b + "," + v2b3);
			}
		}
		return shifted;
	}
	
	public static Vector<MyPoint> sortSlopeSpans(int sortDir, Vector<MyPoint> points, boolean[] sorted, Path path) {
		Vector<MyPoint> newPoints = copyPolygonalPath(points); 
		if(sortDir==0) {
			sorted[0] = false;
			return newPoints;
		}
		else {
			Vector<MyPoint> samples = Approximator.getSamplePoints(path, 100);
			//System.out.println(path);
			int itr = 0;
			int maxItr = 30;
			boolean converged = false;
			int moves = 0;
			while(true) {
				if(converged || itr >= maxItr) {
					if(itr > maxItr) System.out.println(itr);
					break;
				}
				for(int i=1; i<newPoints.size()-1; i++) {
					//System.out.println("before " + itr);
					boolean shiftedW = tryShiftingPoint(newPoints, i, -1, 0, samples, sortDir);
					boolean shiftedE = tryShiftingPoint(newPoints, i, +1, 0, samples, sortDir);
					boolean shiftedN = tryShiftingPoint(newPoints, i, 0, -1, samples, sortDir);
					boolean shiftedS = tryShiftingPoint(newPoints, i, 0, +1, samples, sortDir);
					boolean converged1 = !shiftedW && !shiftedE && !shiftedN && !shiftedS;
					//System.out.println("after " + itr);
					
					boolean shiftedNW = trySplittingPoint(newPoints, i, -1, -1, samples, sortDir);
					//if(shiftedNW) i++;
					boolean shiftedSW = trySplittingPoint(newPoints, i, -1, +1, samples, sortDir);
					//if(shiftedSW) i++;
					boolean shiftedNE = trySplittingPoint(newPoints, i, +1, -1, samples, sortDir);
					//if(shiftedNE) i++;				
					boolean shiftedSE = trySplittingPoint(newPoints, i, +1, +1, samples, sortDir);
					//if(shiftedSE) i++;
					boolean converged2 = !shiftedNW && !shiftedSW && !shiftedNE && !shiftedSE;

					boolean mergeW = tryMergingPoints(newPoints, i, -1, 0, samples, sortDir);
					boolean mergeE = tryMergingPoints(newPoints, i, +1, 0, samples, sortDir);
					boolean mergeN = tryMergingPoints(newPoints, i, 0, -1, samples, sortDir);
					boolean mergeS = tryMergingPoints(newPoints, i, 0, +1, samples, sortDir);
					boolean converged3 = !mergeW && !mergeE && !mergeN && !mergeS;
					
					converged = converged1 && converged2 && converged3;
					moves += 	(shiftedW ? 1 : 0) + (shiftedE ? 1 : 0) + (shiftedN ? 1 : 0) + (shiftedS ? 1 : 0) + 
								(shiftedNW ? 1 : 0) + (shiftedSW ? 1 : 0) + (shiftedNE ? 1 : 0) + (shiftedSE ? 1 : 0) + 
								(mergeW ? 1 : 0) + (mergeE ? 1 : 0) + (mergeN ? 1 : 0) + (mergeS ? 1 : 0);
				}
				itr ++ ;
			}
			//if(moves > 8) System.out.println("moves = " + moves);
		}
		return newPoints;
	}
	
	public static String vectorsToString(Vector<MyPoint> vectors) {
		String s = "";
		for(int i=0; i<vectors.size(); i++) {
			s += vectors.get(i).toString() + ", ";
		}
		return s;
	}
}
