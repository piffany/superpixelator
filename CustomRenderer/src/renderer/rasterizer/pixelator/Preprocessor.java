package renderer.rasterizer.pixelator;

import renderer.geometry.MyCurve;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.util.Vector;


public class Preprocessor {

	public static Path preprocess(Path path, Vector<MyPoint> pixels, boolean[] pixelated) {
		pixels.clear();
		Path cleanPath1 = removeDegenerateSegments(path);
		if(cleanPath1.isEmpty()) {
			pixelated[0] = true;
			return cleanPath1;
		}
		Path cleanPath2 = mergeStraightLines(path);
		/*
		pixelated[0] = boundingBoxSizeCheck(cleanPath1, pixels);
		if(pixelated[0]) {
			return cleanPath1;
		}
		*/

		return cleanPath2;
	}
	
	private static Path removeDegenerateSegments(Path path) {
		Path newPath = new Path();
		// get rid of degenerate curves
		for(int i=0; i<path.getNumCurves(); i++) {
			MyCurve curve = path.getCurve(i);
			if(!curve.isDegenerate()) newPath.addCurve(curve);
		}
		return newPath;
	}
	
	private static Path mergeStraightLines(Path path) {
		Path newPath = path.clone();
		for(int i=newPath.getNumSegments()-2; i>=1; i--) {
			MyCurve curveBefore = newPath.getCurveBeforeSegmentIndex(i);
			MyCurve curveAfter = newPath.getCurveAfterSegmentIndex(i);
			MyPoint vec1 = curveBefore.getP3().minus(curveBefore.getP0());
			MyPoint vec2 = curveAfter.getP3().minus(curveAfter.getP0());
			boolean betweenLines = false;
			if(vec2.isOrigin()) betweenLines = true;
			else {
				double angle = vec1.getAngleTo(vec2);
				betweenLines = curveBefore.isStraight() && curveAfter.isStraight()
								 	   && Math.abs(angle) < 0.01;
			}
			if(betweenLines) {
				newPath.removeSegment(i);
			}
		}
		return newPath;
	}
}
