package renderer.rasterizer.pixelator;

import renderer.geometry.MyCurve;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.util.Collections;
import java.util.Vector;


public class Sorter {

	public static int getSortDirection(MyCurve curve) {
		MyPoint P0 = curve.getP0(), P1 = curve.getP1(), P2 = curve.getP2(), P3 = curve.getP3();
		double e1 = getSignedLineError(P1, P0, P3);
		double e2 = getSignedLineError(P2, P0, P3);
		boolean sameDir = e1*e2 > 0;
		if(!sameDir) return 0; // don't sort
		if(e1 < 0) return -1; // increasing
		else return 1; // decreasing
	}
	
	public static double getSignedLineError(MyPoint p, MyPoint a, MyPoint b) {
		return getSignedLineError(p.x, p.y, a.x, a.y, b.x, b.y);
	}

	private static double getSignedLineError(double x, double y, double x0, double y0, double x1, double y1) {
		double error = (x1-x0)*(y-y0) - (x-x0)*(y1-y0);
		return error;
	}

	public static int getSortDirection(Path path) {
		assert(path.getNumCurves()>0);
		MyCurve firstCurve = path.getCurve(0);
		MyCurve lastCurve = path.getCurve(path.getNumCurves()-1);
		if(java.lang.Double.isNaN(firstCurve.getP0().getX()) || java.lang.Double.isNaN(firstCurve.getP0().getY())) {
			System.out.println("getSortDirection");
			System.out.println(firstCurve.getP0());
		}
		MyCurve curve = new MyCurve(firstCurve.getP0(), firstCurve.getP1(),
				lastCurve.getP2(), lastCurve.getP3());
		return getSortDirection(curve);
	}
	
	public static Vector<MyPoint> sortPathPoints(Path path, Vector<MyPoint> pixelPath, boolean[] sorted) {
		int sortDir = Sorter.getSortDirection(path);
		Vector<MyPoint> sortedPixelPath = sortSlopeSpans(sortDir, pixelPath, sorted);
		return sortedPixelPath;
	}

	public static Vector<MyPoint> sortSlopeSpans(int sortDir, Vector<MyPoint> points, boolean[] sorted) {
		// polygonal approximation
		Vector<MyPoint> vectors = new Vector<MyPoint>();
		for(int i=1; i<points.size(); i++) {
			vectors.add(points.get(i).minus(points.get(i-1)));
		}
		switch(sortDir) {
		case 0:
			sorted[0] = false;
			return points;
		case -1:
			Collections.sort(vectors, MyPoint.getSlopeComparator(true));
			sorted[0] = true;
			break;
		case 1:
			Collections.sort(vectors, MyPoint.getSlopeComparator(false));
			sorted[0] = true;
			break;
		}
		Vector<MyPoint> sortedPoints = new Vector<MyPoint>();
		sortedPoints.add(points.get(0));
		for(int i=0; i<vectors.size(); i++) {
			MyPoint prev = sortedPoints.get(sortedPoints.size()-1);
			sortedPoints.add(prev.add(vectors.get(i)));
		}
		// check against tolerance
		double tol = 1;
		//Vector<MyPoint> samples = getSamplePoints(path, 100);
		for(int i=0; i<sortedPoints.size(); i++) {
			MyPoint p = sortedPoints.get(i);
			double error = Approximator.getMinDist(p, points);
			if(error > tol) {
				//System.out.println("don't sort");
				return points;
			}
		}
		return sortedPoints;
	}
	
	public static String vectorsToString(Vector<MyPoint> vectors) {
		String s = "";
		for(int i=0; i<vectors.size(); i++) {
			s += vectors.get(i).toString() + ", ";
		}
		return s;
	}
}
