package renderer.rasterizer.pixelator;

import renderer.geometry.MyCurve;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.util.Vector;

import renderer.util.MyMath;
import renderer.util.MyVector;

public class Splitter {

	public static Vector<Path> splitByMonotonicity(Path path) {
		return splitByMonotonicity(path, true, false, false);
	}
	
	public static Vector<Path> splitByMonotonicity(Path path, boolean minMax, boolean inflection, boolean straightLineEnds) {
		Vector<FlaggedSegment> unsplitPathSegments = getSegmentsSplitAtCriticalPoints(path, minMax, inflection, straightLineEnds);
		//System.out.println("before: " + unsplitPathSegments.size());
		Vector<Path> splitPaths = mergeFlaggedSegments(unsplitPathSegments);
		//System.out.println("after: " + splitPaths.size());
		return splitPaths;
	}
	
	// locate the min/max points and add them as control points
	private static Vector<FlaggedSegment> getSegmentsSplitAtCriticalPoints(Path path, boolean minMax, boolean inflection, boolean straightLineEnds) {
		Vector<FlaggedSegment> newPathSegments = new Vector<FlaggedSegment>();
		for(int i=0; i<path.getNumCurves(); i++) {
			// flags indicating whether a segment is at a split point
			Vector<FlaggedSegment> splitSegments = splitCurveByMonotonicity(path.getCurve(i), minMax, inflection);
			int n = splitSegments.size();
			FlaggedSegment firstSeg = splitSegments.get(0);
			FlaggedSegment lastSeg = splitSegments.get(n-1);
			//boolean firstFlag = getSegmentFlag(path, i);
			//System.out.println(path.getCurve(i).isStraight() + ", " + path.getCurve(i+1).isStraight());
			boolean firstFlag = getSegmentFlag(path, i);// || i==0 || MyMath.xor(path.getCurve(i-1).isStraight(), path.getCurve(i).isStraight());
			//System.out.println(i + " : " + firstFlag + ", " + firstSeg);
			boolean lastFlag = getSegmentFlag(path, i+1);
			if(i==path.getNumCurves()-1 && !splitSegments.isEmpty() && path.isClosed()) {
				lastFlag = splitSegments.get(0).getFlag();
			}
			if(straightLineEnds) {
				if(path.getCurve(i).isStraight() || (i>0 && path.getCurve(i-1).isStraight())) {
					firstFlag = true;
				}
				if(path.getCurve(i).isStraight() || (i<n-1 && path.getCurve(i+1).isStraight())) {
					lastFlag = true;
				} 
			}
			firstSeg.setFlag(firstFlag);
			lastSeg.setFlag(lastFlag);
			if(!newPathSegments.isEmpty()) {
				FlaggedSegment lastSegAdded = newPathSegments.get(newPathSegments.size()-1);
				firstSeg.setHandleIn(lastSegAdded.getHandleIn());
				
				if(straightLineEnds && path.getCurve(i).isStraight()) {
					firstSeg.setFlag(true);
					lastSegAdded.setFlag(true);
				}
				
				newPathSegments.remove(newPathSegments.size()-1);
			}
			newPathSegments.addAll(splitSegments);
			/*
			if(path.getCurve(i).isStraight() & splitSegments.size()>=2) {
				System.out.println(firstSeg + " ... " + lastSeg);
			}
			*/
		}
		return newPathSegments;
	}

	// split into different paths at the min/max points (now control points)
	private static Vector<Path> mergeFlaggedSegments(Vector<FlaggedSegment> segments) {
		Vector<Path> newPaths = new Vector<Path>();
		Path newPath = new Path();
		for(int i=0; i<segments.size(); i++) {
			FlaggedSegment segment = segments.get(i);
			//System.out.println("flag " + i + " : " + segment);
			/*
			if(1+1==2) {
				newPath.addSegment(segment.clone());
				newPaths.add(newPath.clone());
				newPath.clear();
				newPath.addSegment(segment.clone());
				continue;
			}*/
			if(i==0) {
				newPath.clear();
				newPath.addSegment(segment.clone());
			}
			else if(segment.getFlag()) {
				newPath.addSegment(segment.clone());
				newPaths.add(newPath.clone());
				newPath.clear();
				newPath.addSegment(segment.clone());
			}
			else if(i==segments.size()-1) {
				newPath.addSegment(segment.clone());
				newPaths.add(newPath.clone());
			}
			else {
				newPath.addSegment(segment.clone());
			}
		}
		MyPoint p1 = segments.get(0).getPoint();
		MyPoint p2 = segments.get(segments.size()-1).getPoint();
		boolean closed = Math.abs(p1.x-p2.x)<0.001 &&  Math.abs(p1.y-p2.y)<0.001;
		if(closed && segments.size()>=2) {
			FlaggedSegment segment0 = segments.get(0);
			if(!segment0.getFlag()) {
				Path firstPath = newPaths.get(0).clone();
				Path lastPath = newPaths.get(newPaths.size()-1);
				for(int i=0; i<firstPath.getNumCurves(); i++) {
					lastPath.addCurve(firstPath.getCurve(i));
				}
				newPaths.remove(0);
			}
		}
		return newPaths;
	}
	
	private static boolean getSegmentFlag(Path path, int i) {
		boolean flag = false;
		// is segment i nondifferentiable
		if(path.isNondifferentiableAtSegmentIndex(i)) {
			flag = true;
		}
		// is segment i a local minimum
		else {
			MyCurve curveBefore = path.getCurveBeforeSegmentIndex(i);
			MyCurve curveAfter = path.getCurveAfterSegmentIndex(i);
			if(curveBefore.isMaxAt1('x') && curveAfter.isMaxAt0('x')) flag = true;
			else if(curveBefore.isMinAt1('x') && curveAfter.isMinAt0('x')) flag = true;
			else if(curveBefore.isMaxAt1('y') && curveAfter.isMaxAt0('y')) flag = true;
			else if(curveBefore.isMinAt1('y') && curveAfter.isMinAt0('y')) flag = true;
			else flag = false;
		}
		return flag;
	}
	
	// returns a vector of segments describing the split curve
	// flag = true means a point is a critical point
	// endpoints have flag = false for now, unless they are critical points
	// the status of endpoints can change later depending on the original
	// curve's position in the path
	private static Vector<FlaggedSegment> splitCurveByMonotonicity(MyCurve curve0, boolean withMinMax, boolean withInflectionPts) {
		Vector<java.lang.Double> splitTs = new Vector<java.lang.Double>();
		if(withMinMax) {
			Vector<Double> xCritPts = curve0.tValuesAtLocalMinMax('x');
			Vector<Double> yCritPts = curve0.tValuesAtLocalMinMax('y');
			splitTs.addAll(xCritPts);
			splitTs.addAll(yCritPts);
		}
		if(withInflectionPts) {
			Vector<Double> inflectPts = curve0.tValuesAtInflectionPoints();
			splitTs.addAll(inflectPts);
		}
		MyVector.sortNoDuplicates(splitTs);
		// remove 0's and 1's. they will be tested later
		if(!splitTs.isEmpty()) 	if(splitTs.get(0)==0) splitTs.remove(0);
		if(!splitTs.isEmpty()) if(splitTs.get(splitTs.size()-1)==1) splitTs.remove(splitTs.size()-1);
		Vector<java.lang.Double> ts = new Vector<java.lang.Double>();
		ts.addAll(splitTs); ts.add(0.0); ts.add(1.0);
		MyVector.sortNoDuplicates(ts);

		Vector<MyCurve> splitCurves = new Vector<MyCurve>();
		Vector<Boolean> flags = new Vector<Boolean>();
		for(int j=0; j<ts.size()-1; j++) {
			MyCurve curve = curve0.getSubCurve(ts.get(j), ts.get(j+1));
			splitCurves.add(curve);
			if(j==0) {
				flags.add(!splitTs.isEmpty() && splitTs.get(0)==0);
			}
			else {
				flags.add(!splitTs.isEmpty());
				if(j==ts.size()-2) {
					flags.add(!splitTs.isEmpty() && splitTs.get(splitTs.size()-1)==1);
				}
			}
		}
		Vector<FlaggedSegment> splitSegments = new Vector<FlaggedSegment>();
		for(int k=0; k<splitCurves.size(); k++) {
			MyCurve curve = splitCurves.get(k);
			FlaggedSegment seg1, seg2;
			if(k==0) {
				MyPoint point = curve.getP0(), handleOut = curve.getHandle1();
				seg1 = new FlaggedSegment(point, handleOut.times(-1), handleOut);
				seg1.setFlag(flags.get(k));
				if(curve0.isStraight()) seg1.setFlag(true);
			}
			else {
				MyCurve prevCurve = splitCurves.get(k-1);
				MyPoint point = curve.getP0(), handleIn = prevCurve.getHandle2(), handleOut = curve.getHandle1();
				seg1 = new FlaggedSegment(point, handleIn, handleOut);
				seg1.setFlag(flags.get(k));
				if(prevCurve.isStraight() || curve.isStraight()) seg1.setFlag(true);
			}
			splitSegments.add(seg1);
			if(k==splitCurves.size()-1) {
				MyPoint point = curve.getP3(), handleIn = curve.getHandle2();
				seg2 = new FlaggedSegment(point, handleIn, handleIn.times(-1));
				if(splitTs.isEmpty()) seg2.setFlag(false);
				else if(splitTs.get(splitTs.size()-1) < 1) seg2.setFlag(false);
				else seg2.setFlag(true);
				if(curve0.isStraight()) seg2.setFlag(true);
				splitSegments.add(seg2);
			}
		}
		
		return splitSegments;
	}
}
