package renderer.rasterizer.pixelator;

import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.util.Vector;

import renderer.display.GraphicsState;
import renderer.display.MyGraphics2D;
import renderer.geometry.Pixel;
import renderer.rasterizer.DefaultRasterizer;
import renderer.util.Transform;
import colours.Colour;


public class PixelatorHelper {

	public static Vector<Pixel> getStrokePixels(MyGraphics2D g, Path path) {
		Vector<Pixel> pixels = getPixelation(path, true, true);
		return pixels;
	}
	
	public static Vector<Pixel> getFillPixels(MyGraphics2D g, Path path) {
		RenderedImage image = (new Pixelator()).fillPath(g, path);
		Vector<Pixel> pixels = getNonTransparentPixels((BufferedImage) image);
		return pixels;
	}
	
	public static Vector<Pixel> getNonTransparentPixels(BufferedImage image) {
		Vector<Pixel> pixels = new Vector<Pixel>();
		for(int i=0; i<image.getWidth(); i++) {
			for(int j=0; j<image.getHeight(); j++) {
				Color c = Colour.rgbInt2Color(image.getRGB(i, j));
				//if(c.getRGB() != Color.black.getRGB())	{
				if(c.getAlpha() > 0) {
					Pixel pixel = new Pixel(i, j, c.getAlpha());
					pixels.add(pixel);
				}
			}
		}
		return pixels;
	}

	public static Vector<Pixel> getPixelation(Path path, boolean shifted, boolean sorted) {
		Vector<MyPoint> pixelatedPath = getPixelatedPath(path, shifted, sorted);
		if(pixelatedPath.isEmpty()) return new Vector<Pixel>();
		Vector<MyPoint> pixelPos = PixelConverter.convertPathToPixels(pixelatedPath);
		Vector<Pixel> pixels = Pixel.pointsToPixels(pixelPos);
		return pixels;
	}

	public static Vector<MyPoint> getPixelatedPath(Path path, boolean shifted, boolean sorted) {
		Vector<MyPoint> pixels0 = new Vector<MyPoint>();
		boolean[] pixelated = {false};
		Path step0 = Preprocessor.preprocess(path, pixels0, pixelated);
		if(pixelated[0]) {
			return pixels0;
		}
		if(step0.isEmpty()) return new Vector<MyPoint>();
		//MyPoint centre = new MyPoint();
		//double[] BBcoeffsWithoutAA = {10000, 40, 1, 50, 100};
		//step0 = BoundingBoxShifter.snapShapeByOptimalBoundingBox(step0, centre, BBcoeffsWithoutAA);
		//MyPoint centre = step0.getBoundingBoxCentre();

		Vector<Path> step1 = Splitter.splitByMonotonicity(path);
		Vector<Path> step2 = Shifter.shiftEndpointsToPixelCentres(step1, shifted, true);
		Vector<MyPoint> step3 = Polygonalizer.getPixelatedPath(step2, sorted);
		return step3;
	}
	

	
/*	public static Vector<Path> getPixelationStep(Path path, boolean shifted, boolean sorted, int step) {
		Path pathCopy = path.clone();
		//MyPoint centre = step0.getBoundingBoxCentre();
		Vector<Path> stepResult = getPixelationStepWRTCentre(pathCopy, shifted, sorted, step);
		return stepResult;
	}*/
	
	public static Vector<Path> getPixelationStep(Path path, boolean shifted, boolean sorted, int step) {
		assert(step==0 || step==1 || step==2);
		Vector<Path> step0 = new Vector<Path>();
		Vector<MyPoint> pixels0 = new Vector<MyPoint>();
		boolean[] pixelated = {false};
		step0.add(Preprocessor.preprocess(path, pixels0, pixelated));
		Vector<Path> step1 = Splitter.splitByMonotonicity(step0.get(0));
		Vector<Path> step2 = Shifter.shiftEndpointsToPixelCentres(step1, shifted, true);
		if(step==0) {
			return step0;
		}
		if(step==1) return step1;
		else if(step==2) return step2;
		else return null;
	}
	
	public static RenderedImage drawPaths(MyGraphics2D g, Vector<Path> paths) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		MyGraphics2D mg2 = new MyGraphics2D(g2, g.getWidth(), g.getHeight());
		g2.setTransform(Transform.getIdentity());
		for(int i=0; i<paths.size(); i++) {
			g2.drawRenderedImage((new Pixelator()).drawPath(mg2, paths.get(i)), Transform.getIdentity());
		}
		return (RenderedImage) image;
	}

	public static RenderedImage fillPaths(MyGraphics2D g, Vector<Path> paths, boolean removeOutline) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		MyGraphics2D mg2 = new MyGraphics2D(g2, g.getWidth(), g.getHeight());
		g2.setTransform(Transform.getIdentity());
		for(int i=0; i<paths.size(); i++) {
			g2.drawRenderedImage((new Pixelator()).fillPath(mg2, paths.get(i), removeOutline), Transform.getIdentity());
		}
		return (RenderedImage) image;
	}

	public static Vector<Path> getPathsFromGlyphVector(GlyphVector gv, float x, float y) {
		Rectangle2D bounds = gv.getVisualBounds();
		Shape shape = gv.getOutline();
		Vector<Path> paths = Path.getPaths(shape);
		double offsetX = bounds.getWidth();
		double offsetY = bounds.getHeight();
		for(int i=0; i<paths.size(); i++) {
			paths.get(i).transform(AffineTransform.getTranslateInstance(x+offsetX, y+offsetY));
		}
		return paths;
	}
	
	public static Vector<Pixel> pixelateThickPath(MyGraphics2D g, Path path, boolean shifted, boolean sorted) {
		double strokeWidth = getStrokeWidth(g);
		Path transformedPath = path.getTransformedCopy(g.getTransform());
		if(strokeWidth <= 1) {
			Vector<Pixel> pixels = getPixelation(transformedPath, shifted, sorted);
			return pixels;
		}
		else {
			Vector<Path> outline = getStrokeOutline(g, transformedPath);
			if(outline.isEmpty()) return new Vector<Pixel>();
			outline.set(0, BoundingBoxShifter.snapShapeByBoundingBox(outline.get(0)));
			Vector<Pixel> pixels = getPixelation(outline.get(0), shifted, sorted);
			Path fillArea = new Path();
			fillArea.setWindingRule(PathIterator.WIND_NON_ZERO);
			for(int i=0; i<pixels.size(); i++) {
				MyPoint p = pixels.get(i);
				fillArea.addPoint(p);
			}
			DefaultRasterizer rasterizer = new DefaultRasterizer();
			MyGraphics2D g2 = new MyGraphics2D(g);
			GraphicsState gs = g2.getState();
			g2.setTransform(Transform.getIdentity());
			RenderedImage image = rasterizer.fillPath(g2, fillArea);
			g2.setState(gs);
			Vector<MyPoint> strokePixelPos = getNontransparentPixels((BufferedImage)image);
			Vector<Pixel> strokePixels = Pixel.pointsToPixels(strokePixelPos);
			return strokePixels;
		}
	}

	private static Vector<MyPoint> getNontransparentPixels(BufferedImage image) {
		Vector<MyPoint> pixels = new Vector<MyPoint>();
		for(int x=0; x<image.getWidth(); x++) {
			for(int y=0; y<image.getHeight(); y++) {
				int alpha = Colour.rgbInt2Color(image.getRGB(x, y)).getAlpha();
				if(alpha > 0) pixels.add(new MyPoint(x,y));
			}
		}
		return pixels;
	}

	public static Vector<Path> getStrokeOutline(MyGraphics2D g, Path path) {
		BasicStroke stroke = (BasicStroke) g.getStroke();
		path.setWindingRule(PathIterator.WIND_NON_ZERO);
		Shape outlineShape = stroke.createStrokedShape(path.getShapeToOutline());
		Vector<Path> outlinePaths = Path.getPaths(outlineShape);
		return outlinePaths;
	}

	private static double getStrokeWidth(MyGraphics2D g) {
		BasicStroke stroke = (BasicStroke) g.getStroke();
		double width = stroke.getLineWidth();
		return width;
	}
	
	public static void clearPixels(BufferedImage image, Vector<Pixel> pixels) {
		int transpRGB = Colour.TRANSPARENT.getRGB();
		for(int i=0; i<pixels.size(); i++) {
			Pixel p = pixels.get(i);
			if(p.x>=0 && p.x<image.getWidth(null) && p.y>=0 && p.y<image.getHeight(null)) {
				image.setRGB((int)p.x, (int)p.y, transpRGB);
			}
		}
	}

	// output params = [shifted, sorted]
	public static Path getRectanglePath(MyGraphics2D g, int x, int y, int width, int height, boolean[] outputParams) {
		AffineTransform at = g.getTransform();
		Path path0 = new Path.Rectangle(x, y, width, height);
		path0.transform(at);
		outputParams[0] = true; //shift;
		outputParams[1] = true;
		return path0;
	}

	// output params = [shifted, sorted]
	public static Path getRoundRectanglePath(MyGraphics2D g, int x, int y,
			int width, int height, int arcWidth, int arcHeight, boolean[] outputParams) {
		AffineTransform at = g.getTransform();
		Path path0 = new Path.RoundRectangle(x, y, width, height, arcWidth, arcHeight);
		path0.transform(at);
		outputParams[0] = true; //shift;
		outputParams[1] = true;
		return path0;
	}

	// output params = [shifted, sorted]
	public static Path getOvalPath(MyGraphics2D g, int x, int y, int width, int height, boolean[] outputParams) {
		AffineTransform at = g.getTransform();
		Path path0 = new Path.Oval(x, y, width, height);
		path0.transform(at);
		outputParams[0] = true; //shift;
		outputParams[1] = true;
		return path0;
	}

}
