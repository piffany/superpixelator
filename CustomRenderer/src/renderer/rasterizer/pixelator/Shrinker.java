package renderer.rasterizer.pixelator;

import java.util.Vector;

import renderer.geometry.MyPoint;

public class Shrinker {

	public static Vector<MyPoint> shrinkPathNearZero(Vector<MyPoint> lines) {
		Vector<MyPoint> newLines = new Vector<MyPoint>();
		for(int i=0; i<lines.size(); i++) {
			MyPoint p = lines.get(i).clone();
			if(p.x > 0.01) p.x -=0.5;
			else if(p.x < -0.01) p.x += 0.5;
			if(p.y > 0.01) p.y -=0.5;
			else if(p.y < -0.01) p.y += 0.5;
			newLines.add(p);
		}
		return newLines;
	}

}
