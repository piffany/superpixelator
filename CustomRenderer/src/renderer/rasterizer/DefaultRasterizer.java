package renderer.rasterizer;
import renderer.geometry.Path;
import renderer.rasterizer.superpixelator.SuperpixelatorHelper;
import renderer.util.Transform;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;

import renderer.display.MyGraphics2D;
import colours.Colour;



public class DefaultRasterizer extends Rasterizer {

	public RenderedImage draw(MyGraphics2D g, Shape s) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		AffineTransform at = g.getTransform();
		g2.setTransform(Transform.getIdentity());
		Shape shape = at.createTransformedShape(s);
		g2.draw(shape);
		//g2.draw(s);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
	}

	public RenderedImage fill(MyGraphics2D g, Shape s) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		AffineTransform at = g.getTransform();
		g2.setTransform(Transform.getIdentity());
		Shape shape = at.createTransformedShape(s);
		g2.fill(shape);
		//g2.fill(s);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
	}
	
	public RenderedImage drawPath(MyGraphics2D g, Path path) {
		return draw(g, path.getShape());
	}

	public RenderedImage fillPath(MyGraphics2D g, Path path) {
		return fill(g, path.getShape());
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, AffineTransform xform, ImageObserver obs) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawImage(img, xform, obs);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
	}

	public RenderedImage drawImage(MyGraphics2D g, BufferedImage img, BufferedImageOp op, int x, int y) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawImage(img, op, x, y);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
	}

	public RenderedImage drawRenderedImage(MyGraphics2D g, RenderedImage img, AffineTransform xform) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawRenderedImage(img, xform);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
	}
	
	public RenderedImage drawRenderableImage(MyGraphics2D g, RenderableImage img, AffineTransform xform) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawRenderableImage(img, xform);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		// TODO: test
	}

	public RenderedImage drawString(MyGraphics2D g, String str, int x, int y) {
		float xf = (int)x, yf = (int)y;
		return drawString(g, str, xf, yf);
	}

	public RenderedImage drawString(MyGraphics2D g, String str, float x, float y) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawString(str, x, y);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
	}

	public RenderedImage drawString(MyGraphics2D g, AttributedCharacterIterator iterator, int x, int y) {
		float xf = (int)x, yf = (int)y;
		return drawString(g, iterator, xf, yf);
		// TODO: test
	}

	public RenderedImage drawString(MyGraphics2D g, AttributedCharacterIterator iterator, float x, float y) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawString(iterator, x, y);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		// TODO: test
	}

	public RenderedImage drawGlyphVector(MyGraphics2D g, GlyphVector gv, float x, float y) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawGlyphVector(gv, x, y);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		// TODO: test
	}

	public RenderedImage drawLine(MyGraphics2D g, int x1, int y1, int x2, int y2) {
		Shape shape = new Line2D.Double(x1, y1, x2, y2);
		return draw(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawLine(x1, y1, x2, y2);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}

	public RenderedImage drawRect(MyGraphics2D g, int x, int y, int width, int height) {
		Shape shape = new Rectangle2D.Double(x, y, width, height);
		return draw(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawRect(x, y, width, height);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}
	
	public RenderedImage fillRect(MyGraphics2D g, int x, int y, int width, int height) {
		Shape shape = new Rectangle2D.Double(x, y, width, height);
		return fill(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.fillRect(x, y, width, height);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}
	
	public RenderedImage clearRect(MyGraphics2D g, int x, int y, int width, int height) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.clearRect(x, y, width, height);
		return (RenderedImage) image;
	}

	public RenderedImage drawRoundRect(MyGraphics2D g, int x, int y,
			int width, int height, int arcWidth, int arcHeight) {
		Shape shape = new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight);
		return draw(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setPaint(g.getPaint());
		g2.setColor(Colour.getSolidColor(g.getColor()));
		AffineTransform at = g.getTransform();
		g2.setTransform(Transform.getIdentity());
		Shape shape = at.createTransformedShape(new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight));
		g2.draw(shape);
		//g2.drawRoundRect(x, y, width, height, arcWidth, arcHeight);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}
	
	public RenderedImage fillRoundRect(MyGraphics2D g, int x, int y,
			int width, int height, int arcWidth, int arcHeight) {
		Shape shape = new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight);
		return fill(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setPaint(g.getPaint());
		g2.setColor(Colour.getSolidColor(g.getColor()));
		AffineTransform at = g.getTransform();
		g2.setTransform(Transform.getIdentity());
		Shape shape = at.createTransformedShape(new RoundRectangle2D.Double(x, y, width, height, arcWidth, arcHeight));
		g2.fill(shape);
		//g2.fillRoundRect(x, y, width, height, arcWidth, arcHeight);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}
	
	public RenderedImage drawOval(MyGraphics2D g, int x, int y, int width, int height) {
		Shape shape = new Ellipse2D.Double(x, y, width, height);
		return draw(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		AffineTransform at = g.getTransform();
		g2.setTransform(Transform.getIdentity());
		Shape shape = at.createTransformedShape(new Ellipse2D.Double(x, y, width, height));
		g2.draw(shape);
		//g2.dra.drawOval(x, y, width, height);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}
	
	public RenderedImage fillOval(MyGraphics2D g, int x, int y, int width, int height) {
		Shape shape = new Ellipse2D.Double(x, y, width, height);
		return fill(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		AffineTransform at = g.getTransform();
		g2.setTransform(Transform.getIdentity());
		Shape shape = at.createTransformedShape(new Ellipse2D.Double(x, y, width, height));
		g2.fill(shape);
		//g2.dra.fillOval(x, y, width, height);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}
	
	public RenderedImage fillArc(MyGraphics2D g, int x, int y,
			int width, int height, int startAngle, int arcAngle) {
		Shape shape = new Arc2D.Double(x, y, width, height, startAngle, arcAngle, Arc2D.OPEN);
		return fill(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		AffineTransform at = g.getTransform();
		g2.setTransform(Transform.getIdentity());
		Shape shape = at.createTransformedShape(new Arc2D.Double(x, y, width, height, startAngle, arcAngle, Arc2D.OPEN));
		g2.fill(shape);
		//g2.fillArc(x, y, width, height, startAngle, arcAngle);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}
	
	public RenderedImage drawArc(MyGraphics2D g, int x, int y,
			int width, int height, int startAngle, int arcAngle) {
		Shape shape = new Arc2D.Double(x, y, width, height, startAngle, arcAngle, Arc2D.OPEN);
		return draw(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawArc(x, y, width, height, startAngle, arcAngle);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}
	
	public RenderedImage drawPolyline(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints) {
		Path2D.Double shape = new Path2D.Double();
		for(int i=0; i<nPoints; i++) {
			if(i==0) shape.moveTo(xPoints[i], yPoints[i]);
			else shape.lineTo(xPoints[i], yPoints[i]);
		}
		return draw(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setPaint(g.getPaint());
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawPolyline(xPoints, yPoints, nPoints);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}

	public RenderedImage drawPolygon(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints) {
		Path2D.Double shape = new Path2D.Double();
		for(int i=0; i<nPoints; i++) {
			if(i==0) shape.moveTo(xPoints[i], yPoints[i]);
			else shape.lineTo(xPoints[i], yPoints[i]);
		}
		shape.closePath();
		return draw(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setPaint(g.getPaint());
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.drawPolygon(xPoints, yPoints, nPoints);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}

	public RenderedImage fillPolygon(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints) {
		Path2D.Double shape = new Path2D.Double();
		for(int i=0; i<nPoints; i++) {
			if(i==0) shape.moveTo(xPoints[i], yPoints[i]);
			else shape.lineTo(xPoints[i], yPoints[i]);
		}
		shape.closePath();
		return fill(g, shape);
		/*
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.setPaint(g.getPaint());
		g2.setColor(Colour.getSolidColor(g.getColor()));
		g2.fillPolygon(xPoints, yPoints, nPoints);
		image = Util.setAlpha((BufferedImage)image, g.getColor().getAlpha());
		return (RenderedImage) image;
		*/
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, ImageObserver observer) {
		return drawImage(g, img, x, y, Colour.TRANSPARENT, observer);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, int width, int height, ImageObserver observer) {
		return drawImage(g, img, x, y, width, height, Colour.TRANSPARENT, observer);
	}
	
	public RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, Color bgcolor, ImageObserver observer) {
		return drawImage(g, img, x, y, img.getWidth(observer), img.getHeight(observer), bgcolor, observer);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, 
			int width, int height, Color bgcolor, ImageObserver observer) {
		return drawImage(g, img, x, y, x+width, y+height, 0, 0, width, height, bgcolor, observer);
	}

	public RenderedImage drawImage(MyGraphics2D g, Image img, int dx1, int dy1, int dx2, int dy2,
			int sx1, int sy1, int sx2, int sy2, ImageObserver observer) {
		return drawImage(g, img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, Colour.TRANSPARENT, observer);
	}
	
	public RenderedImage drawImage(MyGraphics2D g, Image img, int dx1, int dy1, int dx2, int dy2,
			int sx1, int sy1, int sx2, int sy2, Color bgcolor, ImageObserver observer) {
		Image image = g.createImageFromGraphics();
		Graphics2D g2 = g.getGraphicsWithProperties(image);
		g2.drawImage(img, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2, bgcolor, observer);
		//helper.setAlpha((BufferedImage)image, bgcolor.getAlpha());
		// TODO semi-transparent bgcolor doesn't work when width and height are specified
		return (RenderedImage) image;
	}
}
