package renderer.rasterizer;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.awt.Color;
import java.awt.Image;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Vector;

import renderer.display.MyGraphics2D;
import renderer.geometry.Pixel;





abstract public class Rasterizer {
	
	public static final int DEFAULT = 0, PIXELATOR = 1, SUPERPIXELATOR = 2;

	public abstract RenderedImage draw(MyGraphics2D g, Shape s);

	public abstract RenderedImage fill(MyGraphics2D g, Shape s);

	public abstract RenderedImage drawPath(MyGraphics2D g, Path path);

	public abstract RenderedImage fillPath(MyGraphics2D g, Path path);

	public abstract RenderedImage drawImage(MyGraphics2D g, Image img, AffineTransform xform, ImageObserver obs);

	public abstract RenderedImage drawImage(MyGraphics2D g, BufferedImage img, BufferedImageOp op, int x, int y);

	public abstract RenderedImage drawRenderedImage(MyGraphics2D g, RenderedImage img, AffineTransform xform);
	
	public abstract RenderedImage drawRenderableImage(MyGraphics2D g, RenderableImage img, AffineTransform xform);

	public abstract RenderedImage drawString(MyGraphics2D g, String str, int x, int y);

	public abstract RenderedImage drawString(MyGraphics2D g, String str, float x, float y);

	public abstract RenderedImage drawString(MyGraphics2D g, AttributedCharacterIterator iterator, int x, int y);

	public abstract RenderedImage drawString(MyGraphics2D g, AttributedCharacterIterator iterator, float x, float y);

	public abstract RenderedImage drawGlyphVector(MyGraphics2D g, GlyphVector gv, float x, float y);

	public abstract RenderedImage drawLine(MyGraphics2D g, int x1, int y1, int x2, int y2);

	public abstract RenderedImage drawRect(MyGraphics2D g, int x, int y, int width, int height);
	
	public abstract RenderedImage fillRect(MyGraphics2D g, int x, int y, int width, int height);
	
	public abstract RenderedImage clearRect(MyGraphics2D g, int x, int y, int width, int height);

	public abstract RenderedImage drawRoundRect(MyGraphics2D g, int x, int y,
			int width, int height, int arcWidth, int arcHeight);
	
	public abstract RenderedImage fillRoundRect(MyGraphics2D g, int x, int y,
			int width, int height, int arcWidth, int arcHeight);
	
	public abstract RenderedImage drawOval(MyGraphics2D g, int x, int y, int width, int height);
	
	public abstract RenderedImage fillOval(MyGraphics2D g, int x, int y, int width, int height);
	
	public abstract RenderedImage fillArc(MyGraphics2D g, int x, int y,
			int width, int height, int startAngle, int arcAngle);
	
	public abstract RenderedImage drawArc(MyGraphics2D g, int x, int y,
			int width, int height, int startAngle, int arcAngle);
	
	public abstract RenderedImage drawPolyline(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints);

	public abstract RenderedImage drawPolygon(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints);

	public abstract RenderedImage fillPolygon(MyGraphics2D g, int[] xPoints, int[] yPoints, int nPoints);

	public abstract RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, ImageObserver observer);

	public abstract RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, int width, int height, ImageObserver observer);
	
	public abstract RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, Color bgcolor, ImageObserver observer);

	public abstract RenderedImage drawImage(MyGraphics2D g, Image img, int x, int y, 
			int width, int height, Color bgcolor, ImageObserver observer);

	public abstract RenderedImage drawImage(MyGraphics2D g, Image img, int dx1, int dy1, int dx2, int dy2,
			int sx1, int sy1, int sx2, int sy2, ImageObserver observer);
	
	public abstract RenderedImage drawImage(MyGraphics2D g, Image img, int dx1, int dy1, int dx2, int dy2,
			int sx1, int sy1, int sx2, int sy2, Color bgcolor, ImageObserver observer);

	public RenderedImage drawPoints(MyGraphics2D g, Vector<MyPoint> pixels) {
		Image image = g.createImageFromGraphics();
		for(int i=0; i<pixels.size(); i++) {
			MyPoint p = pixels.get(i);
			if(p.x>=0 && p.x<g.getWidth() && p.y>=0 && p.y<g.getHeight()) {
				((BufferedImage)image).setRGB((int)p.x, (int)p.y, g.getColor().getRGB());
			}
		}
		return (RenderedImage)image;
	}

	public RenderedImage drawPixels(MyGraphics2D g, Vector<Pixel> pixels) {
		Image image = g.createImageFromGraphics();
		for(int i=0; i<pixels.size(); i++) {
			Pixel p = pixels.get(i);
			if(p.x>=0 && p.x<g.getWidth() && p.y>=0 && p.y<g.getHeight()) {
				Color c = g.getColor();
				if(p.getAlpha() == 0) continue;
				else if(p.getAlpha() < 255) {
					double ratio = (double)p.getAlpha()/255.0;
					int alpha255 = (int) Math.round((double)g.getColor().getAlpha()*ratio);
					if(alpha255 > 255) alpha255 = 255;
					else if(alpha255 < 0) alpha255 = 0;
					c = new Color(c.getRed(), c.getGreen(), c.getBlue(), alpha255);
				}
				((BufferedImage)image).setRGB((int)p.x, (int)p.y, c.getRGB());
			}
		}
		return (RenderedImage)image;
	}
}
