package renderer.rasterizer.superpixelator;

import renderer.geometry.MyCurve;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.util.Vector;


public class Preprocessor {

	public static Path preprocess(Path path, Vector<MyPoint> pixels, boolean[] pixelated) {
		pixels.clear();
		Path cleanPath1 = removeDegenerateSegments(path);
		if(cleanPath1.isEmpty()) {
			pixelated[0] = true;
			return cleanPath1;
		}
		Path cleanPath2 = mergeStraightLines(cleanPath1);
		/*
		pixelated[0] = boundingBoxSizeCheck(cleanPath1, pixels);
		if(pixelated[0]) {
			return cleanPath1;
		}
		*/

		return cleanPath2;
	}
	
	private static boolean boundingBoxSizeCheck(Path path, Vector<MyPoint> pixels) {
		pixels.clear();
		MyPoint[] oldBB = path.getBoundingBox();
		double width = oldBB[1].x - oldBB[0].x;
		double height = oldBB[1].y - oldBB[0].y;
		MyPoint q1 = path.getFirstPoint(), q2 = path.getLastPoint();
		if(width<=1.001) {
			// a single pixel
			if(height<=1.001) {
				MyPoint centre = oldBB[0].plus(oldBB[1]).times(0.5);
				MyPoint snappedCentre = centre.getClosestPixelCentre();
				int minX = (int) Math.floor(snappedCentre.x);
				int minY = (int) Math.floor(snappedCentre.y);
				int maxX = minX + 1;
				int maxY = minY + 1;
				int x1 = (q1.x < q2.x) ? minX : maxX;
				int x2 = (q1.x < q2.x) ? maxX : minX;
				int y1 = (q1.y < q2.y) ? minY : maxY;
				int y2 = (q1.y < q2.y) ? maxY : minY;				
				pixels.add(new MyPoint(x1, y1));
				pixels.add(new MyPoint(x2, y2));
				return true;
			}
			// vertical span
			else {
				int minX = (int) Math.floor(oldBB[0].x);
				int minY = (int) Math.floor(oldBB[0].y);
				int maxX = (int) Math.ceil(oldBB[1].x);
				int maxY = (int) Math.ceil(oldBB[1].y);
				int x1 = (q1.x < q2.x) ? minX : maxX;
				int x2 = (q1.x < q2.x) ? maxX : minX;
				int y1 = (q1.y < q2.y) ? minY : maxY;
				int y2 = (q1.y < q2.y) ? maxY : minY;				
				pixels.add(new MyPoint(x1, y1));
				pixels.add(new MyPoint(x2, y2));
				return true;
			}
		}
		else {
			// horizontal span
			if(height<=1.001) {
				int minX = (int) Math.floor(oldBB[0].x);
				int minY = (int) Math.floor(oldBB[0].y);
				int maxX = (int) Math.ceil(oldBB[1].x);
				int maxY = (int) Math.ceil(oldBB[1].y);
				int x1 = (q1.x < q2.x) ? minX : maxX;
				int x2 = (q1.x < q2.x) ? maxX : minX;
				int y1 = (q1.y < q2.y) ? minY : maxY;
				int y2 = (q1.y < q2.y) ? maxY : minY;				
				pixels.add(new MyPoint(x1, y1));
				pixels.add(new MyPoint(x2, y2));
				return true;
			}
			else return false;
		}
	}
	
	
	private static Path removeDegenerateSegments(Path path) {
		Path newPath = new Path();
		// get rid of degenerate curves
		for(int i=0; i<path.getNumCurves(); i++) {
			MyCurve curve = path.getCurve(i);
			if(!curve.isDegenerate()) newPath.addCurve(curve);
		}
		return newPath;
	}
	
	private static Path mergeStraightLines(Path path) {
		Path newPath = path.clone();
		for(int i=newPath.getNumSegments()-2; i>=1; i--) {
			MyCurve curveBefore = newPath.getCurveBeforeSegmentIndex(i);
			MyCurve curveAfter = newPath.getCurveAfterSegmentIndex(i);
			MyPoint vec1 = curveBefore.getP3().minus(curveBefore.getP0());
			MyPoint vec2 = curveAfter.getP3().minus(curveAfter.getP0());
			boolean betweenLines = false;
			if(vec2.isOrigin()) betweenLines = true;
			else {
				double angle = vec1.getAngleTo(vec2);
				betweenLines = curveBefore.isStraight() && curveAfter.isStraight()
								 	   && Math.abs(angle) < 0.01;
			}
			if(betweenLines) {
				newPath.removeSegment(i);
			}
		}
		return newPath;
	}
}
