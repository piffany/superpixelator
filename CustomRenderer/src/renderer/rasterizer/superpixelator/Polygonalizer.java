package renderer.rasterizer.superpixelator;

import renderer.geometry.MyPoint;
import renderer.geometry.Path;
import renderer.geometry.Segment;
import renderer.util.MyMath;

import java.util.Collections;
import java.util.Vector;

public class Polygonalizer {

	public static Vector<MyPoint> getPixelatedPathWRTCentre(Vector<Path> paths, boolean sorted, MyPoint centre) {
		Vector<MyPoint> shapePixels = new Vector<MyPoint>();
		for(int i=0; i<paths.size(); i++) {
			Path path = paths.get(i);
			// if length 0, don't do anything
			if(path.getFirstPoint().equals(path.getLastPoint())) continue;
			shapePixels.addAll(getPixelatedPathWRTCentre(paths.get(i), sorted, centre));
		}
		return shapePixels;
	}
	
	private static boolean firstPointCloserToCentreThanLastPoint(Path path, MyPoint centre) {
		if(path.getNumSegments() < 2) return true;
		else {
			MyPoint p1 = path.getFirstPoint();
			MyPoint p2 = path.getLastPoint();
			double dx1 = Math.abs(p1.x - centre.x);
			double dx2 = Math.abs(p2.x - centre.x);
			if(dx1 < dx2 - 0.1) return true;
			if(dx2 < dx1 - 0.1) return false;
			else {
				double dy1 = Math.abs(p1.y - centre.y);
				double dy2 = Math.abs(p2.y - centre.y);
				if(dy1 < dy2) return true;
				else return false;
			}
			/*
			double d1 = p1.distance(centre);
			double d2 = p2.distance(centre);
			double diff = d1 - d2;
			if(diff < -0.1) return true;
			else if(diff > 0.1) return false;
			else {
				double dx1 = Math.abs(p1.x - centre.x);
				double dx2 = Math.abs(p2.x - centre.x);
				return dx1 <= dx2;
			}
			*/
		}
	}
	
	private static Vector<MyPoint> reverseVector(Vector<MyPoint> points) {
		Vector<MyPoint> reverse = new Vector<MyPoint>();
		for(int i=points.size()-1; i>=0; i--) {
			reverse.add(points.get(i));
		}
		return reverse;
	}

	
	public static Vector<MyPoint> getPixelatedPathWRTCentre(Path path, boolean toSort, MyPoint centre) {
		toSort = true;
		boolean toSmooth = false;
		Vector<MyPoint> pixelPath = new Vector<MyPoint>();
		boolean reverse = !firstPointCloserToCentreThanLastPoint(path, centre);
		pixelPath = plotUnsortedPathWRTCentre(reverse ? path.getReversePath() : path, centre);
		//for(int i=0; i<pixelPath.size(); i++) {
		//	System.out.println(i + ": " + pixelPath.get(i));
		//}
		//if(reverse) pixelPath = reverseVector(pixelPath);
		if(toSort) {
			boolean[] sorted = {true};
			Vector<MyPoint> sortedPixelPath = Sorter2.sortPathPoints(
					reverse ? path.getReversePath() : path, pixelPath, sorted);
			if(reverse) sortedPixelPath = reverseVector(sortedPixelPath);
			if(reverse) pixelPath = reverseVector(pixelPath);
			// fix kinks from slope +-1 segments
			if(sorted[0]) {
				return sortedPixelPath;
			}
			else return pixelPath;
		}
		else {
			if(reverse) pixelPath = reverseVector(pixelPath);
			return pixelPath;
		}
	}

	public static Vector<MyPoint> plotUnsortedPathWRTCentre(Path path, MyPoint centre) {
		MyPoint firstPt = path.getFirstPoint();
		MyPoint lastPt = path.getLastPoint();
		MyPoint c = centre.clone();
		double dirX = (firstPt.x <= lastPt.x) ? 1 : -1;
		double dirY = (firstPt.y <= lastPt.y) ? 1 : -1;
		MyPoint p1 = firstPt.clone();
		MyPoint p2 = lastPt.clone();
		Path newPath1 = new Path();
		// orient
		for(int i=0; i<path.getNumSegments(); i++) {
			Segment s = path.getSegment(i);
			MyPoint p = new MyPoint(s.getPoint().x*dirX, s.getPoint().y*dirY);
			MyPoint hi = new MyPoint(s.getHandleIn().x*dirX, s.getHandleIn().y*dirY);
			MyPoint ho = new MyPoint(s.getHandleOut().x*dirX, s.getHandleOut().y*dirY);
			newPath1.addSegment(new Segment(p, hi, ho));
		}
		p1 = new MyPoint(p1.x*dirX, p1.y*dirY);
		p2 = new MyPoint(p2.x*dirX, p2.y*dirY);
		c = new MyPoint(c.x*dirX, c.y*dirY);
		boolean flipXY = Sorter.getSortDirection(newPath1) < 0;
		Path newPath2 = new Path();
		// flip x and y
		if(flipXY) {
			for(int i=0; i<newPath1.getNumSegments(); i++) {
				Segment s = newPath1.getSegment(i);
				MyPoint p = new MyPoint(s.getPoint().y, s.getPoint().x);
				MyPoint hi = new MyPoint(s.getHandleIn().y, s.getHandleIn().x);
				MyPoint ho = new MyPoint(s.getHandleOut().y, s.getHandleOut().x);
				newPath2.addSegment(new Segment(p, hi, ho));
			}
			p1 = new MyPoint(p1.y, p1.x);
			p2 = new MyPoint(p2.y, p2.x);
			c = new MyPoint(c.y, c.x);
		}
		else newPath2 = newPath1.clone();
		// pixelate
		Vector<MyPoint> pixelPath = plotCanonicalUnsortedPathBresenhamWRTCentre(newPath2, p1, p2, c);
		/*
		if(Math.abs(lastPt.x-centre.x) < 1 && Math.abs(lastPt.y-centre.y) < 1) {
			System.out.println(firstPt + ", " + lastPt + ", " + centre);
			System.out.println(p1 + "... " + p2 + "... " + c);
			System.out.println(pixelPath.get(0) + "// " + pixelPath.get(pixelPath.size()-1));
		}*/
		// unflip
		if(flipXY) {
			for(int i=0; i<pixelPath.size(); i++) {
				MyPoint p = pixelPath.get(i);
				pixelPath.set(i, new MyPoint(p.y, p.x));
			}
		}
		// reorient
		for(int i=0; i<pixelPath.size(); i++) {
			MyPoint p = pixelPath.get(i);
			pixelPath.set(i, new MyPoint(p.x*dirX, p.y*dirY));
		}
		return pixelPath;
	}
	
	private static boolean isHalf(double v) {
		return Math.abs(Math.abs(v%1) - 0.5) < 0.01;
	}

	// assume increasing value and increasing positive slope
	public static Vector<MyPoint> plotCanonicalUnsortedPathBresenhamWRTCentre(Path path, MyPoint firstPt, MyPoint lastPt, MyPoint centre) {
		Vector<MyPoint> points = new Vector<MyPoint>();
		double startX = isHalf(firstPt.x%1) ? Math.round(firstPt.x-0.5)+0.5 : Math.round(firstPt.x) + 0.5;
		double startY = isHalf(firstPt.y%1) ? Math.round(firstPt.y-0.5)+0.5 : Math.round(firstPt.y) + 0.5;
		double endX = isHalf(lastPt.x%1) ? Math.round(lastPt.x-0.5)+0.5 : Math.round(lastPt.x) - 0.5;
		double endY = isHalf(lastPt.y%1) ? Math.round(lastPt.y-0.5)+0.5 : Math.round(lastPt.y) - 0.5;
		//System.out.println(lastPt.x + ", " + centre.x + ", " + MyMath.ceil((int)lastPt.x,centre.x));
		//System.out.println(lastPt + "~~" + endX + "~~ " + endY);
		/*
			double   
			   endX = Math.ceil(lastPt.x) - 0.5,
			   endY = Math.ceil(lastPt.y) - 0.5;*/
		Vector<MyPoint> points1 = new Vector<MyPoint>();
		Vector<MyPoint> points2 = new Vector<MyPoint>();
		MyPoint p1 = new MyPoint(startX, startY);
		MyPoint p2 = new MyPoint(endX, endY);
		//System.out.print(firstPt + ", " + lastPt + " --> ");
		//System.out.println(p1 + ", " + p2);
		if(path.isStraight()) {
			path = new Path.Line(p1.clone(), p2.clone());
		}
		Vector<MyPoint> samples = Approximator.getSamplePoints(path, 100);
		//if(samples.isEmpty()) System.out.println(path);
		points1.add(p1.clone());
		points2.add(0,p2.clone());
		while(Math.abs(p1.x-p2.x)>0 && Math.abs(p1.y-p2.y)>0) {
			// for path 1
			{
				Vector<MyPoint> candidates = new Vector<MyPoint>();
				MyPoint q1 = p1.add(new MyPoint(1,0));
				MyPoint q2 = p1.add(new MyPoint(0,1));
				MyPoint q3 = p1.add(new MyPoint(1,1));
				if(MyMath.inRange(q1.x, p1.x, p2.x) && MyMath.inRange(q1.y, p1.y, p2.y)) {
					candidates.add(q1);
				}
				if(MyMath.inRange(q2.x, p1.x, p2.x) && MyMath.inRange(q2.y, p1.y, p2.y)) {
					candidates.add(q2);
				}
				if(MyMath.inRange(q3.x, p1.x, p2.x) && MyMath.inRange(q3.y, p1.y, p2.y)) {
					candidates.add(q3);
				}
				if(candidates.isEmpty()) {
					break;
				}
				else {
					MyPoint spanStart = null;
					for(int j=points1.size()-1; j>=0; j--) {
						MyPoint p = points1.get(j);
						if(p.x==p1.x || p.y==p1.y) spanStart = p.clone();
						else break;				
					}
					int bestInd = getBestCandidate(candidates, spanStart, path, samples, 1, 0.1, centre);
					p1 = candidates.get(bestInd);
					points1.add(p1.clone());
				}
			}
			// for path 2
			{
				Vector<MyPoint> candidates = new Vector<MyPoint>();
				MyPoint q1 = p2.add(new MyPoint(-1,0));
				MyPoint q2 = p2.add(new MyPoint(0,-1));
				MyPoint q3 = p2.add(new MyPoint(-1,-1));
				if(MyMath.inRange(q1.x, p1.x, p2.x) && MyMath.inRange(q1.y, p1.y, p2.y)) {
					candidates.add(q1);
				}
				if(MyMath.inRange(q2.x, p1.x, p2.x) && MyMath.inRange(q2.y, p1.y, p2.y)) {
					candidates.add(q2);
				}
				if(MyMath.inRange(q3.x, p1.x, p2.x) && MyMath.inRange(q3.y, p1.y, p2.y)) {
					candidates.add(q3);
				}
				if(candidates.isEmpty()) {
					break;
				}
				else {
					MyPoint spanStart = null;
					for(int j=0; j<=points2.size()-1; j++) {
						MyPoint p = points2.get(j);
						if(p.x==p2.x || p.y==p2.y) spanStart = p.clone();
						else break;				
					}
					int bestInd = getBestCandidate(candidates, spanStart, path, samples, 1, 0.1, centre);
					//p2 = candidates.get(bestInd).clone();
					//points2.add(0,p2.clone());
				}
			}
		}
		points.addAll(points1);
		points.addAll(points2);
		// remove L-shaped corners
		removeExtraPixels(points, samples);
		Vector<MyPoint> corners = convertPixelCentresToCornerPath(points);
		//for(int i=0; i<points.size(); i++) System.out.print(points.get(i) + ", ");
		//System.out.println();
		return corners;
	}
	
	private static boolean isExtraPixel(Vector<MyPoint> points, int i) {
		if(i<=0 || i>=points.size()-1) return false;
		else {
			MyPoint q1 = points.get(i-1);
			MyPoint q2 = points.get(i);
			MyPoint q3 = points.get(i+1);
			return (q2.x-q1.x==0 && q3.y-q2.y==0) || (q2.y-q1.y==0 && q3.x-q2.x==0);
		}
	}
	
	private static void removeExtraPixels(Vector<MyPoint> points, Vector<MyPoint> samples) {
		for(int i=points.size()-2; i>=1; i--) {
			//MyPoint q1 = points.get(i-1);
			//MyPoint q2 = points.get(i);
			//MyPoint q3 = points.get(i+1);
			if(isExtraPixel(points, i)) {
				MyPoint q1 = points.get(i-1);
				MyPoint q2 = points.get(i);
				MyPoint q3 = points.get(i+1);
				boolean remove = true;
				double d1 = Approximator.getMinDist(q1, samples);
				double d2 = Approximator.getMinDist(q2, samples);
				double d3 = Approximator.getMinDist(q3, samples);
				if(isExtraPixel(points, i-1) && d1 < d2) remove = false;
				else if(isExtraPixel(points, i+1) && d3 < d2) remove = false;
				if(remove) {
					points.remove(i);
					i--;
				}
			}
		}
	}
	
	// assume canonical form
	private static Vector<MyPoint> convertPixelCentresToCornerPath(Vector<MyPoint> centres) {
		Vector<MyPoint> corners = new Vector<MyPoint>();
		if(centres.isEmpty()) return corners;
		else {
			MyPoint p = centres.get(0);
			corners.add(new MyPoint((int)Math.floor(p.x), (int)Math.floor(p.y)));
			for(int i=0; i<centres.size(); i++) {
				MyPoint next = centres.get(i);
				if(Math.abs(next.x-p.x) > 0.5 && Math.abs(next.y-p.y) > 0.5) {
					p = next.clone();
					corners.add(new MyPoint((int)Math.floor(p.x), (int)Math.floor(p.y)));
				}
				
				if(i==centres.size()-1) {
					MyPoint last = centres.get(i);
					corners.add(new MyPoint((int)Math.ceil(last.x), (int)Math.ceil(last.y)));
				}
				
			}
			return corners;
		}
	}
	
	public static int getBestCandidate(Vector<MyPoint> candidates, MyPoint currPt, Path path, Vector<MyPoint> samples,
			double posW, double slopeW, MyPoint centre) {
		assert(posW>=0 && slopeW>=0 && posW+slopeW>0);
		// calculate the error associated with each candidate
		Vector<Double> errors = new Vector<Double>();
		
		for(int i=0; i<candidates.size(); i++) {
			MyPoint nextPt = candidates.get(i);
			double distErr = Approximator.getMinDist(nextPt, samples);
			/*
			MyPoint closestSlope1 = getApproxSlopeOfClosestPt(currPt, samples);
			MyPoint closestSlope2 = getApproxSlopeOfClosestPt(nextPt, samples);
			MyPoint closestSlope = (closestSlope1.plus(closestSlope2)).times(0.5);
			MyPoint corner1 = (currPt.x < nextPt.x) ? currPt.minus(new MyPoint(0.5,0.5))
													: currPt.plus(new MyPoint(0.5,0.5));
			MyPoint corner2 = (currPt.x < nextPt.x) ? nextPt.minus(new MyPoint(0.5,0.5))
													: nextPt.plus(new MyPoint(0.5,0.5));
			MyPoint slope = corner2.minus(corner1);
			double slopeErr = Math.abs(closestSlope.getAngleTo(slope));
			double error = posW*distErr + slopeW*slopeErr;
			*/
			double error = distErr;
			errors.add(error);
		}
		double minError = Collections.min(errors);
		Vector<Integer> minInds = new Vector<Integer>();
		for(int i=0; i<candidates.size(); i++) {
			//System.out.println(i + " : " + errors.get(i));
			if(Math.abs(errors.get(i) - minError) < 0.001) {
				minInds.add(i);
			}			
		}
		if(minInds.isEmpty()) {
			for(int i=0; i<candidates.size(); i++) {
					System.out.println(i + " : " + candidates.get(i) + ", " + currPt + ", " + samples.size() + ", " + errors.get(i) + ", " + (path==null));
			}
		}
		assert(!minInds.isEmpty());
		// get the one closest to the centre
		int minInd = minInds.get(0);
		MyPoint bestPt = candidates.get(minInd).clone();
		for(int i=1; i<minInds.size(); i++) {
			MyPoint bestPt2 = candidates.get(minInds.get(i));
			if(bestPt2.distance(centre) < bestPt.distance(centre)) {
				minInd = minInds.get(i);
				bestPt = candidates.get(minInd).clone();
			}
		}
		return minInd;
	}
	
	// find the slope of the segment in samples closest to p 
	public static MyPoint getApproxSlopeOfClosestPt(MyPoint p, Vector<MyPoint> samples) {
		double minDist = -1;
		MyPoint closestSlope = null;
		for(int i=0; i<samples.size()-1; i++) {
			double dist = MyMath.getDistanceToLineSegment(p, samples.get(i), samples.get(i+1));
			if(minDist==-1 || dist<minDist) {
				minDist = dist;
				closestSlope = samples.get(i+1).minus(samples.get(i)).clone();
			}
		}
		
		return closestSlope;
	}

}
