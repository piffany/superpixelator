package renderer.rasterizer.superpixelator;

import renderer.geometry.MyCurve;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.util.Vector;

import renderer.util.MyMath;

public class Approximator {

	public static Vector<MyPoint> getSamplePoints(MyCurve curve, int n) {
		assert(n>1);
		Vector<MyPoint> samples = new Vector<MyPoint>();
		for(int i=0; i<n; i++) {
			double t = (double)i/(double)(n-1);
			samples.add(curve.getPoint(t));
		}
		return samples;
	}

	public static Vector<MyPoint> getSamplePoints(Path path, int n) {
		Vector<Double> lengths = new Vector<Double>();
		double totalLength = 0;
		Vector<MyCurve> curves = path.getCurves();
		for(int i=0; i<curves.size(); i++) {
			double length = curves.get(i).getFrameLength();
			lengths.add(length);
			totalLength += length;
		}
		Vector<MyPoint> samples = new Vector<MyPoint>();
		for(int i=0; i<curves.size(); i++) {
			int samplesPerCurve = (int) Math.round((lengths.get(i)/totalLength*(double)n));
			/*
			if(samplesPerCurve<=2) {
				System.out.println("samples: " + samplesPerCurve);
				System.out.println("length_i = " + lengths.get(i));
				System.out.println("totalLength = " + totalLength);
				System.out.println("n = " + n);
				System.out.println("curve_i = " + curves.get(i));
				System.out.println("path = " + path);
			}
			*/
			samplesPerCurve = Math.max(2, samplesPerCurve);
			Vector<MyPoint> curvePts = getSamplePoints(curves.get(i), samplesPerCurve);
			if(i>0 && !curvePts.isEmpty()) curvePts.remove(0);
			samples.addAll(curvePts);
		}
		return samples;
	}
	
	public static double getMinDist(MyPoint p, Vector<MyPoint> samples) {
		assert(!samples.isEmpty());
		double minDist = -1;
		for(int i=0; i<samples.size()-1; i++) {
			double dist = MyMath.getDistanceToLineSegment(p, samples.get(i), samples.get(i+1));
			if(minDist==-1 || dist<minDist) minDist = dist;
		}
		return minDist;
	}
	
	public static int getMinDistInd(MyPoint p, Vector<MyPoint> samples) {
		assert(!samples.isEmpty());
		int minDistInd = -1;
		double minDist = -1;
		for(int i=0; i<samples.size()-1; i++) {
			double dist = MyMath.getDistanceToLineSegment(p, samples.get(i), samples.get(i+1));
			if(minDistInd==-1 || dist<minDist) {
				minDistInd = i;
				minDist = dist;
			}
		}
		return minDistInd;
	}
	
	public static MyPoint getClosestPoint(MyPoint p, Vector<MyPoint> samples) {
		assert(!samples.isEmpty());
		double minDist = -1;
		MyPoint closest = new MyPoint();
		for(int i=0; i<samples.size()-1; i++) {
			MyPoint q = MyMath.getClosestPointOnLineSegment(p, samples.get(i), samples.get(i+1));
			double dist = p.distance(q);
			if(minDist==-1 || dist<minDist) {
				minDist = dist;
				closest = q.clone();
			}
		}
		return closest;
	}

}
