package renderer.rasterizer.superpixelator;

import renderer.geometry.MyPoint;
import renderer.geometry.Path;
import renderer.geometry.Segment;
import renderer.util.MyMath;

import java.awt.geom.AffineTransform;
import java.util.Vector;

public class BoundingBoxShifter {

	// coefficients (symmetry, size, position, angle, aspectRatio)
	//double[] C = {100,1,1,1,1};
	//double[] C = {100, 40, 1, 100, 100};
	private static double[] C = {100, 40, 1, 10, 100};
	
	public static Path snapShapeByOptimalBoundingBox(Path path, MyPoint centre, double[] C) {
		for(int i=0; i<BoundingBoxShifter.C.length; i++) {
			BoundingBoxShifter.C[i] = C[i];
		}
		MyPoint[] oldBBSidesOnly = path.getBoundingBox();
		MyPoint[] oldBB = {	oldBBSidesOnly[0],
							(oldBBSidesOnly[0].plus(oldBBSidesOnly[1])).times(0.5),
							oldBBSidesOnly[1]};
		MyPoint[] newBB = getOptimalBoundingBox(oldBBSidesOnly, path);
		//System.out.println(newBB[0] + ", " + newBB[1] + ", " + newBB[2]);
		double oldBBWidth = Math.abs(oldBB[2].x - oldBB[0].x);
		double oldBBHeight = Math.abs(oldBB[2].y - oldBB[0].y);
		double bbWidth = Math.abs(newBB[2].x - newBB[0].x);
		double bbHeight = Math.abs(newBB[2].y - newBB[0].y);
		//System.out.println("oldBB: " + oldBBWidth + ", " + oldBBHeight);
		//System.out.println("newBB: " + bbWidth + ", " + bbHeight);
		Path newPath = new Path();
		double threshold = 0.5;
		boolean minW = bbWidth < threshold || oldBBWidth < threshold;
		boolean minH = bbHeight < threshold || oldBBHeight < threshold;
		if(minH) {
			double y = (int) Math.round(oldBB[1].y + 0.5)-0.5;
			if(minW) {
				double x = (int) Math.round(oldBB[1].x + 0.5)-0.5;
				newPath = new Path.Line(new MyPoint(x, y), new MyPoint(x, y));
				centre.x = x; centre.y = y;
			}
			else {
				newPath = new Path.Line(new MyPoint(newBB[0].x, y), new MyPoint(newBB[2].x, y));
				centre.x = newBB[1].x; centre.y = y;
			}
		}
		else {
			if(minW) {
				double x = (int) Math.round(oldBB[1].x + 0.5)-0.5;
				newPath = new Path.Line(new MyPoint(x, newBB[0].y), new MyPoint(x, newBB[2].y));
				centre.x = x; centre.y = newBB[1].y;
			}
			else {
				newPath = path.clone();
				boolean symmX = Math.abs((newBB[1].x - newBB[0].x) - (newBB[2].x - newBB[1].x)) < 0.01;
				boolean symmY = Math.abs((newBB[1].y - newBB[0].y) - (newBB[2].y - newBB[1].y)) < 0.01;
				if(symmX) {
					MyPoint[] newBBx = {new MyPoint(newBB[0].x, oldBB[0].y), new MyPoint(newBB[2].x, oldBB[2].y)}; 
					AffineTransform atBB = getBoundingBoxRescaleTransform(oldBBSidesOnly, newBBx);
					newPath = newPath.getTransformedCopy(atBB);
				}
				else {
				// left half
					double[] oldInt1 = {oldBB[0].x, oldBB[1].x};
					double[] newInt1 = {newBB[0].x, newBB[1].x};
					newPath = scaleValues(newPath, oldInt1, newInt1, 'x');
					// right half
					double[] oldInt2 = {oldBB[1].x, oldBB[2].x};
					double[] newInt2 = {newBB[1].x, newBB[2].x};
					newPath = scaleValues(newPath, oldInt2, newInt2, 'x');
				}
				if(symmY) {
					MyPoint[] newBBy = {new MyPoint(oldBB[0].x, newBB[0].y), new MyPoint(oldBB[2].x, newBB[2].y)}; 
					AffineTransform atBB = getBoundingBoxRescaleTransform(oldBBSidesOnly, newBBy);
					newPath = newPath.getTransformedCopy(atBB);
					//System.out.println("symmY");
				}
				else {
					// top half
					double[] oldInt3 = {oldBB[0].y, oldBB[1].y};
					double[] newInt3 = {newBB[0].y, newBB[1].y};
					newPath = scaleValues(newPath, oldInt3, newInt3, 'y');
					// bottom half
					double[] oldInt4 = {oldBB[1].y, oldBB[2].y};
					double[] newInt4 = {newBB[1].y, newBB[2].y};
					newPath = scaleValues(newPath, oldInt4, newInt4, 'y');
					//System.out.println("not symmY");
				}
				centre.x = newBB[1].x; centre.y = newBB[1].y;
			}
		}
		//System.out.println(newPath);
		return newPath;
	}
	
	private static boolean isInt(double val) {
		double frac = Math.abs(val%1);
		return Math.abs(frac - 0.5) > 0.4;
	}
	
	private static double getBoundingBoxCost(
			double[] newIntX, double[] oldIntX, double[] newIntY, double[] oldIntY, double Ax, double Ay) {
		double costX = getIntervalCost(newIntX, oldIntX, Ax, C);
		double costY = getIntervalCost(newIntY, oldIntY, Ay, C);
		// aspect ratio cost
		double oldW = oldIntX[1] - oldIntX[0];
		double newW = newIntX[1] - newIntX[0];
		double oldH = oldIntY[1] - oldIntY[0];
		double newH = newIntY[1] - newIntY[0];
		double oldAsp = (oldW == 0 && oldH == 0) ? 1 :
						(Math.max(oldW, oldH) / Math.min(oldW, oldH));
		double newAsp = (newW == 0 && newH == 0) ? 1 :
			(Math.max(newW, newH) / Math.min(newW, newH));
		boolean oldSquare = oldAsp < 1.01;
		boolean newSquare = newAsp < 1.01;
		double aspect = (oldSquare && !newSquare) ? 1 : 0;
		// overall cost
		double cost = costX + costY + C[4]*aspect;
		//System.out.println(costX + ", " + costY);
		return cost;
	}
	
	private static double getIntervalCost(double[] interval, double[] interval0, double nAcute, double[] C) {
		// symmetry term
		double firstHalf = interval[1] - interval[0];
		double secondHalf = interval[2] - interval[1];
		//double ratio = (firstHalf==0 && secondHalf==0) ? 1 : 
		//				((firstHalf > secondHalf) ? (firstHalf/secondHalf) : (secondHalf/firstHalf));
		double ratio = Math.max(firstHalf, secondHalf) / Math.min(firstHalf, secondHalf);
		//double split = firstHalf/(firstHalf+secondHalf);
		double symmetry = Math.pow(ratio, 2);
		// size term
		double oldSize = interval0[2] - interval0[0];
		double newSize = interval[2] - interval[0];
		//double ratioSize = (oldSize==0 && newSize==0) ? 1 :
		//					((oldSize > newSize) ? oldSize/newSize : newSize/oldSize);
		//double size = Math.pow(ratioSize, 2);
		double size = Math.abs(oldSize - newSize);
		// position term
		double position = 	Math.abs(interval[0] - interval0[0]) + 
							Math.abs(interval[1] - interval0[1]) +
							Math.abs(interval[2] - interval0[2]);
		// sharpness term
		double frac = Math.abs(interval[1]%1);
		boolean onPixelCentre = Math.abs(frac-0.5) < 0.01;
		double angle = onPixelCentre ? 0 : nAcute;
		//System.out.println(nAcute);
		// total cost
		double cost = C[0]*symmetry + C[1]*size + C[2]*position + C[3]*angle;
		
		// size should include both relative and absolute
		// also aspect ratio
		
		/*
		System.out.println(MyMath.roundToDec(cost,3) + " = " + 
				MyMath.roundToDec(symmetry, 3) + " + " +
				MyMath.roundToDec(size, 3) + " + " +
				MyMath.roundToDec(position, 3) + " + " +
				MyMath.roundToDec(angle, 3));
		*/
		//System.out.println(nAcute + ": " + interval[0] + "," + interval[1] + ", " + interval[2]);
		return cost;
	}
	
	private static double[][] getOptimalIntervals(double x1, double x2, double y1, double y2,
			double tol, double Ax, double Ay) {
		// x
		double cx = (x1 + x2)/2.0;
		Vector<Double> x1s = getNearbyPointFiveValues(x1, tol);
		Vector<Double> cxs = getNearbyHalves(cx, tol);
		Vector<Double> x2s = getNearbyPointFiveValues(x2, tol);
		double[] originalIntervalX = {x1, cx, x2};
		double[] bestIntervalX = {0,0,0};
		// y
		double cy = (y1 + y2)/2.0;
		Vector<Double> y1s = getNearbyPointFiveValues(y1, tol);
		Vector<Double> cys = getNearbyHalves(cy, tol);
		Vector<Double> y2s = getNearbyPointFiveValues(y2, tol);
		double[] originalIntervalY = {y1, cy, y2};
		double[] bestIntervalY = {0,0,0};
		// search
		double bestCost = 0;
		boolean found = false;
		for(int ix=0; ix<x1s.size(); ix++) {
			for(int kx=0; kx<x2s.size(); kx++) {
				for(int jx=0; jx<cxs.size(); jx++) {
					if(Math.abs((Math.abs(0.5*x1s.get(ix) + 0.5*x2s.get(kx)))%1 - 0.5) < 0.1 && 
					   Math.abs(Math.abs(cxs.get(jx))%1) < 0.1) {
						//System.out.println(x1s.get(ix) + ", " + x2s.get(kx) + ", " + cxs.get(jx));
						continue;
					}					
					for(int iy=0; iy<y1s.size(); iy++) {
						for(int ky=0; ky<y2s.size(); ky++) {
							for(int jy=0; jy<cys.size(); jy++) {
								if(Math.abs((Math.abs(0.5*y1s.get(iy) + 0.5*y2s.get(ky)))%1 - 0.5) < 0.1 && 
								   Math.abs(Math.abs(cys.get(jy))%1) < 0.1) {
									//System.out.println(y1s.get(iy) + ", " + y2s.get(ky) + ", " + cys.get(jy));
									continue;
								}
								double[] intervalX = {x1s.get(ix), cxs.get(jx), x2s.get(kx)};
								double[] intervalY = {y1s.get(iy), cys.get(jy), y2s.get(ky)};
									if(	intervalX[0] < intervalX[1] && intervalX[1] < intervalX[2] &&
										intervalY[0] < intervalY[1] && intervalY[1] < intervalY[2]	) {
										
										double cost = getBoundingBoxCost(
												intervalX, originalIntervalX,
												intervalY, originalIntervalY,
												Ax, Ay);
										if(!found || cost < bestCost) {
											for(int ii=0; ii<3; ii++) {
												bestIntervalX[ii] = intervalX[ii];
												bestIntervalY[ii] = intervalY[ii];
											}
											bestCost = cost;
											found = true;
										}
									}
							}
						}
					}
				}
			}
		}
		/*
		if(!found) {
			System.out.println(v1 + ", " + c + ", " + v2);
			System.out.println(v1s.size());
			System.out.println(cs.size());
			System.out.println(v2s.size());
		}
		*/
		assert(found);
		//System.out.println(bestCost + ", " + (bestInterval[1]-bestInterval[0]) + ", " + (bestInterval[2]-bestInterval[1]));

		double[][] bestIntervals = {bestIntervalX, bestIntervalY};
		return bestIntervals;
	}
	
	private static int getNumberOfAcuteAnglesWithCoordinateValue(Path path, char dir, double val) {
		int n = 0;
		for(int i=0; i<path.getNumSegments(); i++) {
			MyPoint p = path.getSegment(i).getPoint();
			boolean sameVal = false;
			if(dir == 'x') 	sameVal = Math.abs(p.x - val) < 0.1;
			else			sameVal = Math.abs(p.y - val) < 0.1;
			boolean isAcute = path.isAcuteAngle(i);
			//if(isAcute) System.out.println(i);
			//System.out.println(i + ": " + path.getSegment(i) + ", "+ sameVal + ", " + val + ", " + isAcute);
			if(sameVal && isAcute) n++;
		}
		return n;
	}
	
	private static void printBB(String t, MyPoint[] bb) {
		String s = t + "[" + bb[0] + " x " + bb[1] + "]";
		System.out.println(s);
	}
	
	private static MyPoint[] getOptimalBoundingBox(MyPoint[] oldBB, Path path) {		
		double x1 = oldBB[0].x, y1 = oldBB[0].y, x2 = oldBB[1].x, y2 = oldBB[1].y;
		double tol = 1;
		int Ax = getNumberOfAcuteAnglesWithCoordinateValue(path, 'x', (x1+x2)/2.0);
		int Ay = getNumberOfAcuteAnglesWithCoordinateValue(path, 'y', (y1+y2)/2.0);
		double[][] optIntervals = getOptimalIntervals(x1, x2, y1, y2, tol, Ax, Ay);
		//double[] xs = getOptimalInterval(x1, x2, tol, Ax);
		//double[] ys = getOptimalInterval(y1, y2, tol, Ay);
		double[] xs = optIntervals[0];
		double[] ys = optIntervals[1];
		/*
		
		double tol = 1;
		Vector<Double> x1s = getNearbyPointFiveValues(x1, tol);
		Vector<Double> x2s = getNearbyPointFiveValues(x2, tol);
		Vector<Double> y1s = getNearbyPointFiveValues(y1, tol);
		Vector<Double> y2s = getNearbyPointFiveValues(y2, tol);
		*/
		
		MyPoint[] bb = {new MyPoint(xs[0], ys[0]), new MyPoint(xs[1], ys[1]), new MyPoint(xs[2], ys[2])};
		//System.out.println("oldBB = " + oldBB[0] + ", " + oldBB[1]);
		//System.out.println("newBB = " + bb[0] + ", " + bb[1] + ", " + bb[2]);
		
		//MyPoint[] bb = {new MyPoint(newX1, newY1), new MyPoint(newX2, newY2)};
		return bb;
	}
	
	// given 3.4, returns 2.5, 3.0, 3.5, 4.0, for example
	private static Vector<Double> getNearbyHalves(double x0, double tol) {
		int xStart = (int) Math.floor(x0 - tol);
		int xEnd = (int) Math.ceil(x0 + tol);
		Vector<Double> vals = new Vector<Double>();
		for(int x=xStart; x<xEnd; x++) {
			if(Math.abs(x - x0) <= tol) vals.add(x + 0.0);
			if(Math.abs(x + 0.5 - x0) <= tol) vals.add(x + 0.5);
		}
		return vals;
	}
	
	// given 3.4, returns 2.5, 3.5, 4.5, for example
	private static Vector<Double> getNearbyPointFiveValues(double x0, double tol) {
		int xStart = (int) Math.floor(x0 - tol) - 1;
		int xEnd = (int) Math.ceil(x0 + tol) + 1;
		Vector<Double> vals = new Vector<Double>();
		for(int x=xStart; x<xEnd; x++) {
			if(Math.abs(x + 0.5 - x0) <= tol) vals.add(x + 0.5);
		}
		return vals;
	}
	
	private static MyPoint[] getBoundingBoxSnappedToPixelCentres(MyPoint[] oldBB) {
		double x1 = oldBB[0].x, y1 = oldBB[0].y, x2 = oldBB[1].x, y2 = oldBB[1].y;
		double[] xs = snapInterval(x1, x2);
		double[] ys = snapInterval(y1, y2);
		MyPoint[] bb = {new MyPoint(xs[0], ys[0]), new MyPoint(xs[1], ys[1])};
		return bb;
	}
	
	private static double[] snapInterval(double v1, double v2) {
		double[] newVals = {v1, v2};
		double centre = 0.5*(v1 + v2);
		if(isInt(v1)) {
			if(isInt(v2)) {
				newVals[0] = Math.round(v1) + 0.5;
				newVals[1] = Math.round(v2) + 0.5;				
			}
			else {
				newVals[0] = Math.round(v1) + 0.5;
				newVals[1] = MyMath.round(v2+0.5, centre) - 0.5;
			}
		}
		else {
			if(isInt(v2)) {
				newVals[0] = MyMath.round(v1-0.5, centre) + 0.5;
				newVals[1] = Math.round(v2) + 0.5;				
			}
			else {
				newVals[0] = MyMath.round(v1-0.5, centre) + 0.5;
				newVals[1] = MyMath.round(v2+0.5, centre) - 0.5;
			}
		}
		//System.out.println("("+v1 + ", " + v2 + ") --> (" + newVals[0] + ", " + newVals[1] + ")");
		return newVals;
	}
	
	private static double scaleValueByInterval(double val, double[] oldInt, double[] newInt) {
		double r = (val - oldInt[0])/(oldInt[1] - oldInt[0]);		
		double newVal = r * (newInt[1]-newInt[0]) + newInt[0];
		//System.out.println(oldInt[0] + ", " + oldInt[1] + " --> " + newInt[0] + ", " + newInt[1]);
		//System.out.println(val + ", " + r + ", " + newVal);
		//System.out.println(val + ", " + newVal);
		return newVal;
	}
	
	private static Path scaleValues(Path path, double[] oldInt, double[] newInt, char dir) {
		Path newPath = path.clone();
		for(int i=0; i<path.getNumSegments(); i++) {
			Segment seg = path.getSegment(i).clone();
			MyPoint p2 = seg.getPoint().clone();
			MyPoint p1 = p2.plus(seg.getHandleIn());
			MyPoint p3 = p2.plus(seg.getHandleOut());
			MyPoint[] p = {p1, p2, p3};
			for(int j=0; j<3; j++) {
				if(dir == 'x') {
					if(MyMath.inRange(p[j].x, oldInt[0]-0.1, oldInt[1]+0.1)) {
						double newVal = scaleValueByInterval(p[j].x, oldInt, newInt);
						p[j].x = newVal;
						
					}
				}
				else {
					if(MyMath.inRange(p[j].y, oldInt[0]-0.1, oldInt[1]+0.1)) {
						double newVal = scaleValueByInterval(p[j].y, oldInt, newInt);
						p[j].y = newVal;
					}
				}
			}
			newPath.set(i, new Segment(p[1], p[0].minus(p[1]), p[2].minus(p[1])));
		}
		return newPath;
	}
	
	public static Path snapShapeByBoundingBox(Path path){
		MyPoint[] oldBB = path.getBoundingBox();
		MyPoint[] newBB = getBoundingBoxSnappedToPixelCentres(oldBB);
		double bbWidth = Math.abs(newBB[1].x - newBB[0].x);
		double bbHeight = Math.abs(newBB[1].y - newBB[0].y);
		Path newPath = new Path();
		if(bbWidth < 0.1 || bbHeight < 0.1) {
			newPath = new Path.Line(newBB[0], newBB[1]);
		}
		else {
			AffineTransform atBB = getBoundingBoxRescaleTransform(oldBB, newBB);
			newPath = path.getTransformedCopy(atBB);
		}
		return newPath;
	}
	
	public static AffineTransform getBoundingBoxRescaleTransform(MyPoint[] oldBB, MyPoint[] newBB) {
		double 	oldCentreX = 0.5*(oldBB[0].x+oldBB[1].x),
				oldCentreY = 0.5*(oldBB[0].y+oldBB[1].y),
				newCentreX = 0.5*(newBB[0].x+newBB[1].x),
				newCentreY = 0.5*(newBB[0].y+newBB[1].y),
				oldW = 0.5*(oldBB[1].x-oldBB[0].x),
				oldH = 0.5*(oldBB[1].y-oldBB[0].y),
				newW = 0.5*(newBB[1].x-newBB[0].x),
				newH = 0.5*(newBB[1].y-newBB[0].y);
		AffineTransform at = AffineTransform.getTranslateInstance(0, 0);
		at.translate(newCentreX, newCentreY);
		at.scale(newW/oldW, newH/oldH);
		if(oldW==0|| oldH==0) System.out.println(oldW + ", " + oldH);
		at.translate(-oldCentreX, -oldCentreY);
		return at;
	}
}
