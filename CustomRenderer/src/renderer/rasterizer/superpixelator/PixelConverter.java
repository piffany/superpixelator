package renderer.rasterizer.superpixelator;

import renderer.geometry.MyPoint;

import java.util.Vector;

import renderer.util.MyMath;

public class PixelConverter {

	public static Vector<MyPoint> convertPathToPixels(Vector<MyPoint> corners) {
		Vector<MyPoint> points = new Vector<MyPoint>();
		for(int i=1; i<corners.size(); i++) {
			MyPoint p1 = corners.get(i-1), p2 = corners.get(i);
			if(p1.x==p2.x || p1.y==p2.y) continue;
			int x1, y1, x2, y2;
			if(p1.x < p2.x) { x1 = (int) Math.round(p1.x); x2 = (int) Math.round(p2.x)-1; }
			else { x1 = (int) Math.round(p1.x)-1; x2 = (int) Math.round(p2.x); }
			if(p1.y < p2.y) { y1 = (int) Math.round(p1.y); y2 = (int) Math.round(p2.y)-1; }
			else { y1 = (int) Math.round(p1.y)-1; y2 = (int) Math.round(p2.y); }

			if(x1==x2 && y1==y2) {
				points.add(new MyPoint(x1,y1));
			}
			else if(x1==x2) {
				int sy = (y1 < y2) ? 1 : -1;
				for(int y = y1; MyMath.inRange(y, y1, y2); y += sy) {
					points.add(new MyPoint(x1,y));
				}
			}
			else if(y1==y2) {
				int sx = (x1 < x2) ? 1 : -1;
				for(int x = x1; MyMath.inRange(x, x1, x2); x += sx) {
					points.add(new MyPoint(x,y1));
				}
			}
			else {
				//assert(false);
				int sx = (x1 < x2) ? 1 : -1;
				int sy = (y1 < y2) ? 1 : -1;
				for(int x = x1; MyMath.inRange(x, x1, x2); x += sx) {
					for(int y = y1; MyMath.inRange(y, y1, y2); y += sy) {
						points.add(new MyPoint(x,y));
					}
				}
			}
		}
		return points;
	}

}
