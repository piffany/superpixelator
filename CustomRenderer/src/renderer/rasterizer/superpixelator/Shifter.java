package renderer.rasterizer.superpixelator;

import renderer.geometry.MyPoint;
import renderer.geometry.Path;
import renderer.geometry.Segment;

import java.awt.geom.AffineTransform;
import java.util.Vector;


public class Shifter {

	public static Vector<Path> shiftEndpointsToPixelCentresWRTCentre(Vector<Path> paths,
			boolean shift, boolean shiftHandles, MyPoint centre) {
		if(shift) {
			Vector<Path> shiftedPaths = new Vector<Path>();		
			for(int i=0; i<paths.size(); i++) {
				shiftedPaths.add(paths.get(i).clone());
				shiftEndpointsToPixelCentresWRTCentre(shiftedPaths.get(i), shiftHandles, centre);
			}
			fixTangentSlopes(paths, shiftedPaths);
			
			return shiftedPaths;
		}
		else return paths;
	}
	
	private static boolean sameDirection(MyPoint v1, MyPoint v2) {
		return Math.abs(v1.x*v2.y - v1.y*v2.x) < 0.01;
	}
	
	private static void fixTangentSlopes(Vector<Path> paths, Vector<Path> shiftedPaths) {
		for(int i=0; i<paths.size()-1; i++) {
			Path path1 = paths.get(i).clone();
			Path path2 = paths.get(i+1).clone();
			MyPoint v1 = path1.getLastSegment().clone().getHandleIn();
			MyPoint v2 = path2.getFirstSegment().clone().getHandleOut().times(-1);
			Path spath1 = shiftedPaths.get(i).clone();
			Path spath2 = shiftedPaths.get(i+1).clone();
			Segment seg1 = spath1.getLastSegment().clone();
			Segment seg2 = spath2.getFirstSegment().clone();
			MyPoint sv1 = seg1.getHandleIn();
			MyPoint sv2 = seg2.getHandleOut().times(-1);
			if(sameDirection(v1, v2) && !sameDirection(sv1, sv2)) {
				MyPoint w = sv1.plus(sv2);
				MyPoint w1 = w.times(sv1.length() / w.length());
				MyPoint w2 = w.times(sv2.length() / w.length()).times(-1);
				seg1.setHandleIn(w1);
				seg2.setHandleOut(w2);
				spath1.set(spath1.getNumSegments()-1, seg1);
				spath2.set(0, seg2);
				shiftedPaths.set(i, spath1);
				shiftedPaths.set(i+1, spath2);
				//System.out.println(v1 + ", " + v2 + " : " + sv1 + ", " + sv2 + ", " + w1 + ", " + w2);
			}
		}
	}
	
	// find intersection of p1 + v1 and p2 + v2
	private static MyPoint getIntersection(MyPoint p1, MyPoint v1, MyPoint p2, MyPoint v2) {
		// p1 + C * v1 = p2 + D * v2
		// C * v1 - D * v2 = (p2-p1)
		//  C * v1.x * v2.y - D * v2.x * v2.y = (p2.x-p1.x)* v2.y
		// -C * v1.y * v2.x + D * v2.y * v2.x = (p1.y-p2.y)* v2.x
		// C * (v1.x * v2.y - v1.y * v2.x) = (p2.x-p1.x)* v2.y + (p1.y-p2.y)* v2.x
		// C = ((p2.x-p1.x)* v2.y + (p1.y-p2.y)* v2.x) / (v1.x * v2.y - v1.y * v2.x)
		// D = (p1.x + C * v1.x - p2.x) / v2.x
		double Cdenom = v1.x * v2.y - v1.y * v2.x;
		double Cnumer = (p2.x-p1.x)* v2.y + (p1.y-p2.y)* v2.x;
		if(Cdenom==0) {
			System.out.println(p1 + ", " + v1 + ", " + p2 + ", " + v2);
			assert(false);
		}
		double C = Cnumer / Cdenom;
		return p1.add(v1.times(C));
	}
	
	public static void shiftEndpointsToPixelCentresWRTCentre(Path path, boolean shiftHandles, MyPoint centre) {
		if(path.isEmpty()) return;
		MyPoint oldP1 = path.getSegment(0).getPoint();
		MyPoint newP1 = oldP1.getClosestPixelCentreWRTCentre(centre);
		if(Math.abs(oldP1.x-centre.x)<0.01) newP1.x = centre.x;
		if(Math.abs(oldP1.y-centre.y)<0.01) newP1.y = centre.y;
		MyPoint oldP2 = path.getSegment(path.getNumSegments()-1).getPoint();
		MyPoint newP2 = oldP2.getClosestPixelCentreWRTCentre(centre);
		if(Math.abs(oldP2.x-centre.x)<0.01) newP2.x = centre.x;
		if(Math.abs(oldP2.y-centre.y)<0.01) newP2.y = centre.y;
		shiftEndpoints(path, newP1, newP2, shiftHandles);
		//System.out.println(oldP1 + ", " + newP1 + ", " + centre);
	}
	
	public static AffineTransform directionalScale(MyPoint from, MyPoint vec, double scale) {
		AffineTransform at = AffineTransform.getTranslateInstance(0,0);
		at.translate(from.x, from.y);
		at.rotate(vec.x, vec.y);
		at.scale(scale, 0);
		at.rotate(-vec.x, -vec.y);
		at.translate(-from.x, -from.y);
		return at;
	}
	
	public static void shiftEndpoints(Path path, MyPoint newP1, MyPoint newP2, boolean shiftHandles) {
		if(path.isEmpty()) return;
		if(shiftHandles) { 
			
			MyPoint oldP1 = path.getSegment(0).getPoint();
			MyPoint oldP2 = path.getSegment(path.getNumSegments()-1).getPoint();
			double oldWidth = oldP2.x - oldP1.x, newWidth = newP2.x - newP1.x;
			double oldHeight = oldP2.y - oldP1.y, newHeight = newP2.y - newP1.y;
			AffineTransform at = AffineTransform.getTranslateInstance(0,0);
			double ratioWidth = (oldWidth == 0) ? 1 : newWidth/oldWidth;
			double ratioHeight = (oldHeight == 0) ? 1 : newHeight/oldHeight;
			// transforms are applied in reverse order
			at.translate(newP1.x, newP1.y);
			at.scale(ratioWidth, ratioHeight);
			at.translate(-oldP1.x, -oldP1.y);
			path.transform(at);
			
			/*
			MyPoint oldP1 = path.getSegment(0).getPoint();
			MyPoint oldV1 = path.getSegment(0).getHandleOut();
			MyPoint oldP2 = path.getSegment(path.getNumSegments()-1).getPoint();
			MyPoint oldV2 = path.getSegment(path.getNumSegments()-1).getHandleIn();
			AffineTransform at = AffineTransform.getTranslateInstance(0,0);
			if(oldV1.isOrigin() || oldV2.isOrigin()) {
				at.translate(newP1.x, newP1.y);
				at.translate(-oldP1.x, -oldP1.y);
				path.transform(at);
			}
			else {
				MyPoint oldMid = getIntersection(oldP1, oldV1, oldP2, oldV2);
				MyPoint newMid = getIntersection(newP1, oldV1, newP2, oldV2);
				double scale1 = (newP1.minus(newMid).length())/(oldP1.minus(oldMid).length()); 
				double scale2 = (newP2.minus(newMid).length())/(oldP2.minus(oldMid).length());
				System.out.println(scale1 + ", " + scale2);
				AffineTransform at1 = directionalScale(new MyPoint(0,0), oldV1.times(-1), scale1);
				AffineTransform at2 = directionalScale(new MyPoint(0,0), oldV2.times(-1), scale2);
				at.translate(newP1.x, newP1.y);
				at.concatenate(at1);
				//at.concatenate(at2);
				at.translate(-oldP1.x, -oldP1.y);
				path.transform(at);
			}
			*/
		}
		else {
			Segment seg1 = path.getSegment(0).clone();
			Segment seg2 = path.getLastSegment().clone();
			seg1.setPoint(newP1);
			seg2.setPoint(newP2);
			path.set(0, seg1);
			path.set(path.getNumSegments()-1, seg2);
		}
	}
}
