package renderer.rasterizer.superpixelator;

import renderer.geometry.MyPoint;
import renderer.geometry.Segment;

public class FlaggedSegment extends Segment {
	
	private boolean flag = false;
	
	public void setFlag(boolean b) {
		flag = b;
	}
	
	public boolean getFlag() {
		return flag;
	}

	public FlaggedSegment() {
		super();
	}

	public FlaggedSegment(double x, double y, double inX, double inY,
			double outX, double outY) {
		super(x, y, inX, inY, outX, outY);
	}

	public FlaggedSegment(MyPoint point, MyPoint handleIn, MyPoint handleOut) {
		super(point, handleIn, handleOut);
	}

	public FlaggedSegment(Segment c) {
		super(c);
	}
	
	public FlaggedSegment(FlaggedSegment c) {
		super(c);
		flag = c.getFlag();
	}
	
	public String toString() {
		String s = super.toString();
		s += ", flag = " + flag;
		return s;
	}
}
