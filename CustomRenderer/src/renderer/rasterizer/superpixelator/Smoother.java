package renderer.rasterizer.superpixelator;

import renderer.geometry.MyPoint;
import renderer.util.MyMath;

import java.util.Vector;


public class Smoother {

	private static void reverseVector(Vector<MyPoint> points) {
		Vector<MyPoint> points2 = new Vector<MyPoint>();
		for(int i=0; i<points.size(); i++) {
			points2.add(points.get(points.size()-1-i).clone());
		}
		for(int i=0; i<points.size(); i++) {
			points.set(i, points2.get(i).clone());
		}
	}
	
	private static MyPoint nextSeg(MyPoint seg) {
		assert(Math.abs(seg.x)==1 || Math.abs(seg.y)==1);
		MyPoint nextSeg = new MyPoint(Math.abs(seg.x), Math.abs(seg.y));
		double sgnX = MyMath.sgn(seg.x);
		double sgnY = MyMath.sgn(seg.y);
		if(Math.abs(seg.x)==1) {
			nextSeg.y++;
		}
		else {
			nextSeg.x--;
		}
		nextSeg.x *= sgnX;
		nextSeg.y *= sgnY;
		return nextSeg;
	}
	
	private static MyPoint prevSeg(MyPoint seg) {
		assert(Math.abs(seg.x)==1 || Math.abs(seg.y)==1);
		MyPoint prevSeg = new MyPoint(Math.abs(seg.x), Math.abs(seg.y));
		double sgnX = MyMath.sgn(seg.x);
		double sgnY = MyMath.sgn(seg.y);
		if(Math.abs(seg.y)==1) {
			prevSeg.x++;
		}
		else {
			prevSeg.y--;
		}
		prevSeg.x *= sgnX;
		prevSeg.y *= sgnY;
		return prevSeg;
	}
	
	private static MyPoint[] getNewSegs(MyPoint seg1, MyPoint seg2) {
		double absSlope1 = Math.abs(seg1.y) / Math.abs(seg1.x);
		double absSlope2 = Math.abs(seg2.y) / Math.abs(seg2.x);
		MyPoint[] newSegs = {seg1.clone(), seg2.clone()};
		if(absSlope1 < absSlope2) {
			MyPoint newSeg1 = nextSeg(seg1);
			MyPoint newSeg2 = prevSeg(seg2);
			double slope1 = Math.abs(newSeg1.y) / Math.abs(newSeg1.x);
			double slope2 = Math.abs(newSeg2.y) / Math.abs(newSeg2.x);
			if(slope1 <= slope2) {
				newSegs[0] = newSeg1;
				newSegs[1] = newSeg2;
			}
			else {
				boolean slopeOne1 = Math.abs(seg1.x)==1 && Math.abs(seg1.y)==1;
				boolean slopeOne2 = Math.abs(seg2.x)==1 && Math.abs(seg2.y)==1;
				boolean slopeTwo1 = Math.abs(seg1.x)==2 || Math.abs(seg1.y)==2;
				boolean slopeTwo2 = Math.abs(seg2.x)==2 || Math.abs(seg2.y)==2;
				if((slopeOne1 && slopeTwo2) || (slopeOne2 && slopeTwo1)) {
					newSegs[0] = seg2.clone();
					newSegs[1] = seg1.clone();
				}
			}
		}
		else if(absSlope1 > absSlope2){
			MyPoint newSeg1 = prevSeg(seg1);
			MyPoint newSeg2 = nextSeg(seg2);
			double slope1 = Math.abs(newSeg1.y) / Math.abs(newSeg1.x);
			double slope2 = Math.abs(newSeg2.y) / Math.abs(newSeg2.x);
			if(slope1 >= slope2) {
				newSegs[0] = newSeg1;
				newSegs[1] = newSeg2;
			}
			else {
				boolean slopeOne1 = Math.abs(seg1.x)==1 && Math.abs(seg1.y)==1;
				boolean slopeOne2 = Math.abs(seg2.x)==1 && Math.abs(seg2.y)==1;
				boolean slopeTwo1 = Math.abs(seg1.x)==2 || Math.abs(seg1.y)==2;
				boolean slopeTwo2 = Math.abs(seg2.x)==2 || Math.abs(seg2.y)==2;
				if((slopeOne1 && slopeTwo2) || (slopeOne2 && slopeTwo1)) {
					newSegs[0] = seg2.clone();
					newSegs[1] = seg1.clone();
				}
			}
		}
		return newSegs;
	}
	
	// remove corners associatd with +-1 slope segments based on deviation from original curve
	public static Vector<MyPoint> smoothOutCorners(Vector<MyPoint> points, Vector<MyPoint> samples) {
		//System.out.println("smooth");
		//reverseVector(points);
		// simple swap
		Vector<MyPoint> segments = new Vector<MyPoint>();
		for(int i=0; i<points.size()-1; i++) segments.add(points.get(i+1).minus(points.get(i)));
		// 0 = none swappable, 0.5 = all swappable
		//Vector<Boolean> swappable = getSwappableSegmentsByProportion(segments, 0.5);

		for(int i=0; i<segments.size()-1; i++) {
			MyPoint seg1 = segments.get(i), seg2 = segments.get(i+1);
			boolean slopeOne1 = Math.abs(seg1.x)==1 && Math.abs(seg1.y)==1;
			boolean slopeOne2 = Math.abs(seg2.x)==1 && Math.abs(seg2.y)==1;
			boolean oneSlopeOne = (slopeOne1 && !slopeOne2) || (!slopeOne1 && slopeOne2);
			boolean diffSlope = !(seg1.x==seg2.x && seg1.y==seg2.y);
			// swap
			if(oneSlopeOne && diffSlope) {
					MyPoint[] newSegs = getNewSegs(seg1, seg2);
					//if(seg1.y+seg2.y != newSegs[0].y+newSegs[1].y) System.out.println(seg1 + ", " + seg2 + ", " + newSegs[0] + ", " + newSegs[1]);
					MyPoint oldCorner = points.get(i+1);
					//MyPoint newCorner = points.get(i).add(seg2);
					MyPoint newCorner = points.get(i).add(newSegs[0]);
					
					// old spans and new spans
					MyPoint p1 = points.get(i).clone();
					MyPoint p2 = p1.add(seg1);
					MyPoint p3 = p2.add(seg2);
					MyPoint newP2 = p1.add(newSegs[0]);
					
					//double distBefore = Sorter2.getSpanError(p1, p2, samples) + Sorter2.getSpanError(p2, p3, samples);
					//double distAfter = Sorter2.getSpanError(p1, newP2, samples) + Sorter2.getSpanError(newP2, p3, samples);
					//double nBefore = Sorter2.getSpanLength(p1, p2) + Sorter2.getSpanLength(p2, p3);
					//double nAfter = Sorter2.getSpanLength(p1, newP2) + Sorter2.getSpanLength(newP2, p3);
					//distBefore /= nBefore;
					//distAfter /= nAfter;
					
					double oldError = Approximator.getMinDist(oldCorner, samples);
					double newError = Approximator.getMinDist(newCorner, samples);
					//System.out.println(seg1 + ", " + seg2 + ", " + newSegs[0] + ", " + newSegs[1]);
					//System.out.println(oldError + ", " + newError);
					if(newError < oldError) {
						/*
						MyPoint tempSeg1 = seg1.clone(), tempSeg2 = seg2.clone();
						segments.set(i, tempSeg2);
						segments.set(i+1, tempSeg1);
						*/
						segments.set(i, newSegs[0]);
						segments.set(i+1, newSegs[1]);
						i++;
					}
			}
		}
		
		Vector<MyPoint> newPoints = new Vector<MyPoint>();
		for(int i=0; i<segments.size(); i++) {
			if(i==0) newPoints.add(points.get(0));
			newPoints.add(newPoints.get(newPoints.size()-1).add(segments.get(i)));
		}
		//reverseVector(points);
		//reverseVector(newPoints);
		
		//Vector<MyPoint> newPoints2 = smoothSlopeHalfToSlopeTwo(newPoints, samples);
		
		return newPoints;
	}
	
	// remove corners associatd with +-1 slope segments based on deviation from original curve
	public static Vector<MyPoint> smoothSlopeHalfToSlopeTwo(Vector<MyPoint> points, Vector<MyPoint> samples) {
		//System.out.println("smooth");
		//reverseVector(points);
		// simple swap
		Vector<MyPoint> segments = new Vector<MyPoint>();
		for(int i=0; i<points.size()-1; i++) segments.add(points.get(i+1).minus(points.get(i)));
		// 0 = none swappable, 0.5 = all swappable
		//Vector<Boolean> swappable = getSwappableSegmentsByProportion(segments, 0.5);
		for(int i=0; i<segments.size()-1; i++) {
			//if(!(swappable.get(i) && swappable.get(i+1))) {
				//System.out.println(swappable.get(i) + ", " + swappable.get(i+1));
				//continue;
			//}
			MyPoint seg1 = segments.get(i), seg2 = segments.get(i+1);
			//if(!(seg1.x==seg2.x) && !(seg1.y==seg2.y)) System.out.println(seg1 + ", " + seg2);
			//boolean slopeOne1 = Math.abs(seg1.x)==1 && Math.abs(seg1.y)==1;
			//boolean slopeOne2 = Math.abs(seg2.x)==1 && Math.abs(seg2.y)==1;
			//boolean oneSlopeOne = (slopeOne1 && !slopeOne2) || (!slopeOne1 && slopeOne2);
			//boolean diffSlope = !(seg1.x==seg2.x && seg1.y==seg2.y);
			// swap
			// special case
			//System.out.println(points.size() + ", " + segments.size() + ", " + (i+2));

			if(	(Math.abs(seg1.x)==2 && Math.abs(seg1.y)==1 && Math.abs(seg2.x)==1 && Math.abs(seg2.y)==2) ||
				(Math.abs(seg1.x)==1 && Math.abs(seg1.y)==2 && Math.abs(seg2.x)==2 && Math.abs(seg2.y)==1)) {
				double sgnX = MyMath.sgn(seg1.x);
				double sgnY = MyMath.sgn(seg1.y);
				MyPoint newSeg = new MyPoint(sgnX, sgnY);
				//MyPoint oldCorner = points.get(i+1);
				//MyPoint newCorner1 = points.get(i).add(newSeg);
				//MyPoint newCorner2 = newCorner1.add(newSeg);
				
				MyPoint p1 = points.get(i).clone();
				MyPoint p2 = p1.add(seg1);
				MyPoint p3 = p2.add(seg2);
				MyPoint p2a = p1.add(newSeg);
				MyPoint p2b = p2a.add(newSeg);
				
				double oldError = Sorter2.getSpanError(p1, p2, samples) + Sorter2.getSpanError(p2, p3, samples);
				double newError = Sorter2.getSpanError(p1, p2a, samples) +
								  Sorter2.getSpanError(p2a, p2b, samples) + 
								  Sorter2.getSpanError(p2b, p3, samples);
				oldError /= 4.0; // 4 pixels involved
				newError /= 3.0; // 3 pixels involved
				//System.out.println(oldError + ", " + newError);
				
				//double oldError = Approximator.getMinDist(oldCorner, samples);
				//double newError = Math.min(	Approximator.getMinDist(newCorner1, samples),
				//							Approximator.getMinDist(newCorner2, samples));
				//System.out.println(oldError + ", " + newError);
				if(newError < oldError) {
					//MyPoint[] newSegs = {newSeg, newSeg, newSeg};
					segments.set(i, newSeg);
					segments.set(i+1, newSeg);
					segments.add(i, newSeg);
					i++;
				}
			}
		}
		
		Vector<MyPoint> newPoints = new Vector<MyPoint>();
		for(int i=0; i<segments.size(); i++) {
			if(i==0) newPoints.add(points.get(0));
			newPoints.add(newPoints.get(newPoints.size()-1).add(segments.get(i)));
		}
		//reverseVector(points);
		//reverseVector(newPoints);
		return newPoints;
	}
	
	// non-swappable segments are those surrounding by k segments of the same slope on either side
	private static Vector<Boolean> getSwappableSegments(Vector<MyPoint> segments, int k) {
		Vector<Integer> sameSlopeBefore = new Vector<Integer>();
		Vector<Integer> sameSlopeAfter = new Vector<Integer>();
		int sameBefore = 0, sameAfter = 0;
		for(int i=0; i<segments.size(); i++) {
			sameSlopeBefore.add(sameBefore);
			sameSlopeAfter.add(sameAfter);
			if(i==0 || segments.get(i).distance(segments.get(i-1)) == 0) sameBefore++;
			else sameBefore = 0;
			if(i==segments.size()-1 || segments.get(i).distance(segments.get(i+1)) == 0) sameAfter++;
			else sameAfter = 0;
		}
		Vector<Boolean> swappable = new Vector<Boolean>();
		for(int i=0; i<segments.size(); i++) {
			swappable.add(sameSlopeBefore.get(i)<k && sameSlopeAfter.get(i)<k);
		}
		return swappable;
	}

	// swappable segments are the r segments at either side for each slope segments
	// e.g. r = 1/3 means the first 1/3 and last 1/3 are swappable
	private static Vector<Boolean> getSwappableSegmentsByProportion(Vector<MyPoint> segments, double r) {
		Vector<Integer> slopeLengths = new Vector<Integer>();
		Vector<Boolean> slopeChanges = new Vector<Boolean>(); 
		int slopeLength = 0;
		boolean slopeChange = false;
		for(int i=0; i<segments.size(); i++) {
			slopeLength++;
			if((i>0 && segments.get(i).distance(segments.get(i-1)) != 0) || i==segments.size()-1) {
				for(int j=0; j<slopeLength; j++) {
					slopeLengths.add(slopeLength);
					slopeChanges.add(slopeChange);
				}
				slopeLength = 0;
				slopeChange = !slopeChange;
			}
		}
		Vector<Boolean> swappable = new Vector<Boolean>();
		int slopeInd = 0;
		r = Math.max(Math.min(r,0.5),0);
		for(int i=0; i<segments.size(); i++) {
			if(i>0 && slopeChanges.get(i)!=slopeChanges.get(i-1)) slopeInd = 0;
			double min = r*(double)slopeLengths.get(i);
			double max = (1-r)*(double)slopeLengths.get(i);
			double val = ((double)slopeInd+1) / ((double)slopeLengths.get(i));
			//boolean inRange = max <= slopeInd || min >= slopeInd+1;
			//boolean inRange = (0 <= slopeInd && slopeInd <= min) ||
			//				  (max <= slopeInd && slopeInd <= slopeLengths.get(i));
			boolean inRange = (0 <= val && val <= r) || (1-r <= val && val <= 1);
			if(inRange) swappable.add(true);
			else {
				//System.out.println(r + ", " + (1-r) + ", " + val);
				swappable.add(false);
			}
			slopeInd++;
		}
		return swappable;
	}
}
