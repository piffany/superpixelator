package renderer.rasterizer.superpixelator;

import renderer.geometry.MyCurve;
import renderer.geometry.MyPoint;
import renderer.geometry.Path;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Vector;

import renderer.display.MyGraphics2D;
import renderer.geometry.Pixel;
import renderer.rasterizer.Rasterizer;
import renderer.util.MyMath;
import colours.Colour;

public class Antialiaser {

	private static Vector<Pixel> defaultAntialias(int w, int h, renderer.geometry.Path path) {
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		MyGraphics2D g = new MyGraphics2D(image);
		g.setRasterizerType(Rasterizer.DEFAULT);
		g.setAA(true);
		g.setStrokeWidth(1);
		g.setColor(Color.black);
		g.drawPath(path);
		Vector<Pixel> pixelsAA = getNonTransparentPixels(image);
		return pixelsAA;
	}
	
	public static Vector<Pixel> getPixelationWithAA(MyGraphics2D g,
			Path path, boolean shifted, boolean sorted) {
		if(path.isEmpty()) return new Vector<Pixel>();
		Vector<Path> preAAPaths = SuperpixelatorHelper.getPreAAPaths(path, shifted, sorted);
		Path compoundPath = makeCompoundPath(preAAPaths);
		MyPoint[] bb = getImageBoundingBox(path);
		MyPoint offset = bb[0];
		int imWidth = (int) (bb[1].x - bb[0].x);
		int imHeight = (int) (bb[1].y - bb[0].y);
		int[][] alphas = new int[imWidth][imHeight];
		
		boolean hasNonAAPath = hasNonAAPath(preAAPaths);
		double thickness = hasNonAAPath ? 0.5 : 0.75;
		
		for(int i=0; i<preAAPaths.size(); i++) {
			//System.out.println(i + " : " + preAAPaths.get(i));
			//System.out.println(i);
			Vector<Pixel> pixels = getPixelationWithAA(preAAPaths.get(i), compoundPath, thickness);
			for(int j=0; j<pixels.size(); j++) {
				Pixel p = pixels.get(j);
				int x = (int) (p.x - offset.x);
				int y = (int) (p.y - offset.y);
				int alpha = p.getAlpha();
				try {
					if(alpha > alphas[x][y]) alphas[x][y] = alpha; // take max opacity
				}
				catch(Exception e) {
					//System.out.println(j);
				}
			}
		}
		Vector<Pixel> pixelsAA = new Vector<Pixel>();
		for(int i=0; i<imWidth; i++) {
			for(int j=0; j<imHeight; j++) {
				pixelsAA.add(new Pixel(offset.x + i, offset.y + j, alphas[i][j]));
			}
		}
		return pixelsAA;
	}
		
	public static Vector<Pixel> getFilledPixelsWithAA(MyGraphics2D g,
			Path path, boolean shifted, boolean sorted) {
		if(path.isEmpty()) return new Vector<Pixel>();
		Vector<Path> preAAPaths = SuperpixelatorHelper.getPreAAPaths(path, shifted, sorted);
		Path compoundPath = makeCompoundPath(preAAPaths);
		MyPoint[] bb = getImageBoundingBox(path);
		MyPoint offset = bb[0];
		int imWidth = (int) (bb[1].x - bb[0].x);
		int imHeight = (int) (bb[1].y - bb[0].y);
		int[][] alphas = new int[imWidth][imHeight];
		
		double k = 4;	
		
		Vector<Pixel> stroke = getPixelationWithAA(g, path, shifted, sorted);
		BufferedImage inside = getAutoAAFillImage(compoundPath, compoundPath, bb, (int) k, (int) k, 0);
		
		for(int i=0; i<stroke.size(); i++) {
			Pixel p = stroke.get(i);
			int x = (int) Math.round(p.x - bb[0].x);
			int y = (int) Math.round(p.y - bb[0].y);
			alphas[x][y] = p.getAlpha(); 
		}
		for(int i=0; i<imWidth; i++) {
			for(int j=0; j<imHeight; j++) {
				int insideAlpha = Colour.getAlpha(inside.getRGB(i, j));
				if(insideAlpha >= 128) {
					alphas[i][j] = 255;
				}
			}
		}
		/*
		double[] opacities = getUniformlyDistributedOpacities(5);
		BufferedImage discretized = discretizeAlphas(inside, opacitiesToAlphas(opacities));
		for(int i=0; i<imWidth; i++) {
			for(int j=0; j<imHeight; j++) {
				int insideAlpha = Colour.getAlpha(discretized.getRGB(i, j));
				alphas[i][j] = insideAlpha;
			}
		}
		*/
		Vector<Pixel> pixelsAA = new Vector<Pixel>();
		for(int i=0; i<imWidth; i++) {
			for(int j=0; j<imHeight; j++) {
				pixelsAA.add(new Pixel(offset.x + i, offset.y + j, alphas[i][j]));
			}
		}
		return pixelsAA;
	}
	
	private static Path makeCompoundPath(Vector<Path> paths) {
		Path path = new Path();
		for(int i=0; i<paths.size(); i++) {
			Vector<MyCurve> curves = paths.get(i).getCurves();
			for(int j=0; j<curves.size(); j++) {
				path.addCurve(curves.get(j).clone());
			}
		}
		return path;
	}
	
	private static MyPoint[] getImageBoundingBox(Path path) {
		MyPoint[] bb = path.getBoundingBox();
		//System.out.println(imWidth + ", " + imHeight);
		//int imWidth = (int) Math.ceil(bb[1].x - bb[0].x + 10);
		//int imHeight = (int) Math.ceil(bb[1].y - bb[0].y + 10);
		bb[0].x = (int) Math.floor(bb[0].x - 5);
		bb[0].y = (int) Math.floor(bb[0].y - 5);
		bb[1].x = (int) Math.floor(bb[1].x + 5);
		bb[1].y = (int) Math.floor(bb[1].y + 5);
		//int imWidth = (int) Math.ceil(bb[1].x - bb[0].x);
		//int imHeight = (int) Math.ceil(bb[1].y - bb[0].y);
		//bb[1].x = bb[0].x + imWidth;
		//bb[1].y = bb[0].y + imHeight;
		return bb;
	}
	
	private static double[] getUniformlyDistributedOpacities(int n) {
		double[] opacities = new double[n];
		for(int i=0; i<n; i++) {
			opacities[i] = (double)i / (double)(n-1);
		}
		return opacities;
	}
	
	public static Vector<Pixel> antialiasBasicPath(Path path, Path compoundPath, double thickness) {
		//System.out.println(path);
		MyPoint[] bb = getImageBoundingBox(path);
		//System.out.println(path);
		//System.out.println(bb[0] + "," + bb[1]);
		MyPoint offset = bb[0];
		//int imWidth = (int) (bb[1].x - bb[0].x);
		//int imHeight = (int) (bb[1].y - bb[0].y);
		//Path offsetPath = path.getTransformedCopy(
		//		AffineTransform.getTranslateInstance(-offset.x, -offset.y));
		//Path offsetCompoundPath = compoundPath.getTransformedCopy(
		//		AffineTransform.getTranslateInstance(-offset.x, -offset.y));
		double k = 4.0;
		double tol = 0.1;
		//BufferedImage image5x = getDefaultAAImage(imWidth, imHeight,
			//					offsetPath, new Path(), k);
		//System.out.println(path);
		//System.out.println(offsetPath);
		//offsetPath.removeSegment(0);
		//BufferedImage image5x = getDefaultAAImage(k*imWidth, k*imHeight,
		//							offsetPath.getScaledCopy(k), offsetCompoundPath.getScaledCopy(k), k);
		//BufferedImage image1x = downsampleOpacities(image5x, k, tol);
		double[] opacities = getUniformlyDistributedOpacities(5);
		//double strokeRatio = 1;
		//int strokeW = (int)Math.round(k*strokeRatio);
		
		//BufferedImage imageAutoAA = getAutoAAImage(path, compoundPath, bb, 4, 4, tol);
		//BufferedImage scaled = logisticScale(imageAutoAA, 1);
		
		
		//BufferedImage imageAutoAA = getAutoAAImage(path, compoundPath, bb, 16, 8, tol);
		BufferedImage imageAutoAA = getAutoAAStrokeImage(path, compoundPath, bb, (int) k, (int) (k*thickness), tol);
		//BufferedImage scaled = multiplyOpacity(imageAutoAA, 2);
		
		BufferedImage scaled1 = scaleOpacity(imageAutoAA, 1);
		//BufferedImage scaled2 = scaleOpacity(scaled1, 1);
		
		int[] alphas = opacitiesToAlphas(opacities);
		BufferedImage discretized = discretizeAlphas(scaled1, alphas);
		BufferedImage rescaled1 = fixDiscretizedAlphas(discretized, 1, alphas);
		
		Vector<Pixel> pixelsAA = getNonTransparentPixels(rescaled1);
		/*
		Pixel p = pixelsAA.get(0);
		double minX = p.x, maxX = p.x, minY = p.y, maxY = p.y;
		for(int i=0; i<pixelsAA.size(); i++) {
			p = pixelsAA.get(i);
			if(p.x < minX) minX = p.x;
			if(p.x > maxX) maxX = p.x;
			if(p.y < minY) minY = p.y;
			if(p.y > maxY) maxY = p.y;
		}
		//System.out.println((maxX - minX) + " ::: " + (maxY - minY));
		System.out.println("("+(minX+offset.x) + ", " + (maxX+offset.x) + ") x (" +
							(minY+offset.y) + ", " + (maxY+offset.y) + ")");
		*/
		for(int i=0; i<pixelsAA.size(); i++) {
			Pixel pix = pixelsAA.get(i);
			MyPoint pos = pix.getPoint();
			pixelsAA.set(i, new Pixel(pos.add(offset), pix.getAlpha()));
			//if(pos.add(offset).x > 190) System.out.println(pos.add(offset));
		}
		return pixelsAA;
	}
	
	public static boolean hasNonAAPath(Vector<Path> paths) {
		for(int i=0; i<paths.size(); i++) {
			Path path = paths.get(i);
			Vector<MyPoint> pixelatedPathNoAA = SuperpixelatorHelper.getPixelatedPath(path, true, false);
			if(pixelatedPathNoAA.isEmpty()) continue;
			MyPoint slope = pixelPathIsStraight(pixelatedPathNoAA);
			if(slope!=null && (Math.abs(slope.x) == 1 && Math.abs(slope.y) == 1)) return true;
		}
		return false;
	}
	
	public static Vector<Pixel> getPixelationWithAA(Path path, Path compoundPath, double thickness) {
		Vector<MyPoint> pixelatedPathNoAA = SuperpixelatorHelper.getPixelatedPath(path, true, false);
		if(pixelatedPathNoAA.isEmpty()) return new Vector<Pixel>();
		
		
		/*MyPoint[] bb = getImageBoundingBox(path);
		MyPoint offset = bb[0];
		BufferedImage imageAutoAA = getAutoAAImage(path, compoundPath, bb, 8, 8, 0);
		Vector<Pixel> pixelsAA = getNonTransparentPixels(imageAutoAA);
		for(int i=0; i<pixelsAA.size(); i++) {
			Pixel pix = pixelsAA.get(i);
			MyPoint pos = pix.getPoint();
			pixelsAA.set(i, new Pixel(pos.add(offset), pix.getAlpha()));
			//if(pos.add(offset).x > 190) System.out.println(pos.add(offset));
		}
		*/
		//System.out.println(path);
		//System.out.println("straight = " + pixelPathIsStraight(pixelatedPathNoAA));
		MyPoint slope = pixelPathIsStraight(pixelatedPathNoAA);
		if(slope==null || (Math.abs(slope.x) > 1 || Math.abs(slope.y) > 1)) {
			Vector<Pixel> pixelsAA = antialiasBasicPath(path, compoundPath, thickness);
			return pixelsAA;
		}
		else {			
			Vector<MyPoint> pixelPosNoAA = PixelConverter.convertPathToPixels(pixelatedPathNoAA);
			Vector<Pixel> pixelsNoAA = Pixel.pointsToPixels(pixelPosNoAA);
			return pixelsNoAA;
		}
		//System.out.println(pixelsAA.size());
		//return pixelsNoAA;
	}
	
	private static MyPoint pixelPathIsStraight(Vector<MyPoint> corners) {
		MyPoint slope = null;
		for(int i=1; i<corners.size(); i++) {
			MyPoint span = corners.get(i).minus(corners.get(i-1));
			int x = (int) Math.round(span.x);
			int y = (int) Math.round(span.y);
			if(slope==null) {
				slope = new MyPoint(x,y);
			}
			else if(slope.x!=x || slope.y!=y) {
				return null;
			}
		}
		return slope;
	}

	private static BufferedImage getAutoAAStrokeImage(Path path, Path compoundPath, MyPoint[] bb, int k, double thick, double tol) {
		MyPoint offset = bb[0];
		int imWidth = (int) (bb[1].x - bb[0].x);
		int imHeight = (int) (bb[1].y - bb[0].y);
		Path offsetPath = path.getTransformedCopy(
				AffineTransform.getTranslateInstance(-offset.x, -offset.y));
		Path offsetCompoundPath = compoundPath.getTransformedCopy(
				AffineTransform.getTranslateInstance(-offset.x, -offset.y));
		BufferedImage imageBig = getDefaultAAStrokeImage(k*imWidth, k*imHeight,
				offsetPath.getScaledCopy(k), offsetCompoundPath.getScaledCopy(k), thick);
		BufferedImage imageSmall = downsampleOpacities(imageBig, k, tol);
		/*
		double n = (double) k / (double) thick;
		for(int x=0; x<imWidth; x++) {
			for(int y=0; y<imHeight; y++) {
				int totalAlpha = Colour.rgbInt2Color(imageSmall.getRGB(x, y)).getAlpha();
				int newAlpha = (int) Math.round(totalAlpha * n);
				newAlpha = Math.max(0, Math.min(255, newAlpha));
				imageSmall.setRGB(x, y, new Color(0,0,0,newAlpha).getRGB());
			}
		}
		*/
		return imageSmall;
	}
	
	private static BufferedImage getAutoAAFillImage(Path path, Path compoundPath, MyPoint[] bb, int k, double thick, double tol) {
		MyPoint offset = bb[0];
		int imWidth = (int) (bb[1].x - bb[0].x);
		int imHeight = (int) (bb[1].y - bb[0].y);
		Path offsetPath = path.getTransformedCopy(
				AffineTransform.getTranslateInstance(-offset.x, -offset.y));
		Path offsetCompoundPath = compoundPath.getTransformedCopy(
				AffineTransform.getTranslateInstance(-offset.x, -offset.y));
		BufferedImage imageBig = getDefaultAAFillImage(k*imWidth, k*imHeight,
				offsetPath.getScaledCopy(k), offsetCompoundPath.getScaledCopy(k), thick);
		BufferedImage imageSmall = downsampleOpacities(imageBig, k, tol);
		return imageSmall;
	}
	
	private static int[] opacitiesToAlphas(double[] opacities) {
		int[] alphas = new int[opacities.length];
		for(int i=0; i<opacities.length; i++) {
			int alpha = (int) Math.round(opacities[i]*255.0);
			alpha = Math.min(255, Math.max(0, alpha));
			alphas[i] = alpha;
		}
		return alphas;
	}
	
	public static BufferedImage getBinaryMask(BufferedImage image, double tol) {
		int w = image.getWidth(), h = image.getHeight();
		BufferedImage mask = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int totalAlpha = Colour.rgbInt2Color(image.getRGB(x, y)).getAlpha();
				//Color c = Colour.rgbInt2Color(image.getRGB(x,y));
				//if(totalAlpha < 10) System.out.println(tol*225.0 + ", " + totalAlpha + ", " + c);
				int newAlpha = (totalAlpha <= tol*255.0) ? 0 : 255;
				mask.setRGB(x, y, new Color(0,0,0,newAlpha).getRGB());
			}
		}
		return mask;
	}
	
	public static BufferedImage applyMask(BufferedImage image, BufferedImage mask) {
		int w = mask.getWidth(), h = mask.getHeight();
		assert(w <= image.getWidth() && h <= image.getHeight());
		BufferedImage trimmed = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int alpha = Colour.rgbInt2Color(mask.getRGB(x, y)).getAlpha();
				if(alpha > 128) trimmed.setRGB(x, y, image.getRGB(x,y));
			}
		}
		return trimmed;
	}
	
	public static BufferedImage downsampleOpacities(BufferedImage largeImage, int k, double tol) {
		int W = largeImage.getWidth(), H = largeImage.getHeight();
		assert(W%k==0 && H%k==0);
		int w = W/k, h = H/k;
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<w; x++) {
			for(int y=0; y<h; y++) {
				int totalAlpha = 0;
				for(int i=x*k; i<(x+1)*k; i++) {
					for(int j=y*k; j<(y+1)*k; j++) {
						totalAlpha += Colour.rgbInt2Color(largeImage.getRGB(i, j)).getAlpha();
						//System.out.println(Colour.rgbInt2Color(largeImage.getRGB(i, j)).getAlpha());
					}
				}
				int meanAlpha = (int) Math.round((double)totalAlpha/(double)(k*k));
				if(meanAlpha < tol*255.0) {
					image.setRGB(x, y, new Color(0,0,0,0).getRGB());
				}
				else {
					if(meanAlpha>255) meanAlpha=255;
					if(meanAlpha>0) image.setRGB(x, y, new Color(0,0,0,meanAlpha).getRGB());
				}
			}
		}
		return image;
	}
	
	public static BufferedImage discretizeAlphas(BufferedImage image, int[] alphas) {
		int width = image.getWidth(), height = image.getHeight();
		BufferedImage discretized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(image.getRGB(x, y)).getAlpha();
				int newAlpha = alphas[MyMath.getClosestMatch(alpha, alphas)];
				Color c = new Color(0, 0, 0, newAlpha);
				discretized.setRGB(x, y, c.getRGB());
			}
		}
		return discretized;
	}
	
	public static BufferedImage fixDiscretizedAlphas(BufferedImage clipped, double strokeWidth, int[] alphas) {
		double[] rowSums = getTotalOpacity("row", clipped);
		double[] colSums = getTotalOpacity("col", clipped);
		int width = clipped.getWidth(), height = clipped.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				Color c = new Color(0, 0, 0, alpha);
				image.setRGB(x, y, c.getRGB());
			}
		}
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				if(alpha > 0) {
					double rowSum = rowSums[y];
					double colSum = colSums[x];
					double smallerSum = Math.min(rowSum, colSum);
					double diff = strokeWidth - smallerSum;
					if(Math.abs(diff) > 0.01) {
						int alpha2 = (int) Math.round(alpha + diff*255.0);
						int newAlpha = alphas[MyMath.getClosestMatch(alpha2, alphas)];
						double opacityDiff = (double)(newAlpha-alpha)/255.0;
						Color c = new Color(0, 0, 0, newAlpha);
						image.setRGB(x, y, c.getRGB());
						rowSums[y] += opacityDiff;
						colSums[x] += opacityDiff;
					}
				}
			}
		}
		return image;
	}
	
	public static BufferedImage fixDiscretizedAlphas2(BufferedImage clipped, double strokeWidth, int[] alphas) {
		double[] rowSums = getTotalOpacity("row", clipped);
		double[] colSums = getTotalOpacity("col", clipped);
		double[] rowPixels = getTotalPixels("row", clipped);
		double[] colPixels = getTotalPixels("col", clipped);
		int width = clipped.getWidth(), height = clipped.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				Color c = new Color(0, 0, 0, alpha);
				image.setRGB(x, y, c.getRGB());
			}
		}
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				if(alpha > 0) {
					double rowSum = rowSums[y];
					double colSum = colSums[x];
					// apparent thickness
					double appThick = rowSum*colSum/(Math.sqrt(rowSum*rowSum+colSum*colSum));
					// actual thickness
					double actThick = Math.min(rowPixels[y], colPixels[x]);
					//System.out.println(actThick);
					double thickness = (appThick*Math.pow(actThick,0.5));
					//double ratio = strokeWidth/thickness;
					//double opacity = ((double)alpha) / 255.0;
					//double newOpacity = opacity*ratio;
					//double smallerSum = Math.min(rowSum, colSum);
					//System.out.println(thickness);
					double diff = strokeWidth - thickness;
					if(Math.abs(diff) > 0.01) {
						int alpha2 = (int) Math.round(alpha + diff*255.0);
						int newAlpha = alphas[MyMath.getClosestMatch(alpha2, alphas)];
						double opacityDiff = (double)(newAlpha-alpha)/255.0;
						Color c = new Color(0, 0, 0, newAlpha);
						image.setRGB(x, y, c.getRGB());
						rowSums[y] += opacityDiff;
						colSums[x] += opacityDiff;
					}
				}
			}
		}
		return image;
	}
	
	public static BufferedImage scaleOpacity(BufferedImage clipped, double strokeWidth) {
		double[] rowSums = getTotalOpacity("row", clipped);
		double[] colSums = getTotalOpacity("col", clipped);
		int width = clipped.getWidth(), height = clipped.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				if(alpha > 0) {
					double rowSum = rowSums[y];
					double colSum = colSums[x];
					double ratio = (rowSum < colSum) ? strokeWidth/rowSum : strokeWidth/colSum;
					double opacity = ((double)alpha) / 255.0;
					double newOpacity = opacity*ratio;
					if(newOpacity < 0) newOpacity = 0;
					else if(newOpacity > 1) newOpacity = 1;
					int newAlpha = (int) Math.round(newOpacity*255);
					Color c = new Color(0, 0, 0, newAlpha);
					image.setRGB(x, y, c.getRGB());
				}
			}
		}
		return image;
	}
	
	public static BufferedImage scaleOpacity2(BufferedImage clipped, double strokeWidth) {
		double[] rowSums = getTotalOpacity("row", clipped);
		double[] colSums = getTotalOpacity("col", clipped);
		double[] rowPixels = getTotalPixels("row", clipped);
		double[] colPixels = getTotalPixels("col", clipped);
		int width = clipped.getWidth(), height = clipped.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				if(alpha > 0) {
					double rowSum = rowSums[y];
					double colSum = colSums[x];
					// apparent thickness
					double appThick = rowSum*colSum/(Math.sqrt(rowSum*rowSum+colSum*colSum));
					// actual thickness
					double actThick = Math.min(rowPixels[y], colPixels[x]);
					//System.out.println(actThick);
					double ratio = strokeWidth/(appThick*Math.pow(actThick,0.4));
					double opacity = ((double)alpha) / 255.0;
					double newOpacity = opacity*ratio;
					if(newOpacity < 0) newOpacity = 0;
					else if(newOpacity > 1) newOpacity = 1;
					int newAlpha = (int) Math.round(newOpacity*255);
					Color c = new Color(0, 0, 0, newAlpha);
					image.setRGB(x, y, c.getRGB());
				}
			}
		}
		return image;
	}
	
	public static BufferedImage multiplyOpacity(BufferedImage clipped, double mult) {
		int width = clipped.getWidth(), height = clipped.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				if(alpha > 0) {
					double opacity = ((double)alpha) / 255.0;
					double newOpacity = opacity*mult;
					//System.out.println(opacity + " x " + mult + " = " + newOpacity);
					if(newOpacity < 0) newOpacity = 0;
					else if(newOpacity > 1) newOpacity = 1;
					int newAlpha = (int) Math.round(newOpacity*255);
					Color c = new Color(0, 0, 0, newAlpha);
					image.setRGB(x, y, c.getRGB());
				}
			}
		}
		return image;
	}
	
	public static BufferedImage logisticScale(BufferedImage clipped, double strokeWidth) {
		int width = clipped.getWidth(), height = clipped.getHeight();
		double[][] opacities = new double[width][height];
		// contrast
		double C = 80;
		double F = (double)(259*(C+255))/(double)(255*(259-C));		

		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				if(alpha > 0) {
					//double opacity = ((double)alpha) / 255.0;
					double newAlpha = F*(double)(alpha - 128) + 128;
					opacities[x][y] = newAlpha/255.0;
					//System.out.println(opacities[x][y]);
					//opacities[x][y] = Math.pow(opacity, 2);
					// logistic
					//opacities[x][y] = 1.0/(1+Math.exp(-12*(opacity-0.5)));
					//opacities[x][y] = Math.pow(opacities[x][y], 0.5);
					//System.out.println(opacity + " --> " + opacities[x][y]);
					
				}
			}
		}
		double[] rowSums = getTotalOpacity("row", opacities);
		double[] colSums = getTotalOpacity("col", opacities);
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<width; x++) {
			for(int y=0; y<height; y++) {
				//int alpha = Colour.rgbInt2Color(clipped.getRGB(x, y)).getAlpha();
				//if(alpha > 0) {
				if(opacities[x][y] > 0) {
					double rowSum = rowSums[y];
					double colSum = colSums[x];
					double ratio = (rowSum < colSum) ? strokeWidth/rowSum : strokeWidth/colSum;
					//double opacity = ((double)alpha) / 255.0;
					double newOpacity = opacities[x][y]*ratio;
					//System.out.println(opacity + " x " + mult + " = " + newOpacity);
					if(newOpacity < 0) newOpacity = 0;
					else if(newOpacity > 1) newOpacity = 1;
					int newAlpha = (int) Math.round(newOpacity*255);
					Color c = new Color(0, 0, 0, newAlpha);
					image.setRGB(x, y, c.getRGB());
				}
			}
		}
		return image;
	}
	
	private static double[] getTotalOpacity(String dir, BufferedImage image) {
		int width = image.getWidth(), height = image.getHeight();
		if(dir == "col") {
			double[] sums = new double[width];
			for(int x=0; x<width; x++) {
				double sum = 0;
				for(int y=0; y<height; y++) {
					int alpha = Colour.rgbInt2Color(image.getRGB(x, y)).getAlpha();
					double opacity = ((double)alpha) / 255.0;
					sum += opacity;
				}
				sums[x] = sum;
			}
			return sums;
		}
		else {
			double[] sums = new double[height];
			for(int y=0; y<height; y++) {
				double sum = 0;
				for(int x=0; x<width; x++) {
					int alpha = Colour.rgbInt2Color(image.getRGB(x, y)).getAlpha();
					double opacity = ((double)alpha) / 255.0;
					sum += opacity;
				}
				sums[y] = sum;
			}
			return sums;
		}
	}
	
	private static double[] getTotalPixels(String dir, BufferedImage image) {
		int width = image.getWidth(), height = image.getHeight();
		if(dir == "col") {
			double[] sums = new double[width];
			for(int x=0; x<width; x++) {
				double sum = 0;
				for(int y=0; y<height; y++) {
					int alpha = Colour.rgbInt2Color(image.getRGB(x, y)).getAlpha();
					if(alpha > 0) sum += 1;
				}
				sums[x] = sum;
			}
			return sums;
		}
		else {
			double[] sums = new double[height];
			for(int y=0; y<height; y++) {
				double sum = 0;
				for(int x=0; x<width; x++) {
					int alpha = Colour.rgbInt2Color(image.getRGB(x, y)).getAlpha();
					if(alpha > 0) sum += 1;
				}
				sums[y] = sum;
			}
			return sums;
		}
	}
	
	private static double[] getTotalOpacity(String dir, double[][] opacities) {
		int width = opacities.length, height = (width==0) ? 0 : opacities[0].length;
		if(dir == "col") {
			double[] sums = new double[width];
			for(int x=0; x<width; x++) {
				double sum = 0;
				for(int y=0; y<height; y++) {
					sum += opacities[x][y];
				}
				sums[x] = sum;
			}
			return sums;
		}
		else {
			double[] sums = new double[height];
			for(int y=0; y<height; y++) {
				double sum = 0;
				for(int x=0; x<width; x++) {
					sum += opacities[x][y];
				}
				sums[y] = sum;
			}
			return sums;
		}
	}
	
	public static Vector<Pixel> getCentrePathPixels(Path path) {
		Vector<Pixel> pixels = SuperpixelatorHelper.getPixelation(path, false, false);
		return pixels;
	}
	
	public static BufferedImage applyClippingMask(int w, int h, BufferedImage pixels, BufferedImage mask) {
		BufferedImage clipped = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		for(int x=0; x<pixels.getWidth(); x++) {
			for(int y=0; y<pixels.getHeight(); y++) {
				int rgb = (pixels).getRGB(x, y);
				int alpha = Colour.rgbInt2Color(rgb).getAlpha();
				Color maskColor = Colour.rgbInt2Color(mask.getRGB(x, y));
				if(maskColor.getAlpha()>0) {
					(clipped).setRGB(x, y, new Color(0,0,0,alpha).getRGB());
				}
			}
		}
		return clipped;
	}
	
	public static BufferedImage getDefaultAAStrokeImage(int w, int h, Path path, Path compoundPath, double strokeWidth) {
		/*
		BufferedImage image1 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		MyGraphics2D g1 = new MyGraphics2D(image1);
		g1.setRasterizerType(Rasterizer.DEFAULT);
		g1.setAA(true);
		//g1.setStrokeWidth(strokeWidth);
		g1.setStroke(new BasicStroke((float) strokeWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g1.setColor(Color.black);
		g1.drawPath(compoundPath);
		*/
		
		BufferedImage image2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		MyGraphics2D g2 = new MyGraphics2D(image2);
		g2.setRasterizerType(Rasterizer.DEFAULT);
		g2.setAA(true);
		//g2.setStrokeWidth(strokeWidth);
		g2.setStroke(new BasicStroke((float) strokeWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g2.setColor(Color.black);
		g2.drawPath(path);
		/*
		int transparentRGB = (new Color(0,0,0,0)).getRGB();
		for(int i=0; i<image2.getWidth(); i++) {
			for(int j=0; j<image2.getHeight(); j++) {
				double alpha2 = Colour.getAlpha(image2.getRGB(i,j));
				if(alpha2 > 0) {
					double alpha1 = Colour.getAlpha(image1.getRGB(i,j));
					if(alpha1!=alpha2) {
						image2.setRGB(i, j, transparentRGB);
					}
				}
			}
		}*/
		
		return image2;
	}
	
	public static BufferedImage getDefaultAAFillImage(int w, int h, Path path, Path compoundPath, double strokeWidth) {
		BufferedImage image2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		MyGraphics2D g2 = new MyGraphics2D(image2);
		g2.setRasterizerType(Rasterizer.DEFAULT);
		g2.setAA(true);
		g2.setStrokeWidth(strokeWidth);
		g2.setColor(Color.black);
		g2.fillPath(path);
		return image2;
	}
	
	public static BufferedImage getPixelsIntersected(int w, int h, Path path) {
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		MyGraphics2D g2 = new MyGraphics2D(image);
		g2.setRasterizerType(Rasterizer.DEFAULT);
		g2.setColor(Color.black);
		g2.setStrokeWidth(5);
		g2.drawPath(path);
		Vector<Pixel> points = getNonTransparentPixels(image);
		BufferedImage intersected = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Vector<MyPoint> samples = Approximator.getSamplePoints(path, 100);
		for(int i=0; i<points.size(); i++) {
			Pixel p = points.get(i);
			p.setLocation(p.x + 0.5, p.y + 0.5);
			MyPoint q = Approximator.getClosestPoint(p, samples);
			if(Math.abs(q.x-p.x) < 0.499 && Math.abs(q.y-p.y) < 0.499) {
				//intersected.add(p.clone());
				(intersected).setRGB(
						(int)p.x, (int)p.y, new Color(0, 0, 0, 255).getRGB());
			
			}
		}
		return intersected;
	}
	
	public static Vector<Pixel> getNonTransparentPixels(BufferedImage image) {
		Vector<Pixel> pixels = new Vector<Pixel>();
		for(int i=0; i<image.getWidth(); i++) {
			for(int j=0; j<image.getHeight(); j++) {
				Color c = Colour.rgbInt2Color(image.getRGB(i, j));
				//if(c.getRGB() != Color.black.getRGB())	{
				if(c.getAlpha() > 0) {
					Pixel pixel = new Pixel(i, j, c.getAlpha());
					pixels.add(pixel);
				}
			}
		}
		return pixels;
	}
}
