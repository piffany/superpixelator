package renderer.rasterizer;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;



public class Util {

	public static Image setAlpha(BufferedImage image, int alpha) {
		if(alpha == 255) return image;
		float[] scales = { 1f, 1f, 1f, (float)alpha/255.0f };
		float[] offsets = new float[4];
		RescaleOp rop = new RescaleOp(scales, offsets, null);
		Image newImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) newImage.getGraphics();
		g2.drawImage((BufferedImage)image, rop, 0, 0);
		return newImage;	
	}
}
