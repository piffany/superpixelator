package renderer.geometry;
import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Point2D;
import java.util.Vector;

import renderer.util.MyMath;
import renderer.util.Transform;



//cubic bezier curve
@SuppressWarnings("serial")
public class MyCurve extends CubicCurve2D.Double {

	public MyCurve() {
		super();
	}

	public MyCurve(double x0, double y0, double x1, double y1,
			double x2, double y2, double x3, double y3) {
		super(x0, y0, x1, y1, x2, y2, x3, y3);
	}

	// quadratic bezier
	public MyCurve(double x0, double y0, double x1, double y1, double x2, double y2) {
		super(x0, y0,
			  (2.0/3.0)*x0 + (1.0/3.0)*x1, (2.0/3.0)*y0 + (1.0/3.0)*y1,
			  (2.0/3.0)*x2 + (1.0/3.0)*x1, (2.0/3.0)*y2 + (1.0/3.0)*y1,
			  x2, y2);
	}

	public MyCurve(MyPoint p0, MyPoint p1, MyPoint p2, MyPoint p3) {
		super(p0.getX(), p0.getY(), p1.getX(), p1.getY(),
			  p2.getX(), p2.getY(), p3.getX(), p3.getY());
	}
	
	// quadratic bezier
	public MyCurve(MyPoint p0, MyPoint p1, MyPoint p2) {
		this(p0.getX(), p0.getY(), p1.getX(), p1.getY(), p2.getX(), p2.getY());
	}

	
	public String toString() {
		String s = "Curve: ";
		s += "p0 = " + getP0().toString() + ", ";
		s += "p1 = " + getP1().toString() + ", ";
		s += "p2 = " + getP2().toString() + ", ";
		s += "p3 = " + getP3().toString();
		return s;
	}
	public MyCurve(MyCurve c) {
		this(c.getP0(), c.getP1(), c.getP2(), c.getP3());
	}
	
	public MyCurve clone() {
		return new MyCurve(this);
	}
	
	public MyPoint getP0() {
		Point2D p = super.getP1();
		return new MyPoint(p.getX(), p.getY());
	}
	
	public void setP0(MyPoint p) {
		super.x1 = p.getX();
		super.y1 = p.getY();
	}

	public MyPoint getP1() {
		Point2D p = super.getCtrlP1();
		return new MyPoint(p.getX(), p.getY());
	}

	public void setP1(MyPoint p) {
		super.ctrlx1 = p.getX();
		super.ctrly1 = p.getY();
	}

	public MyPoint getP2() {
		Point2D p = super.getCtrlP2();
		return new MyPoint(p.getX(), p.getY());
	}

	public void setP2(MyPoint p) {
		super.ctrlx2 = p.getX();
		super.ctrly2 = p.getY();
	}

	public MyPoint getP3() {
		Point2D p = super.getP2();
		return new MyPoint(p.getX(), p.getY());
	}
	
	public void setP3(MyPoint p) {
		super.x2 = p.getX();
		super.y2 = p.getY();
	}
	
	public MyPoint getHandle1() {
		return getP1().minus(getP0());
	}
	
	public MyPoint getHandle2() {
		return getP2().minus(getP3());
	}

	private boolean validT(double t) {
		return 0<=t && t<=1;
	}
	
	// extract the curve from parameter t1 to t2 (0 <= t1 < t2 <= 1)
	public MyCurve getSubCurve(double t1, double t2) {
		assert(validT(t1) && validT(t2) && t1<t2);
		// split at t1 first
		MyPoint P0 = getP0(), P1 = getP1(), P2 = getP2(), P3 = getP3();
		MyPoint P01 = (P0.times(1-t1)).plus(P1.times(t1));
		MyPoint P12 = (P1.times(1-t1)).plus(P2.times(t1));
		MyPoint P23 = (P2.times(1-t1)).plus(P3.times(t1));
		MyPoint P012 = (P01.times(1-t1)).plus(P12.times(t1));
		MyPoint P123 = (P12.times(1-t1)).plus(P23.times(t1));
		MyPoint P0123 = (P012.times(1-t1)).plus(P123.times(t1));
		// two split curves
		//MyCurve part1 = new MyCurve(P0, P01, P012, P0123);
		MyCurve part2 = new MyCurve(P0123, P123, P23, P3);
		// then split part2 at t2 (adjusted)
		double newT2 = MyMath.rescaleToInterval(t2, t1, 1);
		MyPoint P0_ = part2.getP0(), P1_ = part2.getP1(), P2_ = part2.getP2(), P3_ = part2.getP3();
		MyPoint P01_ = (P0_.times(1-newT2)).plus(P1_.times(newT2));
		MyPoint P12_ = (P1_.times(1-newT2)).plus(P2_.times(newT2));
		MyPoint P23_ = (P2_.times(1-newT2)).plus(P3_.times(newT2));
		MyPoint P012_ = (P01_.times(1-newT2)).plus(P12_.times(newT2));
		MyPoint P123_ = (P12_.times(1-newT2)).plus(P23_.times(newT2));
		MyPoint P0123_ = (P012_.times(1-newT2)).plus(P123_.times(newT2));
		// two split curves
		MyCurve part1_ = new MyCurve(P0_, P01_, P012_, P0123_);
		//MyCurve part2_ = new MyCurve(P0123_, P123_, P23_, P3_);
		// return part1 of part2
		return part1_;
	}
	
	public void transform(AffineTransform at) {
		setP0(Transform.transformPoint(getP0(), at));
		setP1(Transform.transformPoint(getP1(), at));
		setP2(Transform.transformPoint(getP2(), at));
		setP3(Transform.transformPoint(getP3(), at));
	}
	
	public MyCurve transformedCopy(AffineTransform at) {
		MyCurve copy = clone();
		copy.transform(at);
		return copy;	
	}
	
	// return symmetric segment with point P0 and handleOut P1-P0
	public Segment getSegment1() {
		MyPoint P0 = getP0(), P1 = getP1();
		return new Segment(P0, P0.minus(P1), P1.minus(P0));
	}
	
	// return symmetric segment with point P3 and handleIn P2-P3
	public Segment getSegment2() {
		MyPoint P2 = getP2(), P3 = getP3();
		return new Segment(P3, P2.minus(P3), P3.minus(P2));
	}

	//www.caffeineowl.com/graphics/2d/vectorial/cubic-inflexion.html
	public Vector<java.lang.Double> tValuesAtInflectionPoints() {
		MyPoint P1 = getP0(), C1 = getP1(), C2 = getP2(), P2 = getP3();
		MyPoint a = C1.minus(P1);
		MyPoint b = C2.minus(C1).minus(a);
		MyPoint c = P2.minus(C2).minus(a).minus(b.times(2));
		// quadratic coefficients
		java.lang.Double coeff2 = b.x*c.y - b.y*c.x;
		java.lang.Double coeff1 = a.x*c.y - a.y*c.x;
		java.lang.Double coeff0 = a.x*b.y - a.y*b.x;
		// get roots
		Vector<java.lang.Double> roots = MyMath.getQuadraticRoots(coeff2, coeff1, coeff0);
		for(int i=roots.size()-1; i>=0; i--) {
			java.lang.Double root = roots.get(i);
			if(!(root>0 && root<1)) roots.remove(i);
		}
		return roots;
	}
	
	// output = {topleft, bottomright}
	public MyPoint[] getBoundingBox() {
		Vector<java.lang.Double> txs = tValuesAtLocalMinMax('x');
		Vector<java.lang.Double> tys = tValuesAtLocalMinMax('y');
		Vector<java.lang.Double> ts = new Vector<java.lang.Double>();
		ts.addAll(txs); ts.addAll(tys); ts.add(1.0);
		double xmin = getPoint(0).x, xmax = getPoint(0).x,
			   ymin = getPoint(0).y, ymax = getPoint(0).y;
		for(int i=0; i<ts.size(); i++) {
			MyPoint p = getPoint(ts.get(i));
			if(p.x < xmin) xmin = p.x;
			if(p.x > xmax) xmax = p.x;
			if(p.y < ymin) ymin = p.y;
			if(p.y > ymax) ymax = p.y;
		}
		MyPoint[] bb = {new MyPoint(xmin,ymin), new MyPoint(xmax, ymax)};
		return bb;
	}
	
	public boolean isMinAt0(char dir) {
		double tol = 0.1;
		double v0 = (dir == 'x') ? getP0().x : getP0().y;
		double v1 = (dir == 'x') ? getP1().x : getP1().y;
		double v2 = (dir == 'x') ? getP2().x : getP2().y;
		double v3 = (dir == 'x') ? getP3().x : getP3().y;
		boolean isMin = v0-v1 <= tol && v0-v2 <= tol & v0-v3 <= tol;
		return isMin;
	}
	
	public boolean isMinAt1(char dir) { 
		double tol = 0.1;
		double v0 = (dir == 'x') ? getP0().x : getP0().y;
		double v1 = (dir == 'x') ? getP1().x : getP1().y;
		double v2 = (dir == 'x') ? getP2().x : getP2().y;
		double v3 = (dir == 'x') ? getP3().x : getP3().y;
		boolean isMin = v3-v0 <= tol && v3-v1 <= tol & v3-v2 <= tol;
		return isMin;
	}
	
	public boolean isMaxAt0(char dir) {
		double tol = 0.1;
		double v0 = (dir == 'x') ? getP0().x : getP0().y;
		double v1 = (dir == 'x') ? getP1().x : getP1().y;
		double v2 = (dir == 'x') ? getP2().x : getP2().y;
		double v3 = (dir == 'x') ? getP3().x : getP3().y;
		boolean isMax = v0-v1 >= -tol && v0-v2 >= -tol & v0-v3 >= -tol;
		return isMax;
	}
	
	public boolean isMaxAt1(char dir) {
		double tol = 0.1;
		double v0 = (dir == 'x') ? getP0().x : getP0().y;
		double v1 = (dir == 'x') ? getP1().x : getP1().y;
		double v2 = (dir == 'x') ? getP2().x : getP2().y;
		double v3 = (dir == 'x') ? getP3().x : getP3().y;
		boolean isMax = v3-v0 >= -tol && v3-v1 >= -tol & v3-v2 >= -tol;
		return isMax;
	}
	
	public Vector<java.lang.Double> tValuesAtLocalMinMax(char dir) {
		double v0 = (dir == 'x') ? getP0().x : getP0().y;
		double v1 = (dir == 'x') ? getP1().x : getP1().y;
		double v2 = (dir == 'x') ? getP2().x : getP2().y;
		double v3 = (dir == 'x') ? getP3().x : getP3().y;
		double discrim = v1*v1 + v2*v2 + v0*(-v2+v3) - v1*(v2+v3);
		double top1 = v0 - 2.*v1 + v2;
		double bottom1 = v0 - 3.*v1 + 3.*v2 - v3;
		Vector<java.lang.Double> roots = new Vector<java.lang.Double>();
		if(Math.abs(bottom1) < 0.001) bottom1 = 0;
		if(bottom1 != 0) {
			double root1 = (top1 - Math.sqrt(discrim))/bottom1;
			double root2 = (top1 + Math.sqrt(discrim))/bottom1;
			if(Math.abs(root1) < 0.001) root1 = 0;
			if(Math.abs(root2) < 0.001) root2 = 0;
			if(Math.abs(1-root1) < 0.001) root1 = 1;
			if(Math.abs(1-root2) < 0.001) root2 = 1;
			// one real root
			if(discrim == 0) {
				if(validT(root1)) roots.add(root1);
			}
			// two real roots
			else if(discrim > 0) {
				if(validT(root1)) roots.add(root1);
				if(validT(root2)) roots.add(root2);
			}
		}
		else {
			double top2 = v0 - v1;
			double bottom2 = 2*(v0 - 2*v1 + v2);
			if(Math.abs(bottom2) < 0.001) bottom2 = 0;
			if(bottom2 != 0) {
				double root = top2/bottom2;
				if(Math.abs(root) < 0.001) root = 0;
				if(Math.abs(1-root) < 0.001) root = 1;
				if(validT(root)) roots.add(root);
			}
			else {
				double top3 = 2.*v1 - v2;
				double bottom3 = 3.*(v1 - v2);
				if(Math.abs(bottom3) < 0.001) bottom3 = 0;
				if(bottom3 != 0) {
					double root = top3/bottom3;
					if(Math.abs(root) < 0.001) root = 0;
					if(Math.abs(1-root) < 0.001) root = 1;
					if(validT(root)) roots.add(root);
				}
				else {
					// equation simplifies to v1, has no root
				}
			}
		}
		return roots;
	}
	
	public MyCurve getReverseCurve() {
		MyCurve reverse = new MyCurve(getP3(), getP2(), getP1(), getP0());
		return reverse;
	}

	public boolean isDegenerate() {
		return Math.abs(getFrameLength()) < 0.001;
	}
	
	public boolean isStraight() {
		double lineDist = getP3().minus(getP0()).length();
		boolean straight = Math.abs(lineDist - getFrameLength()) < 0.01;
		return straight;
	}
	
	public double getFrameLength() {
		MyPoint p0 = getP0(), p1 = getP1(), p2 = getP2(), p3 = getP3();
		double length = p0.distance(p1) + p1.distance(p2) + p2.distance(p3);
		return length;
	}
	
	public MyPoint getPoint(double t) {
		assert(validT(t));
		double r = 1-t;
		// B(t) = (1-t)^3*P0 + 3t(1-t)^2*P1 + 3(1-t)t^2*P2 + t^3*P3
		MyPoint A = getP0().times(r*r*r);
		MyPoint B = getP1().times(3*r*r*t);
		MyPoint C = getP2().times(3*r*t*t);
		MyPoint D = getP3().times(t*t*t);
		return A.add(B).add(C).add(D);
	}
	
	public MyPoint getTangent(double t) {
		assert(validT(t));
		// dx/dt = -3(1-t)^2*x0 + 3(1-t)(1-3t)*x1 + 3t(2-3t)*x2 + 3t^2*x3
		double A = -3*(1-t)*(1-t);
		double B = 3*(1-t)*(1-3*t);
		double C = 3*t*(2-3*t);
		double D = 3*t*t;
		double dxdt = A*getP0().x + B*getP1().x + C*getP2().x + D*getP3().x;
		double dydt = A*getP0().y + B*getP1().y + C*getP2().y + D*getP3().y;
		//sSystem.out.println(A + ", " + B + ", " + C + ", " + D + ", " + dxdt + ", " + dydt);
		return new MyPoint(dxdt, dydt);
	}
}