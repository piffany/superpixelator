package renderer.geometry;

import java.awt.geom.AffineTransform;

import renderer.util.Transform;


// http://scriptographer.org/reference/ai/segment/
// The Segment object represents a part of a path which is described
// by the path.segments array. Every segment of a path corresponds to
// an anchor point (anchor points are the path handles that are visible
// when the path is selected).

public class Segment {

	private MyPoint point = new MyPoint(0,0);
	// vectors
	private MyPoint handleIn = new MyPoint(0,0);
	private MyPoint handleOut = new MyPoint(0,0);
	// horizontal or vertical handles (can be null)
	boolean horizontal, vertical;
	//boolean flag = false;
	
	public Segment() {
		updateHorizontalAndVertical();
	}
	
	private void updateHorizontalAndVertical() {
		horizontal = (Math.abs(handleIn.y) < 0.001 && Math.abs(handleOut.y) < 0.001);
		vertical = (Math.abs(handleIn.x) < 0.001 && Math.abs(handleOut.x) < 0.001);
	}
	
	public Segment(MyPoint point, MyPoint handleIn, MyPoint handleOut) {
		assert(point!=null);
		this.point = point.clone();
		if(handleIn != null) setHandleIn(handleIn.clone());
		else removeHandleIn();
		if(handleOut != null) setHandleOut(handleOut.clone());
		else removeHandleOut();
		updateHorizontalAndVertical();
	}
	
	public Segment(double x, double y, double inX, double inY, double outX, double outY) {
		this(new MyPoint(x,y), new MyPoint(inX,inY), new MyPoint(outX,outY));
	}
	
	public Segment getReverseSegment() {
		Segment segment = new Segment(point, handleOut, handleIn);
		return segment;
	}
	
	/*
	public void setFlag(boolean b) {
		flag = b;
	}
	
	public boolean getFlag() {
		return flag;
	}
	*/
	
	public String toString() {
		String s = "Seg: ";
		s += "point = " + point.toString() + ", ";
		s += "handleIn = " + handleIn.toString() + ", ";
		s += "handleOut = " + handleOut.toString() + ", ";
		if(horizontal) s += "(H)";
		if(vertical) s += "(V) ";
		return s;
	}
	
	public MyPoint getPoint() {
		return point.clone();
	}
	
	public MyPoint getHandleIn() {
		return handleIn.clone();
	}
	
	public MyPoint getHandleOut() {
		return handleOut.clone();
	}
	
	public boolean hasSymmetricHandles() {
		boolean symmX = Math.abs(handleIn.x + handleOut.x) < 0.001;
		boolean symmY = Math.abs(handleIn.y + handleOut.y) < 0.001;
		boolean symmetric = symmX && symmY;
		//if(!symmetric) System.out.println(handleIn + " // " + handleOut);
		return symmetric;
	}
	
	public boolean hasNondifferentiableHandles() {
		double x1 = handleIn.x, y1 = handleIn.y, x2 = handleOut.x, y2 = handleOut.y;
		boolean smooth =  Math.abs(x1*y2-x2*y1) < 0.001 && x1*x2 < 0.001 && y1*y2 < 0.001;
		boolean singular = handleIn==null || handleOut==null ||
						  (Math.abs(x1) < 0.001 && Math.abs(y1) < 0.001) ||
						  (Math.abs(x2) < 0.001 && Math.abs(y2) < 0.001);
		boolean nondiff = !smooth || singular;
		return nondiff;
	}
	
	public void setPoint(MyPoint p) {
		point = p.clone();
	}

	public void setHandleIn(MyPoint p) {
		handleIn = (p==null) ? new MyPoint(0,0) : p.clone();
		if(Math.abs(handleIn.x) < 0.001) handleIn.x = 0;
		if(Math.abs(handleIn.y) < 0.001) handleIn.y = 0;
		updateHorizontalAndVertical();
	}
	
	public void removeHandleIn() {
		handleIn = new MyPoint(0,0);
		updateHorizontalAndVertical();
	}

	public void setHandleOut(MyPoint p) {
		handleOut = (p==null) ? new MyPoint(0,0) : p.clone();
		if(Math.abs(handleOut.x) < 0.001) handleOut.x = 0;
		if(Math.abs(handleOut.y) < 0.001) handleOut.y = 0;
		updateHorizontalAndVertical();
	}

	public void removeHandleOut() {
		handleOut = new MyPoint(0,0);
		updateHorizontalAndVertical();
	}

	public void setHorizontal() {
		handleIn.y = 0; handleOut.y = 0;
		horizontal = true;
	}
	
	public void setVertical() {
		handleIn.x = 0; handleOut.x = 0;
		vertical = true;
	}
	
	public boolean isHorizontal() {
		return (Math.abs(handleIn.y) < 0.001 && Math.abs(handleOut.y) < 0.001);
	}
	
	public boolean isVertical() {
		return (Math.abs(handleIn.x) < 0.001 && Math.abs(handleOut.x) < 0.001);
	}
	public Segment(Segment c) {
		this(c.getPoint().clone(), c.getHandleIn().clone(), c.getHandleOut().clone());
		//setFlag(c.getFlag());
	}
	
	public Segment clone() {
		return new Segment(this);
	}

	public void transform(AffineTransform at) {
		setPoint(Transform.transformPoint(getPoint(), at));
		setHandleIn(Transform.transformVector(getHandleIn(), at));
		setHandleOut(Transform.transformVector(getHandleOut(), at));
	}
	
	public Segment transformedCopy(AffineTransform at) {
		Segment copy = clone();
		copy.transform(at);
		return copy;	
	}
	
	public void translate(double dx, double dy) {
		transform(AffineTransform.getTranslateInstance(dx, dy));
	}
	
	public Segment transformedCopy(double dx, double dy) {
		return transformedCopy(AffineTransform.getTranslateInstance(dx, dy));
	}

}
