package renderer.geometry;


import java.awt.geom.Point2D;
import java.util.Comparator;

import renderer.util.MyMath;



@SuppressWarnings("serial")
public class MyPoint extends Point2D.Double {

	public MyPoint() {
		super(0,0);
	}
	
	public MyPoint(double x, double y) {
		super(x,y);
	}
	
	public MyPoint(MyPoint p) {
		super(p.x, p.y);
	}
	
	public MyPoint getClosestPixelCorner() {
		return new MyPoint(Math.round(x), Math.round(y));
	}
	
	public double length() {
		return this.distance(new MyPoint(0,0));
	}
	
	public MyPoint getClosestPixelCentre() {
		double newX = Math.round(x+0.5)-0.5;
		double newY = Math.round(y+0.5)-0.5;
		return new MyPoint(newX, newY);
	}
	
	public MyPoint getClosestPixelCentreWRTCentre(MyPoint centre) {
		double dirX = MyMath.sgn(x - centre.x);
		double dirY = MyMath.sgn(y - centre.y);
		double newX = (dirX > 0) ? (MyMath.roundDown(x-0.5)+0.5)
								 : (MyMath.roundUp(x+0.5)-0.5);
		double newY = (dirY > 0) ? (MyMath.roundDown(y-0.5)+0.5)
				 				 : (MyMath.roundUp(y+0.5)-0.5);
		return new MyPoint(newX, newY);
	}

	public MyPoint getClosestPixelCentre(MyPoint centre) {
		int dirX = ((x < centre.x) || (x==centre.x && y < centre.y)) ? 1 : -1;
		int dirY = ((y < centre.y) || (y==centre.y && x < centre.x)) ? 1 : -1;
		double newX = 0, newY = 0;
		if(x%1 == 0) newX = (dirX < 0) ? x-0.5 : x+0.5;
		else newX = Math.round(x+0.5)-0.5;
		if(y%1 == 0) newY = (dirY < 0) ? y-0.5 : y+0.5;
		else newY = Math.round(y+0.5)-0.5;
		return new MyPoint(newX, newY);
	}
	
	public boolean equals(MyPoint p) {
		return (x == p.x) && (y == p.y);
	}

	public MyPoint clone() {
		return new MyPoint(this);
	}

	public double dot(MyPoint p) {
		double dotProd = p.x*this.x + p.y*this.y;
		return dotProd;
	}
	
	public String toString() {
		String x2 = String.valueOf(MyMath.roundToDec(x, 2));
		String y2 = String.valueOf(MyMath.roundToDec(y, 2));
		return "(" + x2 + "," + y2 + ")";
	}
	
	public boolean isOrigin() {
		return x==0 && y==0;
	}
	
	// get angle of vector; range: [0, 2pi)
	public double getAngle() {
		assert(!isOrigin()); // cannot both be zero
		// y is reversed
		double yy = -y; // reversed y
		if(x == 0) return (yy > 0) ? 0.5*Math.PI : 1.5*Math.PI;
		else if(yy == 0) return (x > 0) ? 0 : Math.PI;
		else {
			double absAngle = Math.abs(Math.atan(yy/x)); // (0, pi/2)
			// Quadrant I
			if(x > 0 && yy > 0) return absAngle;
			// Quadrant II
			else if(x < 0 && yy > 0) return Math.PI - absAngle;
			// Quadrant III
			else if(x < 0 && yy < 0) return absAngle + Math.PI;
			// Quadrant IV
			else return 2*Math.PI - absAngle;
		}
	}
	
	public static Comparator<MyPoint> getSlopeComparator(boolean increasing) {
		if(increasing) {
			return new Comparator<MyPoint>() {
				public int compare(MyPoint p1, MyPoint p2) {
					return MyMath.sgn(p1.y/p1.x-p2.y/p2.x);
				}
			};
		}
		else {
			return new Comparator<MyPoint>() {
				public int compare(MyPoint p1, MyPoint p2) {
					return -MyMath.sgn(p1.y/p1.x-p2.y/p2.x);
				}
			};
		}
	}
	
	// get smallest angle to another vector
	public double getAngleTo(MyPoint p) {
		double angle1 = getAngle();
		double angle2 = p.getAngle();
		if(angle2 < angle1) angle2 += 2*Math.PI;
		double angleTo = angle2 - angle1;
		//System.out.println("A: " + angleTo);
		if(angleTo > Math.PI) {
			//System.out.println(angleTo + ", " + (-(2*Math.PI-angleTo)));
			angleTo = -(2*Math.PI-angleTo);
		}
		if(Math.abs(Math.abs(angleTo) -0.5*Math.PI) < 0.01) angleTo = 0.5*Math.PI;
		return angleTo;
	}

	public MyPoint plus(MyPoint p) {
		return new MyPoint(this.x+p.x, this.y+p.y);
	}
	
	public MyPoint add(MyPoint p) {
		return plus(p);
	}

	public MyPoint minus(MyPoint p) {
		return new MyPoint(this.x-p.x, this.y-p.y);
	}

	public MyPoint subtract(MyPoint p) {
		return minus(p);
	}

	public MyPoint times(double d) {
		return new MyPoint(this.x*d, this.y*d);
	}

	public MyPoint multiply(double d) {
		return times(d);
	}
	
}
