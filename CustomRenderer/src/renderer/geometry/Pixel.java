package renderer.geometry;

import renderer.geometry.MyPoint;

import java.util.Vector;

@SuppressWarnings("serial")
public class Pixel extends MyPoint {

	public static int alphaMax = 255;
	private int alpha; // 0-255
	
	public Pixel() {
		this(alphaMax);
	}
	
	public Pixel(int alpha) {
		this(0, 0, alpha);
	}
	
	public Pixel(MyPoint p) {
		this(p, alphaMax);
	}
	
	public Pixel(MyPoint p, int alpha) {
		this(p.x, p.y, alpha);
	}
	
	public Pixel(double x, double y) {
		this(x, y, alphaMax);
	}

	public Pixel(double x, double y, int alpha) {
		this((int) Math.round(x), (int) Math.round(y), alpha);
	}
	
	public Pixel(int x, int y) {
		this(x, y, alphaMax);
	}
	
	public Pixel(int x, int y, int alpha) {
		super(x,y);
		this.setAlpha(truncateAlpha(alpha));
	}
	
	public Pixel(Pixel p) {
		super(p);
		setAlpha(p.getAlpha());
	}
	
	public Pixel clone() {
		return new Pixel(this);
	}
	
	public MyPoint getPoint() {
		return ((MyPoint) this).clone();
	}
	
	public void setPoint(MyPoint p) {
		super.x = (int) Math.round(x);
		super.y = (int) Math.round(y);
	}
	
	private int truncateAlpha(int alpha) {
		if(alpha < 0) return 0;
		else if(alpha > alphaMax) return alphaMax;
		else return alpha;
	}
	
	public static Vector<Pixel> pointsToPixels(Vector<MyPoint> points) {
		Vector<Pixel> pixels = new Vector<Pixel>();
		for(int i=0; i<points.size(); i++) {
			pixels.add(new Pixel(points.get(i).clone()));
		}
		return pixels;
	}

	public int getAlpha() {
		return alpha;
	}

	public void setAlpha(int alpha) {
		this.alpha = alpha;
	}
	
	public Pixel add(MyPoint p) {
		return new Pixel(this.x+p.x, this.y+p.y, alpha);
	}
}
