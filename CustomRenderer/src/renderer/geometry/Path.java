package renderer.geometry;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.util.Iterator;
import java.util.Vector;

import renderer.util.Transform;


public class Path {

	protected Vector<Segment> segments = new Vector<Segment>();
	protected int windingRule = PathIterator.WIND_EVEN_ODD;

	public Path() {
		segments = new Vector<Segment>();
	}

	public boolean isAcuteAngle(int i) {
		int n = getNumSegments();
		assert(0 <= i && i < n);
		if((i==0 || i==n-1) && !isClosed()) return false;
		else {
			MyCurve c1 = getCurveBeforeSegmentIndex(i);
			MyCurve c2 = getCurveAfterSegmentIndex(i);
			MyPoint tan1 = c1.getTangent(0.99).times(-1);
			MyPoint tan2 = c2.getTangent(0.01);
			//System.out.println(c1+ ", " + c2);
			double angleTo = tan1.getAngleTo(tan2);
			boolean acute = Math.abs(angleTo) <= 0.5*Math.PI;
			//if(acute) System.out.println(tan1 + ", " + tan2 + ", " + angleTo);
			return acute;
		}
	}
	
	public String toString() {
		String s = "Path:";
		Iterator<Segment> itr = segments.iterator();
		while(itr.hasNext()) {
			s += "\n\t" + itr.next().toString();
		}
		s += "\n";
		Vector<MyCurve> myCurves = getCurves();
		Iterator<MyCurve> itr2 = myCurves.iterator();
		while(itr2.hasNext()) {
			s += "\n\t" + itr2.next().toString();
		}
		return s;
	}

	public Path(Path path) {
		Iterator<Segment> itr = path.getSegments().iterator();
		while(itr.hasNext()) {
			segments.add(itr.next().clone());
		}
	}
	
	public Path getSubPath(int segInd1, int segInd2) {
		assert(0<=segInd1 && segInd1<getNumSegments());
		assert(0<=segInd2 && segInd2<getNumSegments());
		if(segInd1 == segInd2) return new Path();
		if(!isClosed()) assert(segInd1<=segInd2);
		Path subPath = new Path();
		if(segInd1 < segInd2) {
			for(int i=segInd1; i<=segInd2; i++) {
				subPath.addSegment(getSegment(i));//.clone());
			}
		}
		else {
			int n = getNumSegments();
			for(int i=segInd1; i<n; i++) {
				subPath.addSegment(getSegment(i));//.clone());
			}
			for(int i=0; i<=segInd2; i++) {
				subPath.addSegment(getSegment(i));//.clone());
			}
		}
		return subPath;
	}
	
	public MyCurve getCurveBeforeSegmentIndex(int i) {
		if(i==0 && !isClosed()) return null;
		int curveInd = (i==0) ? segments.size()-2 : i-1;
		MyCurve curveBefore = getCurve(curveInd);
		return curveBefore;
	}
	
	public MyCurve getCurveAfterSegmentIndex(int i) {
		if(i==0 && !isClosed()) return null;
		int curveInd = (i==segments.size()-1) ? 0 : i;
		MyCurve curveBefore = getCurve(curveInd);
		return curveBefore;
	}


	public MyPoint getTangentInAtSegmentIndex(int i) {
		MyCurve curveBefore = getCurveBeforeSegmentIndex(i);
		if(curveBefore==null) return new MyPoint(0,0);
		MyPoint p0 = curveBefore.getP0(), p1 = curveBefore.getP1(), p2 = curveBefore.getP2();
		MyPoint p = segments.get(i).getPoint();
		MyPoint v0 = p0.minus(p), v1 = p1.minus(p), v2 = p2.minus(p);
		if(v2.length() > 0.001) return v2;
		else if(v1.length() > 0.001) return v1;
		else return v0;
	}
	
	public MyPoint getTangentOutAtSegmentIndex(int i) {
		MyCurve curveAfter = getCurveAfterSegmentIndex(i);
		if(curveAfter==null) return new MyPoint(0,0);
		MyPoint p1 = curveAfter.getP1(), p2 = curveAfter.getP2(), p3 = curveAfter.getP3();
		MyPoint p = segments.get(i).getPoint();
		MyPoint v1 = p1.minus(p), v2 = p2.minus(p), v3 = p3.minus(p);
		if(v1.length() > 0.001) return v1;
		else if(v2.length() > 0.001) return v2;
		else return v3;
		
	}
	
	/*
	public boolean isBetweenLineAndCurve(int i) {
		assert(isValidSegmentIndex(i));
		if(!isClosed() && (i==0 || i==segments.size()-1)) return false;
		MyCurve c1 = getCurveBeforeSegmentIndex(i);
		MyCurve c2 = getCurveAfterSegmentIndex(i);
		int dir1 = Sorter.getSortDirection(c1);
		int dir2 = Sorter.getSortDirection(c2);
		if((dir1==0 && dir2!=0) || (dir1!=0 &&dir2==0)) {
			return true;
		}
		else return false;
	}
	*/
	
	// assumes no degenerate curves in the path
	public boolean isNondifferentiableAtSegmentIndex(int i) {
		assert(isValidSegmentIndex(i));
		if(!isClosed() && (i==0 || i==segments.size()-1)) return true;
		else {
			MyPoint tangentIn = getTangentInAtSegmentIndex(i);
			MyPoint tangentOut = getTangentOutAtSegmentIndex(i);
			double x1 = tangentIn.x, y1 = tangentIn.y, x2 = tangentOut.x, y2 = tangentOut.y;
			boolean smooth =  Math.abs(x1*y2-x2*y1) < 0.001;
			return !smooth;
		}
	}
	
	public boolean isStraight() {
		double frameLength = 0;
		Vector<MyCurve> curves = getCurves();
		for(int i=0; i<curves.size(); i++) {
			frameLength += curves.get(i).getFrameLength();
			//segments.get(i).getPoint().distance(segments.get(i-1).getPoint());
			//System.out.println(i + " : " + frameLength);
		}
		double straightDist = getFirstPoint().distance(getLastPoint());
		//System.out.println(this);
		//System.out.println(frameLength + ", " + straightDist);
		return Math.abs(straightDist-frameLength) < 0.001; 
	}
	
	public void addSegment(Segment s) {
		segments.add(s.clone());
	}

	public void addSegment(int i, Segment s) {
		segments.add(i,s.clone());
	}

	public void addSegments(Vector<Segment> s) {
		Iterator<Segment> itr = s.iterator();
		while(itr.hasNext()) {
			segments.add(itr.next().clone());
		}
	}

	public void addPoint(double x, double y) {
		addPoint(new MyPoint(x, y));
	}

	public void addPoint(MyPoint p) {
		segments.add(new Segment(p, null, null));
	}

	public void addPoints(Vector<MyPoint> ps) {
		for(int i=0; i<ps.size(); i++) {
			addPoint(ps.get(i).clone());
		}
	}

	public void addCurve(MyCurve c) {
		if(isEmpty()) {
			addSegment(new Segment(c.getP0(), c.getHandle1().times(-1), c.getHandle1()));
		}
		else {
			Segment lastSeg = getLastSegment().clone();
			lastSeg.setHandleOut(c.getHandle1());
			setLastSegment(lastSeg);
		}
		addSegment(new Segment(c.getP3(), c.getHandle2(), c.getHandle2().times(-1)));
	}

	public void addCurves(Vector<MyCurve> curves) {
		Iterator<MyCurve> itr = curves.iterator();
		while(itr.hasNext()) {
			addCurve(itr.next().clone());
		}
	}	

	public void clear() {
		segments.clear();
	}

	protected boolean isValidSegmentIndex(int i) {
		return i>=0 && i<segments.size(); 
	}

	public Segment getSegment(int i) {
		assert(isValidSegmentIndex(i));
		return segments.get(i).clone();
	}

	public void removeSegment(int i) {
		assert(isValidSegmentIndex(i));
		if(isClosed()) {
			int n = getNumSegments()-1;
			if(i==0 || i==n) {
				segments.remove(n);
				segments.remove(0);
			}
			else {
				segments.remove(i);
			}
		}
		else {
			segments.remove(i);
		}
	}

	public void close() {
		if(segments.size() > 1) {
			addPoint(segments.get(0).getPoint());
		}
	}

	public void closeSmoothly() {
		if(segments.size() > 1) {
			addSegment(segments.get(0));
		}
	}

	public void closeSharply() {
		if(segments.size() > 1) {
			segments.get(0).setHandleIn(null);
			segments.get(segments.size()-1).setHandleOut(null);
			addPoint(segments.get(0).getPoint());
		}
	}

	public int getNumCurves() {
		return Math.max(0, segments.size()-1);
	}

	private boolean isValidCurveIndex(int i) {
		return i>=0 && i<getNumCurves(); 
	}

	public MyCurve getCurve(int i) {
		assert(isValidCurveIndex(i));
		MyPoint p0 = segments.get(i).getPoint();
		MyPoint p1 = p0.add(segments.get(i).getHandleOut());
		MyPoint p3 = segments.get(i+1).getPoint();
		MyPoint p2 = p3.add(segments.get(i+1).getHandleIn());
		if(java.lang.Double.isNaN(p0.getX()) || java.lang.Double.isNaN(p0.getY())) {
			System.out.println("getCurve(" + i + ")");
		}
		return new MyCurve(p0,p1,p2,p3);
	}

	public Vector<MyCurve> getCurves() {
		Vector<MyCurve> myCurves = new Vector<MyCurve>();
		for(int i=0; i<getNumCurves(); i++) {
			myCurves.add(getCurve(i));
		}
		return myCurves;
	}

	public Path getReversePath() {
		Path path = new Path();
		for(int i=segments.size()-1; i>=0; i--) {
			path.addSegment(segments.get(i).getReverseSegment());
		}
		return path;
	}

	public Vector<Segment> getSegments() {
		return segments;
	}

	public int getNumSegments() {
		return segments.size();
	}

	public Path clone() {
		return new Path(this);
	}

	public void transform(AffineTransform at) {
		Iterator<Segment> itr = segments.iterator();
		while(itr.hasNext()) {
			itr.next().transform(at);
		}
	}

	public Path getTransformedCopy(AffineTransform at) {
		Path newPath = clone();
		newPath.transform(at);
		return newPath;
	}

	public void translate(double x, double y) {
		AffineTransform at = AffineTransform.getTranslateInstance(x, y);
		transform(at);
	}

	public Path getTranslatedCopy(double x, double y) {
		AffineTransform at = AffineTransform.getTranslateInstance(x, y);
		return getTransformedCopy(at);
	}

	public void scale(double r) {
		AffineTransform at = AffineTransform.getScaleInstance(r, r);
		transform(at);
	}
	
	public Path getScaledCopy(double r) {
		AffineTransform at = AffineTransform.getScaleInstance(r, r);
		return getTransformedCopy(at);
	}

	public boolean isEmpty() {
		return segments.isEmpty();
	}

	public Segment getFirstSegment() {
		return getSegment(0);//.clone();
	}
	
	public Segment getLastSegment() {
		return getSegment(segments.size()-1);//.clone();
	}
	

	public void set(int i, Segment newSegment) {
		if(isClosed()) {
			int n = getNumSegments()-1;
			if(i==0 || i==n) {
				segments.set(0, newSegment.clone());
				segments.set(n, newSegment.clone());
			}
			else segments.set(i, newSegment.clone());
		}
		else {
			segments.set(i, newSegment.clone());
		}
		//if(i==1) System.out.println(newSegment);
	}

	public void insert(int segmentIndex, Segment newSegment) {
		segments.add(segmentIndex, newSegment.clone());
	}

	public Path2D.Double getShapeToOutline() {
		Path2D.Double shape = new Path2D.Double();
		boolean closed = isClosed();
		if(closed) {
			for(int i=-1; i<getNumCurves()+1; i++) {
				int j = (i==-1) ? getNumCurves()-1 : (i==getNumCurves() ? 0 : i);
				MyCurve curve = getCurve(j);
				if(i==-1) shape.moveTo(curve.x1, curve.y1);
				shape.curveTo(curve.ctrlx1, curve.ctrly1, curve.ctrlx2, curve.ctrly2, curve.x2, curve.y2);
			}
		}
		else {
			for(int i=0; i<getNumCurves(); i++) {
				MyCurve curve = getCurve(i);
				if(i==0) shape.moveTo(curve.x1, curve.y1);
				shape.curveTo(curve.ctrlx1, curve.ctrly1, curve.ctrlx2, curve.ctrly2, curve.x2, curve.y2);
			}
		}
		shape.setWindingRule(windingRule);
		return shape;
	}
	
	public Path2D.Double getShape() {
		Path2D.Double shape = new Path2D.Double();
		for(int i=0; i<getNumCurves(); i++) {
			MyCurve curve = getCurve(i);
			if(i==0) shape.moveTo(curve.x1, curve.y1);
			shape.curveTo(curve.ctrlx1, curve.ctrly1, curve.ctrlx2, curve.ctrly2, curve.x2, curve.y2);
		}
		shape.setWindingRule(windingRule);
		return shape;
	}

	public MyPoint getFirstPoint() {
		if(isEmpty()) return new MyPoint(0,0);
		else return segments.get(0).getPoint();
	}

	public MyPoint getLastPoint() {
		if(isEmpty()) return new MyPoint(0,0);
		else return segments.get(segments.size()-1).getPoint();
	}

	public void moveTo(double x, double y) {
		moveTo(new MyPoint(x,y));
	}

	public void moveTo(MyPoint p) {
		if(isEmpty()) addPoint(p);
		else {
			MyPoint lastPt = getLastPoint();
			if(!lastPt.equals(p)) addPoint(p);
		}
	}

	public void lineTo(double x, double y) {
		lineTo(new MyPoint(x, y));
	}

	public void lineTo(MyPoint p) {
		assert(!isEmpty());
		addPoint(p);
	}

	public void curveTo(MyPoint p1, MyPoint p2, MyPoint p3) {
		assert(!isEmpty());
		MyPoint p0 = getLastPoint();
		Segment seg1 = segments.get(segments.size()-1);
		seg1.setHandleOut(p1.minus(p0));
		Segment seg2 = new Segment(p3, p2.minus(p3), null);
		addSegment(seg2);
	}	

	public void curveTo(MyPoint p1, MyPoint p2) {
		assert(!isEmpty());
		MyPoint p0 = getLastPoint();
		// get the cubic equivalent
		MyPoint q1 = (p0.times(1.0/3.0)).add(p1.times(2.0/3.0));
		MyPoint q2 = (p1.times(2.0/3.0)).add(p2.times(1.0/3.0));
		curveTo(q1, q2, p2);
	}	

	public void curveTo(double x1, double y1, double x2, double y2, double x3, double y3) {
		curveTo(new MyPoint(x1, y1), new MyPoint(x2, y2), new MyPoint(x3, y3));
	}

	public void curveTo(double x1, double y1, double x2, double y2) {
		curveTo(new MyPoint(x1, y1), new MyPoint(x2, y2));
	}

	public static Vector<Path> getPaths(Shape shape) {
		PathIterator itr = shape.getPathIterator(Transform.getIdentity());
		Vector<Path> paths = new Vector<Path>();
		while(!itr.isDone()) {
			double[] coords = new double[6];
			int type = itr.currentSegment(coords);
			switch(type) {
			case PathIterator.SEG_MOVETO:
				paths.add(new Path());
				paths.get(paths.size()-1).moveTo(coords[0], coords[1]);
				break;
			case PathIterator.SEG_LINETO:
				paths.get(paths.size()-1).lineTo(coords[0], coords[1]);
				break;
			case PathIterator.SEG_QUADTO:
				paths.get(paths.size()-1).curveTo(coords[0], coords[1], coords[2], coords[3]);
				break;
			case PathIterator.SEG_CUBICTO:
				paths.get(paths.size()-1).curveTo(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
				break;
			case PathIterator.SEG_CLOSE:
				paths.get(paths.size()-1).closeSharply();
				break;
			}
			itr.next();
		}
		// set winding rule
		try {
			int wr = ((Path2D) shape).getWindingRule();
			for(int i=0; i<paths.size(); i++) {
				paths.get(i).setWindingRule(wr);
			}
		}
		catch(Exception e) {}		
		return paths;
	}
	
	public void fixClosedPathEndpoints() {
		if(getNumCurves() > 0 && isClosed()) {
			Segment seg = getFirstSegment();
			seg.setHandleIn(getLastSegment().getHandleIn());
			setFirstSegment(seg);
			setLastSegment(seg);
		}
	}
	
	public void setPath(Path path) {
		clear();
		for(int i=0; i<path.getNumSegments(); i++) {
			addSegment(path.getSegment(i));//.clone());
		}
	}

	public int getWindingRule() {
		return windingRule;
	}

	public void setWindingRule(int windingRule) {
		this.windingRule = windingRule;
	}
	
	public MyPoint getBoundingBoxCentre() {
		MyPoint[] bb = getBoundingBox();
		MyPoint centre = (bb[0].plus(bb[1])).times(0.5);
		return centre;
	}
	
	// output = {topleft, bottomright}
	public MyPoint[] getBoundingBox() {
		assert(getNumSegments() >= 1);
		if(getNumSegments()==0) {
			MyPoint p = getSegment(0).getPoint();
			MyPoint[] bb = {p, p};
			return bb;
		}
		else {
			MyPoint[] bb = getCurve(0).getBoundingBox();
			MyPoint p1 = bb[0], p2 = bb[1];
			for(int i=0; i<getNumCurves(); i++) {
				MyCurve curve = getCurve(i);
				MyPoint[] curveBB = curve.getBoundingBox();
				// xmin
				if(curveBB[0].x < p1.x) p1.x = curveBB[0].x;
				// ymin
				if(curveBB[0].y < p1.y) p1.y = curveBB[0].y;
				// xmax
				if(curveBB[1].x > p2.x) p2.x = curveBB[1].x;
				// ymax
				if(curveBB[1].y > p2.y) p2.y = curveBB[1].y;
			}
			MyPoint[] newBB = {p1, p2};
			return newBB;
		}
	}
	
	public boolean isClosed() {
		MyPoint p1 = getFirstPoint();
		MyPoint p2 = getLastPoint();
		return Math.abs(p1.x-p2.x)<0.001 &&  Math.abs(p1.y-p2.y)<0.001;
	}

	// Line
	public static class Line extends Path {

		public Line(double x1, double y1, double x2, double y2) {
			super();
			moveTo(x1, y1);
			lineTo(x2, y2);
		}

		public Line(MyPoint p1, MyPoint p2) {
			super();
			moveTo(p1);
			lineTo(p2);
		}
	}

	// Rectangle
	public static class Rectangle extends Path {

		public Rectangle(double x, double y, double width, double height) {
			super();
			moveTo(x, y);
			lineTo(x+width, y);
			lineTo(x+width, y+height);
			lineTo(x, y+height);
			close();
		}
	}

	// Round Rectangle
	public static class RoundRectangle extends Path {

		private void quarterArcTo(double x, double y, double width, double height, int quadrant) {
			double ovalX = 0, ovalY = 0, ovalW = 2*width, ovalH = 2*height;
			switch(quadrant) {
			case 1:
				ovalX = x-width; ovalY = y;
				break;
			case 2:
				ovalX = x; ovalY = y;
				break;
			case 3:
				ovalX = x; ovalY = y-height;
				break;
			case 4:
				ovalX = x-width; ovalY = y-height;
				break;
			}
			Path oval = new Path.Oval(ovalX, ovalY, ovalW, ovalH);
			MyCurve arc = oval.getCurve(quadrant-1);
			moveTo(arc.getP0());
			curveTo(arc.getP1(), arc.getP2(), arc.getP3());
		}

		public RoundRectangle(double x, double y, double width, double height, double arcWidth, double arcHeight) {
			super();
			arcWidth = Math.max(Math.min(width, arcWidth), 0);
			arcHeight = Math.max(Math.min(height, arcHeight), 0);
			// if rectangle
			if(arcWidth == 0 || arcHeight == 0) {
				moveTo(x, y);
				lineTo(x+width, y);
				lineTo(x+width, y+height);
				lineTo(x, y+height);
				close();
			}
			else {
				double halfArcW = 0.5*arcWidth, halfArcH = 0.5*arcHeight;
				// quadrant I
				moveTo(x + width, y + height - halfArcH);
				lineTo(x + width, y + halfArcH);
				quarterArcTo(x + width - halfArcW, y, halfArcW, halfArcH, 1);				
				// quadrant II
				lineTo(x + halfArcW, y);
				quarterArcTo(x, y, halfArcW, halfArcH, 2);
				// quadrant III
				lineTo(x, y + height - halfArcH);
				quarterArcTo(x, y + height - halfArcH, halfArcW, halfArcH, 3);
				// quadrant IV
				lineTo(x + width - halfArcW, y + height);
				quarterArcTo(x + width - halfArcW, y + height - halfArcH, halfArcW, halfArcH, 4);
			}
			transform(AffineTransform.getTranslateInstance(0.5, 0.5));
			fixClosedPathEndpoints();
		}
	}

	// Oval
	public static class Oval extends Path {

		public Oval(double x, double y, double width, double height) {
			super();
			MyPoint[] p = Arc.getQuarterArcControlPoints(1);
			for(int i=0; i<4; i++) {
				AffineTransform at = AffineTransform.getTranslateInstance(x+0.5*width, y+0.5*height);
				at.scale(0.5*width, 0.5*height);
				at.rotate(-((double)i*90.0)/180.0*Math.PI);
				MyPoint P0 = Transform.transformPoint(p[0], at);
				MyPoint P1 = Transform.transformPoint(p[1], at);
				MyPoint P2 = Transform.transformPoint(p[2], at);
				MyPoint P3 = Transform.transformPoint(p[3], at);
				if(i == 0) moveTo(P0);
				curveTo(P1, P2, P3);
			}
			fixClosedPathEndpoints();
		}
	}

	// Arc
	public static class Arc extends Path {

		protected final static double KAPPA = 4.0*(Math.sqrt(2)-1.0)/3.0;

		// arcAngle is in degrees
		public MyPoint[] getMinorArcControlPoints(double arcAngle) {
			double phi = 0.5*arcAngle/180.0*Math.PI;
			MyPoint A = new MyPoint(Math.cos(phi), Math.sin(phi));
			MyPoint B = new MyPoint(A.x, -A.y);
			// control points
			MyPoint C1 = new MyPoint((4.0-A.x)/3.0, (1.0-A.x)*(3.0-A.x)/(3.0*A.y));
			MyPoint C2 = new MyPoint(C1.x, -C1.y);
			AffineTransform at = AffineTransform.getRotateInstance(-phi);
			A = Transform.transformPoint(A, at);
			C1 = Transform.transformPoint(C1, at);
			C2 = Transform.transformPoint(C2, at);
			B = Transform.transformPoint(B, at);
			MyPoint[] points = {A, C1, C2, B};
			return points;
		}

		// quadrant = 1, 2, 3, 4
		public static MyPoint[] getQuarterArcControlPoints(int quadrant) {
			double midX = 0, midY = 0,
					minX = -1, maxX = 1,
					minY = -1, maxY = 1,
					halfW = 0.5*(maxX-minX), halfH = 0.5*(maxY-minY);

			// anchor points
			MyPoint pN = new MyPoint(midX, minY), pE = new MyPoint(maxX, midY);

			// handles
			MyPoint hN = new MyPoint(0, -halfH*KAPPA), hE = new MyPoint(halfW*KAPPA, 0);
			MyPoint[] points = {pE, pE.add(hN), pN.add(hE), pN};

			// rotate
			AffineTransform at = AffineTransform.getRotateInstance(((double)quadrant-1)*90.0);
			for(int i=0; i<points.length; i++) {
				points[i] = Transform.transformPoint(points[i], at);
			}
			return points;
		}

		// quadrant range = 1 to 4
		private int getQuadrant(double arcAngle) {
			int rem = (int) Math.floor(arcAngle/90.0) % 4;
			if(rem < 0) rem += 4;
			return rem + 1;
		}

		private double getQuadrantAngle(int quadrant) {
			assert(quadrant >=1 && quadrant <=4);
			double angle = (double)(quadrant-1)*90.0;
			return angle;
		}

		// canonical angle = [0, 360)
		private double getCanonicalAngle(double arcAngle) {
			double rem = arcAngle % 90;
			if(rem < 0) rem += 90;
			int quadrant = getQuadrant(arcAngle);
			double angle = getQuadrantAngle(quadrant) + rem;
			return angle;
		}

		private double angleToNextQuadrant(double arcAngle) {
			double angle = getCanonicalAngle(arcAngle);
			int currQuadrant = getQuadrant(arcAngle);
			int nextQuadrant = (currQuadrant == 4) ? 1 : getQuadrant(arcAngle)+1;
			double nextQuadrantAngle = getQuadrantAngle(nextQuadrant);
			if(nextQuadrantAngle == 0) nextQuadrantAngle = 360;
			double angleDiff = nextQuadrantAngle - angle;
			return angleDiff;
		}

		public Arc(double x, double y, double width, double height, double arcAngle) {
			this(x, y, width, height, 0, arcAngle);
		}

		public Arc(double x, double y, double width, double height, double startAngle, double arcAngle) {

			// reference: http://en.wikipedia.org/wiki/B%C3%A9zier_spline#General_case
			super();
			MyPoint[] q = getQuarterArcControlPoints(1);

			startAngle = getCanonicalAngle(startAngle);
			double sign = (arcAngle > 0 ? 1 : -1);
			if(Math.abs(arcAngle) >= 360) {
				arcAngle = 360*sign;
				startAngle = 0;
			}
			double angle = Math.abs(arcAngle);
			int quadrantOffset = 0;
			while(angle > 0) {
				double minorAngle = Math.min(angleToNextQuadrant(startAngle), angle);
				MyPoint[] p = (minorAngle == 90) ?	q : getMinorArcControlPoints(minorAngle);
				// transform points
				AffineTransform at = AffineTransform.getTranslateInstance(x+0.5*width, y+0.5*height);
				at.scale(0.5*width, 0.5*height);
				at.rotate(-startAngle/180.0*Math.PI);
				MyPoint P0 = Transform.transformPoint(p[0], at);
				MyPoint P1 = Transform.transformPoint(p[1], at);
				MyPoint P2 = Transform.transformPoint(p[2], at);
				MyPoint P3 = Transform.transformPoint(p[3], at);
				if(quadrantOffset == 0) moveTo(P0);
				curveTo(P1, P2, P3);
				// increment
				startAngle += minorAngle;
				angle -= minorAngle;
				quadrantOffset += sign;
			}
			fixClosedPathEndpoints();
		}
	}

	// Polyline
	public static class Polyline extends Path {

		public Polyline(int[] xPoints, int[] yPoints, int nPoints) {
			super();
			for(int i=0; i<nPoints; i++) {
				if(i==0) moveTo(xPoints[i], yPoints[i]);
				else lineTo(xPoints[i], yPoints[i]);
			}			
		}
	}

	// Polygon
	public static class Polygon extends Path {

		public Polygon(int[] xPoints, int[] yPoints, int nPoints) {
			super();
			for(int i=0; i<nPoints; i++) {
				if(i==0) moveTo(xPoints[i], yPoints[i]);
				else lineTo(xPoints[i], yPoints[i]);
			}
			if(nPoints > 2) {
				if(!isClosed()) closeSharply();
			}
		}
	}

	public void setLastSegment(Segment segment) {
		set(getNumSegments()-1, segment);
	}
	
	public void setFirstSegment(Segment segment) {
		set(0, segment);
	}

}
