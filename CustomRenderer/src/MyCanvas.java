import java.awt.Graphics2D;

import renderer.display.CustomRasterizedDoubleBufferedCanvas;
import renderer.display.MyGraphics2D;
import renderer.rasterizer.Rasterizer;



@SuppressWarnings("serial")
public class MyCanvas extends CustomRasterizedDoubleBufferedCanvas {

	public MyCanvas() {
		super();
	}
	
	public void paintComponent(Graphics2D g) {
		
	}
	
	public void myPaint(MyGraphics2D g) {
		//g.setRasterizerType(Rasterizer.DEFAULT);
		//TestDraw.testDraw(g);
		g.setRasterizerType(Rasterizer.SUPERPIXELATOR);
		//g.translate(70, 0);
		TestDraw.testDraw(g);
		//Graphics2D g2 = g.getGraphics2D();
		//g2.setStrokeWidth(2);
	}

}
