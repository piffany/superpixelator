import renderer.geometry.MyPoint;
import renderer.geometry.Path;
import renderer.geometry.Segment;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.util.Vector;

import renderer.display.GraphicsState;
import renderer.display.MyGraphics2D;
import renderer.rasterizer.Rasterizer;
import renderer.rasterizer.superpixelator.BoundingBoxShifter;
import renderer.rasterizer.superpixelator.SuperpixelatorHelper;
import renderer.test.StopWatch;




public class TestDraw {

	public static final Color c1 = new Color(20, 100, 200, 60),
			c2 = Color.black,//new Color(255, 0, 0, 100),
			c3 = new Color(255,130,0,100);
	public static final String word = "Yo!";
	public static final MyPoint[] curves =
		{new MyPoint(0.5,1),
		new MyPoint(0,1), new MyPoint(0,0), new MyPoint(1.0/3.0,0),
		new MyPoint(2.0/3.0,0), new MyPoint(2.0/3.0,0.5), new MyPoint(0.5,0.5),
		new MyPoint(1.0/3.0,0.5), new MyPoint(1.0/3.0,0), new MyPoint(2.0/3.0,0),
		new MyPoint(1,0), new MyPoint(1,1), new MyPoint(0.5,1)};
	public static final MyPoint[] curvesInf =
		{new MyPoint(0.5,0),
		new MyPoint(0.75,0), new MyPoint(0.5,0.5), new MyPoint(0.75,0.5),
		new MyPoint(1,0.5), new MyPoint(1,1), new MyPoint(0.5,1),
		new MyPoint(0,1), new MyPoint(0,0.5), new MyPoint(0.25,0.5),
		new MyPoint(0.5,0.5), new MyPoint(0.25,0), new MyPoint(0.5,0)};
	public static final MyPoint[] curvesInf2 =
		{new MyPoint(0.4,0),
		new MyPoint(0.4,0.2), new MyPoint(0,0.2), new MyPoint(0,0.4),
		new MyPoint(0,0.6), new MyPoint(0.2,0.6), new MyPoint(0.2,0.8),
		new MyPoint(0.2,1.0), new MyPoint(0.4,1.0), new MyPoint(0.4,0.8),
		new MyPoint(0.4,0.6), new MyPoint(0.2,0.6), new MyPoint(0.2,0.4),
		new MyPoint(0.2,0.2), new MyPoint(0.4,0.2), new MyPoint(0.4,0)};
	public static final MyPoint[] lines =
		{new MyPoint(0,0), new MyPoint(1,0), new MyPoint(0,1), new MyPoint(1,1)};		
	public static double[][][] lotusData = 
		{{{ 18 ,  20 ,  4 ,  0 ,  -4 ,  0 },
		{ 12 ,  14 ,  0 ,  4 ,  0 ,  -4 },
		{ 18 ,  0 ,  -1 ,  6 ,  1 ,  7 },
		{ 24 ,  14 ,  0 ,  -4 ,  0 ,  4 },
		{ 18 ,  20 ,  4 ,  0 ,  -4 ,  0 },
		{ 18 ,  20 ,  4 ,  0 ,  -4 ,  0 },
		{ 18 ,  20 ,  4 ,  0 ,  -4 ,  0 }},
		{{ 18 ,  20 ,  6 ,  0 ,  -8 ,  0 },
		{ 6 ,  12 ,  0 ,  6 ,  0 ,  -4 },
		{ 9 ,  2 ,  1 ,  6 ,  5 ,  4 },
		{ 18 ,  13 ,  -2 ,  -5 ,  0 ,  0 },
		{ 28 ,  2 ,  -10 ,  3 ,  -2 ,  7 },
		{ 29 ,  13 ,  0 ,  -6 ,  0 ,  5 },
		{ 18 ,  20 ,  4 ,  0 ,  -4 ,  0 }},
		{{ 0 ,  17 ,  0 ,  0 ,  0 ,  0 },
		{ 8 ,  21 ,  0 ,  0 ,  0 ,  0 },
		{ 2 ,  25 ,  0 ,  0 ,  0 ,  0 },
		{ 14 ,  23 ,  0 ,  0 ,  0 ,  0 },
		{ 18 ,  29 ,  0 ,  0 ,  0 ,  0 },
		{ 22 ,  23 ,  0 ,  0 ,  0 ,  0 },
		{ 34 ,  25 ,  0 ,  0 ,  0 ,  0 },
		{ 28 ,  21 ,  0 ,  0 ,  0 ,  0 },
		{ 36 ,  17 ,  0 ,  0 ,  0 ,  0 },
		{ 0 ,  17 ,  0 ,  0 ,  0 ,  0 }}};

	public static Shape createTestShape(double w, double h) {
		Path2D.Double path = new Path2D.Double();
		path.moveTo(w*curves[0].x, h*curves[0].y);
		for(int i=1; i<curves.length; i+=3) {
			path.curveTo(w*curves[i].x, h*curves[i].y, w*curves[i+1].x, h*curves[i+1].y, w*curves[i+2].x, h*curves[i+2].y);
		}
		return path;
	}
	
	public static Path createCurvyPath1() {
		Path path = new Path();
		path.moveTo(28.72,41.17);
		path.curveTo(28.72,41.17, 16.72,21.83, 28.38,24.0);
		return path;
	}
	
	public static Path createCurvyPath2() {
		Path path = new Path();
		path.moveTo(22.67,48.17);
		path.curveTo(40.17,43.0,51.5,32.0,41.0,26.67);
		return path;
	}
	
	public static Vector<Path> createLotusPaths(double w, double h) {
		Vector<Path> paths = new Vector<Path>();
		for(int i=0; i<lotusData.length; i++) {
			Path path = new Path();
			for(int j=0; j<lotusData[i].length; j++) {
				double[] d = lotusData[i][j];
				path.addSegment(new Segment(d[0], d[1], d[2], d[3], d[4], d[5]));
			}
			paths.add(0,path.getTransformedCopy(AffineTransform.getScaleInstance(w/38.0, h/30.0)));
		}
		return paths;
	}
	
	public static Path createTestPath(double w, double h) {
		Path path = new Path();
		path.moveTo(w*curves[0].x, h*curves[0].y);
		for(int i=1; i<curves.length; i+=3) {
			path.curveTo(w*curves[i].x, h*curves[i].y, w*curves[i+1].x, h*curves[i+1].y, w*curves[i+2].x, h*curves[i+2].y);
		}
		return path;
	}

	public static Path createTestPathWithInflection(double w, double h) {
		Path path = new Path();
		path.moveTo(w*curvesInf[0].x, h*curvesInf[0].y);
		for(int i=1; i<curvesInf.length; i+=3) {
			path.curveTo(w*curvesInf[i].x, h*curvesInf[i].y, w*curvesInf[i+1].x, h*curvesInf[i+1].y, w*curvesInf[i+2].x, h*curvesInf[i+2].y);
		}
		return path;
	}
	
	public static Path createTestPathWithInflection2(double w, double h) {
		Path path = new Path();
		path.moveTo(w*curvesInf2[0].x, h*curvesInf2[0].y);
		for(int i=1; i<curvesInf2.length; i+=3) {
			path.curveTo(w*curvesInf2[i].x, h*curvesInf2[i].y, w*curvesInf2[i+1].x, h*curvesInf2[i+1].y, w*curvesInf2[i+2].x, h*curvesInf2[i+2].y);
		}
		return path;
	}
	
	public static Path createTrianglePath(double w, double h) {
		Path path = new Path();
		path.moveTo(new MyPoint(0,0));
		path.moveTo(new MyPoint(w,0));
		path.moveTo(new MyPoint(0,h));
		path.closeSharply();
		return path;
	}
	
	public static Path createStarPath(double w, double h, int n, double ratio) {
		Path path = new Path();
		if(n < 3) return path;
		double R = 0.5, r = 0.5*ratio;
		double dTheta = 2.0*Math.PI/(double)n;
		MyPoint offset = new MyPoint(0.5*w, 0.5*h);
		double rotation = 0.5*Math.PI;
		for(int i=0; i<n; i++) {
			double theta1 = dTheta*(double)i + rotation;
			double theta2 = dTheta*((double)i+0.5) + rotation;
			MyPoint p1 = (new MyPoint(w*R*Math.cos(theta1), -h*R*Math.sin(theta1))).add(offset);
			//System.out.println(" n = " + p1);
			MyPoint p2 = (new MyPoint(w*r*Math.cos(theta2), -h*r*Math.sin(theta2))).add(offset);
			if(path.isEmpty()) path.moveTo(p1);
			else path.lineTo(p1);
			path.lineTo(p2);
		}
		path.closeSharply();
		return path;
	}
	
	public static Path createRegularPolygonPath(double w, double h, int n) {
		Path path = new Path();
		if(n < 3) return path;
		double R = 0.5;
		double dTheta = 2.0*Math.PI/(double)n;
		MyPoint offset = new MyPoint(0.5*w, 0.5*h);
		double rotation = 0.5*Math.PI;
		
		for(int i=0; i<n; i++) {
			double theta1 = dTheta*(double)i + rotation;
			MyPoint p1 = (new MyPoint(w*R*Math.cos(theta1), -h*R*Math.sin(theta1))).add(offset);
			if(path.isEmpty()) path.moveTo(p1);
			else path.lineTo(p1);
		}
		path.closeSharply();
		
		/*
		for(int i=-n/2; i<n/2; i++) {
			double theta1 = dTheta*(double)i + rotation;
			MyPoint p1 = (new MyPoint(w*R*Math.cos(theta1), -h*R*Math.sin(theta1))).add(offset);
			if(path.isEmpty()) path.moveTo(p1);
			else path.lineTo(p1);
		}
		*/
		
		return path;
	}

	public static void testDrawShape(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);	
		g.translate(1+col*dx, 1+row*dy);
		Shape shape = createTestShape(w, h);
		//g.setColor(c1); g.fill(shape);
		g.setColor(c2); g.draw(shape);
		g.setState(gs);
	}

	public static void testDrawPath(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		Path path = createTestPath(w, h);
		//g.setColor(c1); g.fillPath(path);
		g.setColor(c2); g.drawPath(path);
		g.setState(gs);
	}
	
	public static void testDrawLotus(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy+3);
		Vector<Path> paths = createLotusPaths(w, h);
		for(int i=0; i<paths.size(); i++) {
			Path path = paths.get(i);
			g.setColor((i==0) ? Color.green : (i==1) ? Color.pink : Color.red); g.fillPath(path);
			g.setColor(c2); g.drawPath(path);
		}
		g.setState(gs);
	}

	public static void testDrawPathWithInflection(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		Path path = createTestPathWithInflection2(w, h);
		//g.setColor(c1); g.fillPath(path);
		g.setColor(c2); g.drawPath(path);
		g.setState(gs);
	}

	public static void testDrawImage1(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		BufferedImage image = getTestImage(w, h);
		//int sx1 = 2, sy1 = 2, sx2 = 11, sy2 = 11;
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		g.drawImage(image, 0, 0, null);
		//g.drawImage(image,0,0,w,h,c3,null);
		//g.translate(0, (int)(0.5*dy));
		//g.drawImage(image,0,0,w,h,sx1,sy1,sx2,sy2,c3,null);
		g.setState(gs);
	}

	public static BufferedImage getTestImage(int w, int h) {
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) image.getGraphics();
		g.setColor(c3);
		g.drawOval(1, 1, w-2, h-2);
		for(int x=0; x<w; x+=3) {
			for(int y=0; y<h; y+=3) {
				image.setRGB(x, y, Color.green.getRGB());
			}
		}
		return image;
	}

	public static void testDrawString(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		// setup
		int xOffset = 0, yOffset = 14;
		// draw
		g.setZoom(zoom);
		AffineTransform at = g.getTransform();
		g.translate(1+col*dx, 1+row*dy);
		//g.translate(0, (int)(0.5*dy));
		g.setColor(c2); g.drawString(word, xOffset, yOffset);
		g.setTransform(at);
	}

	public static void testDrawGlyphVector(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		// setup
		Font font = new Font("serif", Font.ITALIC, 24);
		FontRenderContext frc = new FontRenderContext(AffineTransform.getRotateInstance(0.2), false, false);
		GlyphVector gv = font.createGlyphVector(frc, word);
		int xOffset = 0, yOffset = 14;
		// draw
		g.setZoom(zoom);
		AffineTransform at = g.getTransform();
		g.translate(1+col*dx, 1+row*dy);
		g.setColor(c2); g.drawGlyphVector(gv, xOffset, yOffset);
		g.setTransform(at);
	}

	public static void testDrawLine(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		g.setColor(c2); g.drawLine(0, 0, w, h);
		g.setState(gs);
	}

	public static void testDrawLines(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		int[] xPoints = {(int) (w*lines[0].x), (int) (w*lines[1].x), (int) (w*lines[2].x), (int) (w*lines[3].x)};
		int[] yPoints = {(int) (h*lines[0].y), (int) (h*lines[1].y), (int) (h*lines[2].y), (int) (h*lines[3].y)};
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		g.setColor(c2); g.drawPolyline(xPoints, yPoints, xPoints.length);
		g.setState(gs);
	}

	public static void testDrawRect(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		//g.setColor(c1); g.fillRect(0, 0, w, h);
		g.setColor(c2); g.drawRect(0, 0, w, h);
		g.setState(gs);
	}

	public static void testDrawRoundRect(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		int arcWidth = 20, arcHeight = 20;
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		//g.setColor(c1); g.fillRoundRect(0, 0, w, h, arcWidth, arcHeight);
		g.setColor(c2); g.drawRoundRect(0, 0, w, h, arcWidth, arcHeight);
		g.setState(gs);
	}

	public static void testDrawOval(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		//g.setColor(c1); g.fillOval(0, 0, w, h);
		g.setColor(c2); g.drawOval(0, 0, w, h);
		g.setState(gs);
	}

	public static void testDrawRotatedOvals(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		w = 30; h = 15;
		int n = 6;
		int width = 15;

		for(int i=0; i<n; i++) {
			g.setIdentityTransform();			
			int r = i/width;
			int c = i%width;
			boolean[] params = {true,true};
			Path path = SuperpixelatorHelper.getOvalPath(g, 0, 0, w, h, params); path.translate(0.3, 0.6);
			g.translate(col*dx, row*dy);
			g.translate(c*dx, r*dy);
			g.translate(w/2, h/2);
			g.rotate(Math.PI/4.0/(double)(n-1)*(double)i);
			g.translate(-w/2, -h/2);
			//System.out.println(g.getTransform().getTranslateX() + ", " + g.getTransform().getTranslateY());
			g.setColor(c2); g.drawPath(path);//g.drawOval(0, 0, w, h);
		}
		g.setState(gs);
	}
	
	public static void testDrawRotatedRects(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		w = 30; h = 15;
		int n = 6;
		int width = 15;
		for(int i=0; i<n; i++) {
			g.setIdentityTransform();
			int r = i/width;
			int c = i%width;
			boolean[] params = {true,true};
			Path path = SuperpixelatorHelper.getRectanglePath(g, 0, 0, w, h, params); path.translate(0.3, 0.6);
			g.translate(col*dx, row*dy);
			g.translate(c*dx, r*dy);
			g.translate(w/2, h/2);
			g.rotate(Math.PI/4.0/(double)(n-1)*(double)i);
			g.translate(-w/2, -h/2);
			g.setColor(c2); g.drawPath(path);//g.drawRect(0, 0, w, h);
		}
		g.setState(gs);
	}
	
	public static void testDrawRotatedRoundRects(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		w = 30; h = 15;
		int n = 6;
		int width = 15;
		int arc = Math.min(w/2, h/2);
		for(int i=0; i<n; i++) {
			g.setIdentityTransform();
			int r = i/width;
			int c = i%width;
			boolean[] params = {true,true};
			Path path = SuperpixelatorHelper.getRoundRectanglePath(g, 0, 0, w, h, arc, arc, params); path.translate(0.3, 0.6);
			g.translate(col*dx, row*dy);
			g.translate(c*dx, r*dy);
			g.translate(w/2, h/2);
			g.rotate(Math.PI/4.0/(double)(n-1)*(double)i);
			g.translate(-w/2, -h/2);
			g.setColor(c2); g.drawPath(path);//g.drawRoundRect(0, 0, w, h, w/2, h/2);
		}
		g.setState(gs);
	}
	
	public static void testDrawManyStars(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		w = 30; h = 30;
		int n = 6;
		int width = 15;
		//AffineTransform at0 = g.getTransform();
		for(int j=0; j<n; j++) {
			int i = j+3;
			g.setIdentityTransform();
			int r = j/width;
			int c = j%width;
			g.translate(col*dx, row*dy);
			g.translate(c*dx, r*dy);			
			g.setColor(c2); 
			Path path = createStarPath(w, h, i, 0.5); 
		//	if(j==0) System.out.println(g.getTransform().getTranslateX() + ", " + g.getTransform().getTranslateY());
			path.translate(0.3, 0.6);
			g.setColor(c2); g.drawPath(path);
		}
		g.setState(gs);
	}

	public static void testDrawRotatedPaths(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(1);
		w = 50; h = 50;
		int n = 40; // n = 15 had a strangely sorted case
		int width = 10;
		//Path path = createTestPath(w, h);
		//Path path = createTestPathWithInflection(w, h);
		Path path = createTestPathWithInflection2(w, h);
		for(int i=0; i<n; i++) {
			g.setIdentityTransform();
			row = i/width;
			col = i%width;
			g.translate(1+col*dx, 1+row*dy);
			g.translate(w/2, h/2);
			g.setColor(c1); g.drawRect(-21, -21, w+2, h+2);
			g.rotate(2.0*Math.PI/(double)(n-1)*(double)i);
			g.translate(-w/2, -h/2);
			g.setColor(Color.red); g.drawPath(path);
		}
		g.setState(gs);
	}
	
	public static void testDrawManyRegularPolygons(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		w = 30; h = 30;
		int n = 30;
		int width = 10;
		AffineTransform at0 = g.getTransform();
		for(int i=3; i<n; i++) {
			g.setTransform(at0);
			row = i/width;
			col = i%width;
			g.translate(1+col*dx, 1+row*dy);
			g.translate(w/2, h/2);
			g.translate(-w/2, -h/2);
			Path path = createRegularPolygonPath(w, h, i);
			g.setColor(c2); g.drawPath(path);
		}
		g.setState(gs);
	}
	
	public static void testDrawPixelationSteps(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		
		w = 30; h = 15;
		int arc = Math.min(w/2, h/2);
		zoom = 5;
		
		g.setZoom(1);
		g.setIdentityTransform();
		//drawPixelGrid(g, zoom);
		g.setState(gs);
		
		g.setZoom(zoom);
		boolean outputParams[] = {true, true};
		//g.setStroke(new BasicStroke(1.001f,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
		//Path path = createTestPath(w, h);
		//path.removeSegment(4);
		/*
		double trans = 0.0;
		String type = (trans == 0) ? "bad" : "good";
		Segment seg = path.getSegment(1).clone(); seg.translate(trans,0);
		System.out.println(type);
		path.set(1, seg);
		*/
		//Path path = createCurvyPath1();
		//Path path = createCurvyPath2();

		//Path path = createStarPath(w, h, 8, 0.5);
		//Path path = createRegularPolygonPath(w, h, 4); //path.transform(g.getTransform());
		Path path = SuperpixelatorHelper.getRoundRectanglePath(g, 0, 0, w, h, arc, arc, outputParams);
		g.translate(w/2, h/2); g.rotate(3.0*Math.PI/4.0/5.0); g.translate(-w/2, -h/2); 
		g.translate(1+col*dx+0.3, 1+row*dy+0.6);
		//g.scale(2, 0.9);
		//g.scale(2.5, 0.9);
		//g.rotate(Math.PI/3);
		//Path path = PixelatorHelper.getOvalPath(g, 0, 0, w, h, outputParams);
		//for(int i=path.getNumSegments()-1; i>=3; i--) path.removeSegment(i);
		//path.removeSegment(0);
		//Path path = createStarPath(w, h, 5, 0.5);
		//Path path = createTrianglePath(w, h); //path.transform(AffineTransform.getTranslateInstance(0.5, 0.5));
		//g.rotate(Math.PI/3); Path path = PixelatorHelper.getOvalPath(g, 0, 0, w, h, outputParams);
		//Path path = createRegularPolygonPath(w, h, 12);
		//Path path = createTestPathWithInflection2(40, 40);
		//Path path = new Path.Line(new MyPoint(0,0), new MyPoint(w,h));
		//g.translate(20,0);
		//g.translate(10,0); g.rotate(0.2);
		//g.scale(1,0.7);
		g.setColor(new Color(0,0,0,255));
		//g.drawOval(0, 0, w, h);
		//g.drawRoundRect(0, 0, w, h, w/2, h/2);
		//g.drawRect(0, 0, w, h);
		g.drawPath(path);
		
		path.transform(g.getTransform());
		g.setState(gs);
		//g.translate(1+col*dx, 1+row*dy);
		AffineTransform atScaleTranslate = AffineTransform.getScaleInstance(zoom, zoom);
		//atScaleTranslate.translate(1+col*dx, 1+row*dy);
		boolean drawOrig = false, drawStep0 = false, drawStep1 = false, drawStep2 = true, drawStep3 = false;
		
		if(drawOrig) {
			g.setColor(Color.green);
			drawVectorPath(g, path.getTransformedCopy(atScaleTranslate));
		}
		
		if(drawStep0) {
			Vector<Path> step0 = SuperpixelatorHelper.getPixelationStep(path, outputParams[0], outputParams[1], 0);
			for(int i=0; i<step0.size(); i++) {
				g.setColor((i%2==0) ? Color.red : Color.blue);
				drawVectorPath(g, step0.get(i).getTransformedCopy(atScaleTranslate));
			}
		}		
		// draw pixelation step 1 (split by monotonicity)
		if(drawStep1) {
			Vector<Path> step1 = SuperpixelatorHelper.getPixelationStep(path, outputParams[0], outputParams[1], 1);
			for(int i=0; i<step1.size(); i++) {
				g.setColor((i%2==0) ? Color.orange : Color.cyan);
				drawVectorPath(g, step1.get(i).getTransformedCopy(atScaleTranslate));
			}
		}		
		// draw pixelation step 2 (shifted)
		if(drawStep2) {
			Vector<Path> step2 = (outputParams[0]) ?
									SuperpixelatorHelper.getPixelationStep(path, outputParams[0], outputParams[1], 2):
										SuperpixelatorHelper.getPixelationStep(path, outputParams[0], outputParams[1], 1);
			for(int i=0; i<step2.size(); i++) {
				g.setColor((i%2==0) ? Color.cyan : Color.magenta);
				drawVectorPath(g, step2.get(i).getTransformedCopy(atScaleTranslate));
			}
		}
		// draw pixelation step 3 (pixelated path)
		if(drawStep3) {
			Vector<MyPoint> pixelPathPts = SuperpixelatorHelper.getPixelatedPath(path, outputParams[0], outputParams[1]);
			Path pixelPath = new Path(); pixelPath.addPoints(pixelPathPts);
			g.setColor(Color.red);
			drawVectorPath(g, pixelPath.getTransformedCopy(atScaleTranslate));
		}
	}
	
	public static void testDrawThickPixelationSteps(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		w = 180; h = w;
		zoom = 1;
		g.setZoom(zoom);
		boolean outputParams[] = {true, true};
		//g.rotate(Math.PI/16.0);
		//g.translate(1+col*dx, 1+row*dy);
		g.setStroke(new BasicStroke(10.f,BasicStroke.CAP_ROUND,BasicStroke.JOIN_MITER,1));
		//Path path = PixelatorHelper.getOvalPath(g, 0, 0, w, h, outputParams);
		Path path = createTestPathWithInflection2(w, h);
		//Path path = createStarPath(w, h, 4, 0.5);
		Vector<Path> outline = SuperpixelatorHelper.getStrokeOutline(g, path);
		outline.set(0, BoundingBoxShifter.snapShapeByBoundingBox(outline.get(0)));
		//Path path = createCurvyPath();

		//Path path = createStarPath(w, h, 8, 0.5);
		//Path path = createRegularPolygonPath(w, h, 4); path.transform(g.getTransform());
		//Path path = PixelatorHelper.getRoundRectanglePath(g, 0, 0, w, h, w/2, h/2, outputParams);
		//Path path = PixelatorHelper.getRectanglePath(g, 0, 0, w, h, outputParams);
		//Path path = new Path.Line(new MyPoint(0,0), new MyPoint(w,h));
		//g.translate(20,0);
		//g.translate(10,0); g.rotate(0.2);
		//g.scale(1,0.7);
		g.setColor(new Color(0,0,0,100));
		//g.drawOval(0, 0, w, h);
		//g.drawRoundRect(0, 0, w, h, w/2, h/2);
		//g.drawRect(0, 0, w, h);
		g.drawPath(path);
		
		g.setState(gs);
		//path.transform(g.getTransform());
		for(int i=0; i<outline.size(); i++) {
			outline.get(i).transform(AffineTransform.getTranslateInstance(10,10));
		}
		//drawPixelGrid(g, zoom);
		//g.translate(1+col*dx, 1+row*dy);
		AffineTransform atScaleTranslate = AffineTransform.getScaleInstance(zoom, zoom);
		//atScaleTranslate.translate(1+col*dx, 1+row*dy);
		boolean drawOrig = false, drawStep0 = false, drawStep1 = false, drawStep2 = false, drawStep3 = false;
		g.setStrokeWidth(1);
		if(drawOrig) {
			g.setColor(Color.green);
			for(int j=0; j<outline.size(); j++) {
				drawVectorPath(g, outline.get(j).getTransformedCopy(atScaleTranslate));
			}
		}
		
		if(drawStep0) {
			for(int j=0; j<outline.size(); j++){
				Vector<Path> step0 = SuperpixelatorHelper.getPixelationStep(outline.get(j), outputParams[0], outputParams[1], 0);
				for(int i=0; i<step0.size(); i++) {
					g.setColor(Color.red);
					drawVectorPath(g, step0.get(i).getTransformedCopy(atScaleTranslate));
				}
			}
		}		
		// draw pixelation step 1 (split by monotonicity)
		if(drawStep1) {
			for(int j=0; j<outline.size(); j++){
				Vector<Path> step1 = SuperpixelatorHelper.getPixelationStep(outline.get(j), outputParams[0], outputParams[1], 1);
				for(int i=0; i<step1.size(); i++) {
					g.setColor((i%2==0) ? Color.orange : Color.cyan);
					drawVectorPath(g, step1.get(i).getTransformedCopy(atScaleTranslate));
				}
			}
		}		
		// draw pixelation step 2 (shifted)
		if(drawStep2) {
			for(int j=0; j<outline.size(); j++){
				Vector<Path> step2 = (outputParams[0]) ?
										SuperpixelatorHelper.getPixelationStep(outline.get(j), outputParams[0], outputParams[1], 2):
											SuperpixelatorHelper.getPixelationStep(outline.get(j), outputParams[0], outputParams[1], 1);
				for(int i=0; i<step2.size(); i++) {
					g.setColor((i%2==0) ? Color.cyan : Color.magenta);
					drawVectorPath(g, step2.get(i).getTransformedCopy(atScaleTranslate));
				}
			}
		}
		// draw pixelation step 4 (pixelated path)
		if(drawStep3) {
			for(int j=0; j<outline.size(); j++){
				Vector<MyPoint> pixelPathPts = SuperpixelatorHelper.getPixelatedPath(outline.get(j), outputParams[0], outputParams[1]);
				Path pixelPath = new Path(); pixelPath.addPoints(pixelPathPts);
				g.setColor(Color.red);
				drawVectorPath(g, pixelPath.getTransformedCopy(atScaleTranslate));
			}
		}
		g.setState(gs);
	}

	public static void drawVectorPath(MyGraphics2D g, Path path) {
		int rasterizerType = g.getRasterizerType();
		if(rasterizerType == Rasterizer.SUPERPIXELATOR) {
			GraphicsState gs = g.getState();
			g.setIdentityTransform();
			g.setRasterizerType(Rasterizer.DEFAULT);
			//drawPixelGrid(g, zoom);
			g.drawPath(path);
			g.setState(gs);
		}
	}
	
	public static void testDrawRasterAndVectorOvals(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		boolean outputParams[] = new boolean[2];
		Path path = SuperpixelatorHelper.getOvalPath(g, 0, 0, w, h, outputParams);
		path.transform(AffineTransform.getScaleInstance(zoom, zoom));
		g.setColor(Color.black);
		g.drawOval(0, 0, w, h);
		g.setState(gs);
		g.setColor(Color.green);
		drawVectorPath(g, path);		
	}

	public static void drawPixelGrid(MyGraphics2D g, double zoom) {
		GraphicsState gs = g.getState();
		g.setColor(Color.lightGray);
		int w = g.getWidth(), h = g.getHeight();
		for(int x = 0; x < w; x+=zoom) {
			g.drawLine(x, 0, x, h-1);
		}
		for(int y = 0; y < h; y+=zoom) {
			g.drawLine(0, y, w-1, y);
		}
		g.setState(gs);
	}

	public static void testDrawManyTriangles(MyGraphics2D g, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		Vector<int[]> dims = new Vector<int[]>();
		for(int i=0; i<8; i++) {
			int[] dim = {10, 5*(i+1)};
			dims.add(dim);
		}
		for(int i=0; i<dims.size(); i++) {
			Path path = createTrianglePath(dims.get(i)[0], dims.get(i)[1]);
			g.setColor(c2);
			g.drawPath(path);
			//g.setColor(c1);
			//g.fillOval(0,0,i,i);
			g.translate(dims.get(i)[0]+3, 0);
		}
		g.setState(gs);
	}
	
	public static void testDrawManyOvals(MyGraphics2D g, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		for(int i=1; i<20; i++) {
			g.setColor(c2);
			g.drawOval(0,0,i,i);
			g.setColor(c1);
			//g.fillOval(0,0,i,i);
			g.translate(i+1,i+1);
		}
		g.setState(gs);
	}
	
	public static void testDrawArc(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		int startAngle = -45, arcAngle = 270;
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		g.setColor(c1); g.fillArc(0, 0, w, h, startAngle, arcAngle);
		g.setColor(c2); g.drawArc(0, 0, w, h, startAngle, arcAngle);
		g.setState(gs);
	}

	public static void testDrawPath1(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		//(28.72,41.17), p1 = (28.72,41.17), p2 = (16.72,21.83), p3 = (28.38,24.0)
		Path path = new Path();
		path.moveTo(28.72,41.17);
		path.curveTo(28.72,41.17, 16.72,21.83, 28.38,24.0);
		g.setColor(c1); g.fillPath(path);
		g.setColor(c2); g.drawPath(path);
		g.setState(gs);
	}	
	
	public static void testDrawPolygon(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		int[] xPoints = {(int) (w*lines[0].x), (int) (w*lines[1].x), (int) (w*lines[2].x), (int) (w*lines[3].x)};
		int[] yPoints = {(int) (h*lines[0].y), (int) (h*lines[1].y), (int) (h*lines[2].y), (int) (h*lines[3].y)};
		int nPoints = xPoints.length;
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		//g.setColor(c1); g.fillPolygon(xPoints, yPoints, nPoints);
		g.setColor(c2); g.drawPolygon(xPoints, yPoints, nPoints);
		g.setState(gs);
	}

	public static void testDrawCircularShell(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		//g.setColor(c1); g.fillOval(0, 0, w, h);
		g.setColor(c2);
		int r1 = w/2;
		g.drawOval(0, 0, 2*r1, 2*r1);
		int r2 = (int) (0.3*w);
		g.drawOval(w/20, h/2-r2, 2*r2, 2*r2);
		int r3 = (int) (0.15*w);
		g.drawOval(w/10, h/2-r3, 2*r3, 2*r3);
		int r4 = (int) (0.045*w);
		g.drawOval((int) (w*0.53), h/12, 2*r4, 2*r4);
		int r5 = (int) (0.055*w);
		g.drawOval((int) (w*0.7), h/5, 2*r5, 2*r5);
		int r6 = (int) (0.07*w);
		g.drawOval((int) (w*0.77), h/2-r6, 2*r6, 2*r6);
		int r7 = (int) (0.085*w);
		g.drawOval((int) (w*0.65), h/2+h/5, 2*r7, 2*r7);
		g.setState(gs);
	}
	
	public static void testDrawEllipticalShell(MyGraphics2D g, int w, int h, int dx, int dy, int row, int col, double zoom) {
		GraphicsState gs = g.getState();
		g.setZoom(zoom);
		g.translate(1+col*dx, 1+row*dy);
		//g.setColor(c1); g.fillOval(0, 0, w, h);
		GraphicsState gs1 = g.getState();
		g.setColor(c2);
		
		int r1 = w/2;
		g.scale(0.9, 1.2);
		g.drawOval(0, 0, 2*r1, 2*r1);
		
		int r2 = (int) (0.25*w);
		g.drawOval(w/15, h/2-r2, 2*r2, 2*r2);
		
		int r3 = (int) (0.1*w);
		g.drawOval(w/10, h/2-r3, 2*r3, 2*r3);		
		g.setState(gs1);
		
		int r4 = (int) (0.045*w);
		g.translate((int) (w*0.45), h/10);
		g.rotate(0.4);
		g.drawOval(0, 0, 2*r4, 4*r4);
		g.setState(gs1);
		
		int r5 = (int) (0.06*w);
		g.translate((int) (w*0.7), h/4);
		g.rotate(0.9);
		g.drawOval(0,0, 2*r5, 4*r5);
		g.setState(gs1);
		
		int r6 = (int) (0.09*w);
		g.translate((int) (w*0.82), (int) (0.59*h));
		g.rotate(1.4);
		g.drawOval(0, 0, (int) (1.5*r6), 3*r6);
		g.setState(gs1);

		int r7 = (int) (0.085*w);
		g.translate((int) (w*0.75), (int) (0.9*h));
		g.rotate(1.8);
		g.drawOval(0, 0, 2*r7, (int) (3.5*r7));
		g.setState(gs);
	}
	
	public static void testDraw(MyGraphics2D g) {
		double zoom = 1;
		int w = 50, h = 50;
		double absW = w*zoom, absH = h*zoom, absDx = absW + 20, absDy = absH + 20;
		int dx = (int) (absDx/zoom), dy = (int) (absDy/zoom);
		GraphicsState gs = g.getState();
		//g.rotate(Math.PI/5);
		//g.scale(1.1, 0.8);
		//g.translate(0,-80);
		//g.setStrokeWidth(2);
		g.setAA(false);
		
		//testDrawCircularShell(g, w, h, dx, dy, 1, 4, zoom);
		//testDrawLotus(g, w, h, dx, dy, 2, 5, zoom);
		//g.rotate(Math.PI/4);
		//testDrawEllipticalShell(g, w, h, dx, dy, 1, 5, zoom);
		//g.rotate(Math.PI/8); testDrawOval(g, 20, 30, dx, dy, 0, 0, zoom);
		
		w = 50; h = 50;
		absW = w*zoom; absH = h*zoom; absDx = absW + 20; absDy = absH + 20;
		dx = (int) (absDx/zoom); dy = (int) (absDy/zoom);
		/*
		testDrawShape(g, w, h, dx, dy, 1, 1, zoom);
		
		testDrawPath(g, w, h, dx, dy, 1, 2, zoom);
		testDrawPathWithInflection(g, w, h, dx, dy, 1, 3, zoom);
		//testDrawImage1(g, w, h, dx, dy, 0, 3, zoom);
		
		testDrawLines(g, w, h, dx, dy, 2, 1, zoom);
		testDrawRect(g, w, h, dx, dy, 2, 2, zoom);
		testDrawRoundRect(g, w, h, dx, dy, 2, 3, zoom);
		testDrawOval(g, w, h, dx, dy, 2, 4, zoom);
		*/
		w = 17; h = 8;
		absW = w*zoom; absH = h*zoom; absDx = absW + 20; absDy = absH + 20;
		dx = (int) (absDx/zoom); dy = (int) (absDy/zoom);
StopWatch sw = new StopWatch();
sw.start();
		testDrawRotatedOvals(g, w, h, dx, dy, 1, 1, zoom);
		testDrawRotatedRects(g, w, h, dx, dy, 2, 1, zoom);
		testDrawRotatedRoundRects(g, w, h, dx, dy, 3, 1, zoom);
		testDrawManyStars(g, w, h, dx, dy, 4, 1, zoom);
		sw.stop();
		sw.print();
		//testDrawPath1(g, w, h, dx, dy, 0, 0, zoom);
		//testDrawRotatedPaths(g, w, h, dx, dy, 7, 0, zoom);
		//testDrawLines(g, w, h, dx, dy, 0, 0, zoom);
		//testDrawRasterAndVectorOvals(g, w, h, dx, dy, 0, 0, zoom);
		//testDrawPixelationSteps(g, w, h, dx, dy, 1, 1, zoom);
		//testDrawThickPixelationSteps(g, w, h, dx, dy, 2, 2, zoom);
		//testDrawManyOvals(g, dx, dy, 7, 7, zoom);
		//testDrawManyRegularPolygons(g, w, h, dx, dy, 0, 0, zoom);
		//testDrawManyTriangles(g, dx, dy, 0, 0, zoom);
		//testDrawArc(g, w, h, dx, dy, 0, 1, zoom);
		//testDrawPolygon(g, w, h, dx, dy, 0, 2, zoom);
		
		//testDrawString(g, w, h, dx, dy, 0, 5, zoom);
		//testDrawGlyphVector(g, w, h, dx, dy, 0, 1, zoom);
		g.setState(gs);
	}
}
