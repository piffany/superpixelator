package myWidget.panel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import myMath.State;
import myWidget.AddLayerDialog;
import myWidget.RenameLayerDialog;
import myWidget.container.Paint;


@SuppressWarnings("serial")
public class LayerPanel
extends ToolBarPanel
implements ActionListener, ListSelectionListener {

	private JLabel layerLabel;
	private JScrollPane scrollPane = new JScrollPane();
	private DefaultListModel<CheckListItem> layerModel = new DefaultListModel<CheckListItem>();
	private JList<CheckListItem> layerList;
	private JPopupMenu popup;
	private JMenuItem addLayerMenu = new JMenuItem("Add layer..."),
					  deleteLayerMenu = new JMenuItem("Delete layer"),
					  renameLayerMenu = new JMenuItem("Rename layer..."),
					  showLayerMenu = new JMenuItem("Show layer"),
					  hideLayerMenu = new JMenuItem("Hide layer"),
					  rasterizeLayerMenu = new JMenuItem("Rasterize layer");
	private boolean manualAdjust = false;
	private ListCellRenderer<CheckListItem> renderer;
	private ImageIcon eyeIcon = new ImageIcon(getClass().getResource("/icons/eye.gif"));
	private ImageIcon noEyeIcon = new ImageIcon(getClass().getResource("/icons/noEye.gif"));
	private JButton upButton, downButton;
					

	public LayerPanel(State state) {
		super(state);
		addWidgets();
	}
	
	private void initButtons() {
		upButton = new JButton(new ImageIcon(getClass().getResource("/icons/up.gif")));
		downButton = new JButton(new ImageIcon(getClass().getResource("/icons/down.gif")));
		upButton.setMargin(new Insets(-1,-1,-1,-1));
		downButton.setMargin(new Insets(-1,-1,-1,-1));
		upButton.addActionListener(this);
		downButton.addActionListener(this);
	}

	private void addWidgets() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS ) );
		JPanel buttonPanel = new JPanel();
		layerLabel = new JLabel("Layers:");
		buttonPanel.add(layerLabel);
		initButtons();
		buttonPanel.add(upButton);
		buttonPanel.add(downButton);
		add(buttonPanel);
		buttonPanel.setAlignmentX(CENTER_ALIGNMENT);
		layerList = new JList<CheckListItem>(layerModel);
		layerList.addListSelectionListener(this);
		layerList.setVisibleRowCount(3);
		layerList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		renderer = new CheckListRenderer();
		layerList.setCellRenderer(renderer);
		layerList.setFixedCellWidth(1);
		scrollPane.getViewport().setView(layerList);
		scrollPane.setAlignmentX(CENTER_ALIGNMENT);
		add(scrollPane);
		setupPopupMenu();
	}

	private void setupPopupMenu() {
		popup = new JPopupMenu();
		addLayerMenu.addActionListener(this);
		popup.add(addLayerMenu);
		deleteLayerMenu.addActionListener(this);
		popup.add(deleteLayerMenu);
		renameLayerMenu.addActionListener(this);
		popup.add(renameLayerMenu);
		showLayerMenu.addActionListener(this);
		popup.add(showLayerMenu);
		hideLayerMenu.addActionListener(this);
		popup.add(hideLayerMenu);
		rasterizeLayerMenu.addActionListener(this);
		popup.add(rasterizeLayerMenu);

		//Add listener to components that can bring up popup menus.
		MouseListener popupListener = new PopupListener();
		layerList.addMouseListener(popupListener);
	}
	
	private void updatePopupMenus() {
		boolean fileOpen = state.isFileOpen();
		boolean moreThanOneLayer = state.getNumLayers() > 1;
		boolean visible = state.isSelectedLayerVisible();
		boolean hasSelectedLayer = state.hasSelectedLayer();
		int selectedIndex = state.getSelectedLayerIndex();
		boolean bottomLayerSelected = hasSelectedLayer && selectedIndex==0;
		boolean topLayerSelected = hasSelectedLayer && selectedIndex==state.getNumLayers()-1;
		boolean vectorLayerSelected = hasSelectedLayer && state.getSelectedLayerType() == State.VECTOR;
		addLayerMenu.setEnabled(fileOpen);
		deleteLayerMenu.setEnabled(fileOpen && moreThanOneLayer);
		renameLayerMenu.setEnabled(fileOpen);
		showLayerMenu.setEnabled(fileOpen && !visible);
		hideLayerMenu.setEnabled(fileOpen && visible);
		rasterizeLayerMenu.setEnabled(fileOpen && vectorLayerSelected);
		upButton.setEnabled(hasSelectedLayer && !topLayerSelected);
		downButton.setEnabled(hasSelectedLayer && !bottomLayerSelected);
	}
	
	public void updateDisplay() {
		updatePopupMenus();
		if(!state.isLayerChanged()) return;
		try{
			Vector<String> layerNames = state.getLayerNames();
			manualAdjust = true;
			layerModel.clear();
			for(int i=0; i<layerNames.size(); i++) {
				layerModel.add(0,new CheckListItem(layerNames.get(i)));
			}
			layerList.setSelectedIndex(layerNames.size()-1-state.getSelectedLayerIndex());
			manualAdjust = false;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void addLayer() {
		Paint.removeKeyEventDispatcher();
		new AddLayerDialog(state);
		Paint.addKeyEventDispatcher();
	}
	
	private void deleteLayer() {
		state.deleteSelectedLayer();
	}

	private void renameLayer() {
		Paint.removeKeyEventDispatcher();
		new RenameLayerDialog(state);
		Paint.addKeyEventDispatcher();
	}

	private void showLayer() {
		state.setSelectedLayerVisible(true);
	}

	private void hideLayer() {
		state.setSelectedLayerVisible(false);
	}
	
	private void rasterizeLayer() {
		state.rasterizeSelectedLayer();
	}
	
	private void moveLayerUp() {
		state.moveSelectedLayerUp();
	}
	
	private void moveLayerDown() {
		state.moveSelectedLayerDown();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == addLayerMenu) addLayer();
		else if(e.getSource() == deleteLayerMenu) deleteLayer();
		else if(e.getSource() == renameLayerMenu) renameLayer();
		else if(e.getSource() == showLayerMenu) showLayer();
		else if(e.getSource() == hideLayerMenu) hideLayer();
		else if(e.getSource() == rasterizeLayerMenu) rasterizeLayer();
		else if(e.getSource() == upButton) moveLayerUp();
		else if(e.getSource() == downButton) moveLayerDown();
	}

	class PopupListener extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			showPopup(e);
		}

		public void mouseReleased(MouseEvent e) {
			showPopup(e);
		}

		private void showPopup(MouseEvent e) {
			if (e.isPopupTrigger()) {
				layerList.setSelectedIndex( layerList.locationToIndex(e.getPoint()) );
				popup.show(layerList, e.getX(), e.getY());
			}
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(!e.getValueIsAdjusting() && !manualAdjust) {
			int selectedIndex = state.getNumLayers()-1-layerList.getSelectedIndex();
			state.setSelectedLayerIndex(selectedIndex);
		}
	}

	// helpdesk.objects.com.au/java/how-do-add-a-checkbox-to-items-in-a-jlist
	// Represents items in the list that can be selected
	class CheckListItem
	{
		private String text;
		private boolean isVisible = false;
		public CheckListItem(String text)
		{
			this.text = text;
		}
		public boolean isVisible()
		{
			return isVisible;
		}

		public void setVisible(boolean isVisible)
		{
			this.isVisible = isVisible;
		}

		public String getText()
		{
			return text;
		}
	}
	// Handles rendering cells in the list using a check box
	class CheckListRenderer extends JCheckBox
	implements ListCellRenderer<CheckListItem>
	{	
		public CheckListRenderer() {
			super();
		}

		public CheckListRenderer(Icon icon) {
			super(icon);
		}
		
		@Override
		public Component getListCellRendererComponent(
				JList<? extends CheckListItem> list, CheckListItem value, int index,
				boolean isSelected, boolean cellHasFocus)
		{
			try {
				//setEnabled(list.isEnabled());
				setSelected(state.isLayerVisible(state.getNumLayers()-1-index));

				setFont(list.getFont());
				Color backgroundColor = isSelected ? list.getSelectionBackground() : list.getBackground();
				setBackground(backgroundColor);
				setForeground(list.getForeground());
				setText(value.getText());
			}
			catch(Exception e) {
				//e.printStackTrace();
			}
			return this;
		}

		public void setSelected(boolean b) {
			try{
				super.setSelected(b);
				if(b) setIcon(eyeIcon);
				else setIcon(noEyeIcon);
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}

	}
}


