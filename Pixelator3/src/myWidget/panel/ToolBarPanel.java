package myWidget.panel;
import javax.swing.JPanel;

import myMath.State;


@SuppressWarnings("serial")
abstract public class ToolBarPanel
extends JPanel
{
	protected State state;
	
	public ToolBarPanel(State state) {
		super();
		this.state = state;
	}
	
	public abstract void updateDisplay();

}
