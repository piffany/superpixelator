package myWidget.panel;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import colours.Colour;

import myMath.PathState;
import myMath.State;
import myWidget.ColorButton;
import myWidget.ColorToolButton;

@SuppressWarnings("serial")
public class ColorButtonPanel
extends ToolBarPanel 
implements ActionListener, MouseListener {

	private final Dimension iconSize = new Dimension(40,40);
	private ColorButton strokeColorButton, fillColorButton;
	private ColorToolButton transparentButton, defaultButton, swapButton;
	
	public ColorButtonPanel(State state) {
		super(state);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS ) );
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
		strokeColorButton = new ColorButton(state.getPathState().getStrokeColor(), iconSize, PathState.STROKE);
		fillColorButton = new ColorButton(state.getPathState().getFillColor(), iconSize, PathState.FILL);
		strokeColorButton.setToolTipText("Stroke");
		fillColorButton.setToolTipText("Fill");
		strokeColorButton.addMouseListener(this);
		fillColorButton.addMouseListener(this);
		topPanel.add(strokeColorButton);
		topPanel.add(Box.createRigidArea(new Dimension(5,0)));
		topPanel.add(fillColorButton);
		add(topPanel);
		add(Box.createRigidArea(new Dimension(0,5)));
		// other tools
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS ) );
		transparentButton = new ColorToolButton("Transparent", new ImageIcon(getClass().getResource("/icons/transparent.gif")));
		transparentButton.addMouseListener(this);
		defaultButton = new ColorToolButton("Default", new ImageIcon(getClass().getResource("/icons/defaultColors.gif")));
		defaultButton.addMouseListener(this);
		swapButton = new ColorToolButton("Swap", new ImageIcon(getClass().getResource("/icons/swap.gif")));
		swapButton.addMouseListener(this);
		bottomPanel.add(transparentButton);
		bottomPanel.add(Box.createRigidArea(new Dimension(5,0)));
		bottomPanel.add(defaultButton);
		bottomPanel.add(Box.createRigidArea(new Dimension(5,0)));
		bottomPanel.add(swapButton);
		add(bottomPanel);
		setSelected(PathState.STROKE);
		setAlignmentX(CENTER_ALIGNMENT);
	}
			
	public void updateDisplay() {
		strokeColorButton.setColor(state.getPathState().getStrokeColor());
		fillColorButton.setColor(state.getPathState().getFillColor());
	}
	
	public boolean transparentColorSelected() {
		if(strokeColorButton.isSelected()) return state.getPathState().getStrokeColor().getAlpha()==0;
		else return state.getPathState().getFillColor().getAlpha()==0;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	public void setSelectedColor(Colour c) {
		if(strokeColorButton.isSelected()) {
			strokeColorButton.setColor(c);
		}
		else {
			fillColorButton.setColor(c);
		}
		
	}
	
	public void setSelectedColorTransparent() {
		if(strokeColorButton.isSelected()) strokeColorButton.setTransparent();
		else fillColorButton.setTransparent();
	}
	
	private void setSelected(int selected) {
		strokeColorButton.setSelected(selected == PathState.STROKE);
		fillColorButton.setSelected(selected == PathState.FILL);
		state.setSelectedColorType(selected);
	}
	
	public int getSelected() {
		if(strokeColorButton.isSelected()) return PathState.STROKE;
		else return PathState.FILL;
	}
	
	public Colour getSelectedColor() {
		if(strokeColorButton.isSelected()) return strokeColorButton.getColor();
		else return fillColorButton.getColor();
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		int clicks = e.getClickCount();
		if(e.getSource() == strokeColorButton) {
			if(clicks == 1) setSelected(PathState.STROKE);
			else if(clicks == 2) {
				strokeColorButton.openColorChooser();
				state.setStrokeColor(strokeColorButton.getColor());
			}
		}
		else if(e.getSource() == fillColorButton) {
			if(clicks == 1) setSelected(PathState.FILL);
			else if(clicks == 2) {
				fillColorButton.openColorChooser();
				state.setFillColor(fillColorButton.getColor());
			}
		}
		else if(e.getSource() == transparentButton) {
			if(getSelected() == PathState.STROKE) state.setStrokeColorTransparent();
			else state.setFillColorTransparent();
		}
		else if(e.getSource() == defaultButton) {
			strokeColorButton.setColor(state.getPathState().DEFAULT_STROKE_COLOR);
			fillColorButton.setColor(state.getPathState().DEFAULT_FILL_COLOR);
			state.setDefaultStrokeAndFillColors();
		}
		else if(e.getSource() == swapButton) {
			strokeColorButton.setColor(state.getPathState().getFillColor());
			fillColorButton.setColor(state.getPathState().getStrokeColor());
			state.swapStrokeAndFillColors();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}
	
}
