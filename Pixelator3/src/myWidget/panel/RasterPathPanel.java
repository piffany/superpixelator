package myWidget.panel;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import myMath.State;

@SuppressWarnings("serial")
public class RasterPathPanel
extends ToolBarPanel
implements ChangeListener {

	private JLabel weightLabel;
	private JSpinner weightSpinner;
	private SpinnerModel weightModel;
	//private NumericFilter numFilter = new NumericFilter();
	
	public RasterPathPanel(State state) {
		super(state);
		init();
	}
	
	private void init() {
		weightLabel = new JLabel("Weight:");
		weightSpinner = new JSpinner();
		weightModel = new SpinnerNumberModel(state.getPathState().getStrokeWeight(), 0, 100, 1);
		weightSpinner.setModel(weightModel);
		weightSpinner.addChangeListener(this);
		//JTextField tf = ((JSpinner.DefaultEditor) weightSpinner.getEditor()).getTextField();
		//((AbstractDocument) tf.getDocument()).setDocumentFilter(numFilter);
		//tf.setEditable(false);
		add(weightLabel);
		add(weightSpinner);
		updateState();
	}

	public void updateState() {
		state.setStrokeWeight((double)weightSpinner.getValue());
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		updateState();
	}
	
	public void updateDisplay() {
		weightSpinner.setValue(state.getPathState().getStrokeWeight());
		setEnabled(state.getSelectedLayerType()==State.RASTER);
	}

}
