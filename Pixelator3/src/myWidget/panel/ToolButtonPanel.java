package myWidget.panel;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import myMath.State;
import myTool.Tool;
import myTool.ToolKit;
import myWidget.ToolButton;


@SuppressWarnings("serial")
public class ToolButtonPanel
extends ToolBarPanel
implements ActionListener {

	private ToolKit toolKit;
	private JPanel rasterToolButtonPanel = new JPanel();
	private JPanel vectorToolButtonPanel = new JPanel();
	private Vector<ToolButton> toolButtons = new Vector<ToolButton>();
	private JLabel rasterLabel, vectorLabel;
	
	public ToolButtonPanel(State state) {
		super(state);
		toolKit = new ToolKit();
		addWidgets();
	}
		
	private void addToolButton(ToolButton tb, int index) {
		tb.setAlignmentX(CENTER_ALIGNMENT);
		if(tb.getType() == State.RASTER) rasterToolButtonPanel.add(tb);
		else if(tb.getType() == State.VECTOR) vectorToolButtonPanel.add(tb);
		tb.addActionListener(this);
		toolButtons.add(tb);
	}
	
	private void addWidgets() {
		setLayout(new GridLayout(0,2));
		rasterToolButtonPanel.setLayout(new BoxLayout(rasterToolButtonPanel, BoxLayout.Y_AXIS ) );
		vectorToolButtonPanel.setLayout(new BoxLayout(vectorToolButtonPanel, BoxLayout.Y_AXIS ) );
		add(rasterToolButtonPanel);
		add(vectorToolButtonPanel);
		rasterLabel = new JLabel("Raster");
		vectorLabel = new JLabel("Vector");
		rasterLabel.setAlignmentX(CENTER_ALIGNMENT);
		vectorLabel.setAlignmentX(CENTER_ALIGNMENT);
		rasterToolButtonPanel.add(rasterLabel);
		rasterToolButtonPanel.add(Box.createRigidArea(new Dimension(0,5)));
		vectorToolButtonPanel.add(vectorLabel);
		vectorToolButtonPanel.add(Box.createRigidArea(new Dimension(0,5)));
		for(int i=0; i<toolKit.size(); i++) {
			Tool tool = toolKit.getTool(i);
			addToolButton(new ToolButton(tool.getID(), tool.getTooltip(), tool.getIcon(), tool.getType()), i);
		}
		setAlignmentX(CENTER_ALIGNMENT);
	}

	public void actionPerformed(ActionEvent e) {
		ToolButton source = (ToolButton) e.getSource();
		String selectedID = source.getID();
		Tool selectedTool = toolKit.findToolByID(selectedID);
		state.setSelectedTool(selectedTool);
	}

	public void updateDisplay() {
		if(!state.isToolChanged()) return;
		int layerType = state.getSelectedLayerType();
		for(int i=0; i<toolButtons.size(); i++) {
			ToolButton toolButton = toolButtons.get(i);
			Tool tool = toolKit.getTool(i);
			boolean sameType = (layerType == toolButton.getType());
			boolean isSelected = (toolButton.getID() == state.getSelectedTool().getID());
			toolButton.setEnabled(sameType);
			toolButton.setSelected(isSelected);
			tool.setSelectedTool(isSelected, state);
			//if(tool.getType() == State.RASTER) ((RasterTool) tool).setSelected(isSelected, state);
			//else if(tool.getType() == State.VECTOR) ((VectorTool) tool).setSelected(isSelected, state);
		}
	}
	
	public Tool getDefaultTool(int type) {
		return toolKit.getDefaultTool(type);
	}

}
