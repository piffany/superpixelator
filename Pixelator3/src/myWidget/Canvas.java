package myWidget;
// source: http://www.java2s.com/Code/Java/2D-Graphics-GUI/Drawcanvaswithcolorandtext.htm

import java.awt.*;

import javax.swing.*;

import org.apache.batik.svggen.SVGGraphics2D;

import colours.Colour;

import renderer.display.CustomRasterizedDoubleBufferedCanvas;
import renderer.display.MyGraphics2D;
import renderer.rasterizer.Rasterizer;
import myLayer.*;
import myMath.MyDraw;
import myMath.State;
import myTool.rasterizer.*;

import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Vector;
import myWidget.container.Paint;

@SuppressWarnings("serial")
public class Canvas extends CustomRasterizedDoubleBufferedCanvas
implements MouseListener, MouseMotionListener {

	final public static int MIN_WIDTH = 1, MIN_HEIGHT = 1;
	final public static int MAX_WIDTH = 1000, MAX_HEIGHT = 1000;
	private State state = new State();

	private void init() {
		setSize(state.getWidth(), state.getHeight());
		setBorder(BorderFactory.createLineBorder(Color.black));
		setSize(getPreferredSize());
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	public Canvas() {
		init();
		repaint();
	}

	public Canvas(State state) {
		this.state = state;
		init();
		repaint();
	}

	public void setMinimumSize() {
		setSize(MIN_WIDTH, MIN_HEIGHT);
		state.setMinimumSize();
	}
	
	public boolean isMinimumSize() {
		return getWidth()==Canvas.MIN_WIDTH && getHeight()==Canvas.MIN_HEIGHT;
	}
	
	public Dimension getPreferredSize() {
		return new Dimension(state.getTransformedWidth(), state.getTransformedHeight());
	}
	
	private void drawPixels(MyGraphics2D mg, boolean withVectorGuides) {
		mg.setAA(state.getAA());
		revalidate();
		AffineTransform at = state.getTransform();
		Vector<Layer> layers = state.getLayers();
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).isVisible()) {
				if(layers.get(i).getType() == State.RASTER) {
					RasterLayer rasterLayer = (RasterLayer) layers.get(i);
					MyDraw.drawRasterLayer(mg, rasterLayer, at);
				}
				else {
					VectorLayer vectorLayer = (VectorLayer) layers.get(i);
					// draw pixels
					if(mg.getRasterizerType()==Rasterizer.DEFAULT) {
						JavaRasterizer.draw(mg, vectorLayer, at, state);
					}
					else {
						Pixelator.draw(mg, vectorLayer, at, state);
					}
					// draw control points and vector path
					if(withVectorGuides) MyDraw.drawVectorLayer(mg, vectorLayer, at, state);
				}
			}
		}
	}
	
	private void drawVectorGuides(MyGraphics2D mg) {
		mg.setAA(false);
		revalidate();
		AffineTransform at = state.getTransform();
		Vector<Layer> layers = state.getLayers();
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).isVisible() && state.getSelectedLayerIndex()==i) {
				if(layers.get(i).getType() == State.VECTOR) {
					VectorLayer vectorLayer = (VectorLayer) layers.get(i);
					MyDraw.drawVectorLayer(mg, vectorLayer, at, state);
				}
			}
		}
	}

	public void myPaint(MyGraphics2D mg) {
		boolean splitView = state.getSplitView();
		mg.setAA(state.getAA());
		mg.setRasterizerType(state.getRasterizerType());
		revalidate();
		BufferedImage imVector = new BufferedImage(mg.getWidth(), mg.getHeight(), BufferedImage.TYPE_INT_ARGB);
		MyGraphics2D gVector = new MyGraphics2D((Graphics2D) imVector.getGraphics(), mg.getWidth(), mg.getHeight());
		gVector.setBackground(Color.white);
		gVector.setState(mg.getState());
		BufferedImage imPixelator2 = new BufferedImage(mg.getWidth(), mg.getHeight(), BufferedImage.TYPE_INT_ARGB);
		MyGraphics2D gPixelator = new MyGraphics2D((Graphics2D) imPixelator2.getGraphics(), mg.getWidth(), mg.getHeight());
		gPixelator.setBackground(Color.white);
		gPixelator.setState(mg.getState());
		gPixelator.setRasterizerType(Rasterizer.SUPERPIXELATOR);
		BufferedImage imJava = new BufferedImage(mg.getWidth(), mg.getHeight(), BufferedImage.TYPE_INT_ARGB);
		MyGraphics2D gJava = new MyGraphics2D((Graphics2D) imJava.getGraphics(), mg.getWidth(), mg.getHeight());
		gJava.setBackground(Color.white);
		gJava.setState(mg.getState());
		gJava.setRasterizerType(Rasterizer.DEFAULT);
		//drawPixels(mg, true);
		drawVectorGuides(gVector);
		drawPixels(gPixelator, false);
		drawPixels(gJava, false);
		/*
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).isVisible()) {
				if(layers.get(i).getType() == State.RASTER) {
					RasterLayer rasterLayer = (RasterLayer) layers.get(i);
					MyDraw.drawRasterLayer(g, rasterLayer, at);
					MyDraw.drawRasterLayer(mg, rasterLayer, at);
				}
				else {
					VectorLayer vectorLayer = (VectorLayer) layers.get(i);
					// draw pixels
					if(state.getRasterizerType()==Rasterizer.DEFAULT) {
						JavaRasterizer.draw(g, vectorLayer, at, state);
						JavaRasterizer.draw(mg, vectorLayer, at, state);
					}
					else {
						Pixelator.draw(g, vectorLayer, at, state);
						Pixelator.draw(mg, vectorLayer, at, state);
					}
					// draw control points and vector path
					MyDraw.drawVectorLayer(mg, vectorLayer, at, state);
				}
			}
		}
		*/
		//drawPixelGrid(mg);
		
			int scale = (int) Math.max(1, state.getScale());
			BufferedImage thumbPixelator = shrinkImage(imPixelator2, scale);
			BufferedImage thumbJava = shrinkImage(imJava, scale);
			double scale1 = mg.getZoom();
			double scale2 = state.getScale();
			mg.setZoom(scale2);
			if(state.getRasterizerType() == Rasterizer.SUPERPIXELATOR) {
				mg.drawImage(thumbPixelator, 0, 0, state.getWidth(), state.getHeight(), null);
				if(splitView) mg.drawImage(thumbJava, (int) (state.getWidth()*0.5), 0, (int) (state.getWidth()*0.5), (int) (state.getHeight()), null);
			}
			else {
				mg.drawImage(thumbJava, 0, 0, state.getWidth(), state.getHeight(), null);
				if(splitView) mg.drawImage(thumbPixelator, (int) (state.getWidth()*0.5), 0, (int) (state.getWidth()*0.5), (int) (state.getHeight()), null);				
			}
			mg.setZoom(scale1);
			if(!splitView) mg.drawImage(imVector, 0, 0, null);
			if(splitView) {
				mg.setZoom(scale1);
				mg.setColor(Color.black);
				double thumbW1x = 100, thumbH1x = 100;
				int thumbW = (int) Math.min(getWidth()*0.5, thumbW1x*scale2);
				int thumbH = (int) Math.min(getHeight(), thumbH1x*scale2);
				mg.drawRect(0, 0, thumbW, thumbH);
				mg.drawRect(thumbW, 0, thumbW, thumbH);
			}
		
		if(state.getShowThumb()) {
			BufferedImage thumb;
			if(state.getRasterizerType()==Rasterizer.DEFAULT) {
				thumb = shrinkImage(imJava, scale);
			}
			else {
				thumb = shrinkImage(imPixelator2, scale);
			}
			mg.setZoom(scale2);
			mg.drawImage(thumb, 0, 0, 100, 100, null);
			mg.setZoom(scale1);
			mg.setColor(Color.black);
			mg.drawRect(0, 0, 100, 100);
		}
	}
	
	private BufferedImage shrinkImage(BufferedImage image, int scale) {
		BufferedImage thumb = new BufferedImage(state.getWidth(), state.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = (Graphics2D) thumb.getGraphics();
		g2.setColor(Color.white);
		g2.fillRect(0, 0, thumb.getWidth(), thumb.getHeight());
		for(int i=0; i<thumb.getWidth(); i++) {
			for(int j=0; j<thumb.getHeight(); j++) {
				int rgb = image.getRGB(i*scale,j*scale);
				if(Colour.getAlpha(rgb) > 0) thumb.setRGB(i,j,rgb);
			}
		}
		return thumb;
	}
	
	public static void paintToSVG(SVGGraphics2D mg, State state) {
		mg.setBackground(Color.white);
		AffineTransform at = state.getTransform();
		Vector<Layer> layers = state.getLayers();
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getType() == State.RASTER) {
				RasterLayer rasterLayer = (RasterLayer) layers.get(i);
				MyDraw.drawRasterLayerToSVG(mg, rasterLayer, at);
			}
			else {
				VectorLayer vectorLayer = (VectorLayer) layers.get(i);
				if(vectorLayer.isVisible()) {
					if(state.getRasterizerType()==Rasterizer.DEFAULT) JavaRasterizer.draw(mg,  vectorLayer, at, state);
					else Pixelator.draw(mg, vectorLayer, at, state);
					MyDraw.drawVectorLayerToSVG(mg, vectorLayer, at, state);
				}				
			}
		}
		
	}

	public void drawPixelGrid(MyGraphics2D mg) {
		Graphics2D g = mg.getGraphics2D();
		double scale = state.getScale();
		if(scale >= 4) {
			g.setColor(new Color(0,0,0,50));
			int w = getWidth();
			int h = getHeight();
			for(int i=0; i<w; i++) {
				int x = (int) (i*scale);
				g.drawLine(x, 0, x, h);
			}
			for(int j=0; j<h; j++) {
				int y = (int) (j*scale);
				g.drawLine(0, y, w, y);
			}
		}
	}

	public void keyPressed(KeyEvent e) {
		state.canvasKeyPressed(e);
	}

	public void keyReleased(KeyEvent e) {
		state.canvasKeyReleased(e);
	}

	public void keyTyped(KeyEvent e) {
		state.canvasKeyTyped(e);
	}

	public void updateCursor() {
		setCursor(state.getSelectedTool().getCursor());
	}	

	public void updateDisplay() {
		updateCursor();
		setSize(state.getTransformedWidth(), state.getTransformedHeight());
		Vector<Layer> layers = state.getLayers();
		int selectedLayer = state.getSelectedLayerIndex();
		if(Paint.debug) System.out.println("Canvas: update layer " + selectedLayer);
		for(int i=0; i<layers.size(); i++) {
			//layers.get(i).rerender(selectedLayer == i);
			layers.get(i).rerender(true);
		}
		repaint();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		state.canvasMouseDragged(e);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		state.canvasMouseMoved(e);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		state.canvasMouseClicked(e);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		state.canvasMousePressed(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		state.canvasMouseReleased(e);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		state.canvasMouseEntered(e);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		state.canvasMouseExited(e);
	}
}