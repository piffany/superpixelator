package myWidget;
import java.awt.BorderLayout;
import javax.swing.text.AbstractDocument;

import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import myMath.State;
import myMath.MyFilter.NumericFilter;

@SuppressWarnings("serial")
public class NewFileDialog extends JDialog
implements ActionListener {

	private JButton okButton = new JButton("OK");
	private JButton cancelButton = new JButton("Cancel");
	private int width = -1;
	private int height = -1;
	private int type = State.NONE;
	private State state;
	//private JFrame parent = new JFrame();

	private JPanel infoPanel = new JPanel();
	private JPanel widthPanel = new JPanel();
	private JLabel widthLabel = new JLabel("Width:");
	private JTextArea widthArea = new JTextArea("100", 1, 3);
	private JPanel heightPanel = new JPanel();
	private JLabel heightLabel = new JLabel("Height:");
	private JTextArea heightArea = new JTextArea("100", 1, 3);
	private JPanel typePanel = new JPanel();
	private JLabel typeLabel = new JLabel("Type:");
	private String[] types = {"Vector", "Raster"};
	private JComboBox<String> typeCombo = new JComboBox<String>(types);
	private JPanel buttonPane = new JPanel();
	private NumVerifier numVerifier = new NumVerifier();
	private NumericFilter numFilter = new NumericFilter();
	private KeyEventDispatcher keyEventDispatcher;

	public NewFileDialog(State state) {
		super(state.getFrame(), "New Document", true);
		this.state = state;
		JFrame parent = state.getFrame();
		Dimension parentSize = parent.getSize(); 
		Point p = parent.getLocation(); 
		setLocation(p.x + parentSize.width / 3, p.y + parentSize.height / 3);

		widthArea.setInputVerifier(numVerifier);
		widthArea.selectAll();
		heightArea.setInputVerifier(numVerifier);

		widthPanel.add(widthLabel);
		widthPanel.add(widthArea);
		((AbstractDocument) widthArea.getDocument()).setDocumentFilter(numFilter);

		heightPanel.add(heightLabel);
		heightPanel.add(heightArea);
		((AbstractDocument) heightArea.getDocument()).setDocumentFilter(numFilter);

		typePanel.add(typeLabel);
		typePanel.add(typeCombo);

		infoPanel.add(widthPanel);
		infoPanel.add(heightPanel);
		infoPanel.add(typePanel);

		okButton.addActionListener(this);
		okButton.setFocusable(false);
		cancelButton.addActionListener(this);
		cancelButton.setFocusable(false);

		buttonPane.add(okButton);
		buttonPane.add(cancelButton);

		getContentPane().add(infoPanel, BorderLayout.CENTER);

		initKeyEventDispatcher();
		addKeyEventDispatcher();

		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getRootPane().setDefaultButton(okButton);
		pack();
		setVisible(true);
	}

	private void initKeyEventDispatcher() {
		keyEventDispatcher = new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				boolean keyHandled = false;
				if (e.getID() == KeyEvent.KEY_PRESSED) {  
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						ok();  
						keyHandled = true;
					}
					else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {  
						cancel();  
						keyHandled = true;
					}
					else if (e.getKeyCode() == KeyEvent.VK_TAB) {  
						tab();
						keyHandled = true;
					}
				}  
				return keyHandled;  
			}  
		};
	}
	
	private void tab() {
		if(widthArea.hasFocus()) {
			widthArea.transferFocus();
			heightArea.selectAll();
		}
		if(heightArea.hasFocus()) {
			heightArea.transferFocus();
		}
		if(typeCombo.hasFocus()) {
			typeCombo.transferFocus();
			widthArea.selectAll();
		}
	}
	
	private void addKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
	}
	
	private void removeKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);
	}
	
	public void dispose() {
		removeKeyEventDispatcher();
		super.dispose();
	}

	private void ok() {
		boolean valid = numVerifier.verify(widthArea) && numVerifier.verify(heightArea);
		if(valid) {
			int[] vals = numVerifier.getNumEntries(widthArea, heightArea);
			width = vals[0];
			height = vals[1];
			if(typeCombo.getSelectedItem() == "Raster") type = State.RASTER;
			else if(typeCombo.getSelectedItem() == "Vector") type = State.VECTOR;
			setVisible(false); 
			dispose(); 
			state.addNewCanvas(width, height, type);
		}
		else {
			setVisible(false); 
			dispose();
		}
	}

	private void cancel() {
		setVisible(false); 
		dispose();
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton) {
			ok();
		}
		else if(e.getSource() == cancelButton) {
			cancel();
		}
	}

}