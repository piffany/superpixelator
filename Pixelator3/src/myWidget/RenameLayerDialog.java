package myWidget;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import myMath.State;

@SuppressWarnings("serial")
public class RenameLayerDialog extends JDialog
implements ActionListener {

	private State state;
	
	private JButton okButton = new JButton("OK"); 
	private JButton cancelButton = new JButton("Cancel");
	private String name = "";

	private JPanel infoPanel = new JPanel();
	private JPanel namePanel = new JPanel();
	private JLabel nameLabel = new JLabel("Name:");
	private JTextArea nameArea = new JTextArea(name, 1, 6);
	private JPanel buttonPane = new JPanel();
	private KeyEventDispatcher keyEventDispatcher;
	
	public RenameLayerDialog(State state) {
		super(state.getFrame(), "Rename Layer", true);
		this.state = state;
		JFrame parent = state.getFrame();
		Dimension parentSize = parent.getSize(); 
		Point p = parent.getLocation(); 
		setLocation(p.x + parentSize.width / 3, p.y + parentSize.height / 3);
	
		namePanel.add(nameLabel);
		namePanel.add(nameArea);
		nameArea.selectAll();
		
		infoPanel.add(namePanel);
		
		getContentPane().add(infoPanel, BorderLayout.CENTER);
		
		okButton.addActionListener(this);
		okButton.setFocusable(false);
		cancelButton.addActionListener(this);
		cancelButton.setFocusable(false);

		buttonPane.add(okButton);
		buttonPane.add(cancelButton);
		
		initKeyEventDispatcher();
		addKeyEventDispatcher();
		
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getRootPane().setDefaultButton(okButton);
		pack(); 
		setVisible(true);
	}
	
	public String getName() {
		return name;
	}
	
	private void initKeyEventDispatcher() {
		keyEventDispatcher = new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				boolean keyHandled = false;
				if (e.getID() == KeyEvent.KEY_PRESSED) {  
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						ok();  
						keyHandled = true;
					}
					else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {  
						cancel();  
						keyHandled = true;
					}
				}  
				return keyHandled;  
			}  
		};
	}
	
	private void addKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
	}
	
	private void removeKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);
	}
	
	public void dispose() {
		removeKeyEventDispatcher();
		super.dispose();
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton) {
			ok();
		}
		else if(e.getSource() == cancelButton) {
			cancel();
		}
	}
	
	private void ok() {
		name = nameArea.getText();
		if(state.getSelectedLayerType() == State.RASTER) {
			name += " [R]";
		}
		else if(state.getSelectedLayerType() == State.VECTOR) {
			name += " [V]";
		}
		setVisible(false);
		dispose();
		state.renameLayer(name);
	}

	private void cancel() {
		setVisible(false); 
		dispose();
	}

	
}