package myWidget;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;


@SuppressWarnings("serial")
public class ColorToolButton
extends JButton {
	
	private String tooltip;
	private ImageIcon icon;
	
	public ColorToolButton() {
		super();
		setMargin(new Insets(-1,-1,-1,-1));
		setFocusable(false);
	}
	
	public ColorToolButton(String tooltip, ImageIcon icon) {
		super();
		setMargin(new Insets(-1,-1,-1,-1));
		setFocusable(false);
		this.icon = icon;
		setIcon(this.icon);
		this.tooltip = tooltip;
		setToolTipText(this.tooltip);
	}
	
	public String getTooltip() {
		return tooltip;
	}
}
