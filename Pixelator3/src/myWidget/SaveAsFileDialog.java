package myWidget;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public class SaveAsFileDialog extends JFileChooser {

	String extension;
	
	public SaveAsFileDialog(String name, String extension) {
		super(name);
		this.extension = extension;
		this.addChoosableFileFilter(new ImageFilter());
	}
	
	public void approveSelection(){
        File f = getSelectedFile();
        if(f.getPath().toLowerCase().endsWith(extension)) {
	        if(f.exists()){
	            int result = JOptionPane.showConfirmDialog(this,"The file exists, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
	            switch(result){
	                case JOptionPane.YES_OPTION:
	                    super.approveSelection();
	                    return;
	                case JOptionPane.NO_OPTION:
	                    return;
	                case JOptionPane.CLOSED_OPTION:
	                    return;
	                case JOptionPane.CANCEL_OPTION:
	                    cancelSelection();
	                    return;
	            }
	        }
	        else {
	        	super.approveSelection();
                return;
	        }
        }
        else {
        	JOptionPane.showMessageDialog(this,"File name must end with " + extension + ".", "Warning", JOptionPane.YES_NO_CANCEL_OPTION);
        	return;
        }
    }
}
