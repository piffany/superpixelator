package myWidget;

import javax.swing.JFileChooser;

@SuppressWarnings("serial")
public class OpenFileDialog extends JFileChooser {

	public OpenFileDialog() {
		super();
		this.addChoosableFileFilter(new ImageFilter());
	}
}
