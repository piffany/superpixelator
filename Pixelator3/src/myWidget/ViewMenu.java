package myWidget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import myMath.State;
import myWidget.container.Paint;


// Encapsulate the look and behavior of the File menu 
@SuppressWarnings("serial")
public class ViewMenu extends JMenu implements ActionListener { 

	private State state;
	JMenuItem rasterizerMenu, displayMenu;

	public ViewMenu(State state) { 
		super("View");
		this.state = state;
		rasterizerMenu = new JMenuItem("Choose Rasterizer...");
		displayMenu = new JMenuItem("Display Options...");
		add(rasterizerMenu);
		add(displayMenu);
		rasterizerMenu.addActionListener(this);
		displayMenu.addActionListener(this);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == rasterizerMenu) chooseRasterizer();
		else if (e.getSource() == displayMenu) chooseDisplayOptions();
	}
	
	public void chooseRasterizer() {
		Paint.removeKeyEventDispatcher();
		new ChooseRasterizerDialog(state);
		Paint.addKeyEventDispatcher();
	}

	public void chooseDisplayOptions() {
		Paint.removeKeyEventDispatcher();
		new DisplayOptionsDialog(state);
		Paint.addKeyEventDispatcher();
	}

}