package myWidget;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import myMath.State;


@SuppressWarnings("serial")
public class ToolButton
extends JToggleButton {
	
	private String id;
	private String tooltip;
	private ImageIcon icon;
	private int type = State.NONE;
	
	public ToolButton() {
		super();
		setMargin(new Insets(-1,-1,-1,-1));
		setFocusable(false);
	}
	
	public ToolButton(String id, String tooltip, ImageIcon icon, int type) {
		super();
		setMargin(new Insets(-1,-1,-1,-1));
		setFocusable(false);
		this.id = id;
		this.icon = icon;
		setIcon(this.icon);
		this.type = type;
		this.tooltip = tooltip;
		setToolTipText(this.tooltip);
	}
	
	public int getType() {
		return type;
	}
	
	public void setType(int t) {
		type = t;
	}
	
	public String getID() {
		return id;
	}
	
	public String getTooltip() {
		return tooltip;
	}
}
