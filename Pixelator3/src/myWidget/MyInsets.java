package myWidget;

import java.util.Vector;

import renderer.geometry.*;

public class MyInsets {
	
	public double top = 0, left = 0, bottom = 0, right = 0;
	
	public MyInsets() {
		
	}
	
	public MyInsets(double top, double left, double bottom, double right) {
		this.top = top;
		this.left = left;
		this.bottom = bottom;
		this.right = right;
	}
	
	public double getWidth() {
		return (right-left);
	}
	
	public double getHeight() {
		return (bottom-top);
	}
	
	public String toString() {
		String s = "MyInsets: ";
		s += "left = " + left + ", ";
		s += "right = " + right + ", ";
		s += "top = " + top + ", ";
		s += "bottom = " + bottom + "\n";
		return s;
	}
	
	public static MyInsets getBoundingBox(Vector<MyPoint> shapePixels) {
		assert(!shapePixels.isEmpty());
		MyPoint p0 = shapePixels.get(0);
		MyInsets bb = new MyInsets(p0.y, p0.x, p0.y, p0.x);
		System.out.println("size = " + shapePixels.size());
		for(int i=1; i<shapePixels.size(); i++) {
			MyPoint p = shapePixels.get(i);
			if(p.x < bb.left) bb.left = p.x;
			if(p.x > bb.right) bb.right = p.x;
			if(p.y < bb.top) bb.top = p.y;
			if(p.y > bb.bottom) bb.bottom = p.y;
		}
		bb.left = (int) Math.floor(bb.left)-1;
		bb.right = (int) Math.ceil(bb.right)+1;
		bb.top = (int) Math.floor(bb.top)-1;
		bb.bottom = (int) Math.ceil(bb.bottom)+1;
		return bb;
	}
}
