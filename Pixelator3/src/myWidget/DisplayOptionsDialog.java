package myWidget;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import myMath.State;

@SuppressWarnings("serial")
public class DisplayOptionsDialog extends JDialog
implements ActionListener {

	private JButton okButton = new JButton("OK");
	private JButton cancelButton = new JButton("Cancel");
	private State state;
	//private JFrame parent = new JFrame();

	private JPanel infoPanel = new JPanel();
	private JPanel showThumbPanel = new JPanel();
	private JLabel showThumbLabel = new JLabel("Show Thumbnail");
	private JCheckBox showThumbCheck = new JCheckBox();
	private JPanel splitPanePanel = new JPanel();
	private JLabel splitPaneLabel = new JLabel("Split-Pane View:");
	private JCheckBox splitPaneCheck = new JCheckBox();
	private JPanel buttonPane = new JPanel();
	private KeyEventDispatcher keyEventDispatcher;

	public DisplayOptionsDialog(State state) {
		super(state.getFrame(), "Display Options", true);
		this.state = state;
		JFrame parent = state.getFrame();
		Dimension parentSize = parent.getSize(); 
		Point p = parent.getLocation(); 
		setLocation(p.x + parentSize.width / 3, p.y + parentSize.height / 3);

		showThumbPanel.add(showThumbLabel);
		showThumbPanel.add(showThumbCheck);
		showThumbCheck.setSelected(state.getShowThumb());

		splitPanePanel.add(splitPaneLabel);
		splitPanePanel.add(splitPaneCheck);
		splitPaneCheck.setSelected(state.getSplitView());

		//infoPanel.add(showThumbPanel);
		infoPanel.add(splitPanePanel);

		okButton.addActionListener(this);
		okButton.setFocusable(false);
		cancelButton.addActionListener(this);
		cancelButton.setFocusable(false);

		buttonPane.add(okButton);
		buttonPane.add(cancelButton);

		getContentPane().add(infoPanel, BorderLayout.CENTER);

		initKeyEventDispatcher();
		addKeyEventDispatcher();

		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getRootPane().setDefaultButton(okButton);
		pack();
		setVisible(true);
	}

	private void initKeyEventDispatcher() {
		keyEventDispatcher = new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				boolean keyHandled = false;
				if (e.getID() == KeyEvent.KEY_PRESSED) {  
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						ok();
						keyHandled = true;
					}
					else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {  
						cancel();  
						keyHandled = true;
					}
					else if (e.getKeyCode() == KeyEvent.VK_TAB) {  
						keyHandled = true;
					}
				}  
				return keyHandled;  
			}  
		};
	}
	
	private void addKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
	}
	
	private void removeKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);
	}
	
	public void dispose() {
		removeKeyEventDispatcher();
		super.dispose();
	}

	private void ok() {
		state.setShowThumb(showThumbCheck.isSelected());
		state.setSplitView(splitPaneCheck.isSelected());
		setVisible(false); 
		dispose();
	}

	private void cancel() {
		setVisible(false); 
		dispose();
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton) {
			ok();
		}
		else if(e.getSource() == cancelButton) {
			cancel();
		}
	}

}