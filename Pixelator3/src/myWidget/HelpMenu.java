package myWidget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import myMath.State;


// Encapsulate the look and behavior of the File menu 
@SuppressWarnings("serial")
public class HelpMenu extends JMenu implements ActionListener { 

	private State state;
	JMenuItem helpMenu;

	public HelpMenu(State state) { 
		super("Help");
		this.state = state;
		helpMenu = new JMenuItem("User Manual...");
		add(helpMenu);
		helpMenu.addActionListener(this);
	}

	private void showUserManual() {
		new UserManualDialog(state);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == helpMenu) showUserManual();
	}

	public void keyPressed(KeyEvent e) {
	}

}