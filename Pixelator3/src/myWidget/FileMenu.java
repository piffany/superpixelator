package myWidget;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.apache.batik.svggen.SVGGraphics2D;

import SVGIO.TestSVGGen;
import myMath.State;
import myWidget.container.Paint;


// Encapsulate the look and behavior of the File menu 
@SuppressWarnings("serial")
public class FileMenu extends JMenu implements ActionListener { 

	private State state;
	JMenuItem newMenu, openMenu, saveAsSVGMenu, exportToPNGMenu, closeMenu;

	public FileMenu(State state) { 
		super("File");
		this.state = state;
		newMenu = new JMenuItem("New...");
		openMenu = new JMenuItem("Open...");
		saveAsSVGMenu = new JMenuItem("Save As SVG...");
		exportToPNGMenu = new JMenuItem("Export To PNG...");
		closeMenu = new JMenuItem("Close");
		add(newMenu);
		add(openMenu);
		add(saveAsSVGMenu);
		add(exportToPNGMenu);
		add(closeMenu);
		newMenu.addActionListener(this);
		openMenu.addActionListener(this);
		saveAsSVGMenu.addActionListener(this);
		exportToPNGMenu.addActionListener(this);
		closeMenu.addActionListener(this);
	}

	private void newFile() {
		Paint.removeKeyEventDispatcher();
		new NewFileDialog(state);
		Paint.addKeyEventDispatcher();
	}
	
	private void exportToPNGFile() {
		Paint.removeKeyEventDispatcher();
		SaveAsFileDialog dSaveAs = new SaveAsFileDialog("Export AS PNG...", ".png");
		int option = dSaveAs.showSaveDialog(state.getFrame());
		Paint.addKeyEventDispatcher();
		if(option == JFileChooser.APPROVE_OPTION) {
			if(dSaveAs.getSelectedFile()!=null){
				File file = dSaveAs.getSelectedFile();
				state.saveToFile(file, "png");
			}
		}
	}

	private void openFile() {
		Paint.removeKeyEventDispatcher();
		OpenFileDialog dOpen = new OpenFileDialog();
		int option = dOpen.showOpenDialog(state.getFrame());
		Paint.addKeyEventDispatcher();
		if(option == JFileChooser.APPROVE_OPTION) {
			if(dOpen.getSelectedFile()!=null){
				File file = dOpen.getSelectedFile();
				String filename = file.getName().toLowerCase();
				if(filename.endsWith(".png")) state.openFile(file, "png");
				else if(filename.endsWith(".svg")) state.openFile(file, "svg");
			}
		}
	}

	private void closeFile() {
		state.closeCanvas();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == newMenu) newFile();
		else if (e.getSource() == saveAsSVGMenu) saveAsSVGFile();
		else if (e.getSource() == exportToPNGMenu) exportToPNGFile();
		else if (e.getSource() == openMenu) openFile();
		else if (e.getSource() == closeMenu) closeFile();
	}
/*
	private void exportToSVG() {
		SVGGraphics2D svgG2 = TestSVGGen.createSVGGraphics2D(
				new Dimension(state.getCanvasState().getWidth(),state.getCanvasState().getHeight()));
		Canvas.paintToSVG(svgG2, state);

		String filename = "C:\\Users\\Piffany\\Desktop\\pixelatorTest.svg";
		TestSVGGen.writeToSVGFile(filename, svgG2);
	}*/

	private void saveAsSVGFile() {
		Paint.removeKeyEventDispatcher();
		SaveAsFileDialog dSaveAsSVG = new SaveAsFileDialog("Save As SVG...", ".svg");
		int option = dSaveAsSVG.showSaveDialog(state.getFrame());
		Paint.addKeyEventDispatcher();
		if(option == JFileChooser.APPROVE_OPTION) {
			if(dSaveAsSVG.getSelectedFile()!=null){
				File file = dSaveAsSVG.getSelectedFile();
				SVGGraphics2D svgG2 = TestSVGGen.createSVGGraphics2D(
						new Dimension(state.getCanvasState().getWidth(),state.getCanvasState().getHeight()));
				Canvas.paintToSVG(svgG2, state);
				TestSVGGen.writeToSVGFile(file.getAbsolutePath(), svgG2);			
			}
		}
	}

	public void keyPressed(KeyEvent e) {
		if(e.isControlDown()) {
			if(e.getKeyCode() == KeyEvent.VK_N) newFile();
			else if(e.getKeyCode() == KeyEvent.VK_O) openFile();
			else if(e.isShiftDown()) {
				if(e.getKeyCode() == KeyEvent.VK_S) saveAsSVGFile();
			}
			else if(e.getKeyCode() == KeyEvent.VK_W) closeFile();
		}
	}

	public void updateDisplay() {
		saveAsSVGMenu.setEnabled(state.isFileOpen());
		exportToPNGMenu.setEnabled(state.isFileOpen());
		closeMenu.setEnabled(state.isFileOpen());
	}

}