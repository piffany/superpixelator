package myWidget;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import colours.Colour;
import myMath.PathState;


@SuppressWarnings("serial")
public class ColorButton extends JButton
implements Icon
{
	private Colour color;
	private Dimension iconSize;
	private int type;

	public ColorButton(Colour color, Dimension iconSize, int type) {
		this.color = color.clone();
		this.iconSize = iconSize;
		setIcon(this);
		this.setMargin(new Insets(-3,-3,-2,-2));
		this.type = type;
		this.setEnabled(false);
	}
		
	public Colour getColor() {
		return color.clone();
	}
	
	public void setColor(Colour color) {
		this.color = color.clone();
		repaint();
	}

	public void openColorChooser() {
		Color selected = JColorChooser.showDialog(this, "Change color", color);
		if (selected != null) {
			setColor(new Colour(selected));
			repaint();
		}
	}
	
	public void setTransparent() {
		setColor(Colour.TRANSPARENT);
	}

	// Icon methods
	public int getIconWidth() { return iconSize.width; }
	
	public int getIconHeight() { return iconSize.height; }
	
	private void paintOuterBorder(Graphics g) {
		g.setColor(isSelected() ? Colour.black : Colour.lightGray);
		g.drawRect(0, 0, iconSize.width, iconSize.height);
		g.setColor(Colour.white);
		g.drawRect(1, 1, iconSize.width-2, iconSize.height-2);
	}
	
	private void paintInnerBorder(Graphics g) {
		int thick = iconSize.width/4;
		g.setColor(this.getBackground());
		g.fillRect(thick, thick, iconSize.width-2*thick, iconSize.height-2*thick);
		g.setColor(Colour.white);
		g.drawRect(thick, thick, iconSize.width-2*thick-1, iconSize.height-2*thick-1);
		g.setColor(Colour.black);
		g.drawRect(thick+1, thick+1, iconSize.width-2*thick-3, iconSize.height-2*thick-3);
	}
	
	public void paintIcon(Component c, Graphics g, int x, int y) {
		boolean transparent = color.getAlpha() == 0;
		g.setColor(transparent ? Colour.white : color);
		g.fillRect(0, 0, iconSize.width, iconSize.height);
		paintOuterBorder(g);
		if(type == PathState.STROKE) paintInnerBorder(g);
		setBorderPainted(false);
		setContentAreaFilled(false);
		if(transparent) {
			g.setColor(Colour.red);
			g.drawLine(iconSize.width, 0, 0, iconSize.height);
			g.drawLine(iconSize.width+1, 0, 1, iconSize.height);
			g.drawLine(iconSize.width-1, 0, -1, iconSize.height);
			g.setColor(color);
		}
	}
}