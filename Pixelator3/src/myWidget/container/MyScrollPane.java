package myWidget.container;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import myMath.CanvasState;
import renderer.geometry.*;
import myWidget.*;


@SuppressWarnings("serial")
public class MyScrollPane
extends JScrollPane
implements MouseWheelListener {

	//private static JPanel centeringPanel = new JPanel(new GridBagLayout());
	private static JPanel outerPanel = new JPanel(new BorderLayout());
	private static JPanel innerPanel = new JPanel(new BorderLayout());
	private static JPanel centeringPanel = new JPanel(new BorderLayout());
	private Canvas canvas = new Canvas();
	private Insets margins = new Insets(0,0,0,0);
	private CanvasState canvasState;
	private Frame parent; 

	public MyScrollPane(Canvas canvas, Frame parent, CanvasState canvasState) {
		super(outerPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, 
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.parent = parent;
		this.canvasState = canvasState;
		alignToTopLeft();
		setOuterMargins();
		setEmptyMargins();
		this.canvas = canvas;
		centeringPanel.add(canvas);
		setupScrollBars();
		updateVisibility();
		addMouseListeners();
	}

	private void addMouseListeners() {
		canvas.addMouseWheelListener(this);
	}

	private void alignToTopLeft() {
		outerPanel.add(innerPanel, BorderLayout.WEST);
		innerPanel.add(centeringPanel, BorderLayout.NORTH);
	}

	private void setOuterMargins() {
		outerPanel.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
	}

	private void setEmptyMargins() {
		centeringPanel.setBorder(BorderFactory.createEmptyBorder
				(margins.top, margins.left, margins.bottom, margins.right));
		refreshFrame();
		updateScrollBarIncrements();
		refreshFrame();
	}

	public Canvas getCanvas() {
		return canvas;
	}
	
	private void refreshFrame() {
		parent.revalidate();
		parent.repaint();
	}

	private void translateCanvas(int x, int y) {
		// location of canvas relative to the view (including initial margins)
		Insets canvasPos = new Insets(y, x, y + canvasState.getTransformedHeight(), x + canvasState.getTransformedWidth());
		int screenW = getScreenWidth();
		int screenH = getScreenHeight();
		int xx = x, yy = y;
		// if left side is > 0, add extra padding to the left margin
		if(canvasPos.left > 0) margins.left = canvasPos.left;
		else { margins.left = 0; }
		if(canvasPos.right < screenW) margins.right = screenW-canvasPos.right;
		else { margins.right = 0;}
		if(canvasPos.top > 0) margins.top = canvasPos.top;
		else { margins.top = 0;}
		if(canvasPos.bottom < screenH) margins.bottom = screenH-canvasPos.bottom;
		else { margins.bottom = 0;}

		setOuterMargins();
		setEmptyMargins();
		if(margins.left >= 0) {
			horizontalScrollBar.setValue(margins.left);
			horizontalScrollBar.setValue(horizontalScrollBar.getValue()-xx);
		}
		if(margins.top >= 0) {
			verticalScrollBar.setValue(margins.top);
			verticalScrollBar.setValue(verticalScrollBar.getValue()-yy);
		}
	}


	public boolean hasCanvas() {
		return !canvasState.isMinimumSize();
	}

	private void setupScrollBars() {
		updateScrollBarIncrements();
	}

	private void updateScrollBarIncrements() {
		int unit = Math.max(5, (int) canvasState.getScale());
		horizontalScrollBar.setUnitIncrement(unit);
		verticalScrollBar.setUnitIncrement(unit);		
	}

	public void setVisible(boolean b) {
		super.setVisible(b);
		revalidate();
		repaint();
	}

	// add to all components
	public void addKeyListener(KeyListener kl) {
		super.addKeyListener(kl);
		canvas.addKeyListener(kl);
		outerPanel.addKeyListener(kl);
		innerPanel.addKeyListener(kl);
		centeringPanel.addKeyListener(kl);
	}

	public void closeCanvas() {
		canvasState.closeCanvas();
		//updateVisibility();
	}

	public void keyPressed(KeyEvent e) {
		if(e.isControlDown()) {
			// Ctrl +
			if(e.getKeyCode() == KeyEvent.VK_EQUALS) {
				zoom(1);
			}
			// Ctrl -
			else if(e.getKeyCode() == KeyEvent.VK_MINUS) {
				zoom(-1);
			}
		}
		canvas.keyPressed(e);
		//updateVisibility();
	}

	public void keyReleased(KeyEvent e) {
		canvas.keyReleased(e);
		//updateVisibility();
	}

	public void keyTyped(KeyEvent e) {
		canvas.keyTyped(e);
		//updateVisibility();
	}

	private void zoom(int direction) {
		MyInsets absView = getAbsoluteView();
		MyPoint centre = new MyPoint((double)(absView.left+absView.right)*0.5, (double)(absView.top+absView.bottom)*0.5);
		zoom(direction, centre);
	}

	private void zoom(int direction, MyPoint centre) {
		if(!canvasState.isZoomable(direction)) return;

		MyPoint oldCentre = absoluteToRelative(centre);

		MyInsets absView = getAbsoluteView();

		if(direction>0) canvasState.zoomIn();
		else canvasState.zoomOut();
		double scale2 = canvasState.getScale();

		// screen width and height
		double screenW = getScreenWidth();
		double screenH = getScreenHeight();
		MyPoint newCentreUntranslated = new MyPoint(oldCentre.x*scale2, oldCentre.y*scale2);

		double ratioX = (centre.x - absView.left)/(absView.right - absView.left);
		double ratioY = (centre.y - absView.top)/(absView.bottom - absView.top);
		MyPoint newCentreTranslated = new MyPoint(ratioX*screenW, ratioY*screenH);

		int translateX = (int) Math.round((newCentreTranslated.x-newCentreUntranslated.x));
		int translateY = (int) Math.round((newCentreTranslated.y-newCentreUntranslated.y));
		translateCanvas(translateX, translateY);
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		int val = e.getUnitsToScroll();
		MyPoint position = new MyPoint(e.getX(), e.getY());
		if(e.isControlDown()) {
			// scroll down --> zoom out
			if(val == 1) zoom(-1, position);
			else if(val == -1) zoom(1, position);
			//updateVisibility();
		}
		else {
			int scrollDownBy = e.getUnitsToScroll()*verticalScrollBar.getUnitIncrement();
			verticalScrollBar.setValue(verticalScrollBar.getValue() + scrollDownBy);
		}
	}
/*
	// subject to scaling transformation
	// the viewport is always in terms of the actual document size,
	// irrelevant of the amount of zooming involved
	private MyInsets getRelativeView(){
		MyInsets absoluteView = getAbsoluteView();
		double scale = canvasState.getScale();
		MyInsets relativeView = new MyInsets((absoluteView.top/scale),
				(absoluteView.left/scale),
				(absoluteView.bottom/scale),
				(absoluteView.right/scale));
		return relativeView;
	}
*/
	private MyPoint absoluteToRelative(MyPoint p) {
		double scale = canvasState.getScale();
		return new MyPoint(p.x/scale, p.y/scale);		
	}

	// not subject to scaling transformation
	// for example, if a 100x100 image is zoomed in to 200%, a viewport that fits
	// perfectly will go from [0,100]x[0,100] to [0,200]x[0,200]
	// and if it has a 10% margin, then it will change from
	// [-10,110]x[-10,110] to [-20,220]x[-20,220]

	private int getScreenWidth() {
		return horizontalScrollBar.getVisibleAmount();
	}

	private int getScreenHeight() {
		return verticalScrollBar.getVisibleAmount();
	}

	private MyInsets getAbsoluteView() {
		boolean centred = false;
		JScrollBar hsb = horizontalScrollBar;
		JScrollBar vsb = verticalScrollBar;
		double left, right, top, bottom, spaceH, spaceV;
		double canvasW = canvas.getWidth();
		double canvasH = canvas.getHeight();
		double canvasWPadded = canvasW + margins.left + margins.right;
		double canvasHPadded = canvasH + margins.top + margins.bottom;
		double windowW = getScreenWidth();
		double windowH = getScreenHeight();
		// horizontal values
		if(canvasWPadded >= windowW) {
			spaceH = margins.left;
		}
		else {
			if(centred) {
				spaceH = 0.5*(windowW-canvasWPadded) + margins.left;
			}
			else {
				spaceH = margins.left;
			}
		}
		left = hsb.getValue() - spaceH;
		right = left + windowW;
		// vertical values
		if(canvasHPadded >= windowH) {
			spaceV = margins.top;
		}
		else {
			if(centred) {
				spaceV = 0.5*(windowH-canvasHPadded) + margins.top;
			}
			else {
				spaceV = margins.top;
			}
		}
		top = vsb.getValue() - spaceV;
		bottom = top + windowH;
		// get view rectangle
		MyInsets view = new MyInsets(top, left, bottom, right);
		return view;
	}

	public void printView() {
		MyInsets view = getAbsoluteView();
		System.out.println(				
				"canvas = [" + canvas.getWidth() + ", " + canvas.getHeight() + "]\n" +
						"scrollBarH = [" + view.left + ", " + view.right + "]\n" +
						"scrollBarV = [" + view.top + ", " + view.bottom + "]\n"
				);		
	}

	private void updateVisibility() {
		setVisible(hasCanvas());
	}
	
	public void updateDisplay() {
		canvas.updateDisplay();
		updateVisibility();
	}
}
