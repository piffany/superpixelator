package myWidget.container;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import myMath.State;
import myMath.StopWatch;
import myWidget.*;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextArea;


@SuppressWarnings("serial")
public class Paint
extends JFrame
implements MouseListener, MouseMotionListener {

	//private Canvas canvas;
	private static MyScrollPane scrollCanvas;
	private JPanel westPanel;
	private JPanel southPanel;
	private JPanel labelPanel1, labelPanel2;
	private static MyToolBar toolbar;
	private static JLabel zoomLabel, cursorLabel, modeLabel, toolLabel;
	private static JTextArea instructionLabel; 
	private static FileMenu fileMenu;
	private static ViewMenu viewMenu;
	private static JMenu helpMenu;
	private static State state;
	private static boolean finishedInit = false;
	public static KeyEventDispatcher keyEventDispatcher;
	public static final boolean debug = false;

	public Paint() {
		super("Superpixelator");
		setSize(600, 600);
		if(debug) System.out.println("size set");

		// current state
		state = new State();
		state.setFrame(this);
		if(debug) System.out.println("state created");

		// menu
		fileMenu = new FileMenu(state);
		viewMenu = new ViewMenu(state);
		helpMenu = new HelpMenu(state);
		JMenuBar mb = new JMenuBar();
		mb.add(fileMenu);
		mb.add(viewMenu);
		mb.add(helpMenu);
		setJMenuBar(mb);
		if(debug) System.out.println("menu added");

		// canvas
		Canvas canvas = new Canvas(state);
		scrollCanvas = new MyScrollPane(canvas, this, state.getCanvasState());
		getContentPane().add(scrollCanvas);
		canvas.addMouseListener(this);
		canvas.addMouseMotionListener(this);
		if(debug) System.out.println("canvas added");

		// border panels
		westPanel = new JPanel();
		add(westPanel, BorderLayout.WEST);
		southPanel = new JPanel(new GridLayout(0,1));
		add(southPanel, BorderLayout.SOUTH);
		if(debug) System.out.println("border panels added");

		// toolbar
		toolbar = new MyToolBar(state);
		toolbar.setFocusable(false);
		state.setToolBar(toolbar);
		westPanel.add(toolbar);
		if(debug) System.out.println("toolbar added");

		// zoom scale label
		labelPanel1 = new JPanel();
		zoomLabel = new JLabel("");
		cursorLabel = new JLabel("Cursor: ( , )");
		modeLabel = new JLabel("");
		toolLabel = new JLabel("");
		labelPanel1.add(zoomLabel);
		labelPanel1.add(Box.createRigidArea(new Dimension(20,0)));
		labelPanel1.add(cursorLabel);
		labelPanel1.add(Box.createRigidArea(new Dimension(20,0)));
		labelPanel1.add(modeLabel);
		labelPanel1.add(Box.createRigidArea(new Dimension(20,0)));
		labelPanel1.add(toolLabel);
		southPanel.add(labelPanel1);
		labelPanel2 = new JPanel();
		instructionLabel = new JTextArea();
		instructionLabel.setBackground(null);
		instructionLabel.setEditable(false);
		instructionLabel.setBorder(null);
		instructionLabel.setLineWrap(true);
		instructionLabel.setWrapStyleWord(true);
		instructionLabel.setFocusable(false);
		labelPanel2.add(instructionLabel);
		southPanel.add(labelPanel2);
		if(debug) System.out.println("labels added");		

		// listen to all key events
		initKeyEventDispatcher();
		addKeyEventDispatcher();
		if(debug) System.out.println("keyEventDispatcher added");
		
		finishedInit = true;
		updateDisplay();
		if(debug) System.out.println("toolbar added");
		
		// exit program
		addWindowListener(new WindowAdapter() { 
			public void windowClosing(WindowEvent e) { 
				exit(); 
			} 
		});
		/*
		ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
		exec.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				refresh();
			}
		}, 0, 10, TimeUnit.MILLISECONDS);
		*/
	}

	public static void addKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
	}

	public static void removeKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);
	}

	private void initKeyEventDispatcher() {
		keyEventDispatcher = new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				if(e.getID() == KeyEvent.KEY_TYPED) keyTyped(e);
				else if(e.getID() == KeyEvent.KEY_PRESSED) keyPressed(e);
				else if(e.getID() == KeyEvent.KEY_RELEASED) keyReleased(e);

				// If the key should not be dispatched to the
				// focused component, set discardEvent to true
				boolean discardEvent = false;
				return discardEvent;
			}
		};
	}

	public static void main(String[] args) {
		Paint paintApp = new Paint();
		paintApp.setVisible(true);
	}

	public void exit() { 
		setVisible(false);
		dispose(); 
		System.exit(0); 
	}


	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		updateCursorLabel(e.getX(), e.getY());
	}

	@Override
	public void mouseExited(MouseEvent e) {
		cursorLabel.setText("Cursor: ( , )");
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		updateCursorLabel(e.getX(), e.getY());
	}


	@Override
	public void mouseDragged(MouseEvent e) {
	}

	public void closeCanvas() {
		//scrollCanvas.closeCanvas();
		state.closeCanvas();
		scrollCanvas.setVisible(false);
		refresh();
	}

	private static void updateZoomLabel() {
		zoomLabel.setText("Zoom: " + state.getPercentZoom() + "%");
	}

	private void updateCursorLabel(int x, int y) {
		cursorLabel.setText("Cursor: (" + x + ", " + y + ")");
	}

	private static void updateModeLabel() {
		int mode = state.getSelectedLayerType();
		String modeString = ((mode==State.NONE) ? "None" : (mode==State.RASTER) ? "Raster" : "Vector");
		modeLabel.setText("Mode: " + modeString); 
	}
	
	private static void updateToolLabel() {
		if(state.hasSelectedTool()) {
			toolLabel.setText("Tool: " + state.getSelectedTool().getTooltip());
		}
		else {
			toolLabel.setText("Tool: None");
		}
	}
	
	private static void updateInstructionLabel() {
		if(state.hasSelectedTool()) {
			instructionLabel.setText(state.getSelectedTool().getInstruction());
		}
		else {
			instructionLabel.setText("");
		}
	}


	public void refresh() {
		//revalidate();
		//repaint();
	}
	
	public void validate() {
		super.validate();
		if(isValid()) instructionLabel.setSize(new Dimension(getWidth()-30,30));
	}

	public void keyTyped(KeyEvent e) {
		scrollCanvas.keyTyped(e);
	}

	public void keyPressed(KeyEvent e) {
		fileMenu.keyPressed(e);
		scrollCanvas.keyPressed(e);
	}

	public void keyReleased(KeyEvent e) {
		scrollCanvas.keyReleased(e);
	}

	public static void updateDisplay() {
		StopWatch sw = new StopWatch();
		if(finishedInit) {
			if(state.isChanged()) {
				fileMenu.updateDisplay();
				toolbar.updateDisplay();
				sw.start();
				scrollCanvas.updateDisplay();
				sw.stop();
				updateZoomLabel();
				updateModeLabel();
				updateToolLabel();
				updateInstructionLabel();
				state.setChanged(false);
			}
		}
	}

	public void saveToFile(File file, String extension) {
		state.saveToFile(file, extension);
	}

	public void openFile(File file, String extension) {
		state.openFile(file, extension);
		refresh();
	}
} 



