package myWidget.container;

import javax.swing.BoxLayout;
import javax.swing.JToolBar;

import colours.Colour;



import myMath.State;
import myTool.Tool;
import myWidget.panel.ColorButtonPanel;
import myWidget.panel.LayerPanel;
import myWidget.panel.RasterPathPanel;
import myWidget.panel.ToolButtonPanel;


@SuppressWarnings("serial")
public class MyToolBar
extends JToolBar
{
	private ToolButtonPanel toolButtonPanel;
	private ColorButtonPanel colorButtonPanel;
	//private RasterPathPanel rasterPathPanel;
	private LayerPanel layerPanel;
	private State state;
	
	public MyToolBar(State state) {
		super();
		this.state = state;
		createLayout();
	}

	private void createLayout() {		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		toolButtonPanel = new ToolButtonPanel(state);
		add(toolButtonPanel);
		if(Paint.debug) System.out.println("toolButtonPanel created");
		addSeparator();
		colorButtonPanel = new ColorButtonPanel(state);
		add(colorButtonPanel);
		if(Paint.debug) System.out.println("colorButtonPanel created");
		addSeparator();
		//rasterPathPanel = new RasterPathPanel(state);
		//add(rasterPathPanel);
		if(Paint.debug) System.out.println("rasterPathPanel created");
		addSeparator();
		layerPanel = new LayerPanel(state);
		add(layerPanel);
		if(Paint.debug) System.out.println("layerPanel created");
		setFloatable(true);
		setRollover(true);
	}
	
	public void updateDisplay() {
		layerPanel.updateDisplay();
		colorButtonPanel.updateDisplay();
		//rasterPathPanel.updateDisplay();
		toolButtonPanel.updateDisplay();
	}
	
	public void setSelectedColor(Colour c) {
		colorButtonPanel.setSelectedColor(c);
	}
	
	public int getSelectedColorButtonType() {
		return colorButtonPanel.getSelected();
	}
	
	public Tool getDefaultTool(int type) {
		return toolButtonPanel.getDefaultTool(type);
	}
}
