package myWidget;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextArea;

class NumVerifier extends InputVerifier {

	public boolean verify(JComponent input) {
		JTextArea ta = (JTextArea) input;
		double val = 0;
		try
		{
			val = Double.parseDouble(ta.getText());
		}
		catch (NumberFormatException nfe)
		{
			return false;
		}
		return val>=0;
	}
	
	public int[] getNumEntries(JTextArea ta1, JTextArea ta2) {
		int val1 = (int) Double.parseDouble(ta1.getText());
		int val2 = (int) Double.parseDouble(ta2.getText());
		int[] vals = {val1, val2};
		return vals;
	}
}