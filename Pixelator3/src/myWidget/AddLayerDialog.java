package myWidget;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import myMath.State;

@SuppressWarnings("serial")
public class AddLayerDialog extends JDialog
implements ActionListener {

	private State state;
	
	private JButton okButton = new JButton("OK"); 
	private JButton cancelButton = new JButton("Cancel");
	private int type = State.NONE;
	private String name = "";

	private JPanel infoPanel = new JPanel();
	private JPanel namePanel = new JPanel();
	private JLabel nameLabel = new JLabel("Name:");
	private JTextArea nameArea = new JTextArea(name, 1, 6);
	private JPanel typePanel = new JPanel();
	private JLabel typeLabel = new JLabel("Type:");
	private String[] types = {"Vector", "Raster"};
	private JComboBox<String> typeCombo = new JComboBox<String>(types);
	private JPanel buttonPane = new JPanel();

	
	public AddLayerDialog(State state) {
		super(state.getFrame(), "New Layer", true);
		this.state = state;
		JFrame parent = state.getFrame();
		Dimension parentSize = parent.getSize(); 
		Point p = parent.getLocation(); 
		setLocation(p.x + parentSize.width / 3, p.y + parentSize.height / 3);
	
		nameArea.setText("Layer " + (state.getSelectedLayerIndex()+1));
		nameArea.selectAll();
		
		namePanel.add(nameLabel);
		namePanel.add(nameArea);
		
		typePanel.add(typeLabel);
		typePanel.add(typeCombo);
		
		infoPanel.add(namePanel);
		infoPanel.add(typePanel);
		
		getContentPane().add(infoPanel, BorderLayout.CENTER);
		
		buttonPane.add(okButton); 
		okButton.addActionListener(this);
		buttonPane.add(cancelButton); 
		cancelButton.addActionListener(this);
		
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack(); 
		setVisible(true);
	}
	
	public String getName() {
		return name;
	}
	
	public int getLayerType() {
		return type;
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton) {
			name = nameArea.getText();
			if(typeCombo.getSelectedItem() == "Raster") {
				type = State.RASTER;
				name += " [R]";
			}
			else if(typeCombo.getSelectedItem() == "Vector") {
				type = State.VECTOR;
				name += " [V]";
			}
			setVisible(false);
			dispose();
			if(type == State.RASTER) state.addRasterLayer(name);
			else if(type == State.VECTOR) state.addVectorLayer(name);
		}
		else if(e.getSource() == cancelButton) {
			setVisible(false); 
			dispose(); 
		}
	}

}