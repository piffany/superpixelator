package myWidget;
import java.awt.BorderLayout;
import java.awt.Point;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.UIManager;

import myMath.State;

@SuppressWarnings("serial")
public class UserManualDialog extends JDialog {

	public UserManualDialog(State state) {
		super(state.getFrame(), "User Manual", true);
		if(TreeDemo.useSystemLookAndFeel) {
			try {
	            UIManager.setLookAndFeel(
	                UIManager.getSystemLookAndFeelClassName());
	        } catch (Exception e) {
	            System.err.println("Couldn't use system look and feel.");
	        }
		}
		JFrame parent = state.getFrame();
		//Dimension parentSize = parent.getSize(); 
		Point p = parent.getLocation(); 
		setLocation(p.x + 50, p.y + 50);

		getContentPane().add(new TreeDemo(), BorderLayout.CENTER);
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);
	}

}