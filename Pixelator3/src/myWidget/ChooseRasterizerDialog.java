package myWidget;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import renderer.rasterizer.Rasterizer;
import myMath.State;

@SuppressWarnings("serial")
public class ChooseRasterizerDialog extends JDialog
implements ActionListener {

	private JButton okButton = new JButton("OK");
	private JButton cancelButton = new JButton("Cancel");
	private int rasterizerType = 0;
	private State state;
	private boolean AA = true;
	//private JFrame parent = new JFrame();

	private JPanel infoPanel = new JPanel();
	private JPanel typePanel = new JPanel();
	private JLabel typeLabel = new JLabel("Rasterizer Type:");
	private int[] rTypes = {Rasterizer.SUPERPIXELATOR, Rasterizer.DEFAULT};
	private String[] types = {"Pixelator", "Java2D"};
	private JComboBox<String> typeCombo = new JComboBox<String>(types);
	private JPanel AAPanel = new JPanel();
	private JLabel AALabel = new JLabel("Anti-aliasing");
	private JCheckBox AACheck = new JCheckBox();
	private JPanel buttonPane = new JPanel();
	private KeyEventDispatcher keyEventDispatcher;

	public ChooseRasterizerDialog(State state) {
		super(state.getFrame(), "Choose A Rasterizer", true);
		this.state = state;
		JFrame parent = state.getFrame();
		Dimension parentSize = parent.getSize(); 
		Point p = parent.getLocation(); 
		setLocation(p.x + parentSize.width / 3, p.y + parentSize.height / 3);

		typePanel.add(typeLabel);
		typePanel.add(typeCombo);
		for(int i=0; i<rTypes.length; i++) {
			if(state.getRasterizerType() == rTypes[i]) {
				typeCombo.setSelectedItem(types[i]);
				rasterizerType = state.getRasterizerType();
				break;
			}
		}
		
		AAPanel.add(AALabel);
		AAPanel.add(AACheck);
		AACheck.setSelected(state.getAA());

		infoPanel.add(typePanel);
		infoPanel.add(AAPanel);

		okButton.addActionListener(this);
		okButton.setFocusable(false);
		cancelButton.addActionListener(this);
		cancelButton.setFocusable(false);

		buttonPane.add(okButton);
		buttonPane.add(cancelButton);

		getContentPane().add(infoPanel, BorderLayout.CENTER);

		initKeyEventDispatcher();
		addKeyEventDispatcher();

		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getRootPane().setDefaultButton(okButton);
		pack();
		setVisible(true);
	}

	private void initKeyEventDispatcher() {
		keyEventDispatcher = new KeyEventDispatcher() {
			public boolean dispatchKeyEvent(KeyEvent e) {
				boolean keyHandled = false;
				if (e.getID() == KeyEvent.KEY_PRESSED) {  
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						ok();
						keyHandled = true;
					}
					else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {  
						cancel();  
						keyHandled = true;
					}
					else if (e.getKeyCode() == KeyEvent.VK_TAB) {  
						keyHandled = true;
					}
				}  
				return keyHandled;  
			}  
		};
	}
	
	private void addKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
	}
	
	private void removeKeyEventDispatcher() {
		KeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(keyEventDispatcher);
	}
	
	public void dispose() {
		removeKeyEventDispatcher();
		super.dispose();
	}

	private void ok() {
		if(typeCombo.getSelectedItem() == "Java2D") rasterizerType = Rasterizer.DEFAULT;
		else if(typeCombo.getSelectedItem() == "Pixelator") rasterizerType = Rasterizer.SUPERPIXELATOR;
		AA = AACheck.isSelected();
		setVisible(false); 
		dispose();
		state.setRasterizerType(rasterizerType);
		state.setAA(AA);
	}

	private void cancel() {
		setVisible(false); 
		dispose();
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == okButton) {
			ok();
		}
		else if(e.getSource() == cancelButton) {
			cancel();
		}
	}

}