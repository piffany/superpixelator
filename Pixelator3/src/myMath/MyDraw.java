package myMath;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.Element;

import renderer.display.MyGraphics2D;
import renderer.geometry.*;
import renderer.rasterizer.Rasterizer;
import renderer.util.*;

import myImage.RasterImage;
import myImage.VectorImage;
import myLayer.RasterLayer;
import myLayer.VectorLayer;
import myMath.geometry.Path;

public class MyDraw {

	public final static int PATHS_ONLY = 0, WITH_ANCHORS = 1, WITH_HANDLES = 2;
	public final static int ANCHOR_RADIUS = 2, HANDLE_RADIUS = 2;


	public static void fillCircle(Graphics g, int centreX, int centreY, int width) {
		// top left
		int w = width-1;
		int x = (int) (centreX-0.5*w), y = (int) (centreY-0.5*w);
		if(width > 3) {
			g.drawOval(x, y, w, w);
			g.drawOval(x, y, w-1, w-1);
			g.drawOval(x+1, y, w-1, w-1);
			g.drawOval(x, y+1, w-1, w-1);
			g.drawOval(x+1, y+1, w-1, w-1);
			g.fillOval(x+1, y+1, w-2, w-2);
		}
		else if(width > 2) {
			g.drawRect(x, y, w, w);
			g.drawRect(x, y, w-1, w-1);
		}
		else if(width > 1) {
			g.drawRect(x, y, w, w);
			g.drawRect(x, y, w-1, w-1);
		}
		else {
			g.drawRect(centreX, centreY, 0, 0);
		}
	}

	public static void drawThickBrushStroke(Graphics2D g, int x1, int y1, int x2, int y2, int width, int strokeCap, int strokeJoin) {
		fillCircle(g, x1, y1, width);
		fillCircle(g, x2, y2, width);
		drawThickLine(g, x1, y1, x2, y2, width, strokeCap, strokeJoin);
	}
	
	public static void drawThickLine(Graphics2D g, int x1, int y1, int x2, int y2, int width, int strokeCap, int strokeJoin) {
		Stroke originalStroke = g.getStroke();
		g.setStroke(new BasicStroke(width, strokeCap, strokeJoin));
		g.drawLine(x1, y1, x2, y2);
		g.setStroke(originalStroke);
	}
	
	public static void drawThickRectangle(Graphics2D g, int x, int y, int w, int h, int width, int strokeCap, int strokeJoin) {
		Stroke originalStroke = g.getStroke();
		g.setStroke(new BasicStroke(width, strokeCap, strokeJoin));
		g.drawRect(x, y, w, h);
		g.setStroke(originalStroke);
	}

	public static void drawThickOval(Graphics2D g, int x, int y, int w, int h, int width, int strokeCap, int strokeJoin) {
		Stroke originalStroke = g.getStroke();
		g.setStroke(new BasicStroke(width, strokeCap, strokeJoin));
		g.drawOval(x, y, w, h);
		g.setStroke(originalStroke);
	}

	public static void drawPath(MyGraphics2D mg, Path path) {
		Vector<MyCurve> myCurves = path.getCurves();
		Iterator<MyCurve> itr = myCurves.iterator();
		while(itr.hasNext()) {
			drawCurve(mg, itr.next());
		}
	}

	public static void drawCurve(MyGraphics2D mg, MyCurve myCurve) {
		Graphics2D g = mg.getGraphics2D();
		g.draw(myCurve);		
	}

	public static void drawVectorImage(MyGraphics2D mg, VectorImage vectorImage, AffineTransform at, State state, int drawOption) {
		int newWidth = Transform.transformLength(vectorImage.getWidth(), at);
		int newHeight = Transform.transformLength(vectorImage.getHeight(), at);
		//Vector<Path> paths = vectorImage.getPaths();
		int selectedPathIndex = vectorImage.getSelectedPathIndex();
		int selectedSegmentIndex = vectorImage.getSelectedSegmentIndex();
		RasterImage raster = new RasterImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB, state);
		MyGraphics2D mg2 = new MyGraphics2D((Graphics2D) raster.getGraphics(), newWidth, newHeight);
		mg2.setRasterizerType(Rasterizer.DEFAULT);
		for(int pathIndex = 0; pathIndex<vectorImage.getNumPaths();/*paths.size()*/ pathIndex++) {
			// path
			Path path = vectorImage.getPath(pathIndex);//paths.get(pathIndex);
			mg2.setColor(path.getPathColor());
			if(selectedPathIndex == pathIndex) {
				Path transformedPath = path.getTransformedCopy(at);
				MyDraw.drawPath(mg2, transformedPath);
			}
			// anchors and handles
			if(drawOption == WITH_ANCHORS || drawOption == WITH_HANDLES) {
				for(int segmentIndex = 0; segmentIndex<path.getSegments().size(); segmentIndex++) {
					// path
					Segment seg = path.getSegments().get(segmentIndex);
					MyPoint anchor = seg.getPoint();
					MyPoint handleIn = seg.getHandleIn();
					MyPoint handleOut = seg.getHandleOut();
					MyPoint anchorT = Transform.transformPoint(anchor, at);
					MyPoint handleInT = Transform.transformPoint(anchor.plus(handleIn), at);
					MyPoint handleOutT = Transform.transformPoint(anchor.plus(handleOut), at);
					if(pathIndex == selectedPathIndex) {
						if(selectedSegmentIndex != -1) {
							// handle lines
							if(drawOption == WITH_HANDLES) {
								mg2.setColor(path.getPathColor());
								mg2.drawLine((int)anchorT.x, (int)anchorT.y, (int)handleInT.x, (int)handleInT.y);
								mg2.drawLine((int)anchorT.x, (int)anchorT.y, (int)handleOutT.x, (int)handleOutT.y);
							}
							// control points
							mg2.setColor(path.getPathColor());
							if(drawOption == WITH_HANDLES) {
								mg2.fillOval((int)(handleInT.x-HANDLE_RADIUS), (int)(handleInT.y-HANDLE_RADIUS), 2*HANDLE_RADIUS, 2*HANDLE_RADIUS);
								mg2.fillOval((int)(handleOutT.x-HANDLE_RADIUS), (int)(handleOutT.y-HANDLE_RADIUS), 2*HANDLE_RADIUS, 2*HANDLE_RADIUS);
							}
						}
						if(segmentIndex == selectedSegmentIndex || selectedSegmentIndex == -1) {
							mg2.setColor(path.getPathColor());
							mg2.fillRect((int)(anchorT.x-ANCHOR_RADIUS), (int)(anchorT.y-ANCHOR_RADIUS), 2*ANCHOR_RADIUS, 2*ANCHOR_RADIUS);
						}
						else {
							mg2.setColor(Color.white);
							mg2.fillRect((int)(anchorT.x-ANCHOR_RADIUS), (int)(anchorT.y-ANCHOR_RADIUS), 2*ANCHOR_RADIUS, 2*ANCHOR_RADIUS);
							mg2.setColor(path.getPathColor());
							mg2.drawRect((int)(anchorT.x-ANCHOR_RADIUS), (int)(anchorT.y-ANCHOR_RADIUS), 2*ANCHOR_RADIUS, 2*ANCHOR_RADIUS);
						}
					}
					/*
					else {
						mg2.setColor(path.getPathColor());
						mg2.fillRect((int)(anchorT.x-ANCHOR_RADIUS), (int)(anchorT.y-ANCHOR_RADIUS), 2*ANCHOR_RADIUS, 2*ANCHOR_RADIUS);
					}
					*/
				}
			}
		}
		mg2.setRasterizerType(state.getRasterizerType());
		mg.drawRenderedImage(raster, new AffineTransform());
	}
	
	public static void drawVectorImageToSVG(SVGGraphics2D mg, VectorImage vectorImage, AffineTransform at, State state, int drawOption) {
		//int newWidth = Transform.transformLength(vectorImage.getWidth(), at);
		//int newHeight = Transform.transformLength(vectorImage.getHeight(), at);
		//Vector<Path> paths = vectorImage.getPaths();
		//RasterImage raster = new RasterImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB, state);
		for(int pathIndex = 0; pathIndex<vectorImage.getNumPaths();/*paths.size();*/ pathIndex++) {
			// path
			Path path = vectorImage.getPath(pathIndex);//paths.get(pathIndex);
			if(path.getFillColor() != null && path.getFillColor().getAlpha() > 0) {
				mg.setColor(path.getFillColor());
				mg.fill(path.getShape());
			}
			if(path.getStrokeColor() != null && path.getStrokeColor().getAlpha() > 0) {
				mg.setColor(path.getStrokeColor());
				mg.draw(path.getShape());
			}
		}
		//testDrawToSVG(mg);
		//mg2.setRasterizerType(Rasterizer.PIXELATOR);
		//mg.drawRenderedImage(raster, new AffineTransform());
	}
	
	public static Color getRainbowColor(int i) {
		if(i%7==0) return Color.red;
		else if(i%7==1) return Color.green;
		else if(i%7==2) return Color.orange;
		else if(i%7==3) return Color.cyan;
		else if(i%7==4) return Color.magenta;
		else if(i%7==5) return Color.blue;		
		else return Color.lightGray;
	}
	
	public static Color getRandomColor() {
		Random random = new Random();
		int red = random.nextInt(256);
		int green = random.nextInt(256);
		int blue = random.nextInt(256);
		return new Color(red, green, blue);
	}

	public static Vector<MyPoint> getPixelPath(Vector<MyPoint> pixels) {
		Vector<MyPoint> corners = new Vector<MyPoint>();
		for(int i=0; i<pixels.size(); i++) {
			if(i==0) corners.add(pixels.get(i));
			else if(i==pixels.size()-1) corners.add(pixels.get(i));
			else if(pixels.get(i).x!=pixels.get(i-1).x && pixels.get(i).y!=pixels.get(i-1).y) {
				corners.add(pixels.get(i));
			}
		}
		return corners;
	}

	public static void drawRasterLayer(MyGraphics2D mg2, RasterLayer rasterLayer,
			AffineTransform at) {
		if(rasterLayer.isVisible()) {
			mg2.drawRenderedImage(rasterLayer.getRasterImage(), at);
			mg2.drawRenderedImage(rasterLayer.getRasterTempImage(), at);
		}
	}

	public static void drawVectorLayer(MyGraphics2D mg2, VectorLayer vectorLayer,
			AffineTransform at, State state) {
		if(vectorLayer.isVisible()) {
			if(vectorLayer.isSelected()) {
				MyDraw.drawVectorImage(mg2, vectorLayer.getVectorTempImage(), at, state, PATHS_ONLY);
				MyDraw.drawVectorImage(mg2, vectorLayer.getVectorImage(), at, state, WITH_HANDLES);
			}
		}
	}

	public static void drawRasterLayerToSVG(SVGGraphics2D mg2,
			RasterLayer rasterLayer, AffineTransform at) {
		System.out.println("drawRasterLayerToSVG");
		if(rasterLayer.isVisible()) {
			BufferedImage im = rasterLayer.getRasterImage().clone();
			/*
			for(int i=0; i<im.getWidth(); i++) {
				for(int j=0; j<im.getHeight(); j++) {
					if(Colour.getAlpha(im.getRGB(i, j))==0) {
						im.setRGB(i, j, Color.white.getRGB());
					}
				}
			}
			*/
			mg2.drawImage(im, 0, 0, null);
			//mg2.drawRenderedImage(rasterLayer.getRasterTempImage(), at);
		}
	}
	
	public static void drawVectorLayerToSVG(SVGGraphics2D mg2, VectorLayer vectorLayer,
			AffineTransform at, State state) {
		if(vectorLayer.isVisible()) {
			SVGGraphics2D g = (SVGGraphics2D)mg2.create();
			Element topLevelGroup = g.getTopLevelGroup();
			MyDraw.drawVectorImageToSVG(g, vectorLayer.getVectorImage(), at, state, WITH_HANDLES);
			Element cmpGroup = g.getTopLevelGroup();
			cmpGroup.setAttributeNS(null, "id", vectorLayer.getName());
			topLevelGroup.appendChild(cmpGroup);
			mg2.setTopLevelGroup(topLevelGroup);
		}
	}
	
	public static void testDrawToSVG(SVGGraphics2D g2d) {
		g2d.setColor(new Color(100,100,200));
		g2d.translate(1,1);
		g2d.fillOval(10, 10, 100, 100);
		g2d.translate(150,50);
		g2d.scale(0.5, 2);
		g2d.setStroke(new BasicStroke(2));
		g2d.drawRect(10, 10, 100, 100);
		g2d.setColor(Color.red);
		g2d.setTransform(AffineTransform.getTranslateInstance(0, 0));
		g2d.fillOval(100, 10, 100, 100);
		g2d.setColor(Color.green);
		g2d.drawOval(100, 10, 100, 100);
		BufferedImage im = new BufferedImage(50,50,BufferedImage.TYPE_INT_RGB);
		Graphics2D imG = (Graphics2D) im.getGraphics();
		imG.setColor(Color.green);
		imG.fillRect(0, 0, 20, 20);
		g2d.drawImage(im, 0, 0, null);
	}

}
