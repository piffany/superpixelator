package myMath.geometry;

import renderer.display.MyGraphics2D;
import renderer.geometry.MyCurve;
import renderer.geometry.MyPoint;
import renderer.geometry.Pixel;
import renderer.geometry.Segment;
import renderer.rasterizer.superpixelator.SuperpixelatorHelper;

import java.awt.geom.AffineTransform;
import java.util.Vector;

import colours.Colour;
import myMath.PathState;


public class Path extends renderer.geometry.Path {
	
	private PathState pathState;
	private double strokeWidth = 1;
	private Vector<Pixel> strokePixels = null;
	private Vector<Pixel> fillPixels = null;
	private boolean colorChanged = false;

	public Path() {
		super();
		pathState = new PathState();
	}
	
	public Path(PathState pathState) {
		super();
		setPathState(pathState);
	}

	public boolean hasStrokePixels() {
		return strokePixels != null && !strokePixels.isEmpty();
	}
	
	public boolean hasFillPixels() {
		return fillPixels != null && !fillPixels.isEmpty();
	}
	
	public void clearStrokePixels() {
		strokePixels = null;
	}
	
	public void clearFillPixels() {
		fillPixels = null;
	}
	
	public void clearPixels() {
		strokePixels = null;
		fillPixels = null;
	}
	
	public void drawFillPixels(MyGraphics2D mg) {
		mg.setColor(getFillColor());
		if(!hasFillPixels() && getFillColor().getAlpha() > 0) recomputeFillPixel(mg);
		if(fillPixels!=null) mg.drawPixels(fillPixels);
	}
	
	public void drawStrokePixels(MyGraphics2D mg) {
		mg.setColor(getStrokeColor());
		if(!hasStrokePixels() && getStrokeColor().getAlpha() > 0 && getStrokeWidth() > 0) {
			recomputeStrokePixel(mg);
		}
		if(strokePixels!=null) mg.drawPixels(strokePixels);
	}
	
	public void recomputeFillPixel(MyGraphics2D g) {
		Colour fillColor = getFillColor();
		if(fillColor!=null && fillColor.getAlpha() > 0) {
			fillPixels = new Vector<Pixel>();
			fillPixels = SuperpixelatorHelper.getFillPixels(g, this);
		}
		else fillPixels = null;
	}
	
	public void recomputeStrokePixel(MyGraphics2D g) {
		Colour strokeColor = getStrokeColor();
		if(strokeColor!=null && strokeColor.getAlpha() > 0) {
			strokePixels = new Vector<Pixel>();
			//strokePixels = PixelatorHelper.getStrokePixels(g, this);
			strokePixels = SuperpixelatorHelper.pixelateThickPath(g, getTransformedCopy(AffineTransform.getTranslateInstance(0,0)), true, true);
		}
		else strokePixels = null;
		
	}

	public void setPathState(PathState pathState) {
		this.pathState = new PathState(pathState);
		setColorChanged(true);
	}
	
	public PathState getPathState() {
		return new PathState(pathState);
	}

	public Colour getStrokeColor() {
		return pathState.getStrokeColor();
	}

	public void setStrokeColor(Colour c) {
		pathState.setStrokeColor(c);
		//clearPixels();
		setColorChanged(true);
	}

	public Colour getFillColor() {
		return pathState.getFillColor();
	}

	public void setFillColor(Colour c) {
		pathState.setFillColor(c);
		//clearPixels();
		setColorChanged(true);
	}

	public Colour getPathColor() {
		return pathState.getPathColor();
	}

	public void setPathColor(Colour c) {
		pathState.setPathColor(c);
	}

	public double getStrokeWeight() {
		return pathState.getStrokeWeight();
	}

	public void setStrokeWeight(double w) {
		pathState.setStrokeWeight(w);
	}

	public Path(Path path) {
		super(path);
		pathState = new PathState(path.getPathState());
	}

	public renderer.geometry.Path getPath() {
		renderer.geometry.Path path = new renderer.geometry.Path();
		for(int i=0; i<getNumSegments(); i++) {
			path.addSegment(getSegment(i).clone());
		}
		return path;
	}

	public void setPath(renderer.geometry.Path path) {
		clear();
		for(int i=0; i<path.getNumSegments(); i++) {
			addSegment(path.getSegment(i).clone());
		}
		pathState = new PathState();
	}

	public Path clone() {
		return new Path(this);
	}

	public Path getTransformedCopy(AffineTransform at) {
		Path copy = clone();
		renderer.geometry.Path path = getPath();
		path.transform(at);
		copy.setPath(path);
		return copy;
	}

	public double getStrokeWidth() {
		return strokeWidth;
	}

	public void setStrokeWidth(double strokeWidth) {
		this.strokeWidth = strokeWidth;
	}

	// Line
	public static class Line extends Path {

		public Line(double x1, double y1, double x2, double y2, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.Line(x1,y1,x2,y2);
			this.setPath(path);
		}

		public Line(MyPoint p1, MyPoint p2, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.Line(p1,p2);
			this.setPath(path);
		}
	}

	// Rectangle
	public static class Rectangle extends Path {
		public Rectangle(double x, double y, double width, double height, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.Rectangle(x, y, width, height);
			this.setPath(path);
		}
	}

	// Round Rectangle
	public static class RoundRectangle extends Path {
		public RoundRectangle(double x, double y, double width, double height, double arcWidth, double arcHeight, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.RoundRectangle(x, y, width, height, arcWidth, arcHeight);
			this.setPath(path);
		}
	}

	// Oval
	public static class Oval extends Path {
		public Oval(double x, double y, double width, double height, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.Oval(x, y, width, height);
			this.setPath(path);
		}
	}

	// Arc
	public static class Arc extends Path {
		public Arc(double x, double y, double width, double height, double arcAngle, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.Arc(x, y, width, height, arcAngle);
			this.setPath(path);
		}

		public Arc(double x, double y, double width, double height, double startAngle, double arcAngle, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.Arc(x, y, width, height, startAngle, arcAngle);
			this.setPath(path);
		}
	}
	
	// Polyline
	public static class Polyline extends Path {
		public Polyline(int[] xPoints, int[] yPoints, int nPoints, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.Polyline(xPoints, yPoints, nPoints);
			this.setPath(path);		
		}
	}

	// Polygon
	public static class Polygon extends Path {
		public Polygon(int[] xPoints, int[] yPoints, int nPoints, PathState pathState) {
			super(pathState);
			renderer.geometry.Path path = new renderer.geometry.Path.Polygon(xPoints, yPoints, nPoints);
			this.setPath(path);
		}
	}
	
	// methods that change the path
	public void addSegment(Segment s) {
		super.addSegment(s);
		clearPixels();
	}

	public void addSegment(int i, Segment s) {
		super.addSegment(i, s);
		clearPixels();
	}

	public void addSegments(Vector<Segment> s) {
		super.addSegments(s);
		clearPixels();
	}
	
	public void addPoint(double x, double y) {
		super.addPoint(x, y);
		clearPixels();
	}

	public void addPoint(MyPoint p) {
		super.addPoint(p);
		clearPixels();
	}

	public void addPoints(Vector<MyPoint> ps) {
		super.addPoints(ps);
		clearPixels();
	}

	public void addCurve(MyCurve c) {
		super.addCurve(c);
		clearPixels();
	}

	public void addCurves(Vector<MyCurve> curves) {
		super.addCurves(curves);
		clearPixels();
	}	

	public void clear() {
		super.clear();
		clearPixels();
	}
	
	public void removeSegment(int i) {
		super.removeSegment(i);
		clearPixels();
	}
	
	public void transform(AffineTransform at) {
		super.transform(at);
		clearPixels();
	}
	
	public void set(int segmentIndex, Segment newSegment) {
		super.set(segmentIndex, newSegment);
		clearPixels();
	}

	public void insert(int segmentIndex, Segment newSegment) {
		super.insert(segmentIndex, newSegment);
		clearPixels();
	}

	public boolean isColorChanged() {
		return colorChanged;
	}

	public void setColorChanged(boolean colorChanged) {
		this.colorChanged = colorChanged;
	}
}
