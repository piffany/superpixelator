package myMath;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.util.Vector;

import javax.swing.JFrame;

import colours.Colour;

import renderer.util.*;

import myImage.RasterImage;
import myLayer.*;
import renderer.geometry.*;
import myMath.geometry.Path;
import myTool.Tool;
import myTool.raster.RasterTool;
import myTool.vector.VectorTool;
import myWidget.container.MyToolBar;
import myWidget.container.Paint;


public class State {

	public static final int NONE = 0, RASTER = 1, VECTOR = 2; 
	private Tool tool;
	private JFrame frame;
	private MyToolBar toolbar;
	private HitResult selectedObject;
	private boolean layerChanged = false, toolChanged = true;
	private CanvasState canvasState;
	private PathState pathState;
	
	public State() {
		pathState = new PathState();
		toolbar = null;
		tool = new Tool();
		frame = new JFrame();
		selectedObject = new HitResult();
		canvasState = new CanvasState();
		updateTransforms();
	}
	
	public CanvasState getCanvasState() {
		return canvasState;
	}
	
	public PathState getPathState() {
		return pathState;
	}
	
	public int getRasterizerType() {
		return canvasState.getRasterizerType();
	}
	
	public void setRasterizerType(int type) {
		canvasState.setRasterizerType(type);
	}
	
	public boolean getShowThumb() {
		return canvasState.getShowThumb();
	}
	
	public void setShowThumb(boolean b) {
		canvasState.setShowThumb(b);
	}
	
	public void updateTransforms() {
		canvasState.updateTransforms();
	}
	
	public AffineTransform getTransform() {
		return canvasState.getTransform();
	}
	
	public AffineTransform getInverseTransform() {
		return canvasState.getInverseTransform();
	}
	
	public boolean isMinimumSize() {
		return canvasState.isMinimumSize();
	}
	
	public void setMinimumSize() {
		canvasState.setMinimumSize();
		updateTransforms();
	}
	
	public void zoomIn() {
		canvasState.zoomIn();		
	}
	
	public void zoomOut() {
		canvasState.zoomOut();
	}
	
	public int getWidth() {
		return canvasState.getWidth();
	}
	
	public int getHeight() {
		return canvasState.getHeight();
	}
	
	public int getTransformedWidth() {
		return canvasState.getTransformedWidth();
	}
	
	public int getTransformedHeight() {
		return canvasState.getTransformedHeight();
	}

	public void addNewCanvas(int w, int h, int type) {
		canvasState.addNewCanvas(w, h, type, this);
		if(type==State.RASTER) addRasterLayer("Layer 0 [R]");
		else if(type==State.VECTOR) addVectorLayer("Layer 0 [V]");
		layerChanged = true;
		toolChanged = true;
		Paint.updateDisplay();
	}
	
	public void clearLayers() {
		canvasState.clearLayers();
		layerChanged = true;
		toolChanged = true;
		Paint.updateDisplay();
	}
	
	public void addRasterLayer() {
		addRasterLayer("");
	}
	
	private void reselectTool() {
		int layerType = getSelectedLayerType();
		int toolType = getSelectedToolType();
		if(layerType == NONE) tool = new Tool();
		else if(toolType!=layerType) {
			tool = toolbar.getDefaultTool(layerType);
		}
	}
	
	public void addRasterLayer(String name) {
		canvasState.addRasterLayer(name, this);
		reselectTool();
		deselectObject();
		layerChanged = true;
		toolChanged = true;
		Paint.updateDisplay();
	}
	
	public void addVectorLayer() {
		addVectorLayer("");
	}
	
	public void addVectorLayer(String name) {
		canvasState.addVectorLayer(name, this);
		reselectTool();
		deselectObject();
		layerChanged = true;
		toolChanged = true;
		Paint.updateDisplay();
	}

	public void setLayerVisible(int i, boolean b) {
		canvasState.setLayerVisible(i, b);
		Paint.updateDisplay();
	}
	
	public void setSelectedLayerVisible(boolean b) {
		setLayerVisible(canvasState.getSelectedLayerIndex(), b);
	}

	public void deleteLayer(int i) {
		canvasState.deleteLayer(i);
		reselectTool();
		layerChanged = true;
		toolChanged = true;
		Paint.updateDisplay();
	}
	
	public void deleteSelectedLayer() {
		deleteLayer(canvasState.getSelectedLayerIndex());
	}
	
	public boolean hasSelectedLayer() {
		return canvasState.hasSelectedLayer();
	}
	
	public void deselectLayer() {
		canvasState.deselectLayer();
		reselectTool();
		deselectObject();
		toolChanged = true;
		Paint.updateDisplay();
	}
	
	public int getSelectedLayerIndex() {
		return canvasState.getSelectedLayerIndex();
	}

	public void setSelectedLayerIndex(int selectedLayer) {
		canvasState.setSelectedLayerIndex(selectedLayer);
		reselectTool();
		deselectObject();
		toolChanged = true;
		Paint.updateDisplay();
	}
	
	public Layer getSelectedLayer() {
		return canvasState.getSelectedLayer();
	}
	
	public int getSelectedLayerType() {
		return canvasState.getSelectedLayerType();
	}
	
	public Vector<Integer> getLayerTypes() {
		return canvasState.getLayerTypes();
	}
	
	public boolean isLayerVisible(int i) {
		return canvasState.isLayerVisible(i);
	}
	
	public boolean isSelectedLayerVisible() {
		return canvasState.isSelectedLayerVisible();
	}
	
	public Vector<String> getLayerNames() {
		return canvasState.getLayerNames();
	}
	
	public String getLayerName(int i) {
		return canvasState.getLayerName(i);
	}
	
	public void setStrokeWeight(double strokeWeight) {
		pathState.setStrokeWeight(strokeWeight);
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}

	public boolean drawable() {
		return hasSelectedTool() && hasSelectedLayer();
	}
	
	public HitResult getSelectedObject() {
		return selectedObject;
	}
	
	public void setSelectedObject(HitResult selected, Path path) {
		this.selectedObject = selected;
		pathState = new PathState(path.getPathState());
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}
	
	public void deselectObject() {
		selectedObject = new HitResult();
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public MyToolBar getToolBar() {
		return toolbar;
	}
	
	public void setToolBar(MyToolBar toolbar) {
		this.toolbar = toolbar;
	}

	public void refreshFrame() {
		frame.revalidate();
		frame.repaint();
	}
	
	public void deselectTool() {
		tool = new Tool();
	}
	
	public Tool getSelectedTool() {
		return tool;
	}
	
	public int getSelectedToolType() {
		return tool.getType();
	}
	
	public boolean hasSelectedTool() {
		return getSelectedToolType() != NONE;
	}
	
	public void setStrokeColor(Colour c) {
		pathState.setStrokeColor(c);
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}

	public void setStrokeColorTransparent() {
		setStrokeColor(new Colour(255,255,255,0));
	}

	public void setPathColor(Colour c) {
		pathState.setPathColor(c);
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}

	public void setFillColor(Colour c) {
		pathState.setFillColor(c);
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}
	
	public void setFillColorTransparent() {
		setFillColor(new Colour(255,255,255,0));
	}

	public void setSelectedTool(Tool t) {
		tool = t;
		canvasState.setChanged(true);
		toolChanged = true;
		Paint.updateDisplay();
	}

	public void unsetSelectedTool() {
		tool = new Tool();
		canvasState.setChanged(true);
		toolChanged = true;
		Paint.updateDisplay();
	}

	public void incrementScale(int increment) {
		canvasState.incrementScale(increment);
		Paint.updateDisplay();
	}
	
	public double getScale() {
		return canvasState.getScale();
	}
	
	public double getPercentZoom() {
		return MyMath.round(100.0*getScale(), 2);
	}
	
	public boolean isChanged() {
		return isCanvasChanged() || layerChanged || toolChanged;
	}
	
	public void setChanged(boolean b) {
		canvasState.setChanged(b);
		layerChanged = b;
		toolChanged = b;
	}
	
	public boolean isCanvasChanged() {
		return canvasState.isChanged();
	}
	
	public boolean isToolChanged() {
		return toolChanged;
	}

	public boolean isFileOpen() {
		return getNumLayers() > 0;
	}

	public void setCanvasChanged(boolean c) {
		canvasState.setChanged(c);
	}
	
	public void setToolChanged(boolean c) {
		toolChanged = c;
	}

	public boolean isLayerChanged() {
		return layerChanged;
	}

	public void setLayerChanged(boolean c) {
		layerChanged = c;
	}

	public RasterLayer getRasterLayer() {
		return canvasState.getRasterLayer();
	}

	public VectorLayer getVectorLayer() {
		return canvasState.getVectorLayer();
	}

	public Vector<Layer> getLayers() {
		return canvasState.getLayers();
	}
	
	public boolean canvasIsEmpty() {
		return getNumLayers()==0;
	}
	
	public void closeCanvas() {
		canvasState.closeCanvas();
		deselectTool();
		layerChanged = true;
		toolChanged = true;
		Paint.updateDisplay();
	}
	
	public int getNumLayers() {
		return canvasState.getNumLayers();
	}

	public void saveToFile(File file, String extension) {
		canvasState.saveToFile(file, extension, this);
	}
	
	public void renameLayer(String name) {
		canvasState.renameLayer(name);
		layerChanged = true;
		Paint.updateDisplay();
	}
	
	public void setSelectedColor(Colour c) {
		int selectedColorType = toolbar.getSelectedColorButtonType();
		if(selectedColorType == PathState.STROKE) pathState.setStrokeColor(c);
		else pathState.setFillColor(c);
		toolChanged = true;
		Paint.updateDisplay();
	}

	public RasterImage rasterizeAllLayers(boolean visibleOnly) {
		return canvasState.rasterizeAllLayers(this, visibleOnly);
	}
	
	public void rasterizeSelectedLayer() {
		boolean rasterized = canvasState.rasterizeSelectedLayer(this);
		if(rasterized) {
			reselectTool();
			deselectObject();
			layerChanged = true;
			toolChanged = true;
			Paint.updateDisplay();
		}
	}
	
	public void openFile(File file, String extension) {
		deselectTool();
		boolean opened = canvasState.openFile(file, extension, this);
		if(opened){
			layerChanged = true;
			toolChanged = true;
			Paint.updateDisplay();
		}
	}

	public boolean isZoomable(int direction) {
		return canvasState.isZoomable(direction);
	}

	private MyPoint canvasMouseEventHandle1(MouseEvent e) {
		AffineTransform invAt = getInverseTransform();
		MyPoint p = Transform.transformPoint(e.getX(), e.getY(), invAt);
		return p;
	}

	private void canvasMouseEventHandle2(MouseEvent e) {
		e.consume();
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}
	
	private int getLayerAndToolType() {
		int toolType = getSelectedToolType();
		int layerType = getSelectedLayerType();
		if(toolType == RASTER && layerType == RASTER) return RASTER;
		if(toolType == VECTOR && layerType == VECTOR) return VECTOR;
		else return NONE;
	}

	public void canvasMousePressed(MouseEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		MyPoint p = canvasMouseEventHandle1(e);
		if(type == RASTER) ((RasterTool) getSelectedTool()).mousePressed(e, (int)p.x, (int)p.y, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).mousePressed(e, p.x, p.y, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		canvasMouseEventHandle2(e);
	}

	public void canvasMouseReleased(MouseEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		MyPoint p = canvasMouseEventHandle1(e);
		if(type == RASTER) ((RasterTool) getSelectedTool()).mouseReleased(e, (int)p.x, (int)p.y, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).mouseReleased(e, p.x, p.y, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		canvasMouseEventHandle2(e);
	}

	public void canvasMouseExited(MouseEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		MyPoint p = canvasMouseEventHandle1(e);
		if(type == RASTER) ((RasterTool) getSelectedTool()).mouseExited(e, (int)p.x, (int)p.y, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).mouseExited(e, p.x, p.y, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		canvasMouseEventHandle2(e);
	}

	public void canvasMouseEntered(MouseEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		MyPoint p = canvasMouseEventHandle1(e);
		if(type == RASTER) ((RasterTool) getSelectedTool()).mouseEntered(e, (int)p.x, (int)p.y, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).mouseEntered(e, p.x, p.y, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		canvasMouseEventHandle2(e);
	}

	public void canvasMouseClicked(MouseEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		MyPoint p = canvasMouseEventHandle1(e);
		if(type == RASTER) ((RasterTool) getSelectedTool()).mouseClicked(e, (int)p.x, (int)p.y, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).mouseClicked(e, p.x, p.y, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		canvasMouseEventHandle2(e);
	}

	public void canvasMouseDragged(MouseEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		MyPoint p = canvasMouseEventHandle1(e);
		if(type == RASTER) ((RasterTool) getSelectedTool()).mouseDragged(e, (int)p.x, (int)p.y, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).mouseDragged(e, p.x, p.y, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		canvasMouseEventHandle2(e);
	}

	public void canvasMouseMoved(MouseEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		MyPoint p = canvasMouseEventHandle1(e);
		if(type == RASTER) ((RasterTool) getSelectedTool()).mouseMoved(e, (int)p.x, (int)p.y, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).mouseMoved(e, p.x, p.y, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		canvasMouseEventHandle2(e);
	}
	
	public void canvasKeyPressed(KeyEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		if(type == RASTER) ((RasterTool) getSelectedTool()).keyPressed(e, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).keyPressed(e, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		e.consume();
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}
	
	public void canvasKeyReleased(KeyEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		if(type == RASTER) ((RasterTool) getSelectedTool()).keyReleased(e, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).keyReleased(e, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		e.consume();
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}
	
	public void canvasKeyTyped(KeyEvent e) {
		if(!drawable()) return;
		int type = getLayerAndToolType();
		if(type == RASTER) ((RasterTool) getSelectedTool()).keyTyped(e, this);
		else if(type == VECTOR) ((VectorTool) getSelectedTool()).keyTyped(e, this);
		else if(type == NONE) { System.out.println("mismatched types");	return; }
		e.consume();
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}

	public void moveSelectedLayerUp() {
		boolean moved = canvasState.moveSelectedLayerUp();
		if(moved){
			layerChanged = true;
			Paint.updateDisplay();
		}
	}

	public void moveSelectedLayerDown() {
		boolean moved = canvasState.moveSelectedLayerDown();
		if(moved){
			layerChanged = true;
			Paint.updateDisplay();
		}	
	}

	public void swapStrokeAndFillColors() {
		Colour stroke = pathState.getStrokeColor();
		Colour fill = pathState.getFillColor();
		pathState.setStrokeColor(fill);
		pathState.setFillColor(stroke);
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}

	public void setDefaultStrokeAndFillColors() {
		pathState.setDefaultStrokeAndFillColors();
		canvasState.setChanged(true);
		Paint.updateDisplay();
	}

	public boolean getAA() {
		return canvasState.getAA();
	}
	
	public void setAA(boolean b) {
		canvasState.setAA(b);
		layerChanged = true;
		toolChanged = true;
		Paint.updateDisplay();
	}
	
	public boolean getSplitView() {
		return canvasState.getSplitView();
	}
	
	public void setSplitView(boolean b) {
		canvasState.setSplitView(b);
	}

	public void setSelectedColorType(int selected) {
		pathState.setSelected(selected);
	}

}
