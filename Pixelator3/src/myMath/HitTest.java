package myMath;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Vector;

import colours.Colour;

import renderer.geometry.*;

public class HitTest {

	// for segments
	
	//public static HitResult hitTest(Segment segment, MyPoint p, double tolerance) {
	//	return hitTest(segment, p, tolerance, HitResult.ALL);
	//}
	
	public static HitResult hitTest(Segment segment, MyPoint p, double tolerance, int filterType) {
		MyPoint point = segment.getPoint();
		MyPoint handleIn = segment.getHandleIn();
		MyPoint handleOut = segment.getHandleOut();
		HitResult hitResult = new HitResult();
		if(filterType == HitResult.ANCHOR) {
			if(p.distance(point) <= tolerance) {
				hitResult.setType(HitResult.POINT);
			}
		}
		else if(filterType == HitResult.HANDLE) {
			if(p.distance(point.plus(handleIn)) <= tolerance) {
				hitResult.setType(HitResult.HANDLE_IN);
			}
			else if(p.distance(point.plus(handleOut)) <= tolerance) {
				hitResult.setType(HitResult.HANDLE_OUT);
			}
		}
		return hitResult;
	}
	
	// for paths
	/*
	public static HitResult hitTest(Path path, MyPoint p, double tolerance) {
		return hitTest(path, p, tolerance, HitResult.ALL);
	}*/
	
	public static HitResult hitTest(Path path, MyPoint p, double tolerance, int filterType, boolean[] pathHit) {
		Vector<Segment> segments = path.getSegments();
		HitResult hitResult = new HitResult();
		if(filterType == HitResult.PATH) {
			int x1 = (int) Math.floor(p.x-tolerance);
			int y1 = (int) Math.floor(p.y-tolerance);
			int x2 = (int) Math.ceil(p.x+tolerance);
			int y2 = (int) Math.ceil(p.y+tolerance);
			BufferedImage image = new BufferedImage(x2, y2, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2 = (Graphics2D) image.getGraphics();
			Rectangle2D r = new Rectangle2D.Double(x1, y1, 2*tolerance, 2*tolerance);
			g2.setClip(r);
			g2.draw(path.getShape());
			if(hasNonTransparentPixels(image, r)) {
				pathHit[0] = true;
			}
		}
		else {
			for(int i=segments.size()-1; i>=0; i--) {
				HitResult segHitResult = hitTest(segments.get(i), p, tolerance, filterType);
				if(segHitResult.pointIsHit()) {
					hitResult = segHitResult;
					hitResult.setSegmentIndex(i);
					return hitResult;
				}
			}
		}
		
		return hitResult;
	}
	
	public static boolean hasNonTransparentPixels(BufferedImage image, Rectangle2D r) {
		for(int i=Math.max(0,(int) r.getMinX()); i<=Math.min(image.getWidth()-1, (int)r.getMaxX()); i++) {
			for(int j=Math.max(0,(int) r.getMinY()); j<=Math.min(image.getHeight()-1, (int)r.getMaxX()); j++) {
				Color c = Colour.rgbInt2Color(image.getRGB(i, j));
				if(c.getAlpha() > 0) return true;
			}
		}
		return false;
	}

}

