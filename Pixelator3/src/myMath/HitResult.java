package myMath;

public class HitResult {

	// path index
	private int pathIndex;
	// segment index
	private int segmentIndex;
	// type
	public static final int NONE = -1, POINT = 0, HANDLE_IN = 1, HANDLE_OUT = 2, PATH = 3;
	private int type;
	// search filters
	public static final int /*ALL = 0, */ANCHOR = 1, HANDLE = 2;
	
	
	public HitResult() {
		pathIndex = -1;
		segmentIndex = -1;
		type = NONE;
	}
	
	public HitResult(int pathIndex, int segmentIndex, int type) {
		assert(type == POINT || type == HANDLE_IN || type == HANDLE_OUT || type == PATH);
		this.pathIndex = pathIndex;
		this.segmentIndex = segmentIndex;
		this.type = type;
	}
	
	public HitResult(HitResult hit) {
		this.pathIndex = hit.getPathIndex();
		this.segmentIndex = hit.getSegmentIndex();
		this.type = hit.getType();
	}
	
	public HitResult clone() {
		return new HitResult(this);
	}

	public boolean pathIsHit() {
		return pathIndex!=-1;
	}
	
	public boolean segmentIsHit() {
		return segmentIndex!=-1;
	}
	
	public String toString() {
		String s = "";
		s += "path hit = " + pathIndex;
		s += ", seg hit = " + segmentIndex;
		s += ", point type = " + type;
		return s;
	}

	public boolean pointIsHit() {
		return type!=NONE;
	}

	public int getPathIndex() {
		return pathIndex;
	}

	public void setPathIndex(int pathIndex) {
		this.pathIndex = pathIndex;
	}

	public void unsetPathIndex() {
		this.pathIndex = -1;
		unsetSegmentIndex();
	}

	public int getSegmentIndex() {
		return segmentIndex;
	}

	public void setSegmentIndex(int segmentIndex) {
		this.segmentIndex = segmentIndex;
	}
	
	public void unsetSegmentIndex() {
		this.segmentIndex = -1;
		type = NONE;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
