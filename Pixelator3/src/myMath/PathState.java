package myMath;

import colours.Colour;

public class PathState {

	public final Colour DEFAULT_STROKE_COLOR = Colour.BLACK;
	public final Colour DEFAULT_FILL_COLOR = Colour.TRANSPARENT;
	public final Colour DEFAULT_PATH_COLOR = Colour.ROYAL_BLUE;
	public static final int STROKE = 3, FILL = 4;
	private final double DEFAULT_STROKE_WEIGHT = 1;
	private Colour strokeColor, fillColor, pathColor;
	private double strokeWeight;
	private int selected = STROKE;

	public PathState() {
		strokeColor = DEFAULT_STROKE_COLOR;
		fillColor = DEFAULT_FILL_COLOR;
		pathColor = DEFAULT_PATH_COLOR;
		strokeWeight = DEFAULT_STROKE_WEIGHT;
	}
	
	public PathState(PathState other) {
		strokeColor = new Colour(other.getStrokeColor());
		fillColor = new Colour(other.getFillColor());
		pathColor = new Colour(other.getPathColor());
		strokeWeight = other.getStrokeWeight();
	}
	
	public int getSelected() {
		return selected;
	}
	
	public void setSelected(int s) {
		assert(s==STROKE || s==FILL);
		selected = s;
	}
	
	public Colour getSelectedColor() {
		return (selected==STROKE) ? strokeColor.clone() : fillColor.clone();
	}
	
	public boolean defaultColorsSelected() {
		return strokeColor == DEFAULT_STROKE_COLOR && fillColor == DEFAULT_FILL_COLOR;
	}
	
	public Colour getStrokeColor() {
		return strokeColor.clone();
	}
	
	public void setStrokeColor(Colour c) {
		strokeColor = new Colour(c);
	}
	
	public Colour getFillColor() {
		return fillColor.clone();
	}
	
	public void setFillColor(Colour c) {
		fillColor = new Colour(c);
	}

	public Colour getPathColor() {
		return pathColor.clone();
	}

	public void setPathColor(Colour c) {
		pathColor = new Colour(c);
	}

	public double getStrokeWeight() {
		return strokeWeight;
	}
	
	public void setStrokeWeight(double w) {
		strokeWeight = w;
	}

	public void setDefaultStrokeAndFillColors() {
		strokeColor = DEFAULT_STROKE_COLOR;
		fillColor = DEFAULT_FILL_COLOR;
	}
}
