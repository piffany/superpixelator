package myMath;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;

import colours.Colour;

import SVGIO.intransix.osm.termite.svg.SvgConverter;
import SVGIO.intransix.osm.termite.svg.SvgDocument;
import SVGIO.intransix.osm.termite.svg.SvgGeometry;

import renderer.display.MyGraphics2D;
import renderer.rasterizer.Rasterizer;
import myImage.RasterImage;
import myImage.VectorImage;
import myLayer.Layer;
import myLayer.RasterLayer;
import myLayer.VectorLayer;
import myMath.geometry.Path;
import myTool.rasterizer.JavaRasterizer;
import myTool.rasterizer.Pixelator;
import myWidget.Canvas;
import myWidget.container.Paint;

public class CanvasState {

	private int width, height;
	private AffineTransform at, invAt;
	public Vector<Layer> layers = new Vector<Layer>();
	//private final double[] SCALES = {1/4., 1/3., 1/2., 1., 1.5, 2., 3., 4., 6., 8., 12., 16.};
	private final double[] SCALES = {1., 2., 3., 4., 6., 8., 12., 16.};
	private int scaleIndex = 4;
	private double scale;
	private int selectedLayer = -1;
	boolean changed = false;
	private int rasterizerType = Rasterizer.SUPERPIXELATOR;
	private boolean showThumb = false;
	private boolean splitView = false;
	private boolean AA = false;
	
	public CanvasState() {
		scale = SCALES[scaleIndex];
		setMinimumSize();
	}
	
	public int getRasterizerType() {
		return rasterizerType;
	}
	
	public void setRasterizerType(int type) {
		assert(type == Rasterizer.DEFAULT || type == Rasterizer.SUPERPIXELATOR);
		rasterizerType = type;
	}
	
	public boolean getShowThumb() {
		return showThumb;
	}
	
	public void setShowThumb(boolean b) {
		showThumb = b;		
	}
	
	public boolean getSplitView() {
		return splitView;
	}
	
	public void setSplitView(boolean b) {
		splitView = b;
	}

	public boolean isChanged() {
		return changed;
	}
	
	public void setChanged(boolean b) {
		changed = b;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setMinimumSize() {
		width = Canvas.MIN_WIDTH; height = Canvas.MIN_HEIGHT;
	}
	
	public boolean isMinimumSize() {
		return width==Canvas.MIN_WIDTH && height==Canvas.MIN_HEIGHT;
	}
	
	public void addNewCanvas(int w, int h, int type, State state) {
		clearLayers();
		setSize(w, h);
		changed = true;
	}
	
	private boolean isValidSize(int w, int h) {
		return Canvas.MIN_WIDTH < w && Canvas.MAX_WIDTH >= w && Canvas.MIN_HEIGHT < h && Canvas.MAX_HEIGHT >= h;
	}
	
	public void setSize(int w, int h) {
		if(isValidSize(w,h)) {
			width = w; height = h;
		}
		updateTransforms();
	}
	
	public void updateTransforms() {
		at = AffineTransform.getTranslateInstance(0, 0);
		at.scale(scale, scale);
		invAt = new AffineTransform(at);
		try {
			invAt.invert();
		} catch (NoninvertibleTransformException e1) {
			System.out.println("noninvertible transform");
			e1.printStackTrace();
		}
	}

	public AffineTransform getTransform() {
		return at;
	}

	public AffineTransform getInverseTransform() {
		return invAt;
	}

	public int getTransformedWidth() {
		return (int) Math.round((double)width*scale);//Transform.transformLength(width, at);
	}

	public int getTransformedHeight() {
		return (int) Math.round((double)height*scale);//Transform.transformLength(height, at);
	}

	public void clearLayers() {
		layers.clear();
		deselectLayer();
		changed = true;
	}

	public void addRasterLayer(State state) {
		addRasterLayer("", state);
	}
	
	public void addVectorLayer(State state) {
		addVectorLayer("", state);
	}

	public boolean isValidLayerIndex(int i) {
		return i>=0 && i<layers.size();
	}
	
	public String getLayerName(int i) {
		assert(isValidLayerIndex(i));
		return layers.get(i).getName();
	}
	
	public Vector<String> getLayerNames() {
		Vector<String> names = new Vector<String>();
		for(int i=0; i<layers.size(); i++) {
			names.add(layers.get(i).getName());
		}
		return names;
	}
	
	// add after selected layer
	public void addRasterLayer(String name, State state) {
		RasterLayer newLayer = new RasterLayer(width, height, state);
		newLayer.setName(name);
		if(hasSelectedLayer()) {			
			if(selectedLayer+1 < layers.size()) {
				layers.add(selectedLayer+1, newLayer);
				selectedLayer += 1;
			}
			else {
				layers.add(newLayer);
				selectedLayer = layers.size()-1;
			}
		}
		else {
			layers.add(newLayer);
			selectedLayer = layers.size()-1;
		}
		updateLayerSelection();
		changed = true;
	}
	
	public void addVectorLayer(String name, State state) {
		VectorLayer newLayer = new VectorLayer(width, height, state);
		newLayer.setName(name);
		if(hasSelectedLayer()) {
			if(selectedLayer+1 < layers.size()) {
				layers.add(selectedLayer+1, newLayer);
				selectedLayer += 1;
			}
			else {
				layers.add(newLayer);
				selectedLayer = layers.size()-1;
			}
		}
		else {
			layers.add(newLayer);
			selectedLayer = layers.size()-1;
		}
		updateLayerSelection();
		changed = true;
	}

	public void setLayerVisible(int i, boolean b) {
		layers.get(i).setVisible(b);
		changed = true;
	}

	public void setSelectedLayerVisible(boolean b) {
		layers.get(selectedLayer).setVisible(b);
	}
	
	public void deleteLayer(int i) {
		if(i>=0 && i<layers.size()) {
			layers.remove(i);
			if(layers.isEmpty()) closeCanvas();
			else if(i>=0 && i<layers.size()) setSelectedLayerIndex(i);
			else setSelectedLayerIndex(layers.size()-1);
		}
		changed = true;
	}
	
	public void deleteSelectedLayer() {
		deleteLayer(selectedLayer);
		changed = true;
	}


	public int getSelectedLayerType() {
		if(layers.isEmpty() || !hasSelectedLayer()) return State.NONE;
		else return layers.get(selectedLayer).getType();
	}

	public int getLayerType(int i) {
		assert(isValidLayerIndex(i));
		return layers.get(i).getType();
	}
	
	public Vector<Integer> getLayerTypes() {
		Vector<Integer> layerTypes = new Vector<Integer>();
		for(int i=0; i<layers.size(); i++) {
			layerTypes.add(layers.get(i).getType());
		}
		return layerTypes;
	}

	public RasterLayer getRasterLayer() {
		assert(hasSelectedLayer());
		return (RasterLayer) layers.get(selectedLayer);
	}

	public VectorLayer getVectorLayer() {
		assert(hasSelectedLayer());
		return (VectorLayer) layers.get(selectedLayer);
	}

	public Vector<Layer> getLayers() {
		return layers;
	}

	public int getNumLayers() {
		return layers.size();
	}
	
	public void closeCanvas() {
		clearLayers();
		setMinimumSize();
		changed = true;
	}

	public boolean rasterizeSelectedLayer(State state) {
		if(!hasSelectedLayer() || getSelectedLayerType()==State.RASTER) return false;
		else {
			boolean rasterized = rasterizeLayer(selectedLayer, state);
			if(rasterized) changed = true;
			return rasterized;
		}
	}
	
	public Layer getSelectedLayer() {
		return layers.get(selectedLayer);
	}

	public boolean rasterizeLayer(int i, State state) {
		if(!isValidLayerIndex(i) || getLayerType(i)==State.RASTER) return false;
		RasterImage rasterImage = new RasterImage(width, height, BufferedImage.TYPE_INT_ARGB, state);
		VectorLayer vectorLayer = (VectorLayer) layers.get(i);
		MyGraphics2D g2 = new MyGraphics2D((Graphics2D) rasterImage.getGraphics(), width, height);
		AffineTransform at = AffineTransform.getTranslateInstance(0, 0);
		if(rasterizerType==Rasterizer.DEFAULT) JavaRasterizer.draw(g2,  vectorLayer, at, state);
		else Pixelator.draw(g2, vectorLayer, at, state);
		RasterLayer rasterLayer = new RasterLayer(width, height, state);
		rasterLayer.setRasterImage(rasterImage);
		String layerName = vectorLayer.getName();
		if(layerName.endsWith("[V]")) {
			layerName = layerName.substring(0, layerName.length()-3);
		}
		rasterLayer.setName(layerName + "[R]");
		layers.set(i, rasterLayer);
		return true;
	}
	
	public RasterImage rasterizeAllLayers(State state, boolean visibleOnly) {
		RasterImage image = new RasterImage(width, height, BufferedImage.TYPE_INT_RGB, state);
		MyGraphics2D g2 = new MyGraphics2D((Graphics2D) image.getGraphics(), image.getWidth(), image.getHeight());
		AffineTransform at = AffineTransform.getTranslateInstance(0, 0);
		for(int i=0; i<layers.size(); i++) {
			if(visibleOnly && !layers.get(i).isVisible()) continue;
			if(layers.get(i).getType() == State.RASTER) {
				RasterLayer rasterLayer = (RasterLayer) layers.get(i);
				g2.drawRenderedImage(rasterLayer.getRasterImage(), at);

			}
			else {
				VectorLayer vectorLayer = (VectorLayer) layers.get(i);
				if(rasterizerType==Rasterizer.DEFAULT) JavaRasterizer.draw(g2,  vectorLayer, at, state);
				else {
					Pixelator.draw(g2, vectorLayer, at, state);
				}
			}
		}
		return image;
	}
	
	public boolean saveToFile(File file, String extension, State state) {
		RasterImage image = rasterizeAllLayers(state, true);
		// Write generated image to a file
		try {
			// Save as PNG
			ImageIO.write(image, extension, file);
		}
		catch (IOException e) {
			System.out.println("saveToFile failed");
			return false;
		}
		return true;
	}

	public boolean openFile(File file, String extension, State state) {
		if(extension == "png") return openPNGFile(file, state);
		else if(extension == "svg") return openSVGFile(file, state);
		else return false;

	}
	
	private boolean openPNGFile(File file, State state) {
		try {
			BufferedImage image = ImageIO.read(file);
			addNewCanvas(image.getWidth(), image.getHeight(), State.RASTER, state);
			addRasterLayer(state);
			Graphics2D g2 = (Graphics2D) ((RasterLayer) layers.get(0)).getRasterImage().getGraphics();
			AffineTransform at = AffineTransform.getTranslateInstance(0, 0);
			g2.drawRenderedImage(image, at);
		}
		catch (IOException e) {
			return false;
		}
		changed = true;
		return true;
	}
	
	private boolean openSVGFile(File file, State state) {
		deselectLayer();
		try {
			SvgConverter converter = new SvgConverter();
			String s = file.toURI().toString();
			SvgDocument svgDoc = converter.loadSvg(s);
			Vector<Vector<SvgGeometry>> svgLayers = svgDoc.getLayers();
			Vector<String> layerNames = svgDoc.getLayerNames();
			Rectangle2D docSize = svgDoc.getDocSize();
			addNewCanvas((int) docSize.getWidth(), (int) docSize.getHeight(), State.VECTOR, state);
			for(int i=0; i<svgLayers.size(); i++) {
				Vector<SvgGeometry> geoms = svgLayers.get(i);
				if(geoms.size()==1 && geoms.get(0).shape==null) {
					addRasterLayer(layerNames.get(i), state);
					RasterImage image = ((RasterLayer) layers.get(i)).getRasterImage();
					Graphics2D g2 = (Graphics2D) image.getGraphics();
					
					if(geoms.get(0).image!=null) {
						g2.drawImage(geoms.get(0).image, 0, 0, null);
					}
				}
				else {
					addVectorLayer(layerNames.get(i), state);
					VectorImage image = ((VectorLayer) layers.get(i)).getVectorImage();
					for(int j=0; j<geoms.size(); j++) {
						SvgGeometry object = geoms.get(j);
						boolean hasFill = object.fill != null;
						boolean hasStroke = object.stroke != null;
						if(hasFill || hasStroke) {
							Vector<renderer.geometry.Path> paths = renderer.geometry.Path.getPaths(object.shape);
							for(int k=0; k<paths.size(); k++) {
								Path path = new Path();
								path.setPath(paths.get(k).clone());
								Colour fillColor = hasFill ? new Colour(new Color(object.fill)) : null;
								Colour strokeColor = hasStroke ? new Colour(new Color(object.stroke)) : null;
								double strokeWidth = (double) object.strokeWidth;
								if(fillColor!=null) path.setFillColor(fillColor);
								else path.setFillColor(Colour.TRANSPARENT);
								if(strokeColor!=null) path.setStrokeColor(strokeColor);
								else path.setStrokeColor(Colour.TRANSPARENT);
								path.setStrokeWidth(strokeWidth);
								image.addPath(path);
							}
						}
						image.deselectAll();
					}
					((VectorLayer) layers.get(i)).setVectorImage(image);
				}
			}
			/*
			BufferedImage image = ImageIO.read(file);
			addNewCanvas(image.getWidth(), image.getHeight(), State.RASTER, state);
			addRasterLayer(state);
			Graphics2D g2 = (Graphics2D) ((RasterLayer) layers.get(0)).getRasterImage().getGraphics();
			AffineTransform at = AffineTransform.getTranslateInstance(0, 0);
			g2.drawRenderedImage(image, at);
			*/
			
		}
		catch (Exception e) {
			System.out.println("exception");
			return false;
		}
		changed = true;
		return true;
	}

	public void incrementScale(int increment) {
		if(isZoomable(increment)) {
			scaleIndex += increment;
			scale = SCALES[scaleIndex];
		}
		updateTransforms();
		clearRasterTempImages();
		clearAllVectorLayerImageCache();
		changed = true;
	}
	
	public void renameLayer(String name) {
		layers.get(getSelectedLayerIndex()).setName(name);
		changed = true;
	}

	public double getScale() {
		return scale;
	}
	
	public boolean isZoomable(int increment) {
		int newIndex = scaleIndex + increment;
		return (newIndex >=0 && newIndex < SCALES.length);
	}

	public void clearTempImages() {
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getType() == State.RASTER) {
				((RasterLayer) layers.get(i)).getRasterTempImage().clear();
			}
			else if(layers.get(i).getType() == State.VECTOR) {
				((VectorLayer) layers.get(i)).getVectorTempImage().clear();
			}
		}
	}
	
	private void clearRasterTempImages() {
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getType() == State.RASTER) {
				((RasterLayer) layers.get(i)).getRasterTempImage().clear();
			}
		}
	}
	
	public void clearVectorTempImages() {
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getType() == State.RASTER) {
				((RasterLayer) layers.get(i)).getRasterTempImage().clear();
			}
		}
	}
	
	public void zoomIn() {
		incrementScale(1);
		changed = true;
		Paint.updateDisplay();
	}

	public void zoomOut() {
		incrementScale(-1);
		changed = true;
		Paint.updateDisplay();
	}

	public boolean hasSelectedLayer() {
		return selectedLayer!=-1;
	}
	
	public void deselectLayer() {
		selectedLayer = -1;
		updateLayerSelection();
	}
	
	private void updateLayerSelection() {
		for(int i=0; i<layers.size(); i++) {
			Layer layer = layers.get(i);
			layer.setSelected(i == selectedLayer);
		}
	}
	
	public int getSelectedLayerIndex() {
		return selectedLayer;
	}

	public void setSelectedLayerIndex(int selectedLayer) {
		this.selectedLayer = selectedLayer;
		updateLayerSelection();
		changed = true;
	}

	public boolean isLayerVisible(int i) {
		return !layers.isEmpty() && layers.get(i).isVisible();
	}
	
	public boolean isSelectedLayerVisible() {
		return isLayerVisible(selectedLayer);
	}

	public boolean moveSelectedLayerUp() {
		if(!hasSelectedLayer() || selectedLayer == layers.size()-1) return false;
		Layer layer = layers.get(selectedLayer);
		layers.remove(selectedLayer);
		selectedLayer++;
		layers.add(selectedLayer, layer);
		changed = true;
		return true;
	}
	
	public boolean moveSelectedLayerDown() {
		if(!hasSelectedLayer() || selectedLayer == 0) return false;
		Layer layer = layers.get(selectedLayer);
		layers.remove(selectedLayer);
		selectedLayer--;
		layers.add(selectedLayer, layer);
		changed = true;
		return true;
	}

	public boolean getAA() {
		return AA;
	}
	
	public void clearAllVectorLayerImageCache() {
		// clear cache for all vector layers
		for(int i=0; i<layers.size(); i++) {
			if(layers.get(i).getType() == State.VECTOR) {
				VectorImage image = ((VectorLayer) layers.get(i)).getVectorImage();
				image.clearPathCache();
			}
		}
	}
	
	public void setAA(boolean b) {
		if(b!=AA) {
			clearAllVectorLayerImageCache();
			AA = b;
			changed = true;
		}
	}

}
