package myMath;

import java.util.Collections;
import java.util.List;
import java.util.Vector;

@SuppressWarnings("serial")
public class MyVector <T extends Comparable<? super T>> extends Vector<T> {

	public MyVector() {
		super();
	}
	
	public static <T extends Comparable<? super T>> void sort(List<T> vector) {
		Collections.sort(vector);
	}
	
	public static <T extends Comparable<? super T>> void sortNoDuplicates(List<T> vector) {
		sort(vector);
		for(int i=vector.size()-1; i>0; i--) {
			if(vector.get(i).compareTo(vector.get(i-1))==0) {
				vector.remove(i);
			}
		}
	}
}
