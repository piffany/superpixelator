package myMath.MyFilter;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class NumericFilter extends DocumentFilter{

	public void insertString(DocumentFilter.FilterBypass fb, int offset,  
			String text, AttributeSet attr) throws BadLocationException {  
		fb.insertString(offset, text.replaceAll("\\D", ""), attr); 
	}

	public void replace(DocumentFilter.FilterBypass fb, int offset, int length,  
			String text, AttributeSet attr) throws BadLocationException {  
		fb.replace(offset, length, text.replaceAll("\\D", ""), attr);  
	}  
}
