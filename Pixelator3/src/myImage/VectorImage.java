package myImage;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.Vector;

import colours.Colour;

import renderer.display.MyGraphics2D;
import renderer.geometry.Segment;
import renderer.geometry.MyPoint;

import myMath.*;
import myMath.geometry.Path;
import myWidget.container.Paint;

public class VectorImage {

	private int width, height;
	private Vector<Path> paths;
	private State state;
	private int selectedPathIndex = -1, selectedSegmentIndex = -1; // index of selected path and segment
	private BufferedImage rasterCache, vectorCache;
	private boolean cacheEmpty = false;
	
	public VectorImage(int width, int height, State state) {
		this.width = width;
		this.height = height;
		this.state = state;
		paths = new Vector<Path>();
		rasterCache = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		vectorCache = new BufferedImage((int)(width*state.getScale()),
										(int)(height*state.getScale()),
										BufferedImage.TYPE_INT_ARGB);
	}
	
	public VectorImage(VectorImage vectorImage) {
		width = vectorImage.getWidth();
		height = vectorImage.getHeight();
		state = vectorImage.getState();
		selectedPathIndex = vectorImage.getSelectedPathIndex();
		selectedSegmentIndex = vectorImage.getSelectedSegmentIndex();
		paths = new Vector<Path>();
		rasterCache = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		vectorCache = new BufferedImage((int)(width*state.getScale()),
				(int)(height*state.getScale()),
				BufferedImage.TYPE_INT_ARGB);
		//Iterator<Path> itr = vectorImage.getPaths().iterator();
		for(int i=0; i<vectorImage.getNumPaths(); i++) {
		//while(itr.hasNext()) {
			//paths.add(itr.next().clone());
		//}
			paths.add(vectorImage.getPath(i).clone());
		}
	}
	
	public void clearCache() {
		rasterCache = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		vectorCache = new BufferedImage((int)(width*state.getScale()),
				(int)(height*state.getScale()),
				BufferedImage.TYPE_INT_ARGB);
		cacheEmpty = true;
	}
	
	public void clearPathCache() {
		rasterCache = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		vectorCache = new BufferedImage((int)(width*state.getScale()),
				(int)(height*state.getScale()),
				BufferedImage.TYPE_INT_ARGB);
		for(int i=0; i<paths.size(); i++) {
			paths.get(i).clearStrokePixels();
			paths.get(i).clearFillPixels();
		}
		cacheEmpty = true;
	}
	
	private boolean hasChanges() {
		for(int i=0; i<paths.size(); i++) {
			Path path = paths.get(i);
			if(!path.hasFillPixels() || !path.hasStrokePixels()) return true;
		}
		return false;
	}
	
	public boolean updateCache(MyGraphics2D mg, boolean colorChanged) {
		boolean changeInPaths = false;
		boolean changeInColor = false;
		for(int i=0; i<paths.size(); i++) {
			Path path = paths.get(i);
			if(!path.hasFillPixels()) {
				mg.setColor(path.getFillColor());
				path.recomputeFillPixel(mg);
				changeInPaths = true;
			}
			if(!path.hasStrokePixels()) {
				mg.setColor(path.getStrokeColor());
				path.recomputeStrokePixel(mg);
				changeInPaths = true;
			}
			if(path.isColorChanged()) {
				changeInColor = true;
				path.setColorChanged(false);
			}
		}
		
		changeInPaths = changeInPaths || cacheEmpty;
		if(changeInPaths || changeInColor) {
			//if(changeInPaths) {
				clearCache();
				Graphics2D vCache = (Graphics2D)vectorCache.getGraphics();
				double r = state.getScale();
				for(int i=0; i<paths.size(); i++) {
					Path path = paths.get(i);
					Shape shape = path.getScaledCopy(r).getShape();
					if(path.getFillColor()!=null && path.getFillColor().getAlpha() > 0) {
						vCache.setColor(new Color(i,i,i));
						vCache.fill(shape);
					}
					if(path.getStrokeColor()!=null && path.getStrokeColor().getAlpha() > 0) {
						vCache.setColor(new Color(i,i,i));
						vCache.draw(shape);
					}
				}
			//}
			MyGraphics2D rCache = new MyGraphics2D((Graphics2D)rasterCache.getGraphics(), width, height);
			for(int i=0; i<paths.size(); i++) {
				paths.get(i).drawFillPixels(rCache);
				paths.get(i).drawStrokePixels(rCache);
			}
		}
		cacheEmpty = false;
		
		return changeInPaths || changeInColor;
	}
	
	public void drawRasterImage(MyGraphics2D mg, State state) {
		updateCache(mg, state.isCanvasChanged());		
		mg.drawImage(rasterCache, 0, 0, null); 
	}
	
	public int getPathHitFromVectorImage(int xC, int yC, int tolerance) {
		if(hasChanges()) {
			clearCache();
			Graphics2D vCache = (Graphics2D)vectorCache.getGraphics();
			double r = state.getScale();
			for(int i=0; i<paths.size(); i++) {
				Path path = paths.get(i);
				Shape shape = path.getScaledCopy(r).getShape();
				if(path.getFillColor()!=null && path.getFillColor().getAlpha() > 0) {
					vCache.setColor(new Color(i,i,i));
					vCache.fill(shape);
				}
				if(path.getStrokeColor()!=null && path.getStrokeColor().getAlpha() > 0) {
					vCache.setColor(new Color(i,i,i));
					vCache.draw(shape);
				}
			}
		}
		
		int w = vectorCache.getWidth(), h = vectorCache.getHeight();
		int topmostPathInd = -1;
		MyPoint pC = new MyPoint(xC, yC);
		for(int x = Math.max(0,xC-tolerance); x<=Math.min(w-1, xC+tolerance); x++) {
			for(int y = Math.max(0,yC-tolerance); y<=Math.min(h-1, yC+tolerance); y++) {
				double dist = (new MyPoint(x,y)).distance(pC);
				if(dist <= tolerance) {
					int rgba = vectorCache.getRGB(x, y);
					if(Colour.getAlpha(rgba) > 0) {
						int pathInd = (int) Colour.getRed(rgba);
						if(topmostPathInd == -1 || pathInd > topmostPathInd) {
							topmostPathInd = pathInd;
						}
					}
				}
			}
		}
		return topmostPathInd;
	}
	
	public VectorImage clone() {
		return new VectorImage(this);
	}
	
	public State getState() {
		return state;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	// return actual object so it can be edited
	/*
	public Vector<Path> getPaths() {
		return paths;
	}
	*/
	
	public Vector<Path> getPathsCopy() {
		Vector<Path> copy = new Vector<Path>();
		Iterator<Path> itr = paths.iterator();
		while(itr.hasNext()) {
			copy.add(itr.next().clone());
		}
		return copy;
	}
	
	public void clear() {
		paths.clear();
		deselectAll();
	}
	
	public Path getLastPath() {
		return getPath(paths.size()-1);
	}
	
	public void addPath(Path path) {
		paths.add(path.clone());
		setSelectedPath(paths.size()-1);
	}
	
	private boolean isValidPathIndex(int index) {
		return index >=0 && index < paths.size();
	}
	
	public void removePath(int index) {
		assert(isValidPathIndex(index));
		paths.remove(index);
		clearCache();
	}
	
	public void removeCurrentPath() {
		removePath(paths.size()-1);
	}
	
	public void setPath(Path path, int index) {
		clearCache();
		paths.set(index, path.clone());
	}
	
	public void setPaths(Vector<Path> paths) {
		clearCache();
		this.paths.clear();
		Iterator<Path> itr = paths.iterator();
		while(itr.hasNext()) {
			Path path = itr.next();
			this.paths.add(path.clone());
		}
	}
	
	public void setLastPath(Path path) {
		setPath(path, paths.size()-1);
	}
	
	public void addToPath(Segment segment, State state) {
		//getLastPath().setStrokeColor(state.getStrokeColor());
		Path lastPath = getLastPath();
		lastPath.setPathState(state.getPathState());
		lastPath.addSegment(segment);
		setLastPath(lastPath);
		setSelectedSegmentToLastSegment();
	}
	
	public boolean isEmpty() {
		return paths.isEmpty();
	}
	
	private void setSelectedSegmentToLastSegment() {
		if(hasSelectedPath()) {
			Path selected = getPath(getSelectedPathIndex());
			if(!selected.isEmpty()) setSelectedSegment(selected.getNumSegments()-1);
		}
	}
	
	public void addToPath(MyPoint p, State state) {
		if(isEmpty()) startNewPath(state);
		//getLastPath().setStrokeColor(state.getStrokeColor());
		Path lastPath = getLastPath();
		lastPath.setPathState(state.getPathState());
		lastPath.addPoint(p);
		setLastPath(lastPath);
		setSelectedSegmentToLastSegment();
	}

	public void startNewPath(State state) {
		paths.add(new Path(state.getPathState()));
		setSelectedPath(paths.size()-1);
		deselectSegment();
	}
	
	public void deselectPath() {
		selectedPathIndex = -1;
	}
	
	public void deselectAll() {
		deselectPath();
		deselectSegment();
	}
	
	public int getSelectedPathIndex() {
		return selectedPathIndex;
	}
	
	public void setSelectedPath(int index) {
		assert(index >= 0 && index < paths.size());
		selectedPathIndex = index;
	}

	public void deselectSegment() {
		selectedSegmentIndex = -1;
	}
	
	public void setSelectedSegment(int index) {
		assert(index >= 0 && index < paths.get(selectedPathIndex).getNumSegments());
		selectedSegmentIndex = index;
	}
	
	public int getSelectedSegmentIndex() {
		return selectedSegmentIndex;
	}


	public HitResult hitTest(MyPoint p, double tolerance) {
		HitResult hit = hitTest(p, tolerance, HitResult.ANCHOR);
		if(hit.pointIsHit()) return hit;
		else hit = hitTest(p, tolerance, HitResult.HANDLE_IN);
		if(hit.pointIsHit()) return hit;
		else hit = hitTest(p, tolerance, HitResult.HANDLE_OUT);
		if(hit.pointIsHit()) return hit;
		else hit = hitTest(p, tolerance, HitResult.PATH);
		return hit;
		//return hitTest(p, tolerance, HitResult.ALL);
	}
	
	public HitResult hitTest(MyPoint p, double tolerance, int filterType) {
		HitResult hitResult = new HitResult();
		if(filterType == HitResult.PATH ) {
			double r = state.getScale();
			int pathHit = getPathHitFromVectorImage((int) (p.x*r), (int) (p.y*r), (int) (tolerance*r));
			if(pathHit!=-1) {
				hitResult.setPathIndex(pathHit);
				hitResult.setSegmentIndex(-1);
				return hitResult;
			}
		}
		else {
			for(int i=paths.size()-1; i>=0; i--) {
				boolean[] pathHit = {false};
				HitResult pathHitResult = HitTest.hitTest(paths.get(i), p, tolerance, filterType, pathHit);
				if(pathHitResult.pointIsHit()) {
					hitResult = pathHitResult;
					hitResult.setPathIndex(i);
					return hitResult;
				}
				/*
				else if(pathHit[0]) {
					hitResult.setPathIndex(i);
					hitResult.setSegmentIndex(-1);
					return hitResult;
				}
				*/
			}
		}
		return hitResult;
	}
	
	public Path getPath(int index) {
		assert(isValidPathIndex(index));
		return paths.get(index).clone();
	}
	
	
	public int getNumPaths() {
		return paths.size();
	}
	
	public boolean hasSelectedPath() {
		return selectedPathIndex != -1;
	}
	
	public void updateSelectedPath() {
		if(hasSelectedPath()) {
			if(Paint.debug) System.out.println("VectorImage: update path " + getSelectedPathIndex() + " / " + paths.size());
			Path path = paths.get(getSelectedPathIndex());
			path.setPathState(state.getPathState());
		}		
	}
}
