package myImage;
import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Vector;

import renderer.display.MyGraphics2D;

import colours.Colour;
import myMath.State;
import renderer.geometry.MyPoint;


public class RasterImage extends BufferedImage {

	private Colour backgroundColor;
	private int backgroundInt;
	private State state;
	
	public RasterImage(int width, int height, int imageType, State state) {
		super(width, height, imageType);
		this.state = state;
		if(isTransparent()) setBackgroundColor(Colour.TRANSPARENT);
		else setBackgroundColor(Colour.WHITE);
		clear();
	}
	
	public RasterImage resizeImage(int width, int height) {
		RasterImage copy = new RasterImage(width, height, getType(), getState());
		copy.setData(getRaster());
		copy.setBackgroundColor(backgroundColor);
		copy.setState(state);
		return copy;
	}
	
	public MyGraphics2D getMyGraphics2D() {
		Graphics2D g2 = (Graphics2D) getGraphics();
		MyGraphics2D mg2 = new MyGraphics2D(g2, getWidth(), getHeight());
		return mg2;
	}
	
	public boolean isTransparent() {
		return getType() == BufferedImage.TYPE_INT_ARGB;
	}
	
	public RasterImage(VectorImage vectorImage, State state) {
		this(vectorImage.getWidth(), vectorImage.getHeight(), BufferedImage.TYPE_INT_ARGB, state);
	}
	
	public RasterImage clone() {
		RasterImage copy = new RasterImage(getWidth(), getHeight(), getType(), getState());
		copy.setData(getRaster());
		copy.setBackgroundColor(backgroundColor);
		copy.setState(state);
		return copy;
	}
	
	public int getRGB(int x, int y) {
		return getColor(x,y).getRGB();
	}
	
	public void setBackgroundColor(Colour backgroundColor) {
		this.backgroundColor = backgroundColor.clone();
		this.backgroundInt = backgroundColor.getRGB();
	}

	public State getState() {
		return state;
	}
	
	public void setState(State state) {
		this.state = state;
	}
	
	public Colour getBackgroundColor() {
		return backgroundColor.clone();
	}
	
	public Colour getColor(int x, int y) {
		int colorInt = super.getRGB(x, y);
		Colour c = Colour.rgbInt2Color(colorInt);
		return c;
	}
	
	public boolean isTransparent(int x, int y) {
		int colorInt = super.getRGB(x, y);
		int alpha = (colorInt>>24) & 0xff;
		return alpha == 0;
	}

	public void clear() {
		Graphics2D g2 = (Graphics2D) getGraphics();
		// if transparent
		if(backgroundColor.getAlpha() == 0) {
			g2.setComposite(AlphaComposite.Clear);
			g2.fillRect(0, 0, getWidth(), getHeight());
			g2.setComposite(AlphaComposite.SrcOver);
		}
		else {
			g2.setColor(backgroundColor);
			g2.fillRect(0, 0, getWidth(), getHeight());
		}
	}
	
	public void clearPixel(int x, int y) {
		if(x>=0 && x<getWidth() && y>=0 && y<getHeight()) {
			super.setRGB(x, y, backgroundInt);
		}
	}

	public void setPixel(MyPoint p, Colour color) {
		setPixel((int)p.x, (int)p.y, color);
	}
	
	public void setPixel(int x, int y, Colour color) {
		if(x>=0 && x<getWidth() && y>=0 && y<getHeight()) {
			super.setRGB(x, y, color.getRGB());
		}
	}
	
	public void setPixels(Vector<MyPoint> points, Colour color) {
		for(int i=0; i<points.size(); i++) {
			setPixel(points.get(i), color);
		}
	}

	public void addImageAbove(RasterImage image, int offsetX, int offsetY) {
		for(int x=0; x<image.getWidth(); x++) {
			for(int y=0; y<image.getHeight(); y++) {
				if(image.isTransparent(x,y)) continue;
				else {
					int thisX = x + offsetX;
					int thisY = y + offsetY;
					if(thisX >= 0 && thisX < getWidth() && thisY >= 0 && thisY < getHeight()) {
						Colour newColor = image.getColor(x,y);
						setPixel(new MyPoint(thisX, thisY), newColor);
					}
				}
			}
		}
	}
	
	public void addImageAbove(RasterImage image) {
		addImageAbove(image, 0, 0);
	}

}
