package myTool.rasterizer;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.Iterator;
import java.util.Vector;

import myImage.RasterImage;
import myImage.VectorImage;
import myLayer.VectorLayer;
import myMath.State;
import renderer.geometry.MyCurve;
import myMath.geometry.Path;

abstract public class Rasterizer {

	public static void draw(Graphics2D g, VectorLayer vectorLayer,
			AffineTransform at, State state) {
	}
	
	protected static RasterImage rasterizedVectorImage(VectorImage vectorImage, State state) {
		RasterImage raster = new RasterImage(vectorImage, state);
		Graphics2D g = (Graphics2D) raster.getGraphics();
		//Vector<Path> paths = vectorImage.getPaths();
		//Iterator<Path> itr = paths.iterator();
		//while(itr.hasNext()) {
		for(int i=0; i<vectorImage.getNumPaths(); i++) {
			//Path path = itr.next();
			Path path = vectorImage.getPath(i);
			g.setColor(path.getStrokeColor());
			g.setStroke(new BasicStroke((float) path.getStrokeWeight()));
			drawRasterizedPath(g, path);
		}
		return raster;
	}
	
	protected static void drawRasterizedPath(Graphics2D g, Path path) {
		Vector<MyCurve> curves = path.getCurves();
		Iterator<MyCurve> itr = curves.iterator();
		while(itr.hasNext()) {
			drawRasterizedCurve(g, itr.next());
		}
	}
	
	static protected void drawRasterizedCurve(Graphics2D g, MyCurve curve) {
		g.draw(curve);
	}


}
