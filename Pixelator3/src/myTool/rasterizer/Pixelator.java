package myTool.rasterizer;

import java.awt.geom.AffineTransform;
import myImage.VectorImage;
import myLayer.VectorLayer;
import myMath.State;
import renderer.display.GraphicsState;
import renderer.display.MyGraphics2D;

public class Pixelator extends Rasterizer {

	public Pixelator() {
		super();
	}	
	
	public static void draw(MyGraphics2D mg, VectorLayer vectorLayer, AffineTransform at, State state) {
		VectorImage vectorImage = vectorLayer.getVectorImage();
		GraphicsState gs = mg.getState();
		mg.setZoom(at.getScaleX());
		vectorImage.drawRasterImage(mg, state);
		mg.setState(gs);
	}
	
}