package myTool.rasterizer;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;

import renderer.display.MyGraphics2D;
import renderer.geometry.MyCurve;
import myImage.RasterImage;
import myImage.VectorImage;
import myLayer.VectorLayer;
import myMath.State;
import myMath.geometry.Path;

public class JavaRasterizer extends Rasterizer {

	public JavaRasterizer() {
		super();
	}	
	
	public static void draw(MyGraphics2D g, VectorLayer vectorLayer, AffineTransform at, State state) {
		VectorImage vectorImage = vectorLayer.getVectorImage();
		RasterImage rasterImage = rasterizedVectorImage(vectorImage, state);
		g.drawRenderedImage(rasterImage, at);
	}

	protected static RasterImage rasterizedVectorImage(VectorImage vectorImage, State state) {
		RasterImage raster = new RasterImage(vectorImage, state);
		MyGraphics2D g = new MyGraphics2D((Graphics2D) raster.getGraphics(), raster.getWidth(), raster.getHeight());
		g.setRasterizerType(renderer.rasterizer.Rasterizer.DEFAULT);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                (state.getAA() ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF));
		for(int i=0; i<vectorImage.getNumPaths(); i++) {
			Path path = vectorImage.getPath(i);
			g.setColor(path.getFillColor());
			g.fillPath(path);
			g.setColor(path.getStrokeColor());
			g.setStroke(new BasicStroke((float) path.getStrokeWidth()));
			g.drawPath(path);
			//drawRasterizedPath(g, path);
		}
		return raster;
	}
	
	protected static void drawRasterizedPath(Graphics2D g, Path path) {
		Rasterizer.drawRasterizedPath(g, path);
	}

	protected static void drawRasterizedCurve(Graphics2D g, MyCurve curve) {
		g.draw(curve);
	}
	
}
