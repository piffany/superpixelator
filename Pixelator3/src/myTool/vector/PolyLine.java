package myTool.vector;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import myImage.*;
import myMath.*;
import renderer.geometry.MyPoint;


public class PolyLine
extends VectorTool {

	private static String iconFile = "/icons/polyline.gif";
	private final static String instruction = 
			"Draw connected line segments by clicking on points, then finish by double-clicking.";

	public PolyLine() {
		super("VectorPolyLine", "Polyline", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		VectorImage vectorTempImage = state.getVectorLayer().getVectorTempImage();
		if(!hasLastPoint()) {
			vectorImage.startNewPath(state);
			vectorImage.addToPath(new MyPoint(x,y), state);
			vectorTempImage.clear();
			vectorTempImage.startNewPath(state);
			vectorTempImage.addToPath(new MyPoint(x,y), state);
			setLastPoint(x,y);
		}
		else {
			if(getLastX()==x && getLastY()==y) {
				unsetLastPoint();
				vectorImage.deselectAll();
			}
			else {
				vectorImage.addToPath(new MyPoint(x,y), state);
				vectorTempImage.clear();
				vectorTempImage.startNewPath(state);
				vectorTempImage.addToPath(new MyPoint(x,y), state);
				setLastPoint(x,y);
			}
		}
	}

	public void mouseReleased(MouseEvent e, double x, double y, State state) {

	}

	public void mouseExited(MouseEvent e, double x, double y, State state) {
		VectorImage vectorTempImage = state.getVectorLayer().getVectorTempImage();
		vectorTempImage.clear();
	}

	public void mouseEntered(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseDragged(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseMoved(MouseEvent e, double x, double y, State state) {
		VectorImage vectorTempImage = state.getVectorLayer().getVectorTempImage();
		if(hasLastPoint()) {
			vectorTempImage.clear();
			vectorTempImage.startNewPath(state);
			if(getLastX()!=x || getLastY()!=y) {
				vectorTempImage.addToPath(lastPoint, state);
				vectorTempImage.addToPath(new MyPoint(x,y), state);
			}
		}
	}
	
	public void keyPressed(KeyEvent e, State state) {		
	}

	public void keyReleased(KeyEvent e, State state) {
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
