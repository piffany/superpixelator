package myTool.vector;

import renderer.geometry.MyPoint;
import renderer.geometry.Segment;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import myImage.VectorImage;
import myMath.*;
import myMath.geometry.Path;


public class BezierCurve
extends VectorTool {

	private static String iconFile = "/icons/bezierCurve.gif";
	private final static String instruction = 
			"Draw a spline curve by anchor points and handles.";

	public BezierCurve() {
		super("VectorBezierCurve", "Curve", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		VectorImage vectorTempImage = state.getVectorLayer().getVectorTempImage();
		if(!hasLastPoint()) {
			vectorImage.startNewPath(state);
			vectorImage.addToPath(new MyPoint(x,y), state);
			vectorTempImage.clear();
			vectorTempImage.startNewPath(state);
			vectorTempImage.addToPath(new MyPoint(x,y), state);
			setLastPoint(x,y);
		}
		else {
			if(getLastX()==x && getLastY()==y) {
				unsetLastPoint();
			}
			else {
				vectorImage.addToPath(new MyPoint(x,y), state);
				vectorTempImage.clear();
				vectorTempImage.startNewPath(state);
				vectorTempImage.addToPath(new MyPoint(x,y), state);
				setLastPoint(x,y);
			}
		}
	}

	public void mouseReleased(MouseEvent e, double x, double y, State state) {
	}

	public void mouseExited(MouseEvent e, double x, double y, State state) {
		VectorImage vectorTempImage = state.getVectorLayer().getVectorTempImage();
		vectorTempImage.clear();
	}

	public void mouseEntered(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseDragged(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(hasLastPoint()) {
			Path lastPath = vectorImage.getLastPath();
			Segment segment = lastPath.getLastSegment().clone();
			MyPoint cursor = new MyPoint(x,y);
			MyPoint point = segment.getPoint();
			MyPoint handleIn = point.minus(cursor);
			MyPoint handleOut = new MyPoint(-handleIn.x, -handleIn.y);
			segment.setHandleIn(handleIn);
			segment.setHandleOut(handleOut);
			lastPath.setLastSegment(segment);
			vectorImage.setLastPath(lastPath);
		}
	}
	
	public void mouseMoved(MouseEvent e, double x, double y, State state) {
	}
	
	public void keyPressed(KeyEvent e, State state) {		
	}

	public void keyReleased(KeyEvent e, State state) {
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
