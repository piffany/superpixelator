package myTool.vector;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import javax.swing.ImageIcon;

import myImage.*;
import myMath.*;
import renderer.geometry.*;
import renderer.util.MyMath;
import renderer.util.Transform;
import myMath.geometry.Path;
import myTool.MyCursor;


public class Select
extends VectorTool {

	// in absolute pixels
	private double tolerance = 4;
	//private HitResult hitResult = new HitResult();
	private Path hitPath = new Path();
	private Path originalHitPath = new Path();
	//private Segment hitSegment = null;
	private int hitPathInd = -1, hitSegInd = -1;
	private MyPoint mousePos = new MyPoint(-1,-1);
	//private boolean shiftDown = false;
	private MyPoint mouseDownPos = new MyPoint(-1,-1);
	private Path pathBB = null;
	private final int MOVE = 0, ROTATE = 1, SCALE = 2;
	private double rotation = 0;
	private double[] translation = {0,0};
	private MyPoint scaleCentre = null;
	private int mode = MOVE;
	
	private static String iconFile = "/icons/select.gif";
	private static String cursorFile = "/cursors/select.gif";
	private final static Cursor crossHair = new Cursor(Cursor.CROSSHAIR_CURSOR);
	private final static String instruction = 
			"Select an object to move (M), rotate (R), and scale (S). Hold SHIFT for uniform scaling. Remove object with DELETE or BACKSPACE.";

	public Select() {
		super("VectorSelect", "Select", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(MyCursor.getCursor(getClass().getResource(cursorFile)));
		//unsetHitResult(state);
	}
	
	private void setMousePosition(double x, double y) {
		mousePos.x = x; mousePos.y = y;
	}
	
	private void setMouseDownPosition(double x, double y) {
		mouseDownPos.x = x; mouseDownPos.y = y;
	}
	
	private boolean isMouseDown() {
		return mouseDownPos.x >= 0 && mouseDownPos.y >= 0;
	}

	/*
	private Rectangle2D.Double getBBRectangle() {
		if(pathBB==null) return null;
		else {
			MyPoint p1 = pathBB.getSegment(0).getPoint();
			MyPoint p2 = pathBB.getSegment(2).getPoint();
			return new Rectangle2D.Double(	Math.min(p1.x, p2.x), Math.min(p1.y, p2.y),
											Math.abs(p2.x-p1.x), Math.abs(p2.y-p1.y));
		}
	}
	*/
	
	public void hitTest(VectorImage vectorImage, double x, double y, State state) {
		if(x==-1 && y==-1) return;
		HitResult hit = vectorImage.hitTest(new MyPoint(x,y), tolerance/state.getScale(), HitResult.PATH);
		//System.out.println(hit + " : " + (new MyPoint(x,y)) + ", " + (tolerance/state.getScale()));
		setHitResult(hit, vectorImage, state);
	}
	
	public void mousePressed(MouseEvent e, double x, double y, State state) {
		//System.out.println("Select press: " + getButton(e));
		if(!isMouseDown()) {
			setMouseDownPosition(x, y);
		}
		//System.out.println("press: " + mouseDownPos);
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(mode==MOVE) {
			if(pathBB==null) hitTest(vectorImage, x, y, state);
			else {
				hitTest(vectorImage, x, y, state);
				/*
				Rectangle2D.Double rectBB = getBBRectangle();
				if(x < rectBB.getMinX() || x > rectBB.getMaxX() || y < rectBB.getMinY() || y > rectBB.getMaxY()) {
					hitTest(vectorImage, x, y, state);
				}
				else {
					setHitResult(new HitResult(hitPathInd, -1, HitResult.PATH), vectorImage, state);
				}
				*/
			}
		}
		else {
			if(!nearBB(x,y,1)) hitTest(vectorImage, x, y, state);
		}
	}

	private void moveHitPoint(double x, double y, State state) {
		if(hitPathInd!=-1) {
			if(hitSegInd == -1) {
				if(isMouseDown()) {
					translation[0] = x-mouseDownPos.x;
					translation[1] = y-mouseDownPos.y;
					AffineTransform at = AffineTransform.getTranslateInstance(translation[0], translation[1]);
					Path movedPath = originalHitPath.getTransformedCopy(at);
					VectorImage vectorImage = state.getVectorLayer().getVectorImage();
					vectorImage.setPath(movedPath, hitPathInd);
					//originalHitPath = movedPath.clone();
					updateBoundingBox(movedPath, null, state);
				}
			}
		}		
	}
	
	private void rotateHitPath(double x, double y, State state) {
		//System.out.println("rotate");
		if(hitPathInd!=-1) {
			MyPoint centre = originalHitPath.getBoundingBoxCentre();
			MyPoint vec1 = mouseDownPos.minus(centre);
			MyPoint vec2 = (new MyPoint(x,y)).minus(centre);
			double dAngle = vec1.getAngleTo(vec2);
			//System.out.println(rotation + ", " + dAngle);
			//System.out.println(angle);
			//if(Math.abs(angle) < 0.01) angle = 0;
			AffineTransform at = AffineTransform.getRotateInstance(-(rotation + dAngle), centre.x, centre.y);
			Path movedPath = originalHitPath.getTransformedCopy(at);
			VectorImage vectorImage = state.getVectorLayer().getVectorImage();
			vectorImage.setPath(movedPath, hitPathInd);
			updateBoundingBox(originalHitPath, at, state);
		}
	}
	
	private void scaleHitPath(double x, double y, boolean preserveAspectRatio, State state) {
		//System.out.println("rotate");
		if(hitPathInd!=-1) {
			if(scaleCentre==null) {
				MyPoint[] BB = originalHitPath.getBoundingBox();
				double centreX = (Math.abs(BB[0].x-x) > Math.abs(BB[1].x-x)) ? BB[0].x : BB[1].x;
				double centreY = (Math.abs(BB[0].y-y) > Math.abs(BB[1].y-y)) ? BB[0].y : BB[1].y;
				scaleCentre = new MyPoint(centreX, centreY);
			}
			MyPoint vec1 = mouseDownPos.minus(scaleCentre);
			MyPoint vec2 = (new MyPoint(x,y)).minus(scaleCentre);
			//if(Math.abs(vec1.length()) < 0.01) return;
			//double r = vec2.length() / vec1.length();
			double rx = (Math.abs(vec1.x) < 0.01) ? 1 : vec2.x/vec1.x;
			double ry = (Math.abs(vec1.y) < 0.01) ? 1 : vec2.y/vec1.y;
			double r = Math.min(Math.abs(rx), Math.abs(ry)); 
			AffineTransform at = preserveAspectRatio ?
								AffineTransform.getScaleInstance(r*MyMath.sgn(rx), r*MyMath.sgn(ry)) :
								AffineTransform.getScaleInstance(rx, ry);
			at.preConcatenate(AffineTransform.getTranslateInstance(scaleCentre.x, scaleCentre.y));
			at.concatenate(AffineTransform.getTranslateInstance(-scaleCentre.x, -scaleCentre.y));
			Path movedPath = originalHitPath.getTransformedCopy(at);
			VectorImage vectorImage = state.getVectorLayer().getVectorImage();
			vectorImage.setPath(movedPath, hitPathInd);
			updateBoundingBox(movedPath, null, state);
		}		
	}
	
	private void deleteHitPoint(VectorImage vectorImage, State state) {
		if(isHit(state)) {
			if(hitPathInd!=-1 && hitSegInd==-1) {
				//System.out.println(hitPathInd + ", " + hitSegInd);
				vectorImage.removePath(hitPathInd);
				//vectorImage.deselectAll();
				//state.deselectObject();
				unsetHitResult(vectorImage, state);
				return;
			}
		}
	}
	/*
	private int getButton(MouseEvent e) {
		int button = e.getButton();
		if(button==MouseEvent.BUTTON3 && e.isShiftDown()) button = MouseEvent.BUTTON2;
		return button;
	}
	*/
	
	public void mouseReleased(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		//System.out.println("Select release: " + getButton(e) + ", " + isHit(state));
		if(isHit(state)) {
			//System.out.println("isHit");
			double dist =  mouseDownPos.distance(new MyPoint(x,y));
			//System.out.println(dist + ", " + mouseDownPos + ", " + new MyPoint(x,y));
			if(dist > 1) {
				if(mode == MOVE) {
					moveHitPoint(x,y,state);
					originalHitPath = vectorImage.getPath(hitPathInd);
				}
				else if(mode == SCALE) {
					scaleHitPath(x,y,e.isShiftDown(),state);
					originalHitPath = vectorImage.getPath(hitPathInd);
					resetTransformationParameters();
				}
				else if(mode == ROTATE) {
					MyPoint centre = originalHitPath.getBoundingBoxCentre();
					MyPoint vec1 = mouseDownPos.minus(centre);
					MyPoint vec2 = (new MyPoint(x,y)).minus(centre);
					double dAngle = vec1.getAngleTo(vec2);
					rotation += dAngle;
					mouseDownPos = new MyPoint(x,y);
					rotateHitPath(x,y,state);
				}
				//originalHitPath = vectorImage.getPath(hitPathInd);
			}
			//else unsetHitResult(vectorImage, state);
		}
		else {
			unsetHitResult(vectorImage, state);
			removeBoundingBox(state);
		}
		setMouseDownPosition(-1, -1);
	}

	public void mouseExited(MouseEvent e, double x, double y, State state) {
	}

	public void mouseEntered(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseDragged(MouseEvent e, double x, double y, State state) {
		setMousePosition(x, y);
		if(!isMouseDown()) {
			setMouseDownPosition(x, y);
			//buttonDown = e.getButton();
		}
		//System.out.println("Select drag: " + buttonDown);
		if(mode == MOVE) moveHitPoint(x,y,state);
		else if(mode == SCALE) {
			scaleHitPath(x,y,e.isShiftDown(),state);
		}
		else if(mode == ROTATE) rotateHitPath(x,y,state);
	}
	
	public void mouseMoved(MouseEvent e, double x, double y, State state) {
		setMousePosition(x, y);
		if(mode!=MOVE && nearBB(x,y,1)) setCursor(crossHair);
		else setCursor(cursor);
	}
	
	private boolean nearBB(double x, double y, double tol) {
		if(pathBB!=null) {
			MyPoint p = new MyPoint(x,y);
			for(int i=0; i<pathBB.getNumSegments()-1; i++) {
				if(pathBB.getSegment(i).getPoint().distance(p) < tol) return true;
			}
		}
		return false;
	}
	
	private void resetTransformationParameters() {
		rotation = 0;
		translation[0] = 0;
		translation[1] = 0;
		scaleCentre = null;
	}
	
	public void keyPressed(KeyEvent e, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(e.getKeyCode() == KeyEvent.VK_M || e.getKeyChar() == 'm' || e.getKeyChar() == 'M') {
			if(mode!=MOVE) {
				mode = MOVE;
				resetTransformationParameters();
				if(hitPathInd!=-1) {
					originalHitPath = vectorImage.getPath(hitPathInd);
					updateBoundingBox(originalHitPath, Transform.getIdentity(), state);
				}
			}
		}
		else if(e.getKeyCode() == KeyEvent.VK_R || e.getKeyChar() == 'r' || e.getKeyChar() == 'R') {
			if(mode!=ROTATE) {
				mode = ROTATE;
				resetTransformationParameters();
				if(hitPathInd!=-1) {
					originalHitPath = vectorImage.getPath(hitPathInd);
					updateBoundingBox(originalHitPath, Transform.getIdentity(), state);
				}
			}
		}
		else if(e.getKeyCode() == KeyEvent.VK_S || e.getKeyChar() == 's' || e.getKeyChar() == 'S') {
			if(mode!=SCALE) {
				mode = SCALE;
				resetTransformationParameters();
				if(hitPathInd!=-1) {
					originalHitPath = vectorImage.getPath(hitPathInd);
					updateBoundingBox(originalHitPath, Transform.getIdentity(), state);
				}
			}
		}
		/*
		else if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			if(shiftDown) return;
			else {
				shiftDown = true;
				if(isMouseDown()) {
					hitTest(vectorImage, mousePos.x, mousePos.y, state);
				}
			}
		}
		*/
		else if(e.getKeyCode() == KeyEvent.VK_DELETE || e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			deleteHitPoint(vectorImage, state);
		}
	}

	public void keyReleased(KeyEvent e, State state) {
		/*
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftDown = false;
			if(isMouseDown()) {
				hitTest(vectorImage, mousePos.x, mousePos.y, state);
			}
		}*/
	}

	public void keyTyped(KeyEvent e, State state) {
	}

	private void unsetHitResult(VectorImage vectorImage, State state) {
		state.deselectObject();
		vectorImage.deselectPath();
		vectorImage.deselectSegment();
		hitPathInd = -1;
		hitSegInd = -1;
		hitPath = null;
		//hitSegment = null;
		originalHitPath = null;
		pathBB = null;
		resetTransformationParameters();
		removeBoundingBox(state);
	}
	
	private void setHitResult(HitResult hit, VectorImage vectorImage, State state) {
		if(hit.pathIsHit()) {
			if(hit.getPathIndex()!=hitPathInd) {
				resetTransformationParameters();
				vectorImage.setSelectedPath(hit.getPathIndex());
				vectorImage.deselectSegment();
				hitPathInd = hit.getPathIndex();
				hitSegInd = hit.getSegmentIndex();
				hitPath = vectorImage.getPath(hitPathInd).clone();
				originalHitPath = hitPath.clone();
				//hitSegment = null;
				state.setSelectedObject(hit, hitPath);
				updateBoundingBox(hitPath, null, state);
			}
		}
		else unsetHitResult(vectorImage, state);
	}
	
	private void updateBoundingBox(Path path, AffineTransform at, State state) {
		MyPoint[] BB = path.getBoundingBox();
		pathBB = new Path.Rectangle(BB[0].x, BB[0].y,
						BB[1].x-BB[0].x,
						BB[1].y-BB[0].y,
						state.getPathState());
		if(at!=null) pathBB.transform(at);
		state.getVectorLayer().getVectorTempImage().clear();
		state.getVectorLayer().getVectorTempImage().addPath(pathBB);		
	}
	
	private void removeBoundingBox(State state) {
		state.getVectorLayer().getVectorTempImage().clear();
	}
	
	private boolean isHit(State state) {
		return state.getSelectedObject().pathIsHit();//.pointIsHit();
	}

	public void setSelectedTool(boolean b, State state) {
		if(state.isFileOpen()) {
			try {
				VectorImage vectorImage = state.getVectorLayer().getVectorImage();
				unsetHitResult(vectorImage, state);
				mode = MOVE;
			
			}
			catch(Exception e) {}
			/*
			VectorImage vectorImage = state.getVectorLayer().getVectorImage();
			HitResult hit = new HitResult();
			hit.setPathIndex(vectorImage.getSelectedPathIndex());
			hit.setSegmentIndex(vectorImage.getSelectedSegmentIndex());
			this.setHitResult(hit, vectorImage, state);
			*/
			//System.out.println(hitPathInd + ", " + hitSegInd);
		}
		
		/*
		if(state.isFileOpen()) {
			VectorImage vectorImage = state.getVectorLayer().getVectorImage();
			unsetHitResult(vectorImage, state);
		}*/
	}

}
