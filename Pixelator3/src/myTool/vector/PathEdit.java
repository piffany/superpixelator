package myTool.vector;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;

import javax.swing.ImageIcon;

import myImage.*;
import myMath.*;
import renderer.geometry.*;
import myMath.geometry.Path;
import myTool.MyCursor;


public class PathEdit
extends VectorTool {

	// in absolute pixels
	private double tolerance = 4;
	//private HitResult hitResult = new HitResult();
	private Path hitPath = new Path();
	private Path originalHitPath = new Path();
	private Segment hitSegment = null;
	private int hitPathInd = -1, hitSegInd = -1;
	private MyPoint mousePos = new MyPoint(-1,-1);
	private boolean shiftDown = false;
	private MyPoint mouseDownPos = new MyPoint(-1,-1);
	private int buttonDown = -1;
	
	private static String iconFile = "/icons/pathEdit.gif";
	private static String cursorFile = "/cursors/pathEdit.gif";
	private final static String instruction = 
			"Edit a path by its anchor points and handles. Hold SHIFT to keep handles symmetric. Remove points with DELETE.";


	public PathEdit() {
		super("VectorPathEdit", "Path Edit", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(MyCursor.getCursor(getClass().getResource(cursorFile)));
		//unsetHitResult(state);
	}
	
	private void setMousePosition(double x, double y) {
		mousePos.x = x; mousePos.y = y;
	}
	
	private void setMouseDownPosition(double x, double y) {
		mouseDownPos.x = x; mouseDownPos.y = y;
	}
	
	private boolean isMouseDown() {
		return mouseDownPos.x >= 0 && mouseDownPos.y >= 0;
	}

	public void hitTest(VectorImage vectorImage, double x, double y, State state) {
		if(x==-1 && y==-1) return;
		HitResult hitAnchor = vectorImage.hitTest(new MyPoint(x,y), tolerance/state.getScale(), HitResult.ANCHOR);
		HitResult hitHandle = vectorImage.hitTest(new MyPoint(x,y), tolerance/state.getScale(), HitResult.HANDLE);
		HitResult hitPath = vectorImage.hitTest(new MyPoint(x,y), tolerance/state.getScale(), HitResult.PATH);
		HitResult hit;
		if(shiftDown) {
			hit = (hitHandle.pointIsHit()) ? hitHandle : hitAnchor;
		}
		else {
			hit = (hitAnchor.pointIsHit()) ? hitAnchor : hitHandle;
		}
		if(hitPath.pathIsHit() && hitPath.getPathIndex() > hit.getPathIndex()) {
			hit = hitPath;
		}
		else if(!hit.pointIsHit()) hit = hitPath;
		setHitResult(hit, vectorImage, state);
	}
	
	public void mousePressed(MouseEvent e, double x, double y, State state) {
		//System.out.println("Select press: " + getButton(e));
		if(!isMouseDown()) {
			setMouseDownPosition(x, y);
			buttonDown = getButton(e);
		}
		//System.out.println("press: " + mouseDownPos);
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(getButton(e) == MouseEvent.BUTTON1) hitTest(vectorImage, x, y, state);
	}

	private void moveHitPoint(double x, double y, State state) {
		if(hitPathInd!=-1) {
			if(hitSegInd == -1) {
				if(isMouseDown()) {
					MyPoint diff = new MyPoint(x-mouseDownPos.x, y-mouseDownPos.y);
					AffineTransform at = AffineTransform.getTranslateInstance(diff.x, diff.y);
					Path movedPath = originalHitPath.getTransformedCopy(at);
					VectorImage vectorImage = state.getVectorLayer().getVectorImage();
					vectorImage.setPath(movedPath, hitPathInd);
				}
			}
			else {
				MyPoint p = new MyPoint(x,y);
				Segment newSegment = hitSegment.clone();
				HitResult hit = state.getSelectedObject();
				switch(hit.getType()) {
				case HitResult.POINT:
					newSegment.setPoint(p);
					//System.out.println(newSegment);
					break;
				case HitResult.HANDLE_IN:
					newSegment.setHandleIn(p.minus(newSegment.getPoint()));
					if(shiftDown) {
						MyPoint handleIn = new MyPoint(newSegment.getHandleIn());
						newSegment.setHandleOut(new MyPoint(-handleIn.x, -handleIn.y));
					}
					break;
				case HitResult.HANDLE_OUT:
					newSegment.setHandleOut(p.minus(newSegment.getPoint()));
					if(shiftDown) {
						MyPoint handleOut = new MyPoint(newSegment.getHandleOut());
						newSegment.setHandleIn(new MyPoint(-handleOut.x, -handleOut.y));
					}
					break;
				}
				//System.out.println(hitSegment + ", " + newSegment);
				hitSegment = newSegment.clone();
				hitPath.set(hitSegInd, newSegment);
				VectorImage vectorImage = state.getVectorLayer().getVectorImage();
				vectorImage.setPath(hitPath, hitPathInd);
				//System.out.println(vectorImage.getPath(hitPathInd));
			}
		}		
	}
	
	private void deleteHitPoint(VectorImage vectorImage, State state) {
		if(isHit(state)) {
			if(hitPathInd!=-1 && hitSegInd==-1) {
				//System.out.println(hitPathInd + ", " + hitSegInd);
				vectorImage.removePath(hitPathInd);
				vectorImage.deselectAll();
				state.deselectObject();
				return;
			}
			Segment newSegment = hitSegment.clone();
			HitResult hit = state.getSelectedObject();
			switch(hit.getType()) {
			case HitResult.POINT:
				hitPath.removeSegment(hit.getSegmentIndex());
				vectorImage.setPath(hitPath, hitPathInd);
				if(hitPath.isEmpty()) {
					vectorImage.removePath(hit.getPathIndex());
					hit.unsetPathIndex();
				}
				else {
					hit.unsetSegmentIndex();
				}
				break;
			case HitResult.HANDLE_IN:
				newSegment.removeHandleIn();
				hit.setType(HitResult.POINT);
				setHitResult(hit, vectorImage, state);
				hitPath.set(hit.getSegmentIndex(), newSegment);
				vectorImage.setPath(hitPath, hitPathInd);
				break;
			case HitResult.HANDLE_OUT:
				newSegment.removeHandleOut();
				hit.setType(HitResult.POINT);
				setHitResult(hit, vectorImage, state);
				hitPath.set(hit.getSegmentIndex(), newSegment);
				vectorImage.setPath(hitPath, hitPathInd);
				break;
			}
		}
	}
	
	private int getButton(MouseEvent e) {
		int button = e.getButton();
		if(button==MouseEvent.BUTTON3 && e.isShiftDown()) button = MouseEvent.BUTTON2;
		return button;
	}
	
	public void mouseReleased(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		//System.out.println("Select release: " + getButton(e) + ", " + isHit(state));
		if(isHit(state)) {
			//System.out.println("isHit");
			double dist =  mouseDownPos.distance(new MyPoint(x,y));
			//System.out.println(dist + ", " + mouseDownPos + ", " + new MyPoint(x,y));
			if(dist > 1) {
				if(buttonDown == MouseEvent.BUTTON1) moveHitPoint(x,y,state);

				originalHitPath = vectorImage.getPath(hitPathInd);
			}
			//else unsetHitResult(vectorImage, state); 
		}
		else {
			if(buttonDown == MouseEvent.BUTTON1) unsetHitResult(vectorImage, state);
		}
		setMouseDownPosition(-1, -1);
		buttonDown = -1;
	}

	public void mouseExited(MouseEvent e, double x, double y, State state) {
	}

	public void mouseEntered(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseDragged(MouseEvent e, double x, double y, State state) {
		setMousePosition(x, y);
		if(!isMouseDown()) {
			setMouseDownPosition(x, y);
			//buttonDown = e.getButton();
		}
		//System.out.println("Select drag: " + buttonDown);
		if(buttonDown==MouseEvent.BUTTON1) moveHitPoint(x,y,state);
	}
	
	public void mouseMoved(MouseEvent e, double x, double y, State state) {
		setMousePosition(x, y);
	}
	
	public void keyPressed(KeyEvent e, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			if(shiftDown) return;
			else {
				shiftDown = true;
				if(isMouseDown()) {
					if(buttonDown==MouseEvent.BUTTON1) {
						hitTest(vectorImage, mousePos.x, mousePos.y, state);
						moveHitPoint(mousePos.x, mousePos.y, state);
					}
					/*
					else if(buttonDown==MouseEvent.BUTTON3) {
						originalHitPath = vectorImage.getPath(hitPathInd);
					}
					*/
				}
			}
		}
		else if(e.getKeyCode() == KeyEvent.VK_DELETE || e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
			if(hitSegInd!=-1) deleteHitPoint(vectorImage, state);
		}
	}

	public void keyReleased(KeyEvent e, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftDown = false;
			if(isMouseDown()) {
				if(buttonDown==MouseEvent.BUTTON1) hitTest(vectorImage, mousePos.x, mousePos.y, state);
				/*
				else if(buttonDown==MouseEvent.BUTTON3) {
					originalHitPath = vectorImage.getPath(hitPathInd);
				}
				*/
			}
		}
	}

	public void keyTyped(KeyEvent e, State state) {
	}

	private void unsetHitResult(VectorImage vectorImage, State state) {
		state.deselectObject();
		vectorImage.deselectPath();
		vectorImage.deselectSegment();
		hitPathInd = -1;
		hitSegInd = -1;
		hitPath = null;
		hitSegment = null;
	}
	
	private void setHitResult(HitResult hit, VectorImage vectorImage, State state) {
		if(hit.pointIsHit()) {
			vectorImage.setSelectedPath(hit.getPathIndex());
			vectorImage.setSelectedSegment(hit.getSegmentIndex());
			hitPathInd = hit.getPathIndex();
			hitSegInd = hit.getSegmentIndex();
			hitPath = vectorImage.getPath(hitPathInd).clone();
			originalHitPath = hitPath.clone();
			hitSegment = hitPath.getSegment(hitSegInd);
			state.setSelectedObject(hit, hitPath);
		}
		else if(hit.pathIsHit()) {
			vectorImage.setSelectedPath(hit.getPathIndex());
			vectorImage.deselectSegment();
			hitPathInd = hit.getPathIndex();
			hitSegInd = hit.getSegmentIndex();
			hitPath = vectorImage.getPath(hitPathInd).clone();
			originalHitPath = hitPath.clone();
			hitSegment = null;
			state.setSelectedObject(hit, hitPath);
		}
		else unsetHitResult(vectorImage, state);
	}
	
	private boolean isHit(State state) {
		return state.getSelectedObject().pathIsHit();//.pointIsHit();
	}

	public void setSelectedTool(boolean b, State state) {
		if(state.isFileOpen()) {
			try {
			VectorImage vectorImage = state.getVectorLayer().getVectorImage();
			unsetHitResult(vectorImage, state);
			}
			catch(Exception e) {}
			/*
			VectorImage vectorImage = state.getVectorLayer().getVectorImage();
			HitResult hit = new HitResult();
			hit.setPathIndex(vectorImage.getSelectedPathIndex());
			hit.setSegmentIndex(vectorImage.getSelectedSegmentIndex());
			this.setHitResult(hit, vectorImage, state);
			*/
			//System.out.println(hitPathInd + ", " + hitSegInd);
		}
		
		/*
		if(state.isFileOpen()) {
			VectorImage vectorImage = state.getVectorLayer().getVectorImage();
			unsetHitResult(vectorImage, state);
		}*/
	}

}
