package myTool.vector;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import myImage.*;
import myMath.*;
import myMath.geometry.Path;
import renderer.geometry.MyPoint;


public class Line
extends VectorTool {

	private static String iconFile = "/icons/line.gif";
	private final static String instruction = 
			"Draw a straight line. Hold down SHIFT to keep horizontal or vertical.";
	private MyPoint mousePos = null;

	public Line() {
		super("VectorLine", "Line", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		setLastPoint(x,y);
		mousePos = new MyPoint(x,y);
		vectorImage.startNewPath(state);
	}

	public void mouseReleased(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(hasLastPoint()) {
			if(!(getLastX()==x && getLastY()==y)) {
				Path line = getLine(state, lastPoint.x, lastPoint.y, x, y, e.isShiftDown());
				vectorImage.setLastPath(line);
			}
			else {
				vectorImage.removeCurrentPath();
				//vectorImage.deselectAll();
			}
		}
		vectorImage.deselectAll();
		unsetLastPoint();
		mousePos = null;
	}
	
	public void mouseExited(MouseEvent e, double x, double y, State state) {
	}

	public void mouseEntered(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, double x, double y, State state) {
	}
	
	private Path getLine(State state, double x1, double y1, double x2, double y2, boolean shiftDown) {
		Path line = new Path();
		if(shiftDown) {
			double dx = x2-x1;
			double dy = y2-y1;
			if(Math.abs(dx) > Math.abs(dy)) {
				line = new Path.Line(x1, y1, x2, y1, state.getPathState());
			}
			else {
				line = new Path.Line(x1, y1, x1, y2, state.getPathState());
			}
		}
		else {
			line = new Path.Line(x1, y1, x2, y2, state.getPathState());
		}
		line.setPathState(state.getPathState());
		return line;
	}
	
	public void mouseDragged(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(hasLastPoint()) {
			if(!(getLastX()==x && getLastY()==y)) {
				Path line = getLine(state, lastPoint.x, lastPoint.y, x, y, e.isShiftDown());
				vectorImage.setLastPath(line);
			}
			else {
				vectorImage.removeCurrentPath();
				vectorImage.deselectAll();
			}
		}
		mousePos = new MyPoint(x,y);
	}
	
	public void mouseMoved(MouseEvent e, double x, double y, State state) {
	}
	
	private void shiftToggled(State state, boolean shiftDown) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(hasLastPoint()) {
			if(!(getLastX()==mousePos.x && getLastY()==mousePos.y)) {
				Path ellipse = getLine(state, lastPoint.x, lastPoint.y, mousePos.x, mousePos.y, shiftDown);
				vectorImage.setLastPath(ellipse);
			}
		}
	}
	
	public void keyPressed(KeyEvent e, State state) {
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftToggled(state, true);
		}
	}

	public void keyReleased(KeyEvent e, State state) {
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftToggled(state, false);
		}
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
