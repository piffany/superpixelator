package myTool.vector;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import myMath.*;
import renderer.geometry.*;
import myTool.Tool;

public abstract class VectorTool extends Tool {
	
	public VectorTool() {
		super();
	}
	
	public VectorTool(String id, String tooltip, String instruction) {
		super(id, tooltip, instruction);
	}	

	public VectorTool(String id, String instruction, String tooltip, ImageIcon icon) {
		super(id, tooltip, instruction, icon);
	}

	public VectorTool(String id, String tooltip, String instruction, ImageIcon icon, Cursor cursor) {
		super(id, tooltip, instruction, icon, cursor);
	}
	
	public int getType() {
		return State.VECTOR;
	}

	protected MyPoint getLastPoint() {
		return lastPoint.clone();
	}
	
	protected double getLastX() {
		return lastPoint.x;
	}
	
	protected double getLastY() {
		return lastPoint.y;
	}

	protected void setLastPoint(double x, double y) {
		lastPoint.x = x; lastPoint.y = y;
	}
	protected void unsetLastPoint() {
		lastPoint.x = -1; lastPoint.y = -1;
	}
	
	protected boolean hasLastPoint() {
		return (lastPoint.x!=-1 && lastPoint.y!=-1);
	}
	
	abstract public void mousePressed(MouseEvent e, double x, double y, State state);

	abstract public void mouseReleased(MouseEvent e, double x, double y, State state);

	abstract public void mouseExited(MouseEvent e, double x, double y, State state);

	abstract public void mouseEntered(MouseEvent e, double x, double y, State state);

	abstract public void mouseClicked(MouseEvent e, double x, double y, State state);

	abstract public void mouseDragged(MouseEvent e, double x, double y, State state);
	
	abstract public void mouseMoved(MouseEvent e, double x, double y, State state);

	abstract public void keyPressed(KeyEvent e, State state);

	abstract public void keyReleased(KeyEvent e, State state);

	abstract public void keyTyped(KeyEvent e, State state);
	
	public void setSelectedTool(boolean b, State state) {
		unsetLastPoint();
	}
}
