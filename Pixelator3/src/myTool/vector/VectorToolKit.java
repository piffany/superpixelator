package myTool.vector;
import java.util.Iterator;
import java.util.Vector;


public class VectorToolKit {

	private Vector<VectorTool> tools = new Vector<VectorTool>();
	private VectorTool selectTool = new Select();
	private VectorTool curveEditTool = new PathEdit();
	private VectorTool lineTool = new Line();
	private VectorTool polylineTool = new PolyLine();
	private VectorTool bezierCurveTool = new BezierCurve();
	private VectorTool rectangleTool = new Rectangle();
	private VectorTool ellipseTool = new Ellipse();
	private VectorTool eyeDropperTool = new EyeDropper();

	public VectorToolKit() {
		tools.add(selectTool);
		tools.add(curveEditTool);
		tools.add(lineTool);
		tools.add(polylineTool);
		tools.add(bezierCurveTool);
		tools.add(rectangleTool);
		tools.add(ellipseTool);
		tools.add(eyeDropperTool);
	}
	
	public VectorTool getDefaultTool() {
		return tools.get(0);
	}
	
	public VectorTool getTool(int index) {
		return tools.get(index);
	}
	
	public VectorTool findToolByID(String id) throws Exception {
		Iterator<VectorTool> itr = tools.iterator();
		VectorTool tool = null;
		while(itr.hasNext()) {
			tool = itr.next();
			if(tool.getID() == id) {
				return tool;
			}
		}
		throw new Exception();
	}
	
	public int size() {
		return tools.size();
	}

	public boolean hasToolByID(String id) {
		Iterator<VectorTool> itr = tools.iterator();
		while(itr.hasNext()) {
			if(itr.next().getID() == id) return true;
		}
		return false;
	}

}
