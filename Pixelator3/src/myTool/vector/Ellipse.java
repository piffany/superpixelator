package myTool.vector;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import renderer.geometry.MyPoint;

import myImage.*;
import myMath.*;
import myMath.geometry.Path;


public class Ellipse
extends VectorTool {

	private static String iconFile = "/icons/ellipse.gif";
	private final static String instruction = 
			"Draw an ellipse, or a circle by holding down SHIFT.";
	private MyPoint mousePos = null;

	public Ellipse() {
		super("VectorEllipse", "Ellipse", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		setLastPoint(x,y);
		mousePos = new MyPoint(x,y);
		vectorImage.startNewPath(state);
	}

	public void mouseReleased(MouseEvent e, double x, double y, State state) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(hasLastPoint()) {
			if(!(getLastX()==x && getLastY()==y)) {
				Path ellipse = getEllipse(state, lastPoint.x, lastPoint.y, x, y, e.isShiftDown());
				vectorImage.setLastPath(ellipse);
			}
			else {
				vectorImage.removeCurrentPath();
				//vectorImage.deselectAll();
			}
		}
		vectorImage.deselectAll();
		mousePos = null;
		unsetLastPoint();
	}
	
	private Path getEllipse(State state, double x1, double y1, double x2, double y2, boolean shiftDown) {
		double rectW = Math.abs(x1-x2), rectH = Math.abs(y1-y2);
		Path ellipse = new Path();
		if(shiftDown) {
			double R = Math.min(rectW, rectH);
			double x = (x2 > x1) ? x1 : x1 - R;
			double y = (y2 > y1) ? y1 : y1 - R;
			ellipse = new Path.Oval(x, y, R, R, state.getPathState());
		}
		else {
			double x = Math.min(x1, x2), y = Math.min(y1, y2);
			ellipse = new Path.Oval(x, y, rectW, rectH, state.getPathState());
		}
		ellipse.setPathState(state.getPathState());
		return ellipse;
	}

	public void mouseExited(MouseEvent e, double x, double y, State state) {
	}

	public void mouseEntered(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, double x, double y, State state) {
	}
	
	public void mouseDragged(MouseEvent e, double x, double y, State state) {
		mousePos = new MyPoint(x,y);
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(hasLastPoint()) {
			if(!(getLastX()==x && getLastY()==y)) {
				Path ellipse = getEllipse(state, lastPoint.x, lastPoint.y, x, y, e.isShiftDown());
				vectorImage.setLastPath(ellipse);
			}
			else {
				vectorImage.removeCurrentPath();
				vectorImage.deselectAll();
			}
		}
	}
	
	public void mouseMoved(MouseEvent e, double x, double y, State state) {
	}
	
	private void shiftToggled(State state, boolean shiftDown) {
		VectorImage vectorImage = state.getVectorLayer().getVectorImage();
		if(hasLastPoint()) {
			if(!(getLastX()==mousePos.x && getLastY()==mousePos.y)) {
				Path ellipse = getEllipse(state, lastPoint.x, lastPoint.y, mousePos.x, mousePos.y, shiftDown);
				vectorImage.setLastPath(ellipse);
			}
		}
	}
	
	public void keyPressed(KeyEvent e, State state) {
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftToggled(state, true);
		}
	}

	public void keyReleased(KeyEvent e, State state) {
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftToggled(state, false);
		}
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
