package myTool;

import myMath.State;
import myTool.raster.RasterToolKit;
import myTool.vector.VectorToolKit;


public class ToolKit {

	private RasterToolKit rasterToolKit;
	private VectorToolKit vectorToolKit;
	
	public ToolKit() {
		rasterToolKit = new RasterToolKit();
		vectorToolKit = new VectorToolKit();
	}
	
	public Tool getTool(int index) {
		int numRaster = getNumRasterTools();
		int numVector = getNumVectorTools();
		assert(index >= 0 && index < numRaster + numVector);
		if(index < numRaster) {
			return rasterToolKit.getTool(index);
		}
		else {
			return vectorToolKit.getTool(index-numRaster);
		}
	}
	
	public int getNumRasterTools() {
		return rasterToolKit.size();
	}

	public int getNumVectorTools() {
		return vectorToolKit.size();
	}

	public int size() {
		return getNumRasterTools() + getNumVectorTools();
	}
	
	public Tool findToolByID(String id) {
		Tool tool = null;
		try {
			tool = rasterToolKit.findToolByID(id);
		} catch (Exception e) {
			try {
				tool = vectorToolKit.findToolByID(id);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			assert(tool!=null);
			return tool;
		}
		assert(tool!=null);
		return tool;
	}

	public boolean hasToolWithID(String id) {
		return (rasterToolKit.hasToolByID(id) || vectorToolKit.hasToolByID(id));
	}
	
	public Tool getDefaultTool(int type) {
		if(type == State.RASTER) {
			return (Tool) rasterToolKit.getDefaultTool();
		}
		else if(type == State.VECTOR) {
			return (Tool) vectorToolKit.getDefaultTool();
		}
		else {
			assert(false);
			return null;
		}
	}
}
