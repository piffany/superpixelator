package myTool;

import java.awt.Cursor;

import javax.swing.ImageIcon;

import myMath.State;
import renderer.geometry.MyPoint;


public class Tool {
	
	protected String id = "";
	protected MyPoint lastPoint = new MyPoint();
	protected String tooltip = "";
	protected String instruction = "";
	protected ImageIcon icon = new ImageIcon();
	protected Cursor cursor = new Cursor(Cursor.DEFAULT_CURSOR);

	public Tool() {
	}
	
	public Tool(String id, String tooltip, String instruction) {
		this.id = id;
		this.tooltip = tooltip;
		this.instruction = instruction;
	}
	
	public Tool(String id, String tooltip, String instruction, ImageIcon icon) {
		this.id = id;
		this.tooltip = tooltip;
		this.instruction = instruction;
		this.icon = icon;
	}

	public Tool(String id, String tooltip, String instruction, ImageIcon icon, Cursor cursor) {
		this.id = id;
		this.tooltip = tooltip;
		this.instruction = instruction;
		this.icon = icon;
		this.cursor = cursor;
	}
	
	public Cursor getCursor() {
		return cursor;
	}
	
	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
	}
	
	public int getType() {
		return State.NONE;
	}
	
	public String getID() {
		return id;
	}

	public String getTooltip() {
		return tooltip;
	}
	
	public String getInstruction() {
		return instruction;
	}

	public ImageIcon getIcon() {
		return icon;
	}
	
	public void setIcon(ImageIcon icon) {
		this.icon = icon;
	}

	public void setSelectedTool(boolean b, State state) {
	}

}
