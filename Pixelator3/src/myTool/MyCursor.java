package myTool;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.net.URL;

@SuppressWarnings("serial")
public class MyCursor extends Cursor {

	public final static int NW = 0, NE = 1, SW = 2, SE = 3, CENTRE = 4;
	
	protected MyCursor() {
		super(Cursor.DEFAULT_CURSOR);
	}

	public static Cursor getCursor() {
		return getCursor("src\\cursors\\whiteArrow.gif");
	}
	
	public static Cursor getCursor(String filename) {
		return getCursor(filename, NW);
	}
	
	public static Cursor getCursor(URL url) {
		return getCursor(url, NW);
	}
	
	public static Cursor getCursor(String filename, int hotSpotLoc) {
		switch(hotSpotLoc) {
		case NW: return getCursor(filename, new Point(0,0));
		case NE: return getCursor(filename, new Point(31,0));
		case SW: return getCursor(filename, new Point(0,31));
		case SE: return getCursor(filename, new Point(31,31));
		case CENTRE: return getCursor(filename, new Point(15,15));
		}
		return null;
	}	
	
	public static Cursor getCursor(URL url, int hotSpotLoc) {
		switch(hotSpotLoc) {
		case NW: return getCursor(url, new Point(0,0));
		case NE: return getCursor(url, new Point(31,0));
		case SW: return getCursor(url, new Point(0,31));
		case SE: return getCursor(url, new Point(31,31));
		case CENTRE: return getCursor(url, new Point(15,15));
		}
		return null;
	}	

	public static Cursor getCursor(String filename, Point hotSpot) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit.getImage(filename);
		Cursor cursor = toolkit.createCustomCursor(image, hotSpot, "");
		return cursor;
	}
	
	public static Cursor getCursor(URL url, Point hotSpot) {
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit.getImage(url);
		Cursor cursor = toolkit.createCustomCursor(image, hotSpot, "");
		return cursor;
	}
}
