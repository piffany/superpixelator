package myTool.raster;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.ImageIcon;

import myImage.*;
import myMath.*;
import myTool.MyCursor;


public class Fill
extends RasterTool {

	private static String iconFile = "/icons/fill.gif";
	private static String cursorFile = "/cursors/fill.gif";
	private final static String instruction = 
			"Flood fill a region.";

	public Fill() {
		super("RasterFill", "Fill", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(MyCursor.getCursor(getClass().getResource(cursorFile)));
	}

	public void mousePressed(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		int targetColor = rasterImage.getRGB(x, y);
		int replacementColor = state.getPathState().getStrokeColor().getRGB();
		floodFill(rasterImage, x, y, targetColor, replacementColor);
	}

	private static boolean sameRGB(int rgb1, int rgb2) {
		return Math.abs(rgb1-rgb2) < 1;
	}
	
	public static void floodFill(RasterImage rasterImage, int x, int y, Color c) {
		int targetColor = rasterImage.getRGB(x,y);
		int replacementColor = c.getRGB();
		floodFill(rasterImage, x, y, targetColor, replacementColor);
	}
	
	public static void floodFill(RasterImage rasterImage, int x, int y, int targetColor, int replacementColor) {
		// 1. Set Q to the empty queue.
		Queue<Point> Q = new LinkedList<Point>();
		// 2. If the color of node is not equal to target-color, return.
		if(x<0 || x>=rasterImage.getWidth()) return;
		if(y<0 || y>=rasterImage.getHeight()) return;
		int nodeColor = rasterImage.getRGB(x, y);
		if(!sameRGB(nodeColor,targetColor)) return;
		if(sameRGB(nodeColor,replacementColor)) return;
		// 3. Add node to Q.
		Q.add(new Point(x,y));
		// 4. For each element n of Q:
		int itr = 0;
		while(!Q.isEmpty()) {
			itr ++;
			if(itr > rasterImage.getWidth()*rasterImage.getHeight()) System.out.println("fill: " + itr);
			Point n = Q.remove();
			// 5. If the color of n is equal to target-color:
			if(sameRGB(rasterImage.getRGB(n.x, n.y), targetColor)) {
				// 6. Set w and e equal to n.
				Point w = new Point(n), e = new Point(n);
				// 7. Move w to the west until the color of the node to the west of w no longer matches target-color.
				while(w.x>=0 && sameRGB(rasterImage.getRGB(w.x, w.y), targetColor)) {
					w.x--;
				}
				// 8. Move e to the east until the color of the node to the east of e no longer matches target-color.
				while(e.x < rasterImage.getWidth() && sameRGB(rasterImage.getRGB(e.x, e.y), targetColor)) {
					e.x++;
				}
				// 9. Set the color of nodes between w and e to replacement-color.
				for(int xx = w.x+1; xx<e.x; xx++) {
					rasterImage.setRGB(xx, n.y, replacementColor);
				}
				// 10. For each node m between w and e:
				for(int xx = w.x+1; xx<e.x; xx++) {
					//Point m = new Point(xx, n.y);
					// 11. If the color of the node to the north of m is target-color, add that node to Q.
					Point mNorth = new Point(xx, n.y-1);
					if(n.y-1>=0 && sameRGB(rasterImage.getRGB(mNorth.x, mNorth.y), targetColor)) {
						Q.add(mNorth);
					}
					// 12. If the color of the node to the south of m is target-color, add that node to Q.
					Point mSouth = new Point(xx, n.y+1);
					if(n.y+1<rasterImage.getHeight() && sameRGB(rasterImage.getRGB(mSouth.x, mSouth.y), targetColor)) {
						Q.add(mSouth);
					}
				}
			}
			// 13. Continue looping until Q is exhausted.
		}
		// 14. Return.
		return;
	}

	public void mouseReleased(MouseEvent e, int x, int y, State state) {
	}

	public void mouseExited(MouseEvent e, int x, int y, State stat) {
	}

	public void mouseEntered(MouseEvent e, int x, int y, State state) {
	}

	public void mouseClicked(MouseEvent e, int x, int y, State state) {
	}

	public void mouseDragged(MouseEvent e, int x, int y, State state) {
	}

	public void mouseMoved(MouseEvent e, int x, int y, State state) {
	}
	
	public void keyPressed(KeyEvent e, State state) {		
	}

	public void keyReleased(KeyEvent e, State state) {
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
