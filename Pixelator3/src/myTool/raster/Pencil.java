package myTool.raster;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import renderer.display.MyGraphics2D;

import myImage.RasterImage;
import myMath.*;
import renderer.geometry.MyPoint;
import myTool.MyCursor;


public class Pencil
extends RasterTool {

	MyPoint lastTempPixel = new MyPoint(0,0);
	
	private final static ImageIcon icon = new ImageIcon("src\\icons\\pencil.gif");
	private final static Cursor cursor = MyCursor.getCursor("src\\cursors\\pencil.gif");
	private final static String instruction = 
			"Click on pixels to draw.";

	public Pencil() {
		super("RasterPencil", "Pencil", instruction, icon, cursor);
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, int x, int y, State state) {
		setLastPoint(x, y);
	}

	public void mouseReleased(MouseEvent e, int x, int y, State state) {
		unsetLastPoint();
	}

	public void mouseExited(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		Graphics g = rasterImage.getGraphics();
		g.setColor(state.getPathState().getStrokeColor());
		if(hasLastPoint()){
			g.drawLine((int)getLastX(), (int)getLastY(), x, y);
		}
		rasterTempImage.clearPixel((int) lastTempPixel.x, (int) lastTempPixel.y);
		unsetLastPoint();
	}

	public void mouseEntered(MouseEvent e, int x, int y, State state) {
		state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clearPixel((int) lastTempPixel.x, (int) lastTempPixel.y);
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		mg.setColor(state.getPathState().getStrokeColor());
		mg.fillRect(x, y, 1, 1);
		lastTempPixel.x = x; lastTempPixel.y = y;
	}
	
	public void mouseClicked(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		Graphics g = rasterImage.getGraphics();
		g.setColor(state.getPathState().getStrokeColor());
		System.out.println(x + ", " + y);

		g.fillRect(x, y, 1, 1);
		rasterTempImage.clear();
		unsetLastPoint();
	}
	
	public void mouseDragged(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		Graphics g = rasterImage.getGraphics();
		g.setColor(state.getPathState().getStrokeColor());
		if(hasLastPoint()){
			g.drawLine((int)getLastX(), (int)getLastY(), x, y);
		}
		rasterTempImage.clearPixel((int) lastTempPixel.x, (int) lastTempPixel.y);
		setLastPoint(x,y);
	}
	
	public void mouseMoved(MouseEvent e, int x, int y, State state) {
		state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clear();
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		mg.setColor(state.getPathState().getStrokeColor());
		mg.fillRect(x, y, 1, 1);
		lastTempPixel.x = x; lastTempPixel.y = y;
	}
	
	public void keyPressed(KeyEvent e, State state) {		
	}

	public void keyReleased(KeyEvent e, State state) {
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
