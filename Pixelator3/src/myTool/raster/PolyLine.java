package myTool.raster;

import java.awt.BasicStroke;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import renderer.display.MyGraphics2D;

import myImage.RasterImage;
import myMath.*;


public class PolyLine
extends RasterTool {

	private static String iconFile = "/icons/polyline.gif";
	private final static String instruction = 
			"Draw connected line segments by clicking on points. Double-click to finish.";

	public PolyLine() {
		super("RasterPolyLine", "Polyline", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, int x, int y, State state) {
		
	}

	public void mouseReleased(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		if(hasLastPoint()) {
			if((int)getLastX()==x && (int)getLastY()==y) {
				unsetLastPoint();
			}
			else {
				mg.setColor(state.getPathState().getStrokeColor());
				int width = (int) state.getPathState().getStrokeWeight();
				MyDraw.drawThickLine(g, (int)getLastX(), (int)getLastY(), x, y, width,
									   BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
				setLastPoint(x,y);
			}
		}
		else {
			setLastPoint(x,y);
		}
		rasterTempImage.clear();
	}

	public void mouseExited(MouseEvent e, int x, int y, State stat) {
	}

	public void mouseEntered(MouseEvent e, int x, int y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, int x, int y, State state) {
	}
	
	public void mouseDragged(MouseEvent e, int x, int y, State state) {		
	}
	
	public void mouseMoved(MouseEvent e, int x, int y, State state) {		
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clear();
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		if(hasLastPoint()) {
			if((int)getLastX()!=x || (int)getLastY()!=y) {
				mg.setColor(state.getPathState().getStrokeColor());
				int width = (int) state.getPathState().getStrokeWeight();
				MyDraw.drawThickLine(g, (int)getLastX(), (int)getLastY(), x, y, width,
									   BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);

			}
		}
	}
	
	public void keyPressed(KeyEvent e, State state) {		
	}

	public void keyReleased(KeyEvent e, State state) {
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
