package myTool.raster;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import colours.Colour;

import myImage.RasterImage;
import myMath.*;
import myTool.MyCursor;

public class EyeDropper
extends RasterTool {

	private static String iconFile = "/icons/eyeDropper.gif";
	private static String cursorFile = "/cursors/eyeDropper.gif";
	private final static String instruction = 
			"Select colours using the eye dropper.";

	public EyeDropper() {
		super("RasterEyeDropper", "EyeDropper", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(MyCursor.getCursor(getClass().getResource(cursorFile), MyCursor.SW));
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, int x, int y, State state) {
		RasterImage image = state.rasterizeAllLayers(true);
		Colour c = image.getColor(x, y);
		state.setSelectedColor(c);
	}

	public void mouseReleased(MouseEvent e, int x, int y, State state) {
	}

	public void mouseExited(MouseEvent e, int x, int y, State stat) {
	}

	public void mouseEntered(MouseEvent e, int x, int y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, int x, int y, State state) {
	}
	
	public void mouseDragged(MouseEvent e, int x, int y, State state) {
	}
	
	public void mouseMoved(MouseEvent e, int x, int y, State state) {
	}
	
	public void keyPressed(KeyEvent e, State state) {		
	}

	public void keyReleased(KeyEvent e, State state) {
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
