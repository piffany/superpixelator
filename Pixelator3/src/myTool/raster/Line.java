package myTool.raster;

import java.awt.BasicStroke;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import renderer.display.MyGraphics2D;
import renderer.geometry.MyPoint;

import myImage.RasterImage;
import myMath.*;


public class Line
extends RasterTool {

	private static String iconFile = "/icons/line.gif";
	private final static String instruction = 
			"Draw a straight line. Hold down SHIFT to keep horizontal or vertical.";
	private MyPoint mousePos = null;

	public Line() {
		super("RasterLine", "Line", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, int x, int y, State state) {
		setLastPoint(x,y);
		mousePos = new MyPoint(x,y);
	}
	
	private void drawLine(MyGraphics2D mg, State state, int x1, int y1, int x2, int y2, boolean shiftDown) {
		mg.setColor(state.getPathState().getStrokeColor());
		Graphics2D g = mg.getGraphics2D();
		int width = (int) state.getPathState().getStrokeWeight();
		if(shiftDown) {
			int dx = x2-x1;
			int dy = y2-y1;
			if(Math.abs(dx) > Math.abs(dy)) {
				MyDraw.drawThickLine(g, x1, y1, x2, y1, width,
						   BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			}
			else {
				MyDraw.drawThickLine(g, x1, y1, x1, y2, width,
						   BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			}
		}
		else {
			MyDraw.drawThickLine(g, x1, y1, x2, y2, width,
								   BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		}
	}

	public void mouseReleased(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		if(hasLastPoint()) {
			if((int)getLastX()!=x || (int)getLastY()!=y) {
				drawLine(mg, state, (int)getLastX(), (int)getLastY(), x, y, e.isShiftDown());
			}
		}
		unsetLastPoint();
		rasterTempImage.clear();
		mousePos = null;
	}

	public void mouseExited(MouseEvent e, int x, int y, State stat) {
	}

	public void mouseEntered(MouseEvent e, int x, int y, State state) {
	}
	
	public void mouseClicked(MouseEvent e, int x, int y, State state) {
	}
	
	public void mouseDragged(MouseEvent e, int x, int y, State state) {
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clear();
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		if(hasLastPoint()) {
			if((int)getLastX()!=x || (int)getLastY()!=y) {
				drawLine(mg, state, (int)getLastX(), (int)getLastY(), x, y, e.isShiftDown());
			}
		}
		mousePos = new MyPoint(x,y);
	}
	
	public void mouseMoved(MouseEvent e, int x, int y, State state) {
	}
	
	public void keyPressed(KeyEvent e, State state) {
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftToggled(state, true);
		}
	}
	
	private void shiftToggled(State state, boolean shiftDown) {
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clear();
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		if(hasLastPoint()) {
			if((int)getLastX()!=mousePos.x || (int)getLastY()!=mousePos.y) {
				drawLine(mg, state, (int)getLastX(), (int)getLastY(), (int)mousePos.x, (int)mousePos.y, shiftDown);
			}
		}
	}

	public void keyReleased(KeyEvent e, State state) {
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftToggled(state, false);
		}
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
