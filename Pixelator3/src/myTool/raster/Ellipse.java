package myTool.raster;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import renderer.display.MyGraphics2D;
import renderer.geometry.MyPoint;
import myImage.RasterImage;
import myMath.*;
import myMath.geometry.Path;

public class Ellipse
extends RasterTool {

	private static String iconFile = "/icons/ellipse.gif";
	private final static String instruction = 
			"Draw an ellipse, or a circle by holding down SHIFT.";
	private MyPoint mousePos = null;

	public Ellipse() {
		super("RasterEllipse", "Ellipse", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		unsetLastPoint();
	}

	public void mousePressed(MouseEvent e, int x, int y, State state) {
		setLastPoint(x,y);
		mousePos = new MyPoint(x,y);
	}

	public void mouseReleased(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		if(hasLastPoint()) {
			int lastX = (int)getLastX();
			int lastY = (int)getLastY(); 
			if(lastX!=x || lastY!=y) {
				drawEllipse(mg, state, (int)getLastX(), (int)getLastY(), x, y, e.isShiftDown());
			}
		}
		unsetLastPoint();
		rasterTempImage.clear();
		mousePos = null;
	}

	public void mouseExited(MouseEvent e, int x, int y, State stat) {
	}

	public void mouseEntered(MouseEvent e, int x, int y, State state) {
	}

	public void mouseClicked(MouseEvent e, int x, int y, State state) {
	}

	private Path getEllipse(State state, double x1, double y1, double x2, double y2, boolean shiftDown) {
		double rectW = Math.abs(x1-x2), rectH = Math.abs(y1-y2);
		Path ellipse = new Path();
		if(shiftDown) {
			double R = Math.min(rectW, rectH);
			double x = (x2 > x1) ? x1 : x1 - R;
			double y = (y2 > y1) ? y1 : y1 - R;
			ellipse = new Path.Oval(x, y, R, R, state.getPathState());
		}
		else {
			double x = Math.min(x1, x2), y = Math.min(y1, y2);
			ellipse = new Path.Oval(x, y, rectW, rectH, state.getPathState());
		}
		ellipse.setPathState(state.getPathState());
		return ellipse;
	}
	
	private void drawEllipse(MyGraphics2D mg, State state, int x1, int y1, int x2, int y2, boolean shiftDown) {
		//Graphics2D g = mg.getGraphics2D();
		//int width = (int) state.getPathState().getStrokeWeight();
		//int rectW = Math.abs(x1-x2), rectH = Math.abs(y1-y2);
		Path ellipse = getEllipse(state, x1, y1, x2, y2, shiftDown);
		mg.setColor(state.getPathState().getFillColor());
		mg.fillPath(ellipse);
		mg.setColor(state.getPathState().getStrokeColor());
		mg.drawPath(ellipse);
		/*
		if(shiftDown) {
			int R = Math.min(rectW, rectH);
			int x = (x2 > x1) ? x1 : x1 - R;
			int y = (y2 > y1) ? y1 : y1 - R;
			mg.setColor(state.getPathState().getFillColor());
			g.fillOval(x, y, R, R);
			mg.setColor(state.getPathState().getStrokeColor());
			MyDraw.drawThickOval(g, x, y, R, R, width,
					BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
		}
		else {
			int x = Math.min(x1, x2), y = Math.min(y1, y2);
			mg.setColor(state.getPathState().getFillColor());
			g.fillOval(x, y, rectW, rectH);
			mg.setColor(state.getPathState().getStrokeColor());
			MyDraw.drawThickOval(g, x, y, rectW, rectH, width,
				BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
		}
		*/
	}
	
	public void mouseDragged(MouseEvent e, int x, int y, State state) {
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clear();
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		if(hasLastPoint()) {
			int lastX = (int)getLastX();
			int lastY = (int)getLastY(); 
			if(lastX!=x || lastY!=y) {
				drawEllipse(mg, state, (int)getLastX(), (int)getLastY(), x, y, e.isShiftDown());
			}
		}
		mousePos = new MyPoint(x,y);
	}

	public void mouseMoved(MouseEvent e, int x, int y, State state) {
	}

	public void keyPressed(KeyEvent e, State state) {
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftToggled(state, true);
		}
	}

	public void keyReleased(KeyEvent e, State state) {
		if(e.getKeyCode() == KeyEvent.VK_SHIFT) {
			shiftToggled(state, false);
		}
	}

	public void keyTyped(KeyEvent e, State state) {		
	}
	
	private void shiftToggled(State state, boolean shiftDown) {
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clear();
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		if(hasLastPoint()) {
			if((int)getLastX()!=mousePos.x || (int)getLastY()!=mousePos.y) {
				drawEllipse(mg, state, (int)getLastX(), (int)getLastY(), (int)mousePos.x, (int)mousePos.y, shiftDown);
			}
		}
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
