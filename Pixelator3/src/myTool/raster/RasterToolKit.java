package myTool.raster;
import java.util.Iterator;
import java.util.Vector;


public class RasterToolKit {

	private Vector<RasterTool> tools = new Vector<RasterTool>();
	//private RasterTool pencilTool = new Pencil();
	private RasterTool brushTool = new Brush();
	private RasterTool fillTool = new Fill();
	private RasterTool eraserTool = new Eraser();
	private RasterTool lineTool = new Line();
	private RasterTool polylineTool = new PolyLine();
	private RasterTool rectangleTool = new Rectangle();
	private RasterTool ellipseTool = new Ellipse();
	private RasterTool eyeDropperTool = new EyeDropper();
	
	public RasterToolKit() {
		//tools.add(pencilTool);
		tools.add(brushTool);
		tools.add(fillTool);
		tools.add(eraserTool);
		tools.add(lineTool);
		tools.add(polylineTool);
		tools.add(rectangleTool);
		tools.add(ellipseTool);
		tools.add(eyeDropperTool);
	}
	
	public RasterTool getDefaultTool() {
		return tools.get(0);
	}
	
	public RasterTool getTool(int index) {
		return tools.get(index);
	}
	
	public RasterTool findToolByID(String id) throws Exception {
		Iterator<RasterTool> itr = tools.iterator();
		RasterTool tool = null;
		while(itr.hasNext()) {
			tool = itr.next();
			if(tool.getID() == id) {
				return tool;
			}
		}
		throw new Exception();
	}
	
	public int size() {
		return tools.size();
	}

	public boolean hasToolByID(String id) {
		Iterator<RasterTool> itr = tools.iterator();
		while(itr.hasNext()) {
			if(itr.next().getID() == id) return true;
		}
		return false;
	}
}
