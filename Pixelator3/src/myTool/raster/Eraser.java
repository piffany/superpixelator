package myTool.raster;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import renderer.display.MyGraphics2D;

import myImage.RasterImage;
import myMath.*;
import myTool.MyCursor;


public class Eraser
extends RasterTool {

	// all drawings are done directly on the raster layer
	// prior image is used for temporary drawings (i.e. for mouseMoved)

	RasterImage priorImage = null;
	boolean mouseDown = false;

	private static String iconFile = "/icons/eraser.gif";
	private static String cursorFile = "/cursors/eraser.gif";
	private final static String instruction = 
			"Erase to transparency using a brush.";

	public Eraser() {
		super("RasterEraser", "Eraser", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(MyCursor.getCursor(getClass().getResource(cursorFile)));
		unsetLastPoint();
	}

	private void setPriorImage(RasterImage image) {
		priorImage = image.clone();
	}

	private void unsetPriorImage() {
		priorImage = null;
	}

	private boolean hasPriorImage() {
		return priorImage!=null;
	}

	private void initPriorImage(State state) {
		if(!hasPriorImage()) {
			setPriorImage(state.getRasterLayer().getRasterImage());
		}
	}

	public void mousePressed(MouseEvent e, int x, int y, State state) {
		setLastPoint(x,y);
		mouseDown = true;
	}

	public void mouseReleased(MouseEvent e, int x, int y, State state) {
		unsetLastPoint();
		mouseDown = false;
	}

	public void mouseExited(MouseEvent e, int x, int y, State state) {
		if(mouseDown) {
			unsetPriorImage();
			RasterImage rasterImage = state.getRasterLayer().getRasterImage();
			MyGraphics2D mg = rasterImage.getMyGraphics2D();
			Graphics2D g = mg.getGraphics2D();
			Color backgroundColor = rasterImage.getBackgroundColor();
			int width = (int) state.getPathState().getStrokeWeight();
			if(backgroundColor.getAlpha() == 0) {
				mg.setComposite(AlphaComposite.Clear);
				if(hasLastPoint()){
					MyDraw.drawThickBrushStroke(g, (int)getLastX(), (int)getLastY(), x, y, width,
							BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
				}
				mg.setComposite(AlphaComposite.SrcOver);
			}
			else {
				mg.setColor(backgroundColor);
				if(hasLastPoint()){
					MyDraw.drawThickBrushStroke(g, (int)getLastX(), (int)getLastY(), x, y, width,
							BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
				}
			}

		}
		else {
			initPriorImage(state);
			state.getRasterLayer().setRasterImage(priorImage);
			unsetPriorImage();
			unsetLastPoint();
		}
	}

	public void mouseEntered(MouseEvent e, int x, int y, State state) {
		initPriorImage(state);
	}

	public void mouseClicked(MouseEvent e, int x, int y, State state) {
		unsetPriorImage();
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		Color backgroundColor = rasterImage.getBackgroundColor();
		int width = (int) state.getPathState().getStrokeWeight();
		if(backgroundColor.getAlpha() == 0) {
			mg.setComposite(AlphaComposite.Clear);
			if(hasLastPoint()){
				MyDraw.fillCircle(g, x, y, width);
			}
			mg.setComposite(AlphaComposite.SrcOver);
		}
		else {
			mg.setColor(backgroundColor);
			if(hasLastPoint()){
				MyDraw.fillCircle(g, x, y, width);
			}
		}
		unsetLastPoint();
	}

	public void mouseDragged(MouseEvent e, int x, int y, State state) {
		unsetPriorImage();
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		Color backgroundColor = rasterImage.getBackgroundColor();
		int width = (int) state.getPathState().getStrokeWeight();
		if(backgroundColor.getAlpha() == 0) {
			mg.setComposite(AlphaComposite.Clear);
			if(hasLastPoint()){
				MyDraw.drawThickBrushStroke(g, (int)getLastX(), (int)getLastY(), x, y, width,
						BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			}
			mg.setComposite(AlphaComposite.SrcOver);
		}
		else {
			mg.setColor(backgroundColor);
			if(hasLastPoint()){
				MyDraw.drawThickBrushStroke(g, (int)getLastX(), (int)getLastY(), x, y, width,
						BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			}
		}
		setLastPoint(x,y);
	}

	public void mouseMoved(MouseEvent e, int x, int y, State state) {
		initPriorImage(state);
		state.getRasterLayer().setRasterImage(priorImage);
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		Color backgroundColor = rasterImage.getBackgroundColor();
		int width = (int) state.getPathState().getStrokeWeight();
		if(backgroundColor.getAlpha() == 0) {
			mg.setComposite(AlphaComposite.Clear);
			if(hasLastPoint()){
				MyDraw.fillCircle(g, x, y, width);
			}
			mg.setComposite(AlphaComposite.SrcOver);
		}
		else {
			mg.setColor(backgroundColor);
			if(hasLastPoint()){
				MyDraw.fillCircle(g, x, y, width);
			}
		}
		setLastPoint(x,y);
	}

	public void keyPressed(KeyEvent e, State state) {		
	}

	public void keyReleased(KeyEvent e, State state) {
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) {
			unsetLastPoint();
			unsetPriorImage();
		}
	}

}
