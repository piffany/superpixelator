package myTool.raster;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;

import renderer.display.MyGraphics2D;

import myImage.RasterImage;
import myMath.*;
import myTool.MyCursor;

public class Brush
extends RasterTool {

	private static String iconFile = "/icons/brush.gif";
	private static String cursorFile = "/cursors/brush.gif";
	private final static String instruction = 
			"Paint with a rounded brush.";

	public Brush() {
		super("RasterBrush", "Brush", instruction);
		setIcon(new ImageIcon(getClass().getResource(iconFile)));
		setCursor(MyCursor.getCursor(getClass().getResource(cursorFile)));
		unsetLastPoint();
	}
	
	public void mousePressed(MouseEvent e, int x, int y, State state) {
		setLastPoint(x,y);
	}

	public void mouseReleased(MouseEvent e, int x, int y, State state) {
		unsetLastPoint();
	}

	public void mouseExited(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		mg.setColor(state.getPathState().getStrokeColor());
		if(hasLastPoint()){
			int width = (int) state.getPathState().getStrokeWeight();
			MyDraw.drawThickBrushStroke(g, (int)getLastX(), (int)getLastY(), x, y, width,
								   BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		}
		rasterTempImage.clear();
		unsetLastPoint();
	}

	public void mouseEntered(MouseEvent e, int x, int y, State state) {
		state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clear();
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		mg.setColor(state.getPathState().getStrokeColor());
		int width = (int) state.getPathState().getStrokeWeight();
		MyDraw.fillCircle(g, x, y, width);
	}
	
	public void mouseClicked(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		mg.setColor(state.getPathState().getStrokeColor());
		int width = (int) state.getPathState().getStrokeWeight();
		MyDraw.fillCircle(g, x, y, width);
		rasterTempImage.clear();
	}
	
	public void mouseDragged(MouseEvent e, int x, int y, State state) {
		RasterImage rasterImage = state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		MyGraphics2D mg = rasterImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		mg.setColor(state.getPathState().getStrokeColor());
		if(hasLastPoint()){
			int width = (int) state.getPathState().getStrokeWeight();
			MyDraw.drawThickBrushStroke(g, (int)getLastX(), (int)getLastY(), x, y, width,
								   BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
		}
		rasterTempImage.clear();
		setLastPoint(x,y);
	}
	
	public void mouseMoved(MouseEvent e, int x, int y, State state) {
		state.getRasterLayer().getRasterImage();
		RasterImage rasterTempImage = state.getRasterLayer().getRasterTempImage();
		rasterTempImage.clear();
		MyGraphics2D mg = rasterTempImage.getMyGraphics2D();
		Graphics2D g = mg.getGraphics2D();
		mg.setColor(state.getPathState().getStrokeColor());
		int width = (int) state.getPathState().getStrokeWeight();
		MyDraw.fillCircle(g, x, y, width);
	}
	
	public void keyPressed(KeyEvent e, State state) {		
	}

	public void keyReleased(KeyEvent e, State state) {
	}

	public void keyTyped(KeyEvent e, State state) {		
	}

	public void setSelectedTool(boolean b, State state) {
		if(!b) unsetLastPoint();
	}

}
