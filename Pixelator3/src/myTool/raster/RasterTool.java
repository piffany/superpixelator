package myTool.raster;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

import myMath.*;
import renderer.geometry.MyPoint;
import myTool.Tool;

public abstract class RasterTool extends Tool {
	
	public RasterTool() {
		super();
	}
	
	public RasterTool(String id, String tooltip, String instruction) {
		super(id, tooltip, instruction);
	}	

	public RasterTool(String id, String tooltip, String instruction, ImageIcon icon) {
		super(id, tooltip, instruction, icon);
	}
	
	public RasterTool(String id, String tooltip, String instruction, ImageIcon icon, Cursor cursor) {
		super(id, tooltip, instruction, icon, cursor);
	}
	
	public int getType() {
		return State.RASTER;
	}
	
	protected MyPoint getLastPoint() {
		return new MyPoint((int)lastPoint.x, (int)lastPoint.y);
	}
	
	protected int getLastX() {
		return (int) lastPoint.x;
	}
	
	protected int getLastY() {
		return (int) lastPoint.y;
	}

	protected void setLastPoint(int x, int y) {
		lastPoint.x = x; lastPoint.y = y;
	}
	protected void unsetLastPoint() {
		lastPoint.x = -1; lastPoint.y = -1;
	}
	
	protected boolean hasLastPoint() {
		return (lastPoint.x!=-1 && lastPoint.y!=-1);
	}
	
	abstract public void mousePressed(MouseEvent e, int x, int y, State state);

	abstract public void mouseReleased(MouseEvent e, int x, int y, State state);

	abstract public void mouseExited(MouseEvent e, int x, int y, State state);

	abstract public void mouseEntered(MouseEvent e, int x, int y, State state);
	
	abstract public void mouseClicked(MouseEvent e, int x, int y, State state);

	abstract public void mouseDragged(MouseEvent e, int x, int y, State state);
	
	abstract public void mouseMoved(MouseEvent e, int x, int y, State state);

	abstract public void keyPressed(KeyEvent e, State state);

	abstract public void keyReleased(KeyEvent e, State state);

	abstract public void keyTyped(KeyEvent e, State state);
	
	public void setSelectedTool(boolean b, State state) {
		unsetLastPoint();
	}
}
