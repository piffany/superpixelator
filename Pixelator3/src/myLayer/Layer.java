package myLayer;

abstract public class Layer {

	public static String DEFAULT_NAME = "New Layer";
	protected String name = DEFAULT_NAME;
	protected boolean selected = false; // is layer selected
	protected boolean visible = true; // is layer visible
	
	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	abstract public int getType();
	
	public void setSelected(boolean b) {
		selected = b;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setName(String name) {
		if(name.length()>0) this.name = name;
		else name = DEFAULT_NAME;
	}
	
	public String getName() {
		return name;
	}
	
	abstract public void rerender(boolean isSelected);
}
