package myLayer;

import java.awt.image.BufferedImage;

import myImage.RasterImage;
import myMath.State;

public class RasterLayer extends Layer {

	private RasterImage rasterImage, rasterTempImage;
	
	public RasterLayer(int width, int height, State state) {
		rasterImage = new RasterImage(width,  height, BufferedImage.TYPE_INT_ARGB, state);
		rasterTempImage = new RasterImage(width,  height, BufferedImage.TYPE_INT_ARGB,state);
	}
	
	public RasterImage getRasterImage() {
		return rasterImage;
	}

	public void setRasterImage(RasterImage rasterImage) {
		this.rasterImage = rasterImage.clone();
	}

	public RasterImage getRasterTempImage() {
		return rasterTempImage;
	}

	public void setRasterTempImage(RasterImage rasterTempImage) {
		this.rasterTempImage = rasterTempImage.clone();
	}

	@Override
	public int getType() {
		return State.RASTER;
	}

	@Override
	public void rerender(boolean isSelected) {
		selected = isSelected;
	}


}
