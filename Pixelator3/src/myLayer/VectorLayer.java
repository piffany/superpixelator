package myLayer;

import myImage.VectorImage;
import myMath.State;

public class VectorLayer extends Layer {

	private VectorImage vectorImage, vectorTempImage;
	
	public VectorLayer(int width, int height, State state) {
		vectorImage = new VectorImage(width, height, state);
		vectorTempImage = new VectorImage(width, height, state);
	}
	
	public VectorImage getVectorImage() {
		return vectorImage;
	}

	public void setVectorImage(VectorImage vectorImage) {
		this.vectorImage = vectorImage.clone();
	}

	public VectorImage getVectorTempImage() {
		return vectorTempImage;
	}

	public void setVectorTempImage(VectorImage vectorTempImage) {
		this.vectorTempImage = vectorTempImage.clone();
	}

	@Override
	public int getType() {
		return State.VECTOR;
	}

	@Override
	public void rerender(boolean rerender) {
		if(rerender) {
			vectorImage.updateSelectedPath();
		}
		else vectorImage.deselectAll();
	}

}
