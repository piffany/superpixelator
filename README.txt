This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

********************************************************
Instructions for building on Windows

(probably similar on other OS but I haven't tested)
********************************************************

* Install Eclipse
* Start Eclipse
* File -> Open Projects from File System
* Say you extracted the source code to C:\Superpixelator, then put that path in Import source, and a list of projects will be found. Leave them all checked. Then click Finish.
* The five imported projects should appear in the Package Explorer with a "!" on Pixelator3 and SVGReaderWriter. That's due to build path problems. We will reconfigure them now.
* Right-click on SVGReaderWriter, choose Build Path -> Configure Build Path. Under the Libraries tab, select the five commons-codec jar files and remove them. Then choose Add External JARS, navigate to Superpixelator/commons-codec-1.7 and select the five JAR files there. Click OK and now the error for the SVGReaderWriter project should disappear.
* Repeat the previous step for the Pixelator3 project.
* In the Package Explorer, select Pixelator/src/myWidget.container/Paint.java and press the Run button in the top toolbar. The GUI should pop up.

********************************************************
Test example in Superpixelator
********************************************************

* File -> New Document, click OK for the default settings.
* Choose the curve icon in the Vector toolbar (5th one). Drag the control handles and click to draw a curve.
* Choose another icon to finish the curve drawing. Choose the move icon (1st one) in the Vector toolbar. Click anywhere on the curve to move it around.
* You can also edit the curve. Choose the second icon in the Vector toolbar and click on the curve to reveal the control curve. Click on a control point to move it or drag the handles to modify the curve.
* In the Layers panel, right-click and choose Add layer. Choose the Type to be Raster and press OK.
* Now you have access to the Raster toolbar. You can draw an ellipse, for example. This will also use the Superpixelator algorithm, but the shape won't be editable after it's been placed.

If you have any questions, contact me at piffany@gmail.com